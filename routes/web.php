<?php

//Route::group(['domain' => env('APP_URL')], function(){
    Route::get('auth', 'Auth\LoginController@index');
    Route::post('auth', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout');
    Route::get('auth/register', 'Auth\RegisterController@registration');
    Route::post('auth/register', 'Auth\RegisterController@postRegistration');

    Route::get('security-police/es', function (){
        return view('politicas.spanish');
    });
    Route::get('security-police/en', function (){
        return view('politicas.english');
    });

    Route::get('kiosko', 'AbsencesController@kioskoIndex');

    Route::get('lang/{locale}', function ($locale) {
        App::setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    });

    Route::post('employes-filter', 'API\EmployesController@searchEmployee');
    Route::post('register_absences', 'API\AbsencesController@save');

    Route::middleware(['auth'])->group(function () {

        Route::get('/', 'AbsencesController@index');


        Route::get('zipcode', 'ZipCodeController@index');
        Route::post('zipcode', 'ZipCodeController@save');
        Route::delete('zipcode/{id}', 'ZipCodeController@delete');
        Route::get('zipcodelist', 'ZipCodeController@zipcodeList');

        // ROUTES FOR LABOR PLANNING
        Route::get('city', 'CityController@index');
        Route::post('city', 'CityController@save');
        Route::delete('city/{id}', 'CityController@delete');
        Route::get('citylist', 'CityController@cityList');

         // ROUTES FOR SITE
        Route::get('site', 'SiteController@index');
        Route::post('site', 'SiteController@save');
        Route::delete('site/{id}', 'SiteController@delete');
        Route::get('sitelist', 'SiteController@siteList');


        Route::get('state', 'StateController@index');
        Route::post('state', 'StateController@save');
        Route::delete('state/{id}', 'StateController@delete');
        Route::get('statelist', 'StateController@stateList');

        Route::get('unitofmeasure', 'UnitOfMeasureController@index');
        Route::post('unitofmeasure', 'UnitOfMeasureController@save');
        Route::delete('unitofmeasure/{id}', 'UnitOfMeasureController@delete');

        Route::get('area', 'AreaController@index');
        Route::post('area', 'AreaController@save');
        Route::delete('area/{id}', 'AreaController@delete');

        Route::get('relationtype', 'RelationTypeReasonViolationController@index');
        Route::post('relationtype', 'RelationTypeReasonViolationController@save');
        Route::delete('relationtype/{id}', 'RelationTypeReasonViolationController@delete');

        Route::get('activities', 'ActivitiesController@index');
        Route::post('activities', 'ActivitiesController@save');
        Route::delete('activities/{id}', 'ActivitiesController@delete');

        Route::get('categories', 'CategoriesController@index');
        Route::post('categories', 'CategoriesController@save');
        Route::delete('categories/{id}', 'CategoriesController@delete');

        Route::get('tasks', 'TasksController@index');
        Route::post('tasks', 'TasksController@save');
        Route::delete('tasks/{id}', 'TasksController@delete');

        Route::get('timesheet', 'TimeSheetController@index');
        Route::post('timesheet', 'TimeSheetController@save');
        Route::delete('timesheet/{id}', 'TimeSheetController@delete');

        Route::get('workingday', 'WorkingDayController@index');
        Route::post('workingday', 'WorkingDayController@save');
        Route::delete('workingday/{id}', 'WorkingDayController@delete');

        Route::get('control', 'TypesViolationsController@index');
        Route::post('control', 'TypesViolationsController@save');
        Route::delete('control/{id}', 'TypesViolationsController@delete');

        Route::get('status', 'StatusController@index');
        Route::post('status', 'StatusController@save');
        Route::delete('status/{id}', 'StatusController@delete');

        Route::get('roles', 'UsuariosController@roles');
        Route::post('roles', 'UsuariosController@save');
        Route::delete('roles/{id}', 'UsuariosController@delete');

        Route::get('users', 'UsuariosController@users');
        Route::post('users', 'UsuariosController@save');
        Route::delete('users/{id}', 'UsuariosController@delete');
        Route::get('userlist', 'UsuariosController@userList');

        Route::get('violation', 'ViolationController@index');
        Route::post('violation', 'ViolationController@save');
        Route::delete('violation/{id}','ViolationController@delete');

        Route::get('motive', 'MotiveController@index');
        Route::post('motive', 'MotiveController@save');
        Route::delete('motive/{id}','MotiveController@delete');

        Route::get('profession', 'ProfessionController@index');
        Route::post('profession', 'ProfessionController@save');
        Route::delete('profession/{id}','ProfessionController@delete');

        Route::get('schedule', 'ScheduleController@index');
        Route::post('schedule', 'ScheduleController@save');
        Route::delete('schedule/{id}','ScheduleController@delete');

        Route::get('position', 'PositionController@index');
        Route::post('position', 'PositionController@save');
        Route::delete('position/{id}','PositionController@delete');

        Route::get('departament', 'DepartamentController@index');
        Route::post('departament', 'DepartamentController@save');
        Route::delete('departament/{id}','DepartamentController@delete');

        Route::get('employes', 'EmployesController@index');
        Route::post('employes', 'EmployesController@save');
        Route::delete('employes/{id}','EmployesController@delete');
        Route::get('employeelist', 'EmployesController@getEmployee');

        Route::get('violation_absences', 'ViolationAbsencesController@index');
        Route::post('violation_absences', 'ViolationAbsencesController@save');
        Route::delete('violation_absences/{id}','ViolationAbsencesController@delete');

        Route::get('absences', 'AbsencesController@index');
        Route::post('absences', 'AbsencesController@save');
        Route::delete('absences/{id}','AbsencesController@delete');
        Route::post('absences/aprove/{id}','AbsencesController@aprove');
        Route::post('absences/reject/{id}','AbsencesController@reject');
        Route::post('absences/finished/{id}','AbsencesController@finished');
        Route::get('absencelist','AbsencesController@getAbsence');

        Route::get('violationlist','ViolationAbsencesController@getAbsence');

        Route::get('sendmail','MailController@test');

        Route::get('import-excel', 'ImportExcel\ImportExcelController@index');
        Route::post('import-excel', 'ImportExcel\ImportExcelController@import');

        Route::get('export-excel', 'ExportExcel\ExportExcelController@export')->name('export-excel');

        //Route::get('calendar', 'CalendarController@index');

        Route::get('control-point', 'MonitorControlPoint@index');
        Route::get('control-point/{id}', 'MonitorControlPoint@detailControl');
        Route::get('get-control-points', 'MonitorControlPoint@getControlPoints');

        Route::get('dashboard', 'DashBoardController@index');

        //Route::resource('upload-files','FileController');
        route::get('clear-all-cache', function (){
            Artisan::call('config:clear');
            Artisan::call('cache:clear');
            Artisan::call('view:clear');
            Artisan::call('route:clear');
            return \Illuminate\Support\Facades\Response::json(['data'=>'all cache clear']);
        });

        Route::get('errortest', function () {
          Log::critical('This is a critical message Sent from Laravel App');
             return view('welcome');
        });
    });
//});

/*Route::group(['domain' => '{domain}.'.env('APP_URL')],  function($domain){
    Route::get('/', 'AbsencesController@kioskoIndex');

    Route::get('lang/{locale}', function ($domain,$locale) {
        App::setLocale($locale);
        session()->put('locale', $locale);
        return redirect()->back();
    });

    Route::post('employes-filter', 'API\EmployesController@searchEmployee');
    Route::post('register_absences', 'API\AbsencesController@save');
});*/