<?php

use Illuminate\Http\Request;

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {
    Route::post('login', 'Auth\LoginController@loginApi');
    Route::post('logout', 'Auth\LoginController@logoutApi');
    Route::post('refresh', 'Auth\LoginController@refresh');
    Route::post('me', 'Auth\LoginController@me');
});

Route::group(['middleware' => 'jwt.auth'], function () {
    //rutas que quieras que pasen por el jwt auth, requiere que envies el token que te devuelve en el login
});
Route::group(['middleware' => 'api'], function () {
    //rutas que quieras que pasen sin el jwt auth, es decir sin ningun tipo de seguridad
    Route::get('control', 'API\TypesViolationsController@index');
    Route::post('control', 'API\TypesViolationsController@save');
    Route::delete('control/{id}', 'API\TypesViolationsController@delete');

    Route::get('status', 'API\StatusController@index');
    Route::post('status', 'API\StatusController@save');
    Route::delete('status/{id}', 'API\StatusController@delete');
    Route::get('liststatus', 'API\StatusController@status');
    Route::get('liststatus_approved', 'API\StatusController@statusApproved');

    Route::get('roles', 'API\UsuariosController@roles');
    Route::post('roles', 'API\UsuariosController@save');
    Route::delete('roles/{id}', 'API\UsuariosController@delete');

    Route::get('users', 'API\UsuariosController@users');
    Route::post('users', 'API\UsuariosController@save');
    Route::delete('users/{id}', 'API\UsuariosController@delete');

    Route::get('violation', 'API\ViolationController@index');
    Route::post('violation', 'API\ViolationController@save');
    Route::delete('violation/{id}','API\ViolationController@delete');
    Route::get('listviolationAbsences', 'API\ViolationController@violationAbsences');
    Route::get('listviolation', 'API\ViolationController@violation');

    Route::post('register_violation', 'API\ViolationAbsencesController@save');

    Route::get('motive', 'API\MotiveController@index');
    Route::post('motive', 'API\MotiveController@save');
    Route::delete('motive/{id}','API\MotiveController@delete');
    Route::get('listmotive', 'API\MotiveController@motivos');
    Route::get('list_typeAbsences', 'API\MotiveController@typeAbsencesGeneral');
    Route::get('list_razon', 'API\MotiveController@typeAbsencesSpecific');

    Route::get('profession', 'API\ProfessionController@index');
    Route::post('profession', 'API\ProfessionController@save');
    Route::delete('profession/{id}','API\ProfessionController@delete');

    Route::get('schedule', 'API\ScheduleController@index');
    Route::post('schedule', 'API\ScheduleController@save');
    Route::delete('schedule/{id}','API\ScheduleController@delete');

    Route::get('position', 'API\PositionController@index');
    Route::post('position', 'API\PositionController@save');
    Route::delete('position/{id}','API\PositionController@delete');

    Route::get('departament', 'API\DepartamentController@index');
    Route::post('departament', 'API\DepartamentController@save');
    Route::delete('departament/{id}','API\DepartamentController@delete');

    Route::get('employes', 'API\EmployesController@index');
    Route::post('employes', 'API\EmployesController@save');
    Route::delete('employes/{id}','API\EmployesController@delete');
    Route::post('employes_supervisor', 'API\EmployesController@listEmployesSupervisor');
    Route::post('scheduleplan_employes_supervisor', 'API\EmployesController@listSchedulePlanEmployesSupervisor');
    Route::post('associateEmployeeUser', 'API\EmployesController@associateEmployeeUser');
    Route::post('employes-filter', 'API\EmployesController@searchEmployee');
    Route::post('employes-filter-temp', 'API\EmployesController@searchEmployeeTemp');

    Route::get('absences', 'API\AbsencesController@index');
    Route::post('register_absences', 'API\AbsencesController@save');
    Route::post('absenceslist', 'API\AbsencesController@cargarAusencia');
    Route::post('update_status_absences', 'API\AbsencesController@update');
    Route::post('count_employes_absent', 'API\AbsencesController@countEmployesAbsent');
    Route::delete('absences/{id}','API\AbsencesController@delete');
    Route::get('reason/list', 'API\MotiveController@getMotives');

    Route::post('upload_file', 'API\AbsencesController@uploadNote');

    Route::post('register_device', 'API\DeviceController@save');

});
