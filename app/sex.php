<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class sex extends Model
{
    //
     protected $table = 'sex';
	 protected $primaryKey = 'id';
	 public $timestamps = false;
	 protected $guarded = array('id');
}
