<?php

namespace App\Imports;

use App\employes;
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;

class ImportEmployes implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new employes([
            //

            //dd($this->transformDate($row[6]));
            'id' => @$row[0],
            'users_id' => @$row[1],
            'code' => @$row[2], 
            'firstname' => @$row[3],
            'lastname' => @$row[4], 
            'sex' => @$row[5], 
            'birthdate' => $this->transformDate($row[6]),
            'address' => @$row[7], 
            'numbersocial' => @$row[8],
            'zipcode' => @$row[9], 
            'numberphone' => @$row[10],
            'numberhome' => @$row[11], 
            'numberfax' => @$row[12],
            'sociallink' => @$row[13], 
            'departamentid' => @$row[14],
            'scheduleid' => @$row[15], 
            'positionid' => @$row[16],
            'professionid' => @$row[17], 
            'status' => @$row[18],
            'active' => @$row[19],
            'city' => @$row[20],
            'state' => @$row[21],
            'county' => @$row[22],
            'country' => @$row[23]
        ]);
    }


    /**
     * Transform a date value into a Carbon object.
     *
     * @return \Carbon\Carbon|null
     */
    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }
}
