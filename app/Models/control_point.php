<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class control_point extends Model
{
    protected $table = 'control_point';
    protected $primaryKey = 'id';

    public function savePoint($request){
        $newPoint = new control_point();
        $newPoint->employe_id = $this->employe_id;
        $newPoint->absence_id = $request->absence_id;
        $newPoint->point_absence = $request->points;
        $newPoint->consumido = $request->points;

        $puntos = abs($request->points);
        $puntos2 = $this->points;
        $puntos3 = $this->points_adici;

        for ($i = 1; $i <= $puntos; $i++) {
            if($puntos3 != 0){
                $puntos3 = $puntos3 -1;
            }else{
                $puntos2 = $puntos2 -1;
            }
        }

        $newPoint->points = $puntos2;
        $newPoint->points_adici = $puntos3;
        $newPoint->saldo = $puntos2 + $puntos3;
        return $newPoint->save();
    }

    public function saveHours($request){
        $newPoint = new control_point();
        $newPoint->employe_id = $this->employe_id;
        $newPoint->absence_id = $request->absence_id;
        $newPoint->horas = $request->horas;
        $newPoint->horas_consumido = $request->personal_time;
        $newPoint->horas_saldo = $request->horas - $request->personal_time;
        return $newPoint->save();
    }

    public function saveDays($request){
        $newPoint = new control_point();
        $newPoint->employe_id = $this->employe_id;
        $newPoint->absence_id = $request->absence_id;
        $newPoint->dia_vacaciones = $request->dia_vacaciones;
        $newPoint->dias_consumido = $request->dias_consumido;
        $newPoint->dias_saldo = $request->dia_vacaciones - $request->dias_consumido;
        return $newPoint->save();
    }

    public function employe(){
        return $this->hasOne('App\Models\employes','id','employe_id')->first();
    }

}
