<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;


class timesheet extends Model
{
    protected $table = "timesheet";
    public $timestamps = true;
    protected $primaryKey = "id";

     public function softDelete(){
            return $this->delete();
       }

      
    public function saveData(array $data = []){
        $this->Validator($data);
        $this->employesid = (!empty($data['employesid'])) ? $data['employesid'] : $this->employesid;
        $this->timesheet_date = (!empty($data['timesheet_date'])) ? $data['timesheet_date'] : $this->timesheet_date;
        $this->hour_start = (!empty($data['hour_start'])) ? $data['hour_start'] : $this->hour_start;
        $this->hour_stop = (!empty($data['hour_stop'])) ? $data['hour_stop'] : $this->hour_stop;
        $this->comments = (!empty($data['comments'])) ? $data['comments'] : $this->comments;
        parent::save();
        return $this;
    }

    protected function Validator(array $data = [])
    {
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'employesid' => 'required',
                'timesheet_date' => 'required',
                'hour_start' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'employesid' => 'required',
                'timesheet_date' => 'required',
                'hour_start' => 'required',
                'hour_stop' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

     public function employee(){
        return $this->hasOne('App\Models\employes','id','employesid')->first();
    }
}
