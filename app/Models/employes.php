<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;


class employes extends Model
{
    protected $table = 'employes';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public function softDelete(){
        return $this->delete();
    }

    public function saveData(array $data = []){
        $this->Validator($data);
        $this->code = (!empty($data['code'])) ? $data['code'] : $this->code;
		$this->firstname = (!empty($data['firstname'])) ? $data['firstname'] : $this->firstname;
		$this->lastname = (!empty($data['lastname'])) ? $data['lastname'] : $this->lastname;
		$this->sex = (!empty($data['sex'])) ? $data['sex'] : $this->sex;
		$this->birthdate = (!empty($data['birthdate'])) ? $data['birthdate'] : $this->birthdate;
		$this->address = (!empty($data['address'])) ? $data['address'] : $this->address;
        $this->email = (!empty($data['email'])) ? $data['email'] : $this->email;
		$this->numbersocial = (!empty($data['numbersocial'])) ? $data['numbersocial'] : $this->numbersocial;
		$this->zipcode = (!empty($data['zipcode'])) ? $data['zipcode'] : $this->zipcode;
		$this->numberphone = (!empty($data['numberphone'])) ? $data['numberphone'] : $this->numberphone;
		$this->numberhome = (!empty($data['numberhome'])) ? $data['numberhome'] : $this->numberhome;
		$this->sociallink = (!empty($data['sociallink'])) ? $data['sociallink'] : $this->sociallink;
		$this->departamentid= (!empty($data['departamento'])) ? $data['departamento'] : $this->departamentid;
        $this->areaid= (!empty($data['area'])) ? $data['area'] : $this->areaid;
        $this->idsite= (!empty($data['idsite'])) ? $data['idsite'] : $this->idsite;
		$this->scheduleid = (!empty($data['horario'])) ? $data['horario'] : $this->scheduleid;
		$this->positionid = (!empty($data['position'])) ? $data['position'] : $this->positionid;
		$this->professionid = (!empty($data['profesion'])) ? $data['profesion'] : $this->professionid;
		$this->rolesid = (!empty($data['roles'])) ? $data['roles'] : $this->rolesid;
		$this->status = (!empty($data['status'])) ? $data['status'] : $this->status;
        $this->finished_date = (!empty($data['finished_date'])) ? $data['finished_date'] : $this->finished_date;

        return parent::save();
    }

    public function updateEmployeData(array $data = []){
        //$this->Validator($data);
        $this->users_id = (!empty($data['users_id'])) ? $data['users_id'] : $this->users_id;
        
        return parent::save();
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'code' => $required.',code',
                'firstname' => 'required',
                'lastname' => 'required',
                'sex' => 'required',
                'birthdate' => 'required',
                'address' => 'required',
                'email' => 'required',
                'numbersocial' => 'required',
                'zipcode' => 'required',
                'numberphone' => 'required',
                'departamento' => 'required',
                'idsite' => 'required',
                'horario' => 'required',
                'position' => 'required',
                'profesion' => 'required',
                'roles' => 'required',
                'status' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'code' =>  $required.',code,'.$data['id'].','.$this->primaryKey,
                'firstname' => 'required',
                'lastname' => 'required',
                'sex' => 'required',
                'birthdate' => 'required',
                'address' => 'required',
                'email' => 'required',
                'numbersocial' => 'required',
                'zipcode' => 'required',
                'numberphone' => 'required',
                'departamento' => 'required',
                'idsite' => 'required',
                'horario' => 'required',
                'position' => 'required',
                'profesion' => 'required',
                'roles' => 'required',
                'status' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

    public function area()
    {
        return $this->hasOne('App\Models\area','id','areaid')->first();
    }

    public function site()
    {
        return $this->hasOne('App\Models\site','id','idsite')->first();
    }

    public function departament()
    {
        return $this->hasOne('App\Models\departament','id','departamentid')->first();
    }

    public function position()
    {
        return $this->hasOne('App\Models\position','id','positionid')->first();
    }

    public function profession()
    {
        return $this->hasOne('App\Models\profession','id','professionid')->first();
    }

    public function roles()
    {
        return $this->hasOne('App\Models\roles','id','rolesid')->first();
    }

    public function sex()
    {
        return $this->hasOne('App\sex','id','sex')->first();
    }

    public function schedule()
    {
        return $this->hasOne('App\Models\schedule','id','scheduleid')->first();
    }

    public function absence()
    {
        return $this->hasMany('App\Models\absences','employesid','id');
    }

    public function controlPoint(){
        return $this->hasMany('App\Models\control_point','employe_id','id');
    }

     public function getEmployeeTable($request){
        $query = $this->selectraw('employes.*, departament.description as departamentid, area.description as areaid, site.name as siteid')
            ->join('departament','departament.id','=','employes.departamentid')
            ->join('area','area.id','=','employes.areaid')
            ->join('site','site.id','=','employes.idsite')
            ->orderBy('employes.id','DESC');

        $filter = Session::get('filter');

        if(!$filter->all){
            if($filter->site){
                $query = $query->where('employes.idsite', '=', $filter->siteId);
            }
            if($filter->area){
                 $query = $query->where('employes.areaid',$filter->areaId);
            }
            if($filter->departament){
                 $query = $query->where('employes.departamentid',$filter->departId);
            }
        }

        if(!empty($request->code)){
            $query = $query->where('employes.code',$request->code);
        }
        if(!empty($request->firstname)){
            $query = $query->where('employes.firstname',$request->firstname);
        }
        if(!empty($request->lastname)){
            $query = $query->where('employes.lastname',$request->lastname);
        }
        if(!empty($request->departamento)){
            $query = $query->where('employes.departamentid',$request->departamento);
        }
       
        // if(Auth::user()->rol_id != 1 and Auth::user()->site_id != 4 ) {
        //     $query = $query->where('employes.idsite',Auth::user()->site_id);
        // }
        return $query;
    }
}
