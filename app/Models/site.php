<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class site extends Model
{
    protected $table = "site";
    public $timestamps = true;
    protected $primaryKey = "id";

    public function areas(){
        if($this->id == 4){
            return area::all();
        }
        return $this->hasMany('App\Models\area','idsite','id')->get();
    }

    public function softDelete(){
        return $this->delete();
    }

    public function saveData(array $data = []){
        $this->Validator($data);
        $this->name = (!empty($data['description'])) ? $data['description'] : $this->name;
        return parent::save();
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'description' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'description' =>  $required.',description,'.$data['id'].','.$this->primaryKey,
               
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

      public function getSiteList($request){
        $query = site::selectRaw('site.id as siteId, site.name as siteDescription');
           // dd($query);
        if (!empty($request['search']['value'])) {
            $query = $query->where(function ($query1) use($request) {
                $query1->Where('site.name', 'like', '%'.$request['search']['value'].'%');
               
             
            });
        }

        return $query;
    }
}