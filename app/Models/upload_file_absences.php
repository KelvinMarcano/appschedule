<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class upload_file_absences extends Model
{
    protected $table = 'upload_file_absences';
    protected $primaryKey = 'id';

    public function save($request){
        $newPoint = new upload_file_absences();
        $newPoint->absence_id = $request->absence_id;
        $newPoint->filename = $request->file;

        return $newPoint->save();
    }

    public function absences(){
        return $this->hasOne('App\Models\absences','id','absences_id')->first();
    }
}
