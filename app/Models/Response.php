<?php

namespace App\Models;

use Illuminate\Support\Facades\Log;

class Response
{
    public static function status($request, $status,$msg = "",$accion = ""){
        //$status = success,error,warning,info,question
        //$status1 = Éxito,Error,Alerta!,Importante!,Confirmar
        $code = 200;
        switch ($status){
            case "error" : {
                $code = 500;
                break;
            };
            /*case "warning" : {
                $code = 500;
                break;
            };*/
        }

        if($code == 500){
            $response = \Response::json(array('status' => $status,'title' => self::mensaje($status),'msg' =>$msg,'accion' =>$accion,'code' => $code),$code);
            Log::error($response);
        }

        $request->session()->flash('status', array('status' => $status,'title' => self::mensaje($status),'msg' =>$msg,'accion' =>$accion));
        return true;
    }

    public static function statusJson($request, $status,$msg = "",$accion = "",$data = null)
    {
        $code = 200;
        switch ($status){
            case "error" : {
                $code = 500;
                break;
            };
            case "warning" : {
                $code = 500;
                break;
            };
        }
        $response = \Response::json(array('status' => $status,'title' => self::mensaje($status),'msg' =>$msg,'accion' =>$accion,'data' => $data,'code' => $code),$code);

        if($code == 500) Log::error($response);
        return $response;
    }

    public static function mensaje($status){
        $message = "";
        switch ($status){
            case "question": {
                $message = "Confirm";
                break;
            };
            case "info": {
                $message = "Important!";
                break;
            };
            case "warning": {
                $message = "Alert!";
                break;
            };
            case "error": {
                $message = "Error";
                break;
            };
            case "success": {
                $message = "Success";
                break;
            };
        }
        return $message;
    }
}