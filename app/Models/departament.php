<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class departament extends Model
{
    protected $table = "departament";
    public $timestamps = true;
    protected $primaryKey = "id";

    public function softDelete(){
        return $this->delete();
    }

    public function saveData(array $data = []){
        $this->Validator($data);
        $this->code = (!empty($data['code'])) ? $data['code'] : $this->code;
        $this->description = (!empty($data['description'])) ? $data['description'] : $this->description;
        $this->employesid = (!empty($data['employesid'])) ? $data['employesid'] : $this->employesid;
        $this->idsite = (!empty($data['idsite'])) ? $data['idsite'] : $this->idsite;
        $this->areaid = (!empty($data['areaid'])) ? $data['areaid'] : $this->areaid;
        return parent::save();
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'code' => $required.',code',
                'description' => 'required',
                'employesid' => 'required',
                'idsite' => 'required',
                'areaid' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'code' =>  $required.',code,'.$data['id'].','.$this->primaryKey,
                'description' => 'required',
                'employesid' => 'required',
                'idsite' => 'required',
                'areaid' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

     public function site()
    {
        return $this->hasOne('App\Models\site','id','idsite')->first();
    }

     public function area()
    {
        return $this->hasOne('App\Models\area','id','areaid')->first();
    }

     public function employee(){
        return $this->hasOne('App\Models\employes','id','employesid')->first();
    }
}