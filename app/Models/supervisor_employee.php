<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class supervisor_employee extends Model
{
    protected $table = "supervisor_employee";
    public $timestamps = true;
    protected $primaryKey = "id";
}