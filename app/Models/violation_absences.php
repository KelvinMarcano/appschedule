<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class violation_absences extends Model
{
    protected $table = 'absences';
    protected $primaryKey = 'id';

    public function softDelete(){
            return $this->delete();
       }

    public function saveData(array $data = []){
        $this->Validator($data);
        $this->employesid = (!empty($data['employesid'])) ? $data['employesid'] : $this->employesid;
        $this->motiveid = (!empty($data['motiveid'])) ? $data['motiveid'] : $this->motiveid;
        $this->fecha_init = (!empty($data['fecha_init'])) ? $data['fecha_init'] : $this->fecha_init;
        $this->violation_id = (!empty($data['violation_id'])) ? $data['violation_id'] : $this->violation_id;
        $this->hora_init = (!empty($data['hora_init'])) ? $data['hora_init'] : $this->hora_init;
        $this->status_id = (!empty($data['status_id'])) ? $data['status_id'] : 5;
        $this->comment = (!empty($data['comment'])) ? $data['comment'] : $this->comment;
        $this->no_call_no = (!empty($data['no_call_no'])) ? 0 : 1;
        $this->days = (!empty($data['days'])) ? $data['days'] : 0;
        $this->type_absences = 4;
        $this->user_add = $data['user_add'];
        $this->platform = $data['platform'];
        parent::save();
        return $this;
    }

    public function updateStatus(array $data = []){
        //$this->id = (!empty($data['absencesid'])) ? $data['absencesid'] : $this->employesid;
        $this->violation_id = (!empty($data['violation_id'])) ? $data['violation_id'] : $this->violation_id;
        $this->status_id = (!empty($data['status_id'])) ? $data['status_id'] : 5;
        $this->comment = $data['comment'];
        
        parent::save();
        return $this;
    }

    protected function Validator(array $data = [])
    {
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'employesid' => 'required',
                'motiveid' => 'required',
                'fecha_init' => 'required',
                'hora_init' => 'required',
                'violation_id' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'employesid' => 'required',
                'motiveid' => 'required',
                'fecha_init' => 'required',
                'hora_init' => 'required',
                'violation_id' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

    public function reject(){
        $this->status_id = 2;
        $this->fecha_recha = Carbon::now();
        return $this->save();
    }

    public function control(){
        return $this->hasOne('App\Models\control_point','absence_id','id')->get()->last();
    }

    public function employee(){
        return $this->hasOne('App\Models\employes','id','employesid')->first();
    }

    public function motive(){
        return $this->hasOne('App\Models\motive','id','motiveid')->first();
    }

    public function violation(){
        return $this->hasOne('App\Models\violation','id','violation_id')->first();
    }

    public function status(){
        return $this->hasOne('App\Models\status','id','status_id')->first();
    }

    public function getAbsenceTable($request){
        $query = $this->selectraw('absences.*,employes.idsite as site, violation.points as points')
            ->join('employes','employes.id','=','absences.employesid')
            ->join('violation','violation.id','=','absences.violation_id')
            ->where('type_absences','=','4')
            ->orderBy('fecha_init','DESC');

        $filter = Session::get('filter');

        if(!$filter->all){
            if($filter->site){
                $query = $query->where('employes.idsite', '=', $filter->siteId);
            }
            if($filter->area){
                 $query = $query->where('employes.areaid',$filter->areaId);
            }
            if($filter->departament){
                 $query = $query->where('employes.departamentid',$filter->departId);
            }
        }

        if(!empty($request->desde)){
            $request['desde'] = Carbon::createFromFormat('m/d/Y', $request->desde)->format('Y-m-d');

            $query = $query->Where(function($query1) use ($request) {
                $query1->where('fecha_init','=',$request->desde);
            });
        }
        if(!empty($request->hora)){
             $query = $query->where('hora_init',$request->hora);
        }
        if(!empty($request->empleado)){
            $query = $query->where('employesid',$request->empleado);
        }
        if(!empty($request->violacion)){
            $query = $query->where('violation_id',$request->violacion);
        }
        if(!empty($request->status)){
            $query = $query->where('status_id',$request->status);
        }

        return $query;
    }

    public function aprove($request){
        $this->fecha_aprove = Carbon::now();
        $this->note_aprov = $request->note;
        $this->status_id = $request->status;
        return $this->save();
    }
}
