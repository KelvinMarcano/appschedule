<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class zipcode extends Model
{
    protected $table = "zipcode";
    public $timestamps = true;
    protected $primaryKey = "id";

    public function softDelete(){
        return $this->delete();
    }

    public function saveData(array $data = []){
        $this->Validator($data);
        $this->code = (!empty($data['code'])) ? $data['code'] : $this->code;
        $this->description = (!empty($data['description'])) ? $data['description'] : $this->description;
        //$this->stateid = (!empty($data['stateid'])) ? $data['stateid'] : $this->stateid;
        return parent::save();
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'code' => $required.',code',
                'description' => 'required',
                //'stateid' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'code' =>  $required.',code,'.$data['id'].','.$this->primaryKey,
                'description' => 'required',
                //'stateid' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

    //  public function states()
    // {
    //     return $this->hasOne('App\Models\state','id','stateid')->first();
    // }

     public function getzipCodeList($request){
        $query = zipcode::selectRaw('zipcode.id as zipcodeId, zipcode.code as zipcodeCode, zipcode.description as zipcodeDescription');
            //->join('state','state.id','=','stateid');

        if (!empty($request['search']['value'])) {
            $query = $query->where(function ($query1) use($request) {
                $query1->Where('zipcode.code', 'like', '%'.$request['search']['value'].'%')
                ->orWhere('zipcode.description', 'like', '%'.$request['search']['value'].'%');
            
            });
        }

        return $query;
    }

}
