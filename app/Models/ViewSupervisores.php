<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ViewSupervisores extends Model
{
    protected $table = "view_supervisores";
    public $timestamps = false;

    public function viewSupervisor($empleado){
        $relation =  $this->orwhere(function ($query) use ($empleado){
            $query->where('departamentid',$empleado->departamentid)
                ->where('rolesid',5);
        });

        $relation =  $relation->orwhere(function ($query) use ($empleado){
            $query->where('areaid',$empleado->areaid)
                ->where('rolesid',3);
        });

        $relation =  $relation->orwhere(function ($query) use ($empleado){
            $query->where('idsite',$empleado->idsite)
                ->where('rolesid',10);
        });

        return $relation->get();

        /*
         * SELECT * FROM view_supervisores
            WHERE (departamentid=5 AND rolesid=5)
            OR (areaid=2 AND rolesid=3)
            OR (idsite=1 AND rolesid=10);
        */
    }
}