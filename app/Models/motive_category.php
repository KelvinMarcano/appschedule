<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class motive_category extends Model
{
    protected $table = "motive_category";
    public $timestamps = true;
    protected $primaryKey = "cat_id";


}