<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class schedule extends Model
{
    protected $table = "schedule";
    public $timestamps = true;
    protected $primaryKey = "id";

    public function softDelete(){
        return $this->delete();
    }

    public function saveData(array $data = []){
        $this->Validator($data);
        $this->code = (!empty($data['code'])) ? $data['code'] : $this->code;
        $this->description = (!empty($data['description'])) ? $data['description'] : $this->description;
        $this->hour_start = (!empty($data['hour_start'])) ? $data['hour_start'] : $this->hour_start;
        $this->hour_end = (!empty($data['hour_end'])) ? $data['hour_end'] : $this->hour_end;
        $this->break_time_start = (!empty($data['break_time_start'])) ? $data['break_time_start'] : $this->break_time_start;
        $this->break_time_end = (!empty($data['break_time_end'])) ? $data['break_time_end'] : $this->break_time_end;
         $this->break_time_start_2 = (!empty($data['break_time_start_2'])) ? $data['break_time_start_2'] : $this->break_time_start_2;
        $this->break_time_end_2 = (!empty($data['break_time_end_2'])) ? $data['break_time_end_2'] : $this->break_time_end_2;
         $this->working_day_id = (!empty($data['working_day_id'])) ? $data['working_day_id'] : $this->working_day_id;
        return parent::save();
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'code' => $required.',code',
                'description' => 'required',
                'hour_start' => 'required',
                'hour_end' => 'required',
                'break_time_start' => 'required',
                'break_time_end' => 'required',
                'working_day_id' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'code' =>  $required.',code,'.$data['id'].','.$this->primaryKey,
                'description' => 'required',
                'hour_start' => 'required',
                'hour_end' => 'required',
                'break_time_start' => 'required',
                'break_time_end' => 'required',
                'working_day_id' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }
}