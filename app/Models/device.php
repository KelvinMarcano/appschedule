<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class device extends Model
{
    protected $table = 'device_registration';
    protected $primaryKey = 'id';

    public function softDelete(){
            return $this->delete();
       }

    public function saveData(array $data = []){
        //$this->Validator($data);
        $this->id_user = (!empty($data['id_user'])) ? $data['id_user'] : $this->id_user;
        $this->id_employe = (!empty($data['id_employe'])) ? $data['id_employe'] : $this->id_employe;
        $this->token_device = (!empty($data['token_device'])) ? $data['token_device'] : $this->token_device;
        $this->device_manufacturer = (!empty($data['device_manufacturer'])) ? $data['device_manufacturer'] : $this->device_manufacturer;
        $this->device_model = (!empty($data['device_model'])) ? $data['device_model'] : $this->device_model;
        $this->device_brand = (!empty($data['device_brand'])) ? $data['device_brand'] : $this->device_brand;
        $this->device_id = (!empty($data['device_id'])) ? $data['device_id'] : $this->device_id;
        $this->version_android = (!empty($data['version_android'])) ? $data['version_android'] : $this->version_android;
        $this->nombre_android = (!empty($data['nombre_android'])) ? $data['nombre_android'] : 5;
        $this->num_sdk_android = (!empty($data['num_sdk_android'])) ? $data['num_sdk_android'] : $this->num_sdk_android;
        $this->android_id = (!empty($data['android_id'])) ? $data['android_id'] : $this->android_id;
        $this->aplication = (!empty($data['aplication'])) ? $data['aplication'] : 'AppSupervisor';

        parent::save();
        return $this;
    }

    public function updateStatus(array $data = []){
        //$this->id = (!empty($data['absencesid'])) ? $data['absencesid'] : $this->employesid;
        $this->token_device = (!empty($data['token_device'])) ? $data['token_device'] : '';
        
        parent::save();
        return $this;
    }

    protected function Validator(array $data = [])
    {
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'id' => 'required',
                'id_user' => 'required',
                'id_employe' => 'required',
                'token_device' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'id_user' => 'required',
                'id_employe' => 'required',
                'token_device' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

    public function control(){
        return $this->hasOne('App\Models\control_point','absence_id','id')->get()->last();
    }

    public function employee(){
        return $this->hasOne('App\Models\employes','id','employesid')->first();
    }
}
