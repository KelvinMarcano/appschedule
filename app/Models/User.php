<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table = "users";
    public $timestamps = true;
    protected $primaryKey = "id";

    public function role()
    {
        return $this->hasOne('App\Models\roles','id','rol_id')->first();
    }

    public function site()
    {
        return $this->hasOne('App\Models\site','id','site_id')->first();
    }

    public function employes()
    {
        return $this->hasOne('App\Models\employes','users_id','id')->first();
    }

    public function sexo()
    {
        return $this->hasOne('App\Models\sex','id','sex')->first();
    }

    public function departament()
    {
        return $this->hasOne('App\Models\departament','id','departamentid')->first();
    }

    public function schedule()
    {
        return $this->hasOne('App\Models\schedule','id','scheduleid')->first();
    }

    public function position()
    {
        return $this->hasOne('App\Models\position','id','positionid')->first();
    }

    public function profession()
    {
        return $this->hasOne('App\Models\profession','id','professionid')->first();
    }

    public function softDelete(){
        return $this->delete();
    }

    public function area(){
        return $this->hasOne('App\Models\area','id','areaid')->first();
    }

    public function areas(){
        $relation = $this->site();

        if(!$relation) return null;

        $areas = $relation->areas();

        if(!$areas) return null;

        $area = [];
        foreach ($areas as $item){
            $area[] = $item->id;
        }
        return $area;
    }

    public function departamentos(){
        $relation = $this->site();

        if(!$relation) return null;

        $areas = $relation->areas();

        if(!$areas) return null;

        $depart = [];

        foreach($areas as $area){
            $departaments = $area->department();

            foreach ($departaments as $departme){
                $depart[] = $departme->id;
            }
        }
        return $depart;
    }

    public function saveData(array $data = []){
        $data['password'] = (!empty($data['password']) and strlen($data['password']) < 15) ? Hash::make($data['password']) : $this->password;
        $this->Validator($data);
        $this->email = (!empty($data['email'])) ? $data['email'] : $this->email;
        $this->name = (!empty($data['name'])) ? $data['name'] : $this->name;
        $this->password = $data['password'];
        $this->rol_id = (!empty($data['rol_id'])) ? $data['rol_id'] : $this->rol_id;
        $this->site_id = (!empty($data['site_id'])) ? $data['site_id'] : $this->site_id;
        $this->state = (!empty($data['state'])) ? 1 : 0;
        $this->save();

        $employee = (new employes())->find($data['employe']);
        if(!empty($employee)){
            $employee->users_id = $this->id;
            $employee->save();
        }

        return true;
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'email' => $required.',email',
                'name' => 'required',
                'password' => 'required',
                'rol_id' => 'required',
                'site_id' => 'required',
                'state' => 'required',
                'employe' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'email' =>  $required.',email,'.$data['id'].','.$this->primaryKey,
                'name' => 'required',
                'password' => 'required',
                'rol_id' => 'required',
                'site_id' => 'required',
                'state' => 'required',
                'employe' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getUserList($request){
        $query = User::selectRaw('users.id as userId, users.name as userName, users.email as userEmail, users.state as userState, roles.name as roleName, site.name as siteName')
            ->leftjoin('roles','roles.id','=','rol_id')
            ->leftjoin('site','site.id','=','site_id');

        if (!empty($request['search']['value'])) {
            $query = $query->where(function ($query1) use($request) {
                $query1->Where('users.name', 'like', '%'.$request['search']['value'].'%')
                ->orWhere('users.email', 'like', '%'.$request['search']['value'].'%')
                ->orWhere('site.name', 'like', '%'.$request['search']['value'].'%')
                ->orWhere('roles.name', 'like', '%'.$request['search']['value'].'%');
            });
        }

        return $query;
    }
}
