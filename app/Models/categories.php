<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class categories extends Model
{
    protected $table = "categories";
    public $timestamps = true;
    protected $primaryKey = "id";

    public function softDelete(){
        return $this->delete();
    }

    public function saveData(array $data = []){
        $this->Validator($data);
        $this->code = (!empty($data['code'])) ? $data['code'] : $this->code;
        $this->description = (!empty($data['description'])) ? $data['description'] : $this->description;
        return parent::save();
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'code' => $required.',code',
                'description' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'code' =>  $required.',code,'.$data['id'].','.$this->primaryKey,
                'description' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }
}
