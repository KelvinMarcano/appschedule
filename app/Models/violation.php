<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class violation extends Model
{
    protected $table = "violation";
    public $timestamps = true;
    protected $primaryKey = "id";

    public function violationType()
    {
        return $this->hasOne('App\Models\violation_types','id','type_violation_id')->first();
    }

    public function softDelete(){
        return $this->delete();
    }

    public function saveData(array $data = []){
        $this->Validator($data);
        $this->code = (!empty($data['code'])) ? $data['code'] : $this->code;
        $this->description = (!empty($data['description'])) ? $data['description'] : $this->description;
        $this->points = (!empty($data['points'])) ? $data['points'] : $this->points;
        $this->unitofmeasure_id = (!empty($data['unitofmeasure_id'])) ? $data['unitofmeasure_id'] : $this->unitofmeasure_id;
        $this->quantity = (!empty($data['quantity'])) ? $data['quantity'] : $this->quantity;
        $this->type_violation_id = (!empty($data['type_violation_id'])) ? $data['type_violation_id'] : $this->type_violation_id;
        return parent::save();
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'code' => $required.',code',
                'description' => 'required',
                // 'points' => 'required',
                'type_violation_id' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'code' =>  $required.',code,'.$data['id'].','.$this->primaryKey,
                'description' => 'required',
                // 'points' => 'required',
                'type_violation_id' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

    public function unitofmeasure(){
        return $this->hasOne('App\Models\unitofmeasure','id','unitofmeasure_id')->first();
    }

}