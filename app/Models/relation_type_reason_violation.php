<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class relation_type_reason_violation extends Model
{
    protected $table = "relation_type_reason_violation";
    public $timestamps = true;
    protected $primaryKey = "id";

    public function softDelete(){
        return $this->delete();
    }

    public function saveData(array $data = []){
        $this->Validator($data);
        $this->motiveid = (!empty($data['motiveid'])) ? $data['motiveid'] : $this->motiveid;
        $this->motiveid2 = (!empty($data['motiveid2'])) ? $data['motiveid2'] : $this->motiveid;
        $this->violationid = (!empty($data['violationid'])) ? $data['violationid'] : $this->violationid;
        $this->tipo = (!empty($data['tipo'])) ? $data['tipo'] : $this->tipo;
        
        return parent::save();
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, ['motiveid' => $required.',motiveid',
                'violationid' => 'required',
                'tipo' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'motiveid' =>  $required.',motiveid,'.$data['id'].','.$this->primaryKey,
                'violationid' => 'required',
                'tipo' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

     public function motive(){
        return $this->hasOne('App\Models\motive','id','motiveid')->first();
    }

    public function violation(){
        return $this->hasOne('App\Models\violation','id','violationid')->first();
    }

     public function motive2(){
        return $this->hasOne('App\Models\motive','id','motiveid2')->first();
    }
}
