<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class motive extends Model
{
    protected $table = "motive";
    public $timestamps = true;
    protected $primaryKey = "id";

    public function softDelete(){
        return $this->delete();
    }

    public function saveData(array $data = []){
        $this->Validator($data);
        $this->code = (!empty($data['code'])) ? $data['code'] : $this->code;
        $this->description = (!empty($data['description'])) ? $data['description'] : $this->description;
        $this->violation_id = (!empty($data['violation_id'])) ? $data['violation_id'] : $this->violation_id;
        $this->categories_id = (!empty($data['categories_id'])) ? $data['categories_id'] : $this->categories_id;
         $this->required_note = (!empty($data['required_note'])) ? $data['required_note'] : $this->required_note;
        return parent::save();
    }

    protected function Validator(array $data = [])
    {
        $required = 'required|unique:'.$this->table;
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'code' => $required.',code',
                'description' => 'required',
                'categories_id' => 'required',
                'required_note'=>'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'code' =>  $required.',code,'.$data['id'].','.$this->primaryKey,
                'description' => 'required',
                'categories_id' => 'required',
                'required_note'=>'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

    public function categories(){
        return $this->hasOne('App\Models\categories','id','categories_id')->first();
    }

     public function violation(){
        return $this->hasOne('App\Models\violation','id','violation_id')->first();
    }
}