<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class role_filter extends Model
{
    protected $table = "role_filter";
    public $timestamps = true;
    protected $primaryKey = "id";

    public function filters(){
        $filter = [];
        if($this->id == 5) return 'None';
        if($this->all) $filter[] = 'All';
        if($this->site) $filter[] = 'Site';
        if($this->area) $filter[] = 'Area';
        if($this->departament) $filter[] = 'Department';
        return implode(',',$filter);
    }

}