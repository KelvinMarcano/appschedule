<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class sex extends Model
{
    protected $table = 'sex';
    protected $primaryKey = 'id';
    public $timestamps = false;

       public function softDelete(){
            return $this->delete();
       }

      
    public function saveData(array $data = []){
        $this->Validator($data);
        $this->employesid = (!empty($data['employesid'])) ? $data['employesid'] : $this->employesid;
        $this->motiveid = (!empty($data['motiveid'])) ? $data['motiveid'] : $this->motiveid;
        $this->reason = (!empty($data['reason'])) ? $data['reason'] : $this->reason;
        $this->fecha_init = (!empty($data['fecha_init'])) ? $data['fecha_init'] : $this->fecha_init;
        $this->fecha_end = (!empty($data['fecha_end'])) ? $data['fecha_end'] : $this->fecha_end;
        $this->violation_id = (!empty($data['violation_id'])) ? $data['violation_id'] : $this->violation_id;
        $this->hora_init = (!empty($data['hora_init'])) ? $data['hora_init'] : $this->hora_init;
        $this->hora_end = (!empty($data['hora_end'])) ? $data['hora_end'] : $this->hora_end;
        $this->status_id = (!empty($data['status_id'])) ? $data['status_id'] : 5;
        $this->comment = (!empty($data['comment'])) ? $data['comment'] : $this->comment;
        $this->justificada = (!empty($data['justificada'])) ? $data['justificada'] : $this->justificada;
        $this->no_call_no = (!empty($data['justificada'])) ? 1 : 0;
        $this->days = (!empty($data['days'])) ? $data['days'] : 0;
        $this->required_note_oficial = (!empty($data['required_note_oficial'])) ? 1 : 0;
        parent::save();
        return $this;
    }

    protected function Validator(array $data = [])
    {
        if(empty($data['id'])){
            $validator = Validator::make($data, [
                'employesid' => 'required',
                'motiveid' => 'required',
                'fecha_init' => 'required',
                'fecha_end' => 'required',
                'violation_id' => 'required',
            ]);
        }else{
            $validator = Validator::make($data, [
                'employesid' => 'required',
                'motiveid' => 'required',
                'fecha_init' => 'required',
                'fecha_end' => 'required',
                'violation_id' => 'required',
            ]);
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach($errors as $error){
                $err.= $ctn++.')'.$error.'\n';
            }
            throw new \Exception($err);
        }
    }

    public function Reject(){
        $this->status = 2;
        return $this->save();
    }


    public function employee(){
        return $this->hasOne('App\Models\employes','id','employesid')->first();
    }

    public function motive(){
        return $this->hasOne('App\Models\motive','id','motiveid')->first();
    }

    public function violation(){
        return $this->hasOne('App\Models\violation','id','violation_id')->first();
    }

     public function status(){
        return $this->hasOne('App\Models\status','id','status_id')->first();
    }
}
