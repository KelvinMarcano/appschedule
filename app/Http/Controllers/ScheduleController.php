<?php

namespace App\Http\Controllers;

use App\Models\schedule;
use App\Models\working_day;
use Illuminate\Http\Request;
use App\Models\Response as Resp;

class ScheduleController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['schedules'] = (new schedule())->all();
        $data['working_day'] = (new working_day())->all();
        return view('configuracion.schedule', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new schedule();
            $model = $model->find($request->id);

            if(empty($model)) $model = new schedule();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'schedule');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'schedule');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'schedule');
            Resp::status($request,"error",$e->getMessage(),'save '.'schedule');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new schedule();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'schedule');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'schedule',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'schedule');
        }
    }
}
