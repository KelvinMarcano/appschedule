<?php

namespace App\Http\Controllers;

use App\Models\upload_file_absences;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;


class UploadFileAbsencesController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
       // $data = array();
        //$data['upload_file_absences'] = (new upload_file_absences())->orderby('code','ASC')->get();
        //$data['idsites'] = (new site())->all();

        //return view('configuracion.area', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new upload_file_absences();
            $model = $model->find($request->id);

            if(empty($model)) $model = new upload_file_absences();

            $model->save($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'upload');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'upload');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'upload');
            Resp::status($request,"error",$e->getMessage(),'save '.'upload');
            return redirect()->back();
        }
    }


    public function fileUpload(Request $req){
        $req->validate([
        'file' => 'required|mimes:csv,txt,xlx,xls,pdf|max:2048'
        ]);

        $fileModel = new upload_file_absences;

        if($req->upload_file_absences()) {
            $fileName = time().'_'.$req->file->getClientOriginalName();
            console.log($fileName);
            $filePath = $req->file('file')->storeAs('uploads', $fileName, 'public');

            $fileModel->filename = time().'_'.$req->file->getClientOriginalName();
            $fileModel->file_path = '/storage/' . $filePath;
            $fileModel->save();

            return back()
            ->with('success','File has been uploaded.')
            ->with('file', $fileName);
        }
   }
}
