<?php

namespace App\Http\Controllers;

use App\Models\city;
use App\Models\state;
use App\Models\zipcode;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;


class ZipCodeController extends Controller
{
   const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        //$data['city'] = (new city())->all();
         //$data['state'] = (new state())->all();
         $data['zipcode'] = (new zipcode())->all();
        return view('configuracion.zipcode', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new zipcode();
            $model = $model->find($request->id);

            if(empty($model)) $model = new zipcode();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'zipcode');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'zipcode');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'zipcode');
            Resp::status($request,"error",$e->getMessage(),'save '.'zipcode');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new zipcode();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'zipcode');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'zipcode',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'zipcode');
        }
    }

    public function zipcodeList(Request $request){
        $query = new zipcode();

        $query = $query->getzipCodeList($request)->get();

       // dd($query);

        return DataTables::of($query)
            ->addColumn('id', function ($query) {
                return $query->zipcodeId;
            })
            ->addColumn('code', function ($query) {
                return $query->zipcodeCode;
            })
            ->addColumn('description', function ($query) {
                return $query->zipcodeDescription;
            })
          ->addColumn('action', function ($query) {
                return "<button type='button' class='btn btn-primary btn-sm text-white edit' data-toggle='modal' data-target='#Modal' data-whatever='@getbootstrap' data-id='$query->zipcodeId' onclick='edit(this);'>
                      <i class='si si-pencil text-white'></i>
                      Edit
                  </button>
                  <button type='button' class='btn btn-danger btn-sm text-white delete' data-id='$query->zipcodeId' onclick='deleted(this);'>
                    <i class='si si-close text-white'></i>
                    Delete
                  </button>";
                  
            })
            ->rawColumns(['id','code','description','action'])
            ->toJson();
    }

   
}
