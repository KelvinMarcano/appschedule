<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
 
class MailController extends Controller
{
 
    public function test(Request $request)
    {
        dd($request->server('HTTP_HOST'));
        try{
            $emails = [
                "kelmarcano@gmail.com"
            ];
            $data['body'] = "asdasdasdasd";
            $data['name'] = "EEIKKKE";
            Mail::send('mails.test', $data, function ($message) use ($emails) {
                $message->from(env('MAIL_USERNAME'), 'Witmakers');
                $message->to($emails)->subject('Email de pruebas.')
                //->bcc('eikerdjforever@gmail.com')
                ->getHeaders()
                ->addTextHeader('Disposition-Notification-To','eikerdjforever@gmail.com')
                ->addTextHeader('Disposition-Notification-To','eikerdjforever@gmail.com');
            });
            dd("enviado");
        }catch (\Exception $e){
            dd($e->getMessage());
        }

    }
 
    public function sendMailAbsence($data, $server, $supervisor = null, $origen)
    {
        try{
            $url = ['www.intergrow.site'];
            $emails = ["pedro.marcano@intergrowgreenhouses.com"];

            if(in_array($server,$url)){
                if (!empty($supervisor->email)) $emails[] = $supervisor->email;
            }

            $empleado = $data->employee();
            if($empleado->idsite == 3){
                $emails[] = 'mark.manacle@intergrowgreenhouses.com';
                $emails[] = 'trejojose853@gmail.com';
                $emails[] = 'admin@intergrowgreenhouses.com';
            }

            if ($origen=='absence'){
                $subject = 'Absence Notice';
            }if ($origen=='violation'){
                $subject = 'Violation Notice';
            }

            $dataMail['origenValidation'] = $origen;
            $dataMail['absence'] = $data;
            $dataMail['employee'] = $empleado;
            $dataMail['supervisor'] = $supervisor;
            Mail::send('mails.absence_test', $dataMail, function ($message) use ($emails, $subject) {
                $message->from('dontrepply@intergrowgreenhouses.com', 'No Repply');
                $message->to($emails)->subject($subject)->bcc('office@intergrowgreenhouses.com');
            });
            return true;
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
    }
}