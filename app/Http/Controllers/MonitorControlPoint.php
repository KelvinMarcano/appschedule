<?php

namespace App\Http\Controllers;

use App\Models\employes;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

class MonitorControlPoint extends Controller
{
    public function index(Request $request)
    {
        $start = Carbon::today()->startOfMonth();
        $end = Carbon::today();
        $data = array();

        return view('monitor.control_puntos', $data);
    }

    public function getControlPoints(Request $request){
        try{
            $query = new employes();

            if(Auth::user()->rol_id != 1 and Auth::user()->site_id != 4 ) $query = $query->where('idsite',Auth::user()->site_id);

            if (!empty($request['search']['value'])) {
                $query = $query->where(function ($query1) use($request) {
                    $query1->Where('firstname', 'like', '%'.$request['search']['value'].'%')
                    ->orWhere('lastname', 'like', '%'.$request['search']['value'].'%');
                });
            }

            $query = $query->orderby('firstname','ASC')->get();

            foreach ($query as $employe){
                $employe['point'] = $employe->controlPoint()->get()->last();
            }

            return DataTables::of($query)
                ->addColumn('employee', function ($query) {
                    return "$query->firstname $query->lastname";
                })
                ->addColumn('points', function ($query) {
                    return $query->point->points;
                })
                ->addColumn('pointsAdi', function ($query) {
                    return $query->point->points_adici;
                })
                ->addColumn('consumed', function ($query) {
                    return $query->point->consumido;
                })
                ->addColumn('vacationDay', function ($query) {
                    return (!empty($query->point->dia_vacaciones)) ? $query->point->dia_vacaciones : 0;
                })
                ->addColumn('vacationDayC', function ($query) {
                    return !empty($query->point->dias_consumido) ? $query->point->dias_consumido : 0;
                })
                ->addColumn('personalTime', function ($query) {
                    return !empty($query->point->horas) ? $query->point->horas : 0;
                })
                ->addColumn('personalTimeC', function ($query) {
                    return !empty($query->point->horas_consumido) ? $query->point->horas_consumido : 0;
                })
                ->addColumn('action', function ($query) {
                    return '<a href="control-point/'.$query->id.'">Details</a>';
                })
                ->rawColumns(['employee','points','pointsAdi','consumed','vacationDay','vacationDayC','personalTime','personalTime','action'])
                ->toJson();
        }catch (\Exception $e){
            dd($e->getLine(),$e->getFile(),$e->getMessage());
        }
    }

    public function detailControl(Request $request)
    {
        $start = Carbon::today()->startOfMonth();
        $end = Carbon::today();
        $data = array();

        return view('monitor.control_puntos', $data);
    }
}
