<?php

namespace App\Http\Controllers;

use App\Models\categories;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;

class CategoriesController extends Controller
{
   const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['categories'] = (new categories())->all();
        $data['categoriess'] = DB::table('categories')
        ->select('categories.id as id','categories.code as code','categories.description as description')
        ->get();
        return view('configuracion.categories', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new categories();
            $model = $model->find($request->id);

            if(empty($model)) $model = new categories();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'categories');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'categories');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'categories');
            Resp::status($request,"error",$e->getMessage(),'save '.'categories');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new categories();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'categories');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'categories',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'categories');
        }
    }
}
