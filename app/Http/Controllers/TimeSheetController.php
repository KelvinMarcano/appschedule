<?php

namespace App\Http\Controllers;

use App\Models\timesheet;
use App\Models\employes;
use App\Models\departament;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;
use Illuminate\Support\Facades\Auth;

class TimeSheetController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';
    const SUCCESS_REJECT = 'Your record has been reject.';
   
    public function index(Request $request)
    {
        $data = array();
        $data['timesheet'] = (new timesheet())->orderBy('id','DESC')->get();
        $empleados = (new employes());
        $data['timesheetss'] = (new timesheet())->get();
    
       
         $data['departaments'] = (new departament())->all();
        // $data['motives'] = (new motive())->all();
        // $data['violations'] = (new violation())->all();
        // $data['status'] = (new status())->all();

        if(Auth::user()->rol_id != 1 and Auth::user()->site_id != 4 ) $empleados = $empleados->where('idsite',Auth::user()->site_id);

        $data['employes'] =$empleados->get();

        return view('registro.timesheet', $data);
    }

    public function save(Request $request)
    {
        try{
            $request['timesheet_date'] = Carbon::createFromFormat('m/d/Y', $request->timesheet_date);

            $model = new timesheet();
            $model = $model->find($request->id);

            if(empty($model)) $model = new timesheet();

            $timesheet = $model->saveData($request->all());

            // $supervisor = (new employes())->find($request->supervisor);

            // $mail = new MailController();
            // $mail->sendMailAbsence($absence,$request->server('HTTP_HOST'),$supervisor);

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'absences');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'absences');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'absences');
            Resp::status($request,"error",$e->getMessage(),'save '.'absences');
            return redirect()->back()->withInput($request->all());
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new timesheet();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete ' . 'absences');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete ' . 'absences', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete ' . 'absences');
        }
    }

}
