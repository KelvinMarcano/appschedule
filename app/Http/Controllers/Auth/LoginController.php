<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\Response as Resp;
use Session;
use JWTFactory;
use JWTAuth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function index()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $validator = $this->validateLogin($request->all());

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach ($errors as $error) {
                $err .= $ctn++ . ')' . $error . '\n';
            }
            Resp::status($request,"warning",$err,"Login");
            return back()->withInput($request->all());
        }

        $user = new User();
        $user = $user->where('email',$request->email)->first();

        if (empty($user)) {
            Resp::status($request,"warning",'Unregistered user.',"Login");
            return redirect()->back()->withInput($request->all());
        }

        if (!$user->state) {
            Resp::status($request,"warning",'User blocked.',"Login");
            return redirect()->back()->withInput($request->all());
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function loginApi(Request $request)
    {
        $validator = $this->validateLogin($request->all());

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err = null;
            $ctn = 1;
            foreach ($errors as $error) {
                $err .= $ctn++ . ')' . $error . '\n';
            }
            return Resp::statusJson($request,"warning",$err,"Login");
        }

        $userModel = new User();
        $user = $userModel->selectraw("users.id as idUser, users.name, users.email, users.password, users.rol_id, users.state, users.site_id, users.rol_id, employes.id AS idEmployes, employes.code, employes.firstname, employes.lastname, employes.sex, employes.birthdate, employes.address, employes.numbersocial, employes.zipcode, employes.numberphone, employes.numberhome, employes.numberfax, employes.sociallink, employes.idsite, employes.areaid, employes.departamentid, employes.scheduleid, employes.positionid, employes.professionid")
           ->where('users.email',$request->email)
           ->leftjoin('employes', 'employes.users_id', '=', 'users.id')
           ->leftjoin('roles', 'roles.id', '=', 'users.rol_id')
           ->leftjoin('role_filter', 'roles.id_filter', '=', 'role_filter.id')
           ->first();

        $filter = $userModel->selectraw("id_filter, role_filter.`all`, role_filter.site, role_filter.area, role_filter.departament")
           ->where('users.email',$request->email)
           ->leftjoin('employes', 'employes.users_id', '=', 'users.id')
           ->leftjoin('roles', 'roles.id', '=', 'users.rol_id')
           ->leftjoin('role_filter', 'roles.id_filter', '=', 'role_filter.id')
           ->first();

        try{
            $user->nameRol = (!empty($user->rol_id)) ? $user->role()->name : '';
            $user->code = (!empty($user->code)) ? $user->code : '';
            $user->firstname = (!empty($user->firstname)) ? $user->firstname : '';
            $user->lastname = (!empty($user->lastname)) ? $user->lastname : '';
            $user->sex = (!empty($user->sex)) ? $user->sexo()->description : '';
            $user->birthdate = (!empty($user->birthdate)) ? $user->birthdate : '';
            $user->address = (!empty($user->address)) ? $user->address : '';
            $user->numbersocial = (!empty($user->numbersocial)) ? $user->numbersocial : '';
            $user->zipcode = (!empty($user->zipcode)) ? $user->zipcode : '';
            $user->numberphone = (!empty($user->numberphone)) ? $user->numberphone : '';
            $user->numberhome = (!empty($user->numberhome)) ? $user->numberhome : '';
            $user->numberfax = (!empty($user->numberfax)) ? $user->numberfax : '';
            $user->sociallink = (!empty($user->sociallink)) ? $user->sociallink : '';
            $user->nameSite = (!empty($user->idsite)) ? $user->site()->name : '';
            $user->descriptionArea = (!empty($user->areaid)) ? $user->area()->description : '';
            $user->descriptionDepartament = (!empty($user->departamentid)) ? $user->departament()->description : '';
            $user->descriptionSchedule = (!empty($user->scheduleid)) ? $user->schedule()->description : '';
            $user->descriptionPosition = (!empty($user->positionid)) ? $user->position()->description : '';
            $user->descriptionProfession = (!empty($user->professionid)) ? $user->profession()->description : '';
        }catch (\Exception $e){
            //throw new \Exception($e->getMessage());
            return Resp::statusJson($request,"warning",'Unregistered user.' . $e->getMessage() ,'login');
        }

        if (empty($user)) {
            return Resp::statusJson($request,"warning",'Unregistered user.','login');
        }

        if (!$user->state) {
            return Resp::statusJson($request,"warning",'User blocked.','login');
        }

        try {
            if (! $token = JWTAuth::attempt($request->only('email', 'password'))) {
                return Resp::statusJson($request,"warning",'Invalid Credentials.','login',$token);
            }
        }catch (JWTException $e) {
            return Resp::statusJson($request,"warning",'Could not create token.','login',$token);
        }

        return Resp::statusJson($request,"success",'User Logged In.','login', ['token' => $token, 'user' => $user, 'filters' => $filter]);
    }

    protected function validateLogin(array $data)
    {
        return Validator::make($data, [
            'email' => 'required',
            'password' => 'required',
        ]);
    }

    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt($this->credentials($request), $request->filled('remember'));
    }

    protected function credentials(Request $request)
    {
        return $request->only('email', 'password');
    }

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        //$this->clearLoginAttempts($request);
        return $this->authenticated($request, $this->guard()->user()) ?: redirect('/');
    }

    protected function authenticated(Request $request, $user)
    {
        //
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        Resp::status($request,"warning",'Wrong password.',"Login");
        return redirect()->back()->withInput($request->all());
    }

    protected function guard()
    {
        return Auth::guard();
    }

    public function logout(Request $request) {
        $this->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return $this->loggedOut($request) ?: redirect('auth');
        //Session::flush();
        //Auth::logout();
        //return Redirect('auth');
    }

    public function logoutApi(Request $request) {
        auth()->logout();
        auth()->logout(true);
        return Resp::statusJson($request,"success",'User Logout.','login');
    }

    protected function loggedOut(Request $request)
    {
        //
    }

    public function me()
    {
        return response()->json(auth()->user());
    }
}
