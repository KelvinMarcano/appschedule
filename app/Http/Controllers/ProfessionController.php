<?php

namespace App\Http\Controllers;

use App\Models\profession;
use Illuminate\Http\Request;
use App\Models\Response as Resp;


class ProfessionController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['professions'] = (new profession())->all();
        return view('configuracion.profession', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new profession();
            $model = $model->find($request->id);

            if(empty($model)) $model = new profession();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'profession');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'profession');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'profession');
            Resp::status($request,"error",$e->getMessage(),'save '.'profession');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new profession();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'profession');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'profession',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'profession');
        }
    }

}