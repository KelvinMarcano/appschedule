<?php

namespace App\Http\Controllers\ImportExcel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Imports\ImportEmployes;
use App\Exports\ExportEmployes;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use App\employes;

class ImportExcelController extends Controller
{
    //
     public function index()
    {   

        $employes = employes::orderBy('created_at','DESC')->get();

         $employes2 = DB::table('employes')
        ->select('employes.id as id','employes.code as code','employes.firstname as firstname','employes.lastname as lastname','employes.sex as sex','employes.birthdate as birthdate','employes.address as address','employes.numbersocial as numbersocial','employes.zipcode as zipcode','employes.numberphone as numberphone','employes.numberhome as numberhome','employes.numberfax as numberfax','employes.sociallink as sociallink','departament.id as departamentid','schedule.id as scheduleid','position.id as positionid','profession.id as professionid','employes.status as status','employes.active','employes.active','employes.city','employes.state','employes.county','employes.country')
        ->leftJoin('departament', 'departament.id', '=', 'employes.departamentid')
        ->leftJoin('position', 'position.id', '=', 'employes.positionid')
        ->leftJoin('profession', 'profession.id', '=', 'employes.professionid')
        ->leftJoin('schedule', 'schedule.id', '=', 'employes.scheduleid')
        ->get();

        //dd($employes2);
        return view('import_excel.index', compact('employes','employes2'));
    }

    public function import(Request $request)
    {
        $request->validate([
            'import_file' => 'required'
        ]);
        Excel::import(new ImportEmployes, request()->file('import_file'));
        return back()->with('success', 'Employes imported successfully.');
    }

}
