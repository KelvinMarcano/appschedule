<?php

namespace App\Http\Controllers;

use App\Models\activities;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;

class ActivitiesController extends Controller
{
   const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['activities'] = (new activities())->all();
         $data['activitiess'] = DB::table('activities')
        ->select('activities.id as id','activities.code as code','activities.description as description')
        ->get();
        return view('configuracion.activities', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new activities();
            $model = $model->find($request->id);

            if(empty($model)) $model = new activities();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'activities');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'activities');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'activities');
            Resp::status($request,"error",$e->getMessage(),'save '.'activities');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new activities();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'activities');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'activities',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'activities');
        }
    }
}
