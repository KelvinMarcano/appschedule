<?php

namespace App\Http\Controllers;

use App\Models\tasks;
use App\Models\categories;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;

class TasksController extends Controller
{
   const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['tasks'] = (new tasks())->all();
        $data['taskss'] = DB::table('tasks')
        ->select('tasks.id as id','tasks.code as code','tasks.description as description')
        ->get();
        $data['categories'] = (new categories())->select('categories.id as id','categories.description as description')
        ->orderby('description','DESC')
        ->get();
        return view('configuracion.tasks', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new tasks();
            $model = $model->find($request->id);

            if(empty($model)) $model = new tasks();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'tasks');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'tasks');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'tasks');
            Resp::status($request,"error",$e->getMessage(),'save '.'tasks');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new tasks();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'tasks');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'tasks',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'tasks');
        }
    }
}
