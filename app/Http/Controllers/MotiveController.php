<?php

namespace App\Http\Controllers;

use App\Models\violation;
use App\Models\motive;
use App\Models\categories;
use Illuminate\Http\Request;
use App\Models\Response as Resp;

class MotiveController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['motives'] = (new motive())->where('id','!=',4)->get();
        $data['violations'] = (new violation())->orderby('code','ASC')->get();
        $data['categories'] = (new categories())->select('categories.id as id','categories.description as description')
        ->orderby('description','DESC')
        ->get();
        return view('configuracion.motive', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new motive();
            $model = $model->find($request->id);

            if(empty($model)) $model = new motive();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'motive');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'motive');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'motive');
            Resp::status($request,"error",$e->getMessage(),'save '.'motive');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new motive();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'motive');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'motive',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'motive');
        }
    }

}