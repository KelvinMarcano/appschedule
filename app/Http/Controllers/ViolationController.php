<?php

namespace App\Http\Controllers;

use App\Models\violation;
use App\Models\violation_types;
use App\Models\unitofmeasure;
use Illuminate\Http\Request;
use App\Models\Response as Resp;

class ViolationController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['violations'] = (new violation())->all();
        $data['violationType'] = (new violation_types())->all();
        $data['unitofmeasure'] = (new unitofmeasure())->all();
        return view('configuracion.violation', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new violation();
            $model = $model->find($request->id);

            if(empty($model)) $model = new violation();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'violation');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'violation');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'violation');
            Resp::status($request,"error",$e->getMessage(),'save '.'violation');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new violation();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'violation');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'violation',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'violation');
        }
    }
}

