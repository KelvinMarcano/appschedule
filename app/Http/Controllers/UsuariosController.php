<?php

namespace App\Http\Controllers;

use App\Models\employes;
use App\Models\role_filter;
use App\Models\roles;
use App\Models\site;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use Yajra\Datatables\Datatables;

class UsuariosController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function roles()
    {
        $data = array();
        $data['roles'] = (new roles())->all();
        $data['niveles'] = (new role_filter())->all();
        return view('configuracion.roles', $data);
    }

    public function users()
    {
        $data = array();
        $data['users'] = (new User())->all();
        $data['roles'] = (new roles())->where('rol_active',1)->get();
        $data['sites'] = (new site())->all();
        $data['employes'] = employes::orderby('code','ASC')->get();
        return view('configuracion.users', $data);
    }

    public function save(Request $request)
    {
        try{
            $className = 'App\Models\\'.$request->table;
            $model = new $className();
            $model = $model->find($request->id);

            if(empty($model)) $model = new $className();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.$request->table);

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.$request->table);
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.$request->table);
            Resp::status($request,"error",$e->getMessage(),'save '.$request->table);
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $className = 'App\Models\\'.$request->table;
            $model = new $className();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.$request->table);

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.$request->table,$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.$request->table);
        }
    }

    public function userlist(Request $request){
        $query = new User();

        $query = $query->getUserList($request)->get();

        return DataTables::of($query)
            ->addColumn('name', function ($query) {
                return $query->userName;
            })
            ->addColumn('email', function ($query) {
                return $query->userEmail;
            })
            ->addColumn('role', function ($query) {
                return $query->roleName;
            })
            ->addColumn('state', function ($query) {
                return ($query->userState) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>';
            })
            ->addColumn('site', function ($query) {
                return $query->siteName;
            })
            ->addColumn('action', function ($query) {
                return "<button type='button' class='btn btn-primary btn-sm text-white' data-toggle='modal' data-target='#roleModal' data-whatever='@getbootstrap' data-id='$query->userId' style='width: 100%' onclick='edit(this);'>
                  <i class='si si-pencil text-whitesi si-pencil text-white'></i>Edit
                    </button>
                  <button type='button' class='btn btn-danger btn-sm text-white' data-id='$query->userId' style='width: 100%' onclick='deleted(this);'>
                    <i class='si si-close text-white'></i>
                    Delete
                  </button>";
            })
            ->rawColumns(['name','email','role','state','site','action'])
            ->toJson();
    }
}
