<?php

namespace App\Http\Controllers;

use App\Models\area;
use App\Models\site;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;


class AreaController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['area'] = (new area())->orderby('code','ASC')->get();
        $data['idsites'] = (new site())->all();

        return view('configuracion.area', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new area();
            $model = $model->find($request->id);

            if(empty($model)) $model = new area();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'area');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'area');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'area');
            Resp::status($request,"error",$e->getMessage(),'save '.'area');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new area();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete ' . 'area');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete ' . 'area', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete ' . 'area');
        }
    }
}
