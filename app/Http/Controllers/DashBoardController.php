<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Response as Resp;
use App\Models\absences;
use App\Models\employes;
use App\Models\motive;
use App\Models\status;
use App\Models\violation;
use App\Models\control_point;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class DashBoardController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index(Request $request)
    {
        $data = array();
        $absenceModel = new absences();
        $fechaInit = $absenceModel->getAbsenceTable($request)
            ->where('violation.type_violation_id', 1)->groupBy('fecha_init')->orderBy("fecha_init",'DESC')->first();

        $absences = $absenceModel->getAbsenceTable($request)
            ->where('violation.type_violation_id', 1);

        if(!empty($fechaInit)) $absences = $absences->where('fecha_init', $fechaInit->fecha_init);

        $data['fechaInit'] = (!empty($fechaInit)) ? $fechaInit->fecha_init : '';
        $data['absences'] = $absences->get();
        $data['motives'] = (new motive())->orderby('code','ASC')->get();
        $data['violations'] = (new violation())->orderby('code','ASC')->get();
        $data['status'] = (new status())->orderby('description')->get();
        $empleados = (new employes());
        $data['dates'] = $absenceModel->select('fecha_init')->groupBy('fecha_init')->orderBy("fecha_init",'ASC')->get();

        $filter = Session::get('filter');

        if(!$filter->all){
            if($filter->site){
                $empleados = $empleados->where('employes.idsite', '=', $filter->siteId);
            }
            if($filter->area){
                 $empleados = $empleados->where('employes.areaid',$filter->areaId);
            }
            if($filter->departament){
                 $empleados = $empleados->where('employes.departamentid',$filter->departId);
            }
        }

        $data['employes'] =$empleados->orderby('firstname','ASC')->get();

        if(!$request->ajax()) return view('dashboard', $data);
        return \Response::json(\View::make('dashboard-child', $data)->render());
    }

}