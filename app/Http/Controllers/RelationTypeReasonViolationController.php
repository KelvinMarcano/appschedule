<?php

namespace App\Http\Controllers;

use App\Models\motive;
use App\Models\violation;
use App\Models\categories;
use App\Models\relation_type_reason_violation;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;

class RelationTypeReasonViolationController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['motives'] = (new motive())->whereIn('categories_id',[2,3])->orderby('code','ASC')->get();
       // $data['motives_catgen'] = (new motive())->all();
       // $data['violations'] = (new motive())->all();
        $data['motives_catgen'] = (new motive())->whereIn('categories_id',[1])->orderby('code','ASC')->get();
        $data['violations'] = (new violation())->whereIn('id',[1,2,3,8,11,12,16,17,18,19,20,22,23])->orderby('code','ASC')->get();
        $data['relation'] = (new relation_type_reason_violation())->all();
         //$data['categoriess'] = DB::table('categories')
        // ->select('categories.id as id','categories.code as code','categories.description as description')
        // ->get();
        return view('configuracion.relationtypereasonviolation', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new relation_type_reason_violation();
            $model = $model->find($request->id);

            if(empty($model)) $model = new relation_type_reason_violation();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'categories');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'categories');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'categories');
            Resp::status($request,"error",$e->getMessage(),'save '.'categories');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new relation_type_reason_violation();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'categories');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'categories',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'categories');
        }
    }
}
