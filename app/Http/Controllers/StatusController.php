<?php

namespace App\Http\Controllers;

use App\Models\status;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;

class StatusController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['estatus'] = (new status())->all();

         $data['status1'] = DB::table('status')
        ->select('status.id as id','status.code as code','status.description as description')
        ->get();
       // dd($data);
        return view('configuracion.status', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new status();
            $model = $model->find($request->id);

            if(empty($model)) $model = new status();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'status');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'status');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'status');
            Resp::status($request,"error",$e->getMessage(),'save '.'status');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new status();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'status');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'status',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'status');
        }
    }

}