<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\device;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Response as Resp;
use DB;
use Illuminate\Support\Facades\Auth;

class DeviceController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const SUCCESS_UPDATE = 'Record update successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';
    const NO_SAVE_DEVICE = 'Ya esta registrado el telefono';
   
    public function index(Request $request)
    {
        $data = array();
        $data['device'] = (new device())->orderBy('id','DESC')->get();

        if(Auth::user()->rol_id != 1 and Auth::user()->site_id != 4 ) $empleados = $empleados->where('idsite',Auth::user()->site_id);

        $data['employes'] =$empleados->get();

        return Resp::statusJson($request,"success",self::SUCCESS_MSG,'get devices',$data);
    }

    public function save(Request $request)
    {
        try{
/*
            $model = new device();
            $model = $model->find($request->id);

            if(empty($model)) $model = new device();

            $devices = $model->saveData($request->all());

            //return Resp::statusJson($request, "success", self::SUCCESS_MSG, 'save device');
            $result = Resp::statusJson($request, "success", self::SUCCESS_MSG, 'save device ', ['idDevice' => $devices->id]);
            //return Resp::statusJson($request, "success", self::SUCCESS_MSG, 'save device', ['idDevice' => $devices->id]);
*/

            $device = new device();
            //$device = $device->find($request->id);

            if(empty($device)) $device = new device();

            //$device = $device->select('id','id_user','id_employe','token_device','token_device','device_manufacturer','device_model','  device_brand','device_id','version_android','nombre_android','num_sdk_android','android_id')
            $device = $device->where('android_id', $request->android_id)
                             ->where('aplication', $request->aplication)
                             ->get();

            if(count($device)==0){
                $device = $device->find($request->id);

                if(empty($device)) $device = new device();

                $devices = $device->saveData($request->all());

                $result = Resp::statusJson($request, "success", self::SUCCESS_MSG, 'save device', ['idDevice' => $devices->id]);
            }else{
                foreach ($device as $catego){
                    $motiveId[] = $catego->id;
                }

                $idDevice = array_unique($motiveId);

                $device = $device->find($idDevice[0]);
                if(empty($device)) $device = new device();
                $devices = $device->saveData($request->all());

                $result = Resp::statusJson($request, "success", self::SUCCESS_MSG, 'save device', ['idDevice' => $devices->id]);
            }

            //$devices = $device->saveData($request->all());

            //return Resp::statusJson($request, "success", self::SUCCESS_MSG, 'save device', ['idDevice' => $devices->id]);
            return $result;
        }catch(\Exception $e){
            return Resp::statusJson($request, "error",$e->getMessage(), 'save device');
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new device();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete device');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete device', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete device');
        }
    }

    public function update(Request $request)
    {
        try{
            $validator = $this->ValidatorUpdate($request->all());

            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                $err = null;
                $ctn = 1;
                foreach ($errors as $error) {
                    $err .= $ctn++ . ')' . $error . '\n';
                }
                return Resp::statusJson($request,"warning",$err,"update device");
            }

            $device = new device();
            $device = $device->find($request->deviceid);

            $device = $device->updateStatus($request->all());

            return Resp::statusJson($request,"success",self::SUCCESS_UPDATE,'update absendeviceces');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'update device');
        }
    }
}
