<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\roles;
use App\Models\site;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Response as Resp;

class UsuariosController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function roles(Request $request)
    {
        $data = array();
        $data['roles'] = (new roles())->all();
        return Resp::statusJson($request,"success",self::SUCCESS_MSG,'get '.'roles',$data);
    }

    public function users(Request $request)
    {
        $data = array();
        $data['users'] = (new User())->all();
        $data['roles'] = (new roles())->where('rol_active',1)->get();
        $data['sites'] = (new site())->all();
        return Resp::statusJson($request,"success",self::SUCCESS_MSG,'get '.'users',$data);
    }

    public function save(Request $request)
    {
        try{
            $className = 'App\Models\\'.$request->table;
            $model = new $className();
            $model = $model->find($request->id);

            if(empty($model)) $model = new $className();

            $model->saveData($request->all());

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.$request->table);
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'save '.$request->table);
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $className = 'App\Models\\'.$request->table;
            $model = new $className();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.$request->table);

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.$request->table,$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.$request->table);
        }
    }
}
