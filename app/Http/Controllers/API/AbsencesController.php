<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\absences;
use App\Models\departament;
use App\Models\employes;
use App\Models\motive;
use App\Models\status;
use App\Models\supervisor_employee;
use App\Models\violation;
use App\Models\device;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Response as Resp;
use App\Services\SendPush as Push;
use App\Http\Controllers\MailController;
use DB;
use Illuminate\Support\Facades\Auth;

class AbsencesController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const SUCCESS_UPDATE = 'Record update successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';
   
    public function index(Request $request)
    {
        $data = array();
        $data['absences'] = (new absences())->orderBy('id','DESC')->get();
        $empleados = (new employes());
        $data['departaments'] = (new departament())->all();
        $data['motives'] = (new motive())->all();
        $data['violations'] = (new violation())->all();
        $data['status'] = (new status())->all();

        if(Auth::user()->rol_id != 1 and Auth::user()->site_id != 4 ) $empleados = $empleados->where('idsite',Auth::user()->site_id);

        $data['employes'] =$empleados->get();

        return Resp::statusJson($request,"success",self::SUCCESS_MSG,'get absences',$data);
    }

    public function save(Request $request)
    {
        try{
            //$request['fecha_init'] = Carbon::createFromFormat('Y-m-d', $request->fecha_init);
            //$request['fecha_end'] = Carbon::createFromFormat('Y-m-d', $request->fecha_end);

            $model = new absences();
            $model = $model->find($request->id);

            if(empty($model)) $model = new absences();

            $absences = new absences();
            $absences = $absences->where('employesid', $request->employesid)
                                 ->where('fecha_init', $request->fecha_init)
                                 ->where('motiveid2', $request->motiveid2)
                                 ->count();

            if (!$absences){
                $absence = $model->saveData($request->all());

                $supervisor = (new employes())->find($request->supervisor);

                $mail = new MailController();
                $mail->sendMailAbsence($absence, $request->server('HTTP_HOST'), $supervisor, 'absence');

                if($request->platform == 'Android (App Supervisor)'){
                    $device = new device();

                    $device = $device->where('id_employe', $request->employesid)
                                     ->where('aplication', 'AppSupervisor')
                                     ->first();

                    $token = $device->token_device;
                }else{
                    $token = '';
                }

                $platform  = $request->platform;

                Push::sendFCM($token, "save_absences", $absence, $supervisor, "Intergrow - Save absences", "", $platform);

                return Resp::statusJson($request, "success", self::SUCCESS_MSG, 'save absences');
            }else{
                $result = Resp::statusJson($request, "success", 'Previously created record', 'save absences');
                return $result;
            }
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'save absences');
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new absences();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete absences');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete absences', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete absences');
        }
    }

    public function update(Request $request)
    {
        try{
            $validator = $this->ValidatorUpdate($request->all());

            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                $err = null;
                $ctn = 1;
                foreach ($errors as $error) {
                    $err .= $ctn++ . ')' . $error . '\n';
                }
                return Resp::statusJson($request,"warning",$err,"List Absences");
            }

            $absences = new absences();
            $absences = $absences->find($request->absencesid);

            //if(empty($absences)) $absences = new absences();   //Para determinar si es sera un Insert

            $absence = $absences->updateStatus($request->all());

            return Resp::statusJson($request,"success",self::SUCCESS_UPDATE,'update absences');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'update absences');
        }
    }

    public function cargarAusencia(Request $request)
    {
        //$validator = $this->validateAbsences($request->all());

        //if ($validator->fails()) {
        //    $errors = $validator->errors()->all();
        //    $err = null;
        //    $ctn = 1;
        //    foreach ($errors as $error) {
        //        $err .= $ctn++ . ')' . $error . '\n';
        //    }
        //    return Resp::statusJson($request,"warning",$err,"List Absences");
        //}

        $absences = new absences();

        $absences = $absences->selectraw("absences.id, employesid, CONCAT(employes.firstname, ' ', employes.lastname) AS nameEmploye, motiveid, motive.description AS descriptionMotive, absences.violation_id, violation.description AS descriptionViolation, fecha_init, fecha_end, hora_init, hora_end, absences.status_id, status.description AS descriptionStatus, absences.comment, supervisor, type_absences")
            //->Join('employes', function($join) use ($request) {
            //    $join->on('absences.employesid', '=', 'employes.id')
            //         ->where('employes.idsite', '=', $request->idsite)
            //         ->where('employes.departamentid', '=', $request->departamentid);
            //})
            ->join('motive', 'motive.id', '=', 'absences.motiveid')
            ->join('violation', 'violation.id', '=', 'absences.violation_id')
            ->join('status', 'status.id', '=', 'absences.status_id')
            ->where('absences.status_id', '=', '5')
            ->orderBy('absences.created_at', 'DESC');
            //->get();
            //->limit($request->limitDesde, ',', $request->limitHasta)

        if(empty($request->idsite) and empty($request->areaid) and empty($request->departamentid)){
            //->join('employes', 'absences.employesid', '=', 'employes.id');
            $absences = $absences->Join('employes', function($join) use ($request) {
                $join->on('absences.employesid', '=', 'employes.id');
            });
        }else if(!empty($request->idsite) and empty($request->areaid) and empty($request->departamentid)){
            $absences = $absences->Join('employes', function($join) use ($request) {
                $join->on('absences.employesid', '=', 'employes.id')
                     ->where('employes.idsite', '=', $request->idsite);
            });
        }else if(!empty($request->idsite) and !empty($request->areaid) and empty($request->departamentid)){
            $absences = $absences->Join('employes', function($join) use ($request) {
                $join->on('absences.employesid', '=', 'employes.id')
                     ->where('employes.idsite', '=', $request->idsite)
                     ->where('employes.areaid', '=', $request->areaid);
            });
        }else if(!empty($request->idsite) and !empty($request->areaid) and !empty($request->departamentid)){
            $absences = $absences->Join('employes', function($join) use ($request) {
                $join->on('absences.employesid', '=', 'employes.id')
                     ->where('employes.idsite', '=', $request->idsite)
                     ->where('employes.areaid', '=', $request->areaid)
                     ->where('employes.departamentid', '=', $request->departamentid);
            });
        }


        if(!empty($request->fechaActual)) $absences = $absences->where('fecha_init', $request->fechaActual);
        if(!empty($request->idEmpleado)) $absences = $absences->where('employesid', $request->idEmpleado);

        $absences = $absences->paginate(10);

        foreach($absences as $absence){
            $absence->hora_init = (!empty($absences->hora_init)) ? $absences->hora_init : '';
            $absence->hora_end = (!empty($absences->hora_end)) ? $absences->hora_end : '';
            //$absence->comment = (!empty($absences->comment)) ? $absences->comment : '';
            $absence->supervisor = (!empty($absences->supervisor)) ? $absences->supervisor : '';
        }

        return Resp::statusJson($request, "success", 'List Absences Supervisor.','List Absences', ['listAbsences' => $absences]);
    }

    public function countEmployesAbsent(Request $request)
    {
        $absences = new absences();

        if(empty($request->idsite) and empty($request->areaid) and empty($request->departamentid)){
            $absences = $absences->join('employes', 'absences.employesid', '=', 'employes.id')
                                 ->whereBetween('fecha_init', [$request->fechaDesde, $request->fechaHasta])
                                 ->whereBetween('fecha_end', [$request->fechaDesde, $request->fechaHasta])
                                 ->orderBy('fecha_init', 'ASC')
                                 ->count();
        }else if(!empty($request->idsite) and empty($request->areaid) and empty($request->departamentid)){
            $absences = $absences->join('employes', 'absences.employesid', '=', 'employes.id')
                                 ->where('employes.idsite', '=', $request->idsite)
                                 ->whereBetween('fecha_init', [$request->fechaDesde, $request->fechaHasta])
                                 ->whereBetween('fecha_end', [$request->fechaDesde, $request->fechaHasta])
                                 ->orderBy('fecha_init', 'ASC')
                                 ->count();
        }else if(!empty($request->idsite) and !empty($request->areaid) and empty($request->departamentid)){
            $absences = $absences->join('employes', 'absences.employesid', '=', 'employes.id')
                                 ->where('employes.idsite', '=', $request->idsite)
                                 ->where('employes.areaid', '=', $request->areaid)
                                 ->whereBetween('fecha_init', [$request->fechaDesde, $request->fechaHasta])
                                 ->whereBetween('fecha_end', [$request->fechaDesde, $request->fechaHasta])
                                 ->orderBy('fecha_init', 'ASC')
                                 ->count();
        }else if(!empty($request->idsite) and !empty($request->areaid) and !empty($request->departamentid)){
            $absences = $absences->join('employes', 'absences.employesid', '=', 'employes.id')
                                 ->where('employes.idsite', '=', $request->idsite)
                                 ->where('employes.areaid', '=', $request->areaid)
                                 ->where('employes.departamentid', '=', $request->departamentid)
                                 ->whereBetween('fecha_init', [$request->fechaDesde, $request->fechaHasta])
                                 ->whereBetween('fecha_end', [$request->fechaDesde, $request->fechaHasta])
                                 ->orderBy('fecha_init', 'ASC')
                                 ->count();
        }

        return Resp::statusJson($request, "success", 'count Employes Absent.','count Employes Absent', ['countAbsents' => $absences]);
    }

    protected function validateAbsences(array $data)
    {
        return Validator::make($data, [
                               'idsite' => 'required',
                               //'departamentid' => 'required',
        ]);
    }

    protected function ValidatorUpdate(array $data)
    {
        return Validator::make($data, [
                               'absencesid' => 'required',
                               'status_id' => 'required',
        ]);
    }

    public function uploadNote (Request $request){
        $file = $request->file64;
        $extFile = $request->ext;
        $nameFile = $request->name_file;

        if ($extFile=='png' || $extFile=='PNG' || $extFile=='jpg' || $extFile=='JPG' || $extFile=='jpge' ||
            $extFile=='JPGE' || $extFile=='bmp' || $extFile=='BMP' || $extFile=='git' || $extFile=='GIT'){
            $path = public_path().'/notas/images/';
            //$path2 = 'images/';

            if (!is_dir($path)) {
                File::makeDirectory($path,0777,true);
            }
        }

        if ($extFile=='pdf' || $extFile=='PDF' || $extFile=='doc' || $extFile=='DOC'){
            $path = public_path().'/notas/files/';
            //$path2 = 'images/';

            if (!is_dir($path)) {
                File::makeDirectory($path,0777,true);
            }
        }

        $encodeFile = base64_decode($file);
        $success = file_put_contents($path . $nameFile . '.' . $extFile, $encodeFile);

        return Resp::statusJson($request, "success", 'File uploaded successfully.','File uploaded successfully');
    }
}
