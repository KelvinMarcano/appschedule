<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\violation;
use App\Models\violation_types;
use Illuminate\Http\Request;
use App\Models\Response as Resp;

class ViolationController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index(Request $request)
    {
        $data = array();
        $data['violations'] = (new violation())->all();
        $data['violationType'] = (new violation_types())->all();
        return Resp::statusJson($request,"success",self::SUCCESS_MSG,'get violations',$data);
    }

    public function save(Request $request)
    {
        try{
            $model = new violation();
            $model = $model->find($request->id);

            if(empty($model)) $model = new violation();

            $model->saveData($request->all());

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'violation');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'save '.'violation');
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new violation();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'violation');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'violation',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'violation');
        }
    }

    public function violationAbsences(Request $request)
    {
        $violation = new violation();
        $violation = $violation->select('id', 'code', 'description', 'points', 'type_violation_id')
        					   ->where('type_violation_id', '1')
                               ->orderby('description', 'ASC')
                               ->get(); 

        return Resp::statusJson($request, "success", 'List violations absences.','violations absences', ['datosViolacionesAusencias' => $violation]);
    }

    public function violation(Request $request)
    {
        $violation = new violation();
        $violation = $violation->select('id', 'code', 'description', 'points', 'type_violation_id')
        					   ->where('type_violation_id', '2')
                               ->orderby('description', 'ASC')
                               ->get(); 

        return Resp::statusJson($request, "success", 'List violations.','violations', ['datosViolaciones' => $violation]);
    }
}

