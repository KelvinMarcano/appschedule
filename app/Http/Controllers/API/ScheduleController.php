<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\schedule;
use Illuminate\Http\Request;
use App\Models\Response as Resp;

class ScheduleController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index(Request $request)
    {
        $data = array();
        $data['schedules'] = (new schedule())->all();
        return Resp::statusJson($request,"success",self::SUCCESS_MSG,'get '.'schedules',$data);
    }

    public function save(Request $request)
    {
        try{
            $model = new schedule();
            $model = $model->find($request->id);

            if(empty($model)) $model = new schedule();

            $model->saveData($request->all());

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'schedule');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'save '.'schedule');
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new schedule();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'schedule');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'schedule',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'schedule');
        }
    }
}
