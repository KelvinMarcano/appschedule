<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\departament;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;


class DepartamentController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index(Request $request)
    {
        $data = array();
        $data['departaments'] = (new departament())->all();

        $data['supervisor'] = DB::table('employes')
        ->select('employes.id as id','employes.code as code','employes.lastname as lastname', 'employes.firstname as firstname')
        ->join('roles', 'roles.id', '=', 'employes.rolesid' )
        ->where('roles.name', '=', 'Supervisor')
        ->orderBy('employes.code')
        ->get();
        
         $data['departament'] = DB::table('departament')
        ->select('departament.id as id','departament.code as code','departament.description as description', 'employes.lastname as lastname', 'employes.firstname as firstname')
        ->leftjoin('employes', 'employes.id', '=', 'departament.employesid' )
        ->orderBy('employes.code')
        ->get();

        return Resp::statusJson($request,"success",'success','get departaments',$data);
    }

    public function save(Request $request)
    {
        try{
            $model = new departament();
            $model = $model->find($request->id);

            if(empty($model)) $model = new departament();

            $model->saveData($request->all());

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'departament');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'save '.'departament');
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new departament();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete ' . 'departament');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete ' . 'departament', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete ' . 'departament');
        }
    }
}
