<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\profession;
use Illuminate\Http\Request;
use App\Models\Response as Resp;


class ProfessionController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index(Request $request)
    {
        $data = array();
        $data['professions'] = (new profession())->all();
        return Resp::statusJson($request,"success",self::SUCCESS_MSG,'get '.'professions',$data);
    }

    public function save(Request $request)
    {
        try{
            $model = new profession();
            $model = $model->find($request->id);

            if(empty($model)) $model = new profession();

            $model->saveData($request->all());

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'profession');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'save '.'profession');
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new profession();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'profession');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'profession',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'profession');
        }
    }

}