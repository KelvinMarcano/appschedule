<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\status;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;

class StatusController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index(Request $request)
    {
        $data = array();
        $data['estatus'] = (new status())->all();

        $data['status1'] = DB::table('status')
        ->select('status.id as id','status.code as code','status.description as description')
        ->get();
        return Resp::statusJson($request,"success",self::SUCCESS_MSG,'get '.'estatus',$data);
    }

    public function save(Request $request)
    {
        try{
            $model = new status();
            $model = $model->find($request->id);

            if(empty($model)) $model = new status();

            $model->saveData($request->all());

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'status');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'save '.'status');
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new status();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'status');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'status',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'status');
        }
    }

    public function status(Request $request)
    {
        $status = new status();
        $status = $status->select('id', 'code', 'description')
                               ->orderby('id', 'ASC')
                               ->get(); 

        return Resp::statusJson($request, "success", 'List status.','status', ['datosStatus' => $status]);
    }

    public function statusApproved(Request $request)
    {
        $status = new status();
        $status = $status->select('id', 'code', 'description')
                         ->where('category_id', '1')
                         ->orderby('id', 'ASC')
                         ->get(); 

        return Resp::statusJson($request, "success", 'List status.','status', ['datosStatusApproved' => $status]);
    }
}