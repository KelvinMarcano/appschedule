<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\motive;
use App\Models\motive_category;
use Illuminate\Http\Request;
use App\Models\Response as Resp;

class MotiveController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index(Request $request)
    {
        $data = array();
        $data['motives'] = (new motive())->where('id','!=',4)->get();
        return Resp::statusJson($request,"success",self::SUCCESS_MSG,'get '.'motives',$data);
    }

    public function save(Request $request)
    {
        try{
            $model = new motive();
            $model = $model->find($request->id);

            if(empty($model)) $model = new motive();

            $model->saveData($request->all());

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'motive');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'save '.'motive');
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new motive();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'motive');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'motive',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'motive');
        }
    }

    public function motivos(Request $request)
    {
        $motive = new motive();
        $motive = $motive->select('id', 'code', 'description')
                         ->orderby('id', 'ASC')
                         ->get();

        return Resp::statusJson($request, "success", 'List motive.','motive', ['datoMotivosAusencia' => $motive]);
    }

    public function typeAbsencesGeneral(Request $request)
    {
        $motive = new motive();
        $motive = $motive->select('id', 'code', 'description')
                         ->where('categories_id', '=', '1')
                         ->orderby('description', 'ASC')
                         ->get();

        return Resp::statusJson($request, "success", 'List type absences.','type absences', ['datoTypeAbsences' => $motive]);
    }

    public function typeAbsencesSpecific(Request $request)
    {
        $motive = new motive();
        $motive = $motive->select('id', 'code', 'description')
                         ->where('categories_id', '=', '2')
                         ->orderby('description', 'ASC')
                         ->get();

        return Resp::statusJson($request, "success", 'List razon.','razon', ['datoRazon' => $motive]);
    }

    public function getMotives(Request $request){
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"],0,5))=='https'?'https':'http';
        $data = [];
        $motiveId = [];
        $category = (new motive_category())->all();

        foreach ($category as $cat){
            $cat['fullUrl'] = $protocol.'://'.$_SERVER["HTTP_HOST"].'/'.$cat->icon;
        }

        foreach ($category as $catego){
            $motiveId[] = $catego->motive_id;
        }
        $motives = (new motive())->whereIn('id',array_unique($motiveId))->get();

        foreach($motives as $motive){
            $motive['fullUrl'] = $protocol.'://'.$_SERVER["HTTP_HOST"].'/'.$motive->icon;
        }

        $data['motives'] = $motives;
        $data['category'] = $category;
        return Resp::statusJson($request,"success",'Success','motives',$data);
    }
}