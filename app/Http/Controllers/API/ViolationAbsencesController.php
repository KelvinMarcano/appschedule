<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\violation_absences;
use App\Models\absences;
use App\Models\departament;
use App\Models\employes;
use App\Models\motive;
use App\Models\status;
use App\Models\supervisor_employee;
use App\Models\violation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Response as Resp;
use App\Services\SendPush as Push;
use App\Http\Controllers\MailController;
use DB;
use Illuminate\Support\Facades\Auth;

class ViolationAbsencesController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';
    const SUCCESS_REJECT = 'Your record has been reject.';
   
     public function index(Request $request)
    {
        if(Auth::user()->rol_id == 5) return redirect('dashboard');
        $data = array();
        $data['absences'] = (new violation_absences())->getAbsenceTable($request)->get();

        $empleados = (new employes());
        $data['departaments'] = (new departament())->all();
        $data['motives_nocall'] = (new motive())->all();
        $data['violation_nocall'] = (new violation())->all();
        $data['violations'] = (new violation())->whereIn('type_violation_id',[2])->orderby('code','ASC')->get();
        $data['absences2'] = (new absences())->Where('type_absences','=','2');

        $data['status'] = (new status())->orderby('description')->get();
        $data['motives'] = (new motive())->Where('code','=','NAP')->orderby('code','ASC')->get();
        //dd($data);

        if(Auth::user()->rol_id != 1 and Auth::user()->site_id != 4 ) $empleados = $empleados->where('idsite',Auth::user()->site_id);

        $data['employes'] = $empleados->orderby('firstname','ASC')->get();
       
        return view('registro.violation_absences', $data);
    }

    public function save(Request $request)
    {
        try{
            $request['fecha_init'] = Carbon::createFromFormat('Y-m-d', $request->fecha_init);

            $model = new violation_absences();
            $model = $model->find($request->id);

            if(empty($model)) $model = new violation_absences();

            $absence = $model->saveData($request->all());

            $supervisor = (new employes())->find($request->supervisor);

            $mail = new MailController();
            $mail->sendMailAbsence($absence, $request->server('HTTP_HOST'), $supervisor, "violation");

            $result = Resp::statusJson($request, "success", self::SUCCESS_MSG, 'save violation');
            Push::sendFCM("", "save_violation", $absence, $supervisor, "Intergrow - Save violation", "");

            return $result;
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'save violation');
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new violation_absences();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete ' . 'absences');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete ' . 'absences', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete ' . 'absences');
        }
    }

    public function reject(Request $request,$id)
    {
        try{
            $model = new violation_absences();
            $model = $model->find($request->id);

            if(empty($model)) return Resp::statusJson($request,"error",ERROR_MSG,'reject absences');

            $model->reject();

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'reject absences');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'reject absences');
        }
    }

    public function aprove(Request $request, $id)
    {
        try{
            $model = (new violation_absences())->find($id);

            if(empty($model)) return Resp::statusJson($request,"error",ERROR_MSG,'reject absences');

            $model->aprove($request);

            $request['absence_id'] = $model->id;

            $controlPoint = (new control_point())->where('employe_id',$model->employesid)
                ->get()->last();

            if(!empty($controlPoint)) $controlPoint->savePoint($request);

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'reject absences');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'reject absences');
        }
    }

    public function getAbsence(Request $request){
        try{
            $query = new violation_absences();
            $query = $query->getAbsenceTable($request)->get();
           
            return DataTables::of($query)
                ->addColumn('employee', function ($query) {
                    $employee = $query->employee();
                    // dd($employee);
                    return "$employee->firstname $employee->lastname";
                })
                ->addColumn('violation', function ($query) {
                    $violation = $query->violation();
                    return $violation->description;
                })
                ->addColumn('desde', function ($query) {
                    return Carbon::parse($query->fecha_init)->format('m/d/Y');
                })
                ->addColumn('hora', function ($query) {
                     $hora = $query->hora_init;
                    return $hora;
                })
                ->addColumn('estatus', function ($query) {
                    $status = $query->status();
                    return $status->description;
                })
                ->rawColumns(['employee','violation','fecha','hora','estatus'])
                ->toJson();
        }catch (\Exception $e){
            dd($e->getLine(),$e->getFile(),$e->getMessage());
        }
    }

}
