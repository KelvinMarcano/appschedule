<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use DB;
use App\Models\departament;
use App\Models\employes;
use App\Models\position;
use App\Models\schedule;
use App\Models\site;
use App\Models\profession;
use App\Models\roles;
use App\Models\ViewSupervisores;
use App\sex;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use Illuminate\Support\Facades\Auth;

class EmployesController extends Controller
{
    const SUCCESS_UPDATE_MSG = 'Record Update successfully!';
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index(Request $request)
    {

        $data = array();

        $empleados = (new employes());

        $data['employes'] = (new employes())->Where('idsite','=',Auth::user()->site_id)->orderBy('id','DESC')->get();
        $data['departamento'] = (new departament())->get();
        $data['idsite'] = (new site())->get();
        $data['cargo'] = (new position())->get();
        $data['horario'] = (new schedule())->all();
        $data['sexo'] = (new sex())->all();
        $data['roles'] = (new roles())->all();
        $data['profesion'] = (new profession())->all();

        if(Auth::user()->site_id != 4) $empleados = $empleados->where('idsite', Auth::user()->site_id);

        $data['employess'] = $empleados->get();
        return Resp::statusJson($request,"success",self::SUCCESS_MSG,'get '.'employes',$data);

}

    public function save(Request $request)
    {
        try{
            $model = new employes();
            $model = $model->find($request->id);

            if(empty($model)) $model = new employes();

            $model->saveData($request->all());

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'employes');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'save '.'employes');
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new employes();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete ' . 'employes');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete ' . 'employes', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete ' . 'employes');
        }
    }

    public function listEmployesSupervisor(Request $request)
    {
        $employes = new employes();

        if(empty($request->idsite) and empty($request->areaid) and empty($request->departamentid)){
            $employes = $employes->select('id', 'code', 'firstname', 'lastname', 'numbersocial', 'idsite', 'areaid', 'departamentid', 'email')
                                 ->orderby('firstname', 'ASC')
                                 ->get();
        }else if(!empty($request->idsite) and empty($request->areaid) and empty($request->departamentid)){
            $employes = $employes->select('id', 'code', 'firstname', 'lastname', 'numbersocial', 'idsite', 'areaid', 'departamentid', 'email')
                                 ->where('employes.idsite', '=', $request->idsite)
                                 ->where('employes.active', '=', '1')
                                 ->where('employes.status', '=', 'AC')
                                 ->orderby('firstname', 'ASC')
                                 ->get();
        }else if(!empty($request->idsite) and !empty($request->areaid) and empty($request->departamentid)){
            $employes = $employes->select('id', 'code', 'firstname', 'lastname', 'numbersocial', 'idsite', 'areaid', 'departamentid', 'email')
                                 ->where('employes.idsite', '=', $request->idsite)
                                 ->where('employes.areaid', '=', $request->areaid)
                                 ->where('employes.active', '=', '1')
                                 ->where('employes.status', '=', 'AC')
                                 ->orderby('firstname', 'ASC')
                                 ->get();
        }else if(!empty($request->idsite) and !empty($request->areaid) and !empty($request->departamentid)){
            $employes = $employes->select('id', 'code', 'firstname', 'lastname', 'numbersocial', 'idsite', 'areaid', 'departamentid', 'email')
                                 ->where('employes.idsite', '=', $request->idsite)
                                 ->where('employes.areaid', '=', $request->areaid)
                                 ->where('employes.departamentid', '=', $request->departamentid)
                                 ->where('employes.active', '=', '1')
                                 ->where('employes.status', '=', 'AC')
                                 ->orderby('firstname', 'ASC')
                                 ->get();
        }

        foreach($employes as $employe){
            $employe->email = (!empty($employes->email)) ? $employes->email : '';
        }

        return Resp::statusJson($request, "success", 'List employes for supervisor.','list employes', ['datoListEmployes' => $employes]);
    }

    public function listSchedulePlanEmployesSupervisor(Request $request)
    {
        $employes = new employes();

        if(empty($request->idsite) and empty($request->areaid) and empty($request->departamentid)){
            $employes = $employes->select('employes.id', 'employes.code', 'firstname', 'lastname', 'numbersocial', 'idsite', 'areaid', 'departamentid', 'scheduleid', 'email', 'hour_start', 'hour_end', 'sunday', 'monday', 'tuesday', 'wednesday', 'thurday', 'friday', 'saturday')
                                 ->leftjoin('schedule', 'scheduleid', '=', 'schedule.id')
                                 ->leftjoin('scheduleplan', 'schedule.id', '=', 'scheduleplan.id')
                                 ->orderby('firstname', 'ASC')
                                 ->get();
        }else if(!empty($request->idsite) and empty($request->areaid) and empty($request->departamentid)){
            $employes = $employes->select('employes.id', 'employes.code', 'firstname', 'lastname', 'numbersocial', 'idsite', 'areaid', 'departamentid', 'scheduleid', 'email', 'hour_start', 'hour_end', 'sunday', 'monday', 'tuesday', 'wednesday', 'thurday', 'friday', 'saturday')
                                 ->leftjoin('schedule', 'scheduleid', '=', 'schedule.id')
                                 ->leftjoin('scheduleplan', 'schedule.id', '=', 'scheduleplan.id')
                                 ->where('employes.idsite', '=', $request->idsite)
                                 ->orderby('firstname', 'ASC')
                                 ->get();
        }else if(!empty($request->idsite) and !empty($request->areaid) and empty($request->departamentid)){
            $employes = $employes->select('employes.id', 'employes.code', 'firstname', 'lastname', 'numbersocial', 'idsite', 'areaid', 'departamentid', 'scheduleid', 'email', 'hour_start', 'hour_end', 'sunday', 'monday', 'tuesday', 'wednesday', 'thurday', 'friday', 'saturday')
                                 ->leftjoin('schedule', 'scheduleid', '=', 'schedule.id')
                                 ->leftjoin('scheduleplan', 'schedule.id', '=', 'scheduleplan.id')
                                 ->where('employes.idsite', '=', $request->idsite)
                                 ->where('employes.areaid', '=', $request->areaid)
                                 ->orderby('firstname', 'ASC')
                                 ->get();
        }else if(!empty($request->idsite) and !empty($request->areaid) and !empty($request->departamentid)){
            $employes = $employes->select('employes.id', 'employes.code', 'firstname', 'lastname', 'numbersocial', 'idsite', 'areaid', 'departamentid', 'scheduleid', 'email', 'hour_start', 'hour_end', 'sunday', 'monday', 'tuesday', 'wednesday', 'thurday', 'friday', 'saturday')
                                 ->leftjoin('schedule', 'scheduleid', '=', 'schedule.id')
                                 ->leftjoin('scheduleplan', 'schedule.id', '=', 'scheduleplan.id')
                                 ->where('employes.idsite', '=', $request->idsite)
                                 ->where('employes.areaid', '=', $request->areaid)
                                 ->where('employes.departamentid', '=', $request->departamentid)
                                 ->orderby('firstname', 'ASC')
                                 ->get();
        }

        foreach($employes as $employe){
            $employe->email = (!empty($employes->email)) ? $employes->email : '';
            $employe->ausencia = $employe->absence()->select('id', 'reason', 'fecha_init', 'fecha_end', 'hora_init', 'hora_end',
                                                             'motiveid', 'violation_id', 'status_id', 'comment')
                                                    ->whereBetween('fecha_init', [$request->fechaDesde, $request->fechaHasta])
                                                    ->get();

            //$employe->descripMotive = $employe->motive()->select('description')
            //                                            ->where('id', $employe->ausencia->motiveid)
            //                                            ->first();
        }

        return Resp::statusJson($request, "success", 'List scheduleplan employes for supervisor.','list scheduleplan employes', ['datoListSchedulesEmployes' => $employes]);
    }

    public function searchEmployee(Request $request)
    {
        $employes = new employes();
        $supervisores = new ViewSupervisores();

        if($request->option == 1) $employes = $employes->where('numbersocial', $request->value);
        if($request->option == 2) $employes = $employes->where('numberphone', $request->value);
        if($request->option == 3) $employes = $employes->where('code', $request->value);
        if($request->option == 4) {
            $employes = $employes->where('code', $request->value);
            $employes = $employes->where('numbersocial', $request->value2);
        }

        $employes = $employes->first();

        if(!empty($employes)){
            $employes->site = $employes->site();
            $employes->departament = $employes->departament();
            //$employes->supervisor = $supervisores->viewSupervisor($employes);
            $employes->supervisor = $employes->departament()->employee();
            $employes->position = $employes->position();
            $employes->profession = $employes->profession();
            $employes->roles = $employes->roles();
            $employes->sex = $employes->sex();

            $result = Resp::statusJson($request, "success", 'Employee found successfully','employes', $employes);
        }else{
            $result = Resp::statusJson($request, "success", 'Error Search Employee.','employes', $employes);
        }

        //return Resp::statusJson($request, "success", 'Search Employee','employes', $employes);
        return $result;
    }

    public function searchEmployeeTemp(Request $request)
    {
        $employes = new employes();
        $supervisores = new ViewSupervisores();

        if($request->option == 1) $employes = $employes->where('numbersocial', $request->value);
        if($request->option == 2) $employes = $employes->where('numberphone', $request->value);
        if($request->option == 3) $employes = $employes->where('code', $request->value);
        if($request->option == 4) {
            $employes = $employes->where('code', $request->value);
            $employes = $employes->where('numbersocial', $request->value2);
        }

        $employes = $employes->first();

        if(!empty($employes)){
            $employes->site = $employes->site();
            $employes->departament = $employes->departament();
            $employes->supervisor = $supervisores->viewSupervisor($employes);
            $employes->position = $employes->position();
            $employes->profession = $employes->profession();
            $employes->roles = $employes->roles();
            $employes->sex = $employes->sex();

            $result = Resp::statusJson($request, "success", 'Employee found successfully','employes', $employes);
        }else{
            $result = Resp::statusJson($request, "success", 'Error Search Employee.','employes', $employes);
        }

        //return Resp::statusJson($request, "success", 'Search Employee','employes', $employes);
        return $result;
    }

    public function associateEmployeeUser(Request $request)
    {
        try{
            $employes = new employes();
            $employes = $employes->select('id')
                                 ->where('code', $request->code)
                                 ->first();

            $employes = $employes->find($employes->id);

            if(empty($employes)) $employes = new employes();

            $employes->updateEmployeData($request->all());

            return Resp::statusJson($request,"success",self::SUCCESS_UPDATE_MSG,'update success '.'employes');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'update '.'employes');
        }
    }
}
