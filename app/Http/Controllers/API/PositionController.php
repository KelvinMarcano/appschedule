<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\position;
use Illuminate\Http\Request;
use App\Models\Response as Resp;

class PositionController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index(Request $request)
    {
        $data = array();
        $data['positions'] = (new position())->all();
        return Resp::statusJson($request,"success",self::SUCCESS_MSG,'get '.'positions',$data);
    }

    public function save(Request $request)
    {
        try{
            $model = new position();
            $model = $model->find($request->id);

            if(empty($model)) $model = new position();

            $model->saveData($request->all());

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'position');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'save '.'position');
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new position();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete ' . 'position');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete ' . 'position', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete ' . 'position');
        }
    }
}
