<?php

namespace App\Http\Controllers;

use App\Models\state;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class StateController extends Controller
{
   const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['state'] = (new state())->all();
        // $data['state'] = DB::table('state')
        //->select('state.id as id','state.code as code','state.description as description')
        //->get();
        return view('configuracion.state', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new state();
            $model = $model->find($request->id);

            if(empty($model)) $model = new state();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'state');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'state');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'state');
            Resp::status($request,"error",$e->getMessage(),'save '.'state');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new state();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'state');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'state',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'state');
        }
    }

    public function stateList(Request $request){
        $query = new state();
        $query = $query->getStateList($request)->get();

       // dd($query);

        return DataTables::of($query)
            ->addColumn('id', function ($query) {
                return $query->stateId;
            })
            ->addColumn('code', function ($query) {
                return $query->stateCode;
            })
            ->addColumn('description', function ($query) {
                return $query->stateDescription;
            })
            ->addColumn('action', function ($query) {
                return "<button type='button' class='btn btn-primary btn-sm text-white edit' data-toggle='modal' data-target='#Modal' data-whatever='@getbootstrap' data-id='$query->stateId' onclick='edit(this);'>
                      <i class='si si-pencil text-white'></i>
                      Edit
                  </button>
                  <button type='button' class='btn btn-danger btn-sm text-white delete' data-id='$query->stateId' onclick='deleted(this);'>
                    <i class='si si-close text-white'></i>
                    Delete
                  </button>";
              
            })
            ->rawColumns(['id','code','description','action'])
            ->toJson();
    }
}
