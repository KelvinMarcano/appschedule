<?php

namespace App\Http\Controllers;

use App\Models\violation_types;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;


class TypesViolationsController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['violation_types'] = (new violation_types())->all();
        $data['control'] = DB::table('violation_types')
        ->select('violation_types.id as id','violation_types.code as code','violation_types.name as description')
        ->get();
        return view('configuracion.type_violation', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new violation_types();
            $model = $model->find($request->id);

            if(empty($model)) $model = new violation_types();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'control');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'control');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'control');
            Resp::status($request,"error",$e->getMessage(),'save '.'control');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new violation_types();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'control');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'control',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'control');
        }
    }

}