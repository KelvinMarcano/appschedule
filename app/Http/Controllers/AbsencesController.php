<?php

namespace App\Http\Controllers;

use App\Models\absences;
use App\Models\control_point;
use App\Models\departament;
use App\Models\employes;
use App\Models\motive;
use App\Models\motive_category;
use App\Models\status;
use App\Models\ViewSupervisores;
use App\Models\violation;
use App\Models\relation_type_reason_violation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;
use DB;

class AbsencesController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';
    const SUCCESS_REJECT = 'Your record has been reject.';
    const ABSENCES_EXIST = 'Previously created record, not has been insert.';
   
    public function index(Request $request)
    {

        $data = array();
        $data['absences'] = (new absences())->getAbsenceTable($request)->get();
        $empleados = (new employes());
        $data['departaments'] = (new departament())->all();
        $data['motives_nocall'] = (new motive())->all();
        $data['violation_nocall'] = (new violation())->all();
        $data['motives'] = (new motive())->whereIn('categories_id',[2,3])->orderby('code','ASC')->get();

        $data['motives_catgen'] = (new motive())->whereIn('categories_id',[1])->orderby('code','ASC')->get();

        $data['violations'] = (new violation())->where('type_violation_id',1)
            ->orderby('code','ASC')->get();

        $data['status'] = (new status())->orderby('id')->get();

        $data['relation'] = DB::table('relation_type_reason_violation')
        ->select('relation_type_reason_violation.id as id','relation_type_reason_violation.motiveid as idmotive','relation_type_reason_violation.motiveid2 as idmotive2','relation_type_reason_violation.violationid as violationid')
        ->join('motive', 'motive.id', '=', 'relation_type_reason_violation.motiveid' )
        ->join('motive as reason', 'reason.id', '=', 'relation_type_reason_violation.motiveid2' )
        ->join('violation', 'violation.id', '=', 'relation_type_reason_violation.violationid' )
        ->get();

        $filter = Session::get('filter');

        if(!$filter->all){
            if($filter->site){
                $empleados = $empleados->where('employes.idsite', '=', $filter->siteId);
            }
            if($filter->area){
                 $empleados = $empleados->where('employes.areaid',$filter->areaId);
            }
            if($filter->departament){
                 $empleados = $empleados->where('employes.departamentid',$filter->departId);
            }
        }
        $data['employes'] = $empleados->orderby('firstname','ASC')->get();
       
        return view('registro.absences', $data);
    }

    public function save(Request $request)
    {
        try{
            $request['fecha_init'] = Carbon::createFromFormat('m/d/Y', $request->fecha_init);
            $request['fecha_end'] = Carbon::createFromFormat('m/d/Y', $request->fecha_end);

            $model = new absences();
            $model = $model->find($request->id);

            if(empty($model)) $model = new absences();

            $absences = new absences();
            $absences = $absences->where('employesid', $request->employesid)
                             ->where('fecha_init',  $request->fecha_init = Carbon::now()->format('Y-m-d'))
                             ->where('motiveid', $request->motiveid)
                             ->count();

            if($absences){
                Resp::status($request,"Failed",self::ABSENCES_EXIST,'save '.'absences');
                return redirect()->back();
            }


            $request['user_add'] = Auth::user()->id;
            $request['platform'] = 'Web App';


            if($request->multiple){
                $empleados = $request->employesidM;
                foreach ($empleados as $empleado){
                    $request['employesid'] = $empleado;
                    $model = new absences();
                    $absence = $model->saveData($request->all());
                }
                Resp::status($request,"success",self::SUCCESS_MSG,'save '.'absences');
                return redirect()->back();
            }

            $absence = $model->saveData($request->all());
            $supervisor = (new employes())->find($request->supervisor);

            $mail = new MailController();
            $mail->sendMailAbsence($absence, $request->server('HTTP_HOST'), $supervisor, 'absence');

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'absences');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'absences');
            return redirect()->back();

        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'absences');
            Resp::status($request,"error",$e->getMessage(),'save '.'absences');
            return redirect()->back()->withInput($request->all());
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new absences();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete ' . 'absences');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete ' . 'absences', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete ' . 'absences');
        }
    }

    public function reject(Request $request,$id)
    {
        try{
            $model = new absences();
            $model = $model->find($request->id);

            if(empty($model)) return Resp::statusJson($request,"error",ERROR_MSG,'reject absences');

            $model->reject();

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'reject absences');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'reject absences');
        }
    }

    public function aprove(Request $request, $id)
    {
        try{
            $model = (new absences())->find($id);

            if(empty($model)) return Resp::statusJson($request,"error",ERROR_MSG,'approve absences');

            $model->aprove($request);

            if(!in_array($request->status_id,[1,6,7])){
                return Resp::statusJson($request,"success",self::SUCCESS_MSG,'approve absences');
            }

            $request['absence_id'] = $model->id;

            $controlPoint = (new control_point())->where('employe_id',$model->employesid)
                ->get()->last();

            if(!empty($request->personal_time) and !empty($controlPoint)){
                $controlPoint->saveHours($request);
                return Resp::statusJson($request,"success",self::SUCCESS_MSG,'approve absences');
            }

            if(!empty($request->points) and !empty($controlPoint)){
                $controlPoint->savePoint($request);
                return Resp::statusJson($request,"success",self::SUCCESS_MSG,'approve absences');
            }

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'approve absences');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'approve absences');
        }
    }

    public function getAbsence(Request $request){
        try{
            $query = new absences();
            $query = $query->getAbsenceTable($request)->where('violation.type_violation_id', 1)->get();

            return DataTables::of($query)
                ->addColumn('employee', function ($query) {
                    $employee = $query->employee();
                    return "$employee->firstname $employee->lastname";
                })
                ->addColumn('reasons', function ($query) {
                   $reason = $query->motive();
                    return (!empty($reason)) ? $reason->description : 'Reason don`t exist';
                })
                ->addColumn('desde', function ($query) {
                    return (!empty($query->fecha_init)) ? Carbon::parse($query->fecha_init)->format('m/d/Y') : '';
                })
                ->addColumn('hasta', function ($query) {
                    return (!empty($query->fecha_end)) ? Carbon::parse($query->fecha_end)->format('m/d/Y') : '';
                })
                ->addColumn('estatus', function ($query) {
                    $status = $query->status();
                    return (!empty($status)) ? $status->description : 'Empty Status';
                })
                ->rawColumns(['employee','reasons','desde','hasta','estatus'])
                ->toJson();
        }catch (\Exception $e){
            dd($e->getLine(),$e->getFile(),$e->getMessage(),$query);
        }
    }

    public function finished(Request $request,$id)
    {
        try{
            $model = new absences();
            $model = $model->find($request->id);

            if(empty($model)) return Resp::statusJson($request,"error",ERROR_MSG,'finished absences');

            $model->finished();

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'finished absences');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'finished absences');
        }
    }

    protected function kioskoIndex(Request $request){
        $data = [];
        $motiveId = [];
        $category = (new motive_category())->all();

        foreach ($category as $catego){
            $motiveId[] = $catego->motive_id;
        }
        $data['motives'] = (new motive())->whereIn('id',array_unique($motiveId))->get();
        $data['category'] = $category;
        return view('kiosko.index',$data);
    }

}
