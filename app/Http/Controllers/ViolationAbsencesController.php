<?php

namespace App\Http\Controllers;

use App\Models\violation_absences;
use App\Models\absences;
use App\Models\control_point;
use App\Models\departament;
use App\Models\employes;
use App\Models\motive;
use App\Models\status;
use App\Models\supervisor_employee;
use App\Models\violation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;

class ViolationAbsencesController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';
    const SUCCESS_REJECT = 'Your record has been reject.';
   
     public function index(Request $request)
    {
        if(Auth::user()->rol_id == 5) return redirect('dashboard');
        $data = array();
        $data['absences'] = (new violation_absences())->getAbsenceTable($request)->get();

        $empleados = (new employes());
        $data['departaments'] = (new departament())->all();
        $data['motives_nocall'] = (new motive())->all();
        $data['violation_nocall'] = (new violation())->all();
        $data['violations'] = (new violation())->whereIn('type_violation_id',[2])->orderby('code','ASC')->get();
        $data['absences2'] = (new absences())->Where('type_absences','=','4');

        $data['status'] = (new status())->orderby('description')->get();
        $data['motives'] = (new motive())->Where('code','=','NAP')->orderby('code','ASC')->get();

        $filter = Session::get('filter');

        if(!$filter->all){
            if($filter->site){
                $empleados = $empleados->where('employes.idsite', '=', $filter->siteId);
            }
            if($filter->area){
                 $empleados = $empleados->where('employes.areaid',$filter->areaId);
            }
            if($filter->departament){
                 $empleados = $empleados->where('employes.departamentid',$filter->departId);
            }
        }

        $data['employes'] = $empleados->orderby('firstname','ASC')->get();
        return view('registro.violation_absences', $data);
    }

    public function save(Request $request)
    {
        try{
            $request['fecha_init'] = Carbon::createFromFormat('m/d/Y', $request->fecha_init);
          
            $model = new violation_absences();
            $model = $model->find($request->id);

            if(empty($model)) $model = new violation_absences();

            $request['user_add'] = Auth::user()->id;
            $request['platform'] = 'Web App';

            $absence = $model->saveData($request->all());

            $supervisor = (new employes())->find($request->supervisor);

            //$mail = new MailController();
           // $mail->sendMailAbsence($absence,$request->server('HTTP_HOST'),$supervisor);

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'absences');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'absences');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'absences');
            Resp::status($request,"error",$e->getMessage(),'save '.'absences');
            return redirect()->back()->withInput($request->all());
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new violation_absences();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete ' . 'absences');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete ' . 'absences', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete ' . 'absences');
        }
    }

    public function reject(Request $request,$id)
    {
        try{
            $model = new violation_absences();
            $model = $model->find($request->id);

            if(empty($model)) return Resp::statusJson($request,"error",ERROR_MSG,'reject absences');

            $model->reject();

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'reject absences');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'reject absences');
        }
    }

    public function aprove(Request $request, $id)
    {
        try{
            $model = (new violation_absences())->find($id);

            if(empty($model)) return Resp::statusJson($request,"error",ERROR_MSG,'reject absences');

            $model->aprove($request);

            $request['absence_id'] = $model->id;

            $controlPoint = (new control_point())->where('employe_id',$model->employesid)
                ->get()->last();

            if(!empty($controlPoint)) $controlPoint->savePoint($request);

            return Resp::statusJson($request,"success",self::SUCCESS_MSG,'reject absences');
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'reject absences');
        }
    }

    public function getAbsence(Request $request){
        try{
            $query = new violation_absences();
            $query = $query->getAbsenceTable($request)->where('violation.type_violation_id', 2)->get();
           
            return DataTables::of($query)
                ->addColumn('employee', function ($query) {
                    $employee = $query->employee();
                    // dd($employee);
                    return "$employee->firstname $employee->lastname";
                })
                ->addColumn('violation', function ($query) {
                    $violation = $query->violation();
                    return $violation->description;
                })
                ->addColumn('desde', function ($query) {
                    return Carbon::parse($query->fecha_init)->format('m/d/Y');
                })
                ->addColumn('hora', function ($query) {
                     $hora = $query->hora_init;
                    return $hora;
                })
                ->addColumn('estatus', function ($query) {
                    $status = $query->status();
                    return $status->description;
                })
                ->rawColumns(['employee','violation','fecha','hora','estatus'])
                ->toJson();
        }catch (\Exception $e){
            dd($e->getLine(),$e->getFile(),$e->getMessage());
        }
    }

}
