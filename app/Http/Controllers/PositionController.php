<?php

namespace App\Http\Controllers;

use App\Models\position;
use Illuminate\Http\Request;
use App\Models\Response as Resp;

class PositionController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['positions'] = (new position())->all();
        return view('configuracion.position', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new position();
            $model = $model->find($request->id);

            if(empty($model)) $model = new position();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'position');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'position');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'position');
            Resp::status($request,"error",$e->getMessage(),'save '.'position');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new position();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete ' . 'position');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete ' . 'position', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete ' . 'position');
        }
    }
}
