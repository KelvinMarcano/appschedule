<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\working_day;
use App\Models\Response as Resp;
use DB;

class WorkingDayController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['working_day'] = (new working_day())->all();
        $data['work'] = DB::table('working_day')
        ->select('working_day.id as id','working_day.code as code','working_day.description as description')
        ->get();
        return view('configuracion.working_day', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new working_day();
            $model = $model->find($request->id);

            if(empty($model)) $model = new working_day();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'workingday');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'workingday');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'motive');
            Resp::status($request,"error",$e->getMessage(),'save '.'motive');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new working_day();
            $model = $model->find($id);

            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete '.'workingday');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'workingday',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'workingday');
        }
    }

}
