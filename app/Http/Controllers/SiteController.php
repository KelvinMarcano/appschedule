<?php

namespace App\Http\Controllers;

use App\Models\site;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;


class SiteController extends Controller
{
   const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['site'] = (new site())->all();
        return view('configuracion.sites', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new site();
            $model = $model->find($request->id);

            if(empty($model)) $model = new site();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'site');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'site');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save'.'site');
            Resp::status($request,"error",$e->getMessage(),'save '.'site');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            $model = new site();
            $model = $model->find($id);
          //  dd($model);
            if(empty($model)) return Resp::statusJson($request,"warning",self::ERROR_MSG,'delete'.'site');

            return Resp::statusJson($request,"success",self::SUCCESS_DELETE,'delete '.'site',$model->softDelete());
        }catch(\Exception $e){
            return Resp::statusJson($request,"error",$e->getMessage(),'delete '.'site');
        }
    }

    public function siteList(Request $request){
        $query = new site();

        $query = $query->getSiteList($request)->get();

       // dd($query);

        return DataTables::of($query)
            ->addColumn('id', function ($query) {
                return $query->siteId;
            })
            ->addColumn('description', function ($query) {
                return $query->siteDescription;
            })
            ->addColumn('action', function ($query) {
                return "<button type='button' class='btn btn-primary btn-sm text-white edit' data-toggle='modal' data-target='#Modal' data-whatever='@getbootstrap' data-id='$query->siteId' onclick='edit(this);'>
                      <i class='si si-pencil text-white'></i>
                      Edit
                  </button>
                  <button type='button' class='btn btn-danger btn-sm text-white delete' data-id='$query->siteId' onclick='deleted(this);'>
                    <i class='si si-close text-white'></i>
                    Delete
                  </button>";
            })
            ->rawColumns(['id','code','description','action'])
            ->toJson();
    }

   
}
