<?php

namespace App\Http\Controllers;

use App\Models\unitofmeasure;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use DB;


class UnitOfMeasureController extends Controller
{
    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index()
    {
        $data = array();
        $data['unitofmeasure'] = (new unitofmeasure())->orderby('code','ASC')->get();
        // dd($data);
        // $data['supervisor'] = DB::table('employes')
        // ->select('employes.id as id','employes.code as code','employes.lastname as lastname', 'employes.firstname as firstname')
        // ->join('roles', 'roles.id', '=', 'employes.rolesid' )
        // ->where('roles.name', '=', 'Supervisor')
        // ->orderBy('employes.code')
        // ->get();
        
        //  $data['departament'] = DB::table('departament')
        // ->select('departament.id as id','departament.code as code','departament.description as description', 'employes.lastname as lastname', 'employes.firstname as firstname')
        // ->leftjoin('employes', 'employes.id', '=', 'departament.employesid' )
        // ->orderBy('employes.code')
        // ->get();

        return view('configuracion.unitofmeasure', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new unitofmeasure();
            $model = $model->find($request->id);

            if(empty($model)) $model = new unitofmeasure();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'unitofmeasure');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'unitofmeasure');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'unitofmeasure');
            Resp::status($request,"error",$e->getMessage(),'save '.'unitofmeasure');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new unitofmeasure();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete ' . 'unitofmeasure');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete ' . 'unitofmeasure', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete ' . 'unitofmeasure');
        }
    }
}
