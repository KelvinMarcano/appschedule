<?php

namespace App\Http\Controllers;

use DB;
use App\Models\departament;
use App\Models\area;
use App\Models\employes;
use App\Models\position;
use App\Models\schedule;
use App\Models\site;
use App\Models\profession;
use App\Models\roles;
use App\sex;
use Illuminate\Http\Request;
use App\Models\Response as Resp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class EmployesController extends Controller
{

    const SUCCESS_MSG = 'Record saved successfully!';
    const ERROR_MSG = 'The requested registration does not exist';
    const SUCCESS_DELETE = 'Your record has been deleted.';

    public function index(Request $request)
    {
        $data = array();
        
        $empleados = (new employes());
        $data['employes'] = $empleados->getEmployeeTable($request)->get();
        $data['departamento'] = (new departament())->all();
        $data['area'] = (new area())->all();
        $data['idsite'] = (new site())->all();
        $data['cargo'] = (new position())->all();
        $data['horario'] = (new schedule())->all();
        $data['sexo'] = (new sex())->all();
        $data['roles'] = (new roles())->all();
        $data['profesion'] = (new profession())->all();

        $filter = Session::get('filter');

        if(!$filter->all){
            if($filter->site){
                $empleados = $empleados->where('employes.idsite', '=', $filter->siteId);
            }
            if($filter->area){
                 $empleados = $empleados->where('employes.areaid',$filter->areaId);
            }
            if($filter->departament){
                 $empleados = $empleados->where('employes.departamentid',$filter->departId);
            }
        }

        $data['employes2'] =$empleados->orderBy('id','DESC')->get();

        return view('registro.employes', $data);
    }

    public function save(Request $request)
    {
        try{
            $model = new employes();
            $model = $model->find($request->id);

            if(empty($model)) $model = new employes();

            $model->saveData($request->all());

            if($request->ajax()) return Resp::statusJson($request,"success",self::SUCCESS_MSG,'save '.'employes');

            Resp::status($request,"success",self::SUCCESS_MSG,'save '.'employes');
            return redirect()->back();
        }catch(\Exception $e){
            if($request->ajax()) return Resp::statusJson($request,"error",$e->getMessage(),'save '.'employes');
            Resp::status($request,"error",$e->getMessage(),'save '.'employes');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id)
    {
        try {
            $model = new employes();
            $model = $model->find($id);

            if (empty($model)) return Resp::statusJson($request, "warning", self::ERROR_MSG, 'delete ' . 'employes');

            return Resp::statusJson($request, "success", self::SUCCESS_DELETE, 'delete ' . 'employes', $model->softDelete());
        } catch (\Exception $e) {
            return Resp::statusJson($request, "error", $e->getMessage(), 'delete ' . 'employes');
        }
    }

    public function getEmployee(Request $request){
        try{
            $query = new employes();
            $query = $query->getEmployeeTable($request)->get();

            return DataTables::of($query)
                ->addColumn('code', function ($query) {
                   return $query->code;
                     
                })

                ->addColumn('firstname', function ($query) {
                    return $query->firstname;
                  
                })

                ->addColumn('lastname', function ($query) {
                   return $query->lastname;
                   
                })

                ->addColumn('birthdate', function ($query) {
                    return Carbon::parse($query->birthdate)->format('m/d/Y');
                })

                ->addColumn('address', function ($query) {
                    return $query->address;
                   
                })

                ->addColumn('area', function ($query) {
                    //  $area = $query->area();
                    // return $area;
                     return $query->areaid;
                    //return $area->description;
                })
                ->addColumn('departament', function ($query) {
                    // $departament = $query->departament();
                    // return $departament;
                    return $query->departamentid;
                     //$departament->description;
                })
                ->addColumn('site', function ($query) {
                    // $site = $query->site();
                    // return $site;
                     return $query->siteid;
                    //  //$site->name;
                })
                ->addColumn('action', function ($query) {
                return "<div class='dropdown'>
                            <button type='button' class='btn btn-primary dropdown-toggle' id='dropdown-default-primary' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                Actions
                            </button>
                        <div class='dropdown-menu' aria-labelledby='dropdown-default-primary'>
                                                    <a class='dropdown-item' onclick='editEmploye(this);' data-toggle='tooltip' data-animation='true' data-placement=top' data-id='$query->id'>Edit</a>
                                                    <a class='dropdown-item' onclick='deleteEmploye(this);' data-toggle='tooltip' data-animation='true' data-placement=top' data-id='$query->id'>Delete</a>
                                                    
                                            </div>";
            })
                ->rawColumns(['code','firstname','lastname','birthdate','address','area','departament','site','action'])
                ->toJson();
        }catch (\Exception $e){
            dd($e->getLine(),$e->getFile(),$e->getMessage());
        }
    }


}


