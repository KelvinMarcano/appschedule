<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SetRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::user()) return $next($request);
        $employe = Auth::user()->employes();
        $role = (!empty(Auth::user())) ? Auth::user()->role() : null;
        $roleFilter = (!empty($role)) ? $role->roleFilter() : null;
        $roleFilter['siteId'] = (!empty($employe)) ? $employe->idsite : null;
        $roleFilter['areaId'] = (!empty($employe)) ? $employe->areaid : null;
        $roleFilter['departId'] = (!empty($employe)) ? $employe->departamentid : null;
        Session::put('filter',$roleFilter);
        //if (!Session::has('filter')) {
        //    Session::put('filter',$roleFilter);
        //}
        return $next($request);
    }
}
