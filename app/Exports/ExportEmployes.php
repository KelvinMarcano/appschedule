<?php

namespace App\Exports;

use App\employes as employes;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class ExportEmployes implements FromCollection,WithHeadings
{

	/**
    * @return \Illuminate\Support\Collection
    */
    public function headings(): array
    {
        return [
            'Id',
            'users_id',
            'code',
            'firstname',
            'lastname',
            'sex',
            'birthdate',
            'address',
            'numbersocial',
            'zipcode',
            'numberphone',
            'numberhome',
            'numberfax',
            'sociallink',
            'departamentid',
            'scheduleid',
            'positionid',
            'professionid',
            'status',
            'Email',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

		$employes = DB::table('employes')->get();    	//dd(employes::all());
		return $employes;
        //return employes::all();
    }
}
