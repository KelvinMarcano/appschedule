<?php 

namespace App\Services;

class SendPush
{
	public static function sendFCM($token, $type, $data, $supervisor, $title, $message, $platform){
		$empleado = $data->employee();

        $ch=curl_init("https://fcm.googleapis.com/fcm/send");
        $header=array("Content-Type:application/json","Authorization: key=AAAAZhtb1RI:APA91bHHBENFd8Mhxluf1wHeTZBhE9cGY195qhhR86XobIVt5nkZGSGDuSM2ClOHm1_Zj5ch7B4CrEUU4DZnz0HwRYofcX7WT5EFU6AmTUo-cCW7cy9k4P9P9Q8mt-o5rhlbOuOD1YXA");

        if($token==""){
            if($type=="save_absences"){
                $data=json_encode(array("to"=>"/topics/allDevices",
                                        "data"=>array("title"=>$title,
                                                      "message"=>'Attendance Notice',
                                                      "not_type"=>"save_absences",
                                                      "extra_data"=>"Platform: " . $platform . "\nEmployee: " . $empleado->firstname . " - " . $empleado->lastname . "\nSupervisor: " . $supervisor->firstname ." - " . $supervisor->lastname . "\nSite: " . $empleado->site()->name . "\nArea: " . $empleado->departament()->description . "\nReason: " . $data->motive()->description . "\nReason for Attendance: " . $data->reason . "\nPersonal time / Points / Violations: " . $data->violation()->description . "\nComment: " . $data->comment)));
            }

            if($type=="save_violation"){
                $data=json_encode(array("to"=>"/topics/allDevices",
                                        "data"=>array("title"=>$title,
                                                      "message"=>'Violation Notice',
                                                      "not_type"=>"save_violation",
                                                      "extra_data"=>"Platform: " . $platform . "\nEmployee: " . $empleado->firstname . " - " . $empleado->lastname . "\nSupervisor: " . $supervisor->firstname ." - " . $supervisor->lastname . "\nSite: " . $empleado->site()->name . "\nArea: " . $empleado->departament()->description . "\nPersonal time / Points / Violations: " . $data->violation()->description . "\nComment: " . $data->comment)));
            }

            if($type=="bigimage"){
                $data=json_encode(array("to"=>"/topics/allDevices",
                                        "data"=>array("title"=>$title,
                                                      "message"=>$_REQUEST['message'],
                                                      "not_type"=>"bigimage",
                                                      "extra_data"=>"https://i.picsum.photos/id/638/200/200.jpg")));
            }

            if($type=="bigimage_withoutsideicon"){
                $data=json_encode(array("to"=>"/topics/allDevices",
                                        "data"=>array("title"=>$title,
                                                      "message"=>$_REQUEST['message'],
                                                      "not_type"=>"bigimage_withoutsideicon",
                                                      "extra_data"=>"https://i.picsum.photos/id/638/200/200.jpg")));
            }

            if($type=="inbox_style"){
                $array_message=array("Rahul : Hi How Are You?","Aman : I am Fine ","Vishal : Are You Ok?");
                $json_message=json_encode($array_message);

                $data=json_encode(array("to"=>"/topics/allDevices",
                                        "data"=>array("title"=>$title,
                                                      "message"=>$_REQUEST['message'],
                                                      "not_type"=>"inbox_style",
                                                      "extra_data"=>$json_message)));
            }

            if($type=="message_style"){
                $data=json_encode(array("to"=>"/topics/allDevices",
                                        "data"=>array("title"=>$title,
                                                      "message"=>'Attendance Notice',
                                                      "not_type"=>"message_style",
                                                      "extra_data"=>"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s")));
            }
        }else if(is_array($token)){
            if($type=="save_absences"){
                $data=json_encode(array("registation_ids"=>array($token),
                                        "data"=>array("title"=>$title,
                                                      "message"=>'Attendance Notice',
                                                      "not_type"=>"save_absences",
                                                      "extra_data"=>"Platform: " . $platform . "\nEmployee: " . $empleado->firstname . " - " . $empleado->lastname . "\nSupervisor: " . $supervisor->firstname ." - " . $supervisor->lastname . "\nSite: " . $empleado->site()->name . "\nArea: " . $empleado->departament()->description . "\nReason: " . $data->motive()->description . "\nReason for Attendance: " . $data->reason . "\nPersonal time / Points / Violations: " . $data->violation()->description . "\nComment: " . $data->comment)));
            }

            if($type=="save_violation"){
                $data=json_encode(array("registation_ids"=>array($token),
                                        "data"=>array("title"=>$title,
                                                      "message"=>'Violation Notice',
                                                      "not_type"=>"save_violation",
                                                      "extra_data"=>"Platform: " . $platform . "\nEmployee: " . $empleado->firstname . " - " . $empleado->lastname . "\nSupervisor: " . $supervisor->firstname ." - " . $supervisor->lastname . "\nSite: " . $empleado->site()->name . "\nArea: " . $empleado->departament()->description . "\nPersonal time / Points / Violations: " . $data->violation()->description . "\nComment: " . $data->comment)));
            }

            //$data=json_encode(array("registation_ids"=>array($token),
            //                        "data"=>array("title"=>$title,
            //                                      "message"=>$_REQUEST['message'])));
        }else{
            if($type=="save_absences"){
                $data=json_encode(array("to"=>$token,
                                        "data"=>array("title"=>$title,
                                                      "message"=>'Attendance Notice',
                                                      "not_type"=>"save_absences",
                                                      "extra_data"=>"Platform: " . $platform . "\nEmployee: " . $empleado->firstname . " - " . $empleado->lastname . "\nSupervisor: " . $supervisor->firstname ." - " . $supervisor->lastname . "\nSite: " . $empleado->site()->name . "\nArea: " . $empleado->departament()->description . "\nReason: " . $data->motive()->description . "\nReason for Attendance: " . $data->reason . "\nPersonal time / Points / Violations: " . $data->violation()->description . "\nComment: " . $data->comment)));
            }

            if($type=="save_violation"){
                $data=json_encode(array("to"=>$token,
                                        "data"=>array("title"=>$title,
                                                      "message"=>'Violation Notice',
                                                      "not_type"=>"save_violation",
                                                      "extra_data"=>"Platform: " . $platform . "\nEmployee: " . $empleado->firstname . " - " . $empleado->lastname . "\nSupervisor: " . $supervisor->firstname ." - " . $supervisor->lastname . "\nSite: " . $empleado->site()->name . "\nArea: " . $empleado->departament()->description . "\nPersonal time / Points / Violations: " . $data->violation()->description . "\nComment: " . $data->comment)));
            }

            //$data=json_encode(array("to"=>$token,"data"=>array("title"=>$_REQUEST['title'],"message"=>$_REQUEST['message'])));
            //$data=json_encode(array("to"=>$token,
            //                        "data"=>array("title"=>$title,
            //                                      "message"=>"Prueba")));
        }

		//now let's see data message

		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$response = curl_exec($ch);
		$response = json_decode($response);

		curl_close($ch);


		return $response;
	}
}