@extends('layouts.backend')
<style>
  div.dataTables_wrapper div.dataTables_processing {
    position: absolute;
    top: 30%;
    left: 50%;
    width: 30%;
    height: 64px;
    margin-left: -20%;
    margin-top: -25px;
    padding-top: 20px;
    text-align: center;
    font-size: 1.2em;
    background:#00703c;
    color: mintcream;
    border-radius: 10px;
  }
  td.details-control {
    background: url('{{ asset("images/details_open.png") }}') no-repeat center center;
    cursor: pointer;
  }
  tr.shown td.details-control {
    background: url('{{ asset("images/details_close.png") }}') no-repeat center center;
  }
  .timeleft {
    background-color: white !important;
    color: #FFB119;
  }
  .approved {
    background-color: white !important;
    color: green;
  }
  .reject {
    background-color: white !important;
    color: red;
  }
  .pending {
    background-color: white !important;
    color: #3C90DF;
  }
</style>

@section('content')
  <div class="content">

    <div class="block block-rounded block-bordered">
      <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" href="#btabs-animated-fade-form" id="form">Violation</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#btabs-animated-fade-list" id="list">Violation List</a>
        </li>
      </ul>
      <div class="block-content tab-content overflow-hidden">

        {{-- Tab 1--}}
        <div class="tab-pane fade show active" id="btabs-animated-fade-form" role="tabpanel">
          <form action="violation_absences" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="supervisor" id="supervisor" value="">
            <input type="hidden" name="id" id="id" value="">
            <div class="block block-rounded block-bordered">
              <div class="block-header block-header-default">
                <div class="block-options">
                  <button type="submit" class="btn btn-sm btn-outline-primary">
                    <i class="fa fa-check"></i> Save
                  </button>
                  <button type="reset" class="btn btn-sm btn-outline-danger resetForm">
                    <i class="fa fa-repeat"></i> Reset
                  </button>
                </div>
              </div>
              <div class="block-content">
                <div class="row push">
                  <div class="col-lg-12">

                    <div class="form-group form-row">
                      <div class="col-4">
                        <label for="employesid">Employee <span class="text-danger">*</span></label>
                        <select class="js-select2 form-control" id="employesid" name="employesid" style="width: 100%;" data-placeholder="Choose one.." onchange="changeEmployee(this)" required>
                          <option></option>
                          @foreach($employes as $employe)
                            <option value="{{ $employe->id }}" data-depart="{{ $employe->departamentid }}">{{ $employe->code }}-{{ $employe->firstname }} {{ $employe->lastname }}-{{ $employe->numbersocial }}</option>
                          @endforeach
                        </select>
                        <span id="depart"></span>
                        <span id="super"></span>
                      </div>
                      <input type="hidden" name="motiveid" value="36">
                       {{--<div class="col-4">
                        <label for="motiveid">Type of Violation<span class="text-danger">*</span></label>
                        <select class="js-select2 form-control motiveid" id="motiveid" name="motiveid" style="width: 100%;" data-placeholder="Choose one.." onchange="changeMotive(this);setRelation(this);" required>
                          <!-- <option></option> -->
                          @foreach($motives as $motive)
                            <option value="{{ $motive->id }}" data-id="{{ $motive->id }}">{{ $motive->description }} - {{ $motive->code }}</option>
                          @endforeach
                        </select>
                        <span id="idmotive"></span>
                      </div>--}}

                      <div class="col-4">
                        <label for="violation_id">Personal Time<span class="text-danger">*</span></label>
                        <select class="js-select2 form-control violation_id" id="violation_id" name="violation_id" style="width: 100%;" data-placeholder="Choose one.." required onchange="changeViolation(this)">
                          <option></option>
                          @foreach($violations as $violation)
                            <option value="{{ $violation->id }}" data-point="{{ $violation->points }}">{{ $violation->description }}-{{ $violation->code }} {{ $violation->points }}</option>
                          @endforeach
                        </select>
                        <strong><span id="point"></span></strong>
                      </div>
                      <div class="col-4">
                        <label for="">Violation Date<span class="text-danger">*</span></label>
                        <div class="input-daterange input-group" data-date-format="mm/dd/yyyy" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                            <input type="text" class="form-control" id="fecha_init" name="fecha_init" placeholder="From" data-week-start="1" data-autoclose="true" data-today-highlight="true" required value="{{ date('m/d/Y') }}" onchange="validateFecha();">
                            <div class="input-group-prepend input-group-append">
                                <span class="input-group-text font-w600">
                                    <i class="fa fa-fw fa-arrow-right"></i>
                                </span>
                            </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group form-row">
                      <div class="col-4">
                        <label for="hora_init">Violation Hour</label>
                        <input type="text" class="form-control" id="hora_init" name="hora_init" placeholder="00:00">
                        <strong><span id="horas_justificacion_real"></span></strong>
                      </div>
                      <div class="col-4" id="statusid">
                        <label for="status_id">Status <span class="text-danger">*</span></label>
                        <select class="js-select2 form-control status_id" id="status_id" name="status_id" style="width: 100%;" data-placeholder="Choose one.." required>
                          <option></option>
                          @foreach($status as $stat)
                            <option value="{{ $stat->id }}" @if($stat->id == 5) selected @endif>{{ $stat->description }}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-8">
                        <label for="comment">Comments</label>
                        <textarea class="js-maxlength form-control" id="comment" name="comment" rows="3" maxlength="100" placeholder="Write the comments you consider necessary.." data-always-show="true"></textarea>
                      </div>

                      <div class="col-4" style="text-align: -webkit-center;margin: auto;" id="call">
                          <div class="custom-control custom-switch custom-control-info custom-control-lg mb-2">
                              <input type="checkbox" class="custom-control-input" id="no_call_no" name="no_call_no" value="" onchange="nocall(this);">
                              <label class="custom-control-label" for="no_call_no">No Call, No Show</label>
                          </div>
                            <br>
                          <div class="col-8" id="file_upload">
                              <label for="file_upload">This Employee required note official</label>
                                <input type="file" name="file" class="form-control" >
                          </div>

                      </div>

                    </div>

                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
        {{-- End Tab 1--}}

        {{-- Tab 2--}}
        <div class="tab-pane fade" id="btabs-animated-fade-list" role="tabpanel" style="overflow-x: auto;">
          <div class="col-md-auto">
            <form method="get" action="#" id="search1">
              <div class="form-group form-row">
                <div class="col-4">
                  <label for="">Attendance Date</label>
                  <div class="input-daterange input-group" data-date-format="mm/dd/yyyy" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                    <input type="text" class="form-control" id="desde" placeholder="From" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                    <div class="input-group-prepend input-group-append">
                      <span class="input-group-text font-w600">
                        <i class="fa fa-fw fa-arrow-right"></i>
                      </span>
                    </div>
                   <!--  <input type="text" class="form-control" id="hasta" placeholder="To" data-week-start="1" data-autoclose="true" data-today-highlight="true"> -->
                  </div>
                </div>
                <div class="col-4">
                  <label for="employesid">Employee</label>
                  <select class="js-select2 form-control" id="empleado" style="width: 100%;" data-placeholder="Choose one.." onchange="changeEmployee(this)" required>
                    <option></option>
                    @foreach($employes as $employe)
                      <option value="{{ $employe->id }}" data-depart="{{ $employe->departamentid }}">{{ $employe->code }}-{{ $employe->firstname }} {{ $employe->lastname }}-{{ $employe->numbersocial }}</option>
                    @endforeach
                  </select>
                </div>
                
              </div>
              <div class="form-group form-row">
                <div class="col-4">
                  <label for="violation_id">Personal Time</label>
                    <select class="js-select2 form-control" id="violacion" style="width: 100%;" data-placeholder="Choose one.." required onchange="changeViolation(this)">
                      <option></option>
                      @foreach($violations as $violation)
                        <option value="{{ $violation->id }}" data-point="{{ $violation->points }}">{{ $violation->code }}-{{ $violation->description }}</option>
                      @endforeach
                    </select>
                </div>
                <div class="col-4" id="staid">
                  <label for="estatus">Status</label>
                  <select class="js-select2 form-control" id="estatus" style="width: 100%;" data-placeholder="Choose one.." required>
                    <option></option>
                    @foreach($status as $stat)
                      <option value="{{ $stat->id }}">{{ $stat->description }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="col-2" style="padding-top: 28px">
                  <button type="button" class="btn btn-hero-info" onclick="searchTable1();">
                    <i class="fa fa-fw fa-search mr-1"></i> Search
                  </button>
                </div>
                <div class="col-2" style="padding-top: 28px">
                  <button type="button" class="btn btn-hero-secondary" onclick="resetAll();">
                    <i class="fa fa-fw fa-redo-alt mr-1"></i> reset
                  </button>
                </div>
              </div>
            </form>
          </div>
          <br>
          <br>
          <div class="row">
            <div class="col-md-4 offset-md-8">
              <a class="btn btn-rounded btn-outline-success opend">
                <img src="{{ asset('images/details_open.png') }}"> Expand all
              </a>
              <a class="btn btn-rounded btn-outline-danger closed">
                <img src="{{ asset('images/details_close.png') }}"> Collapse all
              </a>
            </div>
          </div>

          <br>
          <table class="table table-bordered table-striped table-vcenter tabladata" style="width: 100%" >
            <thead>
              <tr>
                <th>Employee</th>
                <th>Violation</th>
                <th>Date</th>
                <th>Hour</th>
                <th>Status</th>
                <th>More</th>
              </tr>
            </thead>
          </table>
        </div>
        {{-- End Tab 2--}}

      </div>
    </div>
  </div>

  <!-- From Left Block Modal -->
  <div class="modal fade" id="approveModal" role="dialog" aria-labelledby="modal-block-fromleft" aria-hidden="true">
    <div class="modal-dialog modal-dialog-fromleft" role="document">
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark" style="background-color: #00703c !important;">
            <h3 class="block-title">Attendance Process</h3>
            <div class="block-options">
              <button type="button" class="btn-block-option close-modal" aria-label="Close">
                <i class="fa fa-fw fa-times"></i>
              </button>
            </div>
          </div>
          <form action="absences_aprob" method="post" onsubmit="return false;" id="absenceAprob">
            <div class="block-content">
              <input type="hidden" id="id2" name="id">
              <input type="hidden" id="points" name="points">
              <div class="form-group">
                <label for="status_id">Status <span class="text-danger">*</span></label>
                <select class="js-select2 form-control" id="status_id2" name="status_id" style="width: 100%;" data-placeholder="Choose one.." required>
                  <option></option>
                    @foreach($status as $stat)
                     
                        <option value="{{ $stat->id }}">{{ $stat->description }}</option>
                    
                    @endforeach
                </select>
              <div>
              <div class="form-group">
                <label for="note_aprov">Note</label>
                <input type="text" class="form-control" id="note_aprov" name="note_aprov" placeholder="Note">
              </div>
            </div>
            <div class="block-content block-content-full text-right ">
              <a type="button" class="btn btn-sm btn-hero-light close-modal">Close</a>
              <button type="submit" class="btn btn-sm btn-hero-primary">Approve</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- END From Left Block Modal -->

  <script>

    var tabla1;
    var urlTabla1 = "violationlist";

    $(function() {
      tabla1 = $('.tabladata').DataTable({
        processing: true,
        serverSide: true,
        ajax: urlTabla1,
        dom: 'Bfrtip',
        buttons: [
           'copy', 'excel', 'csv', 'pdf', 'print'
        ],
        columns: [
          { data: 'employee', name: 'employee' },
          { data: 'violation', name: 'violation' },
          { data: 'desde', name: 'desde' },
          { data: 'hora', name: 'hora' },
          { data: 'estatus', name: 'estatus' },
          {
            "className":      'details-control',
            "orderable":      false,
            "data":           null,
            "defaultContent": '',
            "searchable": false
          }
        ],
        "order": [[ 2, "desc" ]],
        "createdRow": function( row, data, dataIndex ) {
          if(data.status_id == "3") {
            $(row).addClass('timeleft');
          }
          if($.inArray(data.status_id,["1","6","7"])) {
            $(row).addClass('approved');
          }
          if(data.status_id == 2) {
            $(row).addClass('reject');
          }

          if(data.status_id == 5) {
            $(row).addClass('pending');
          }
        }
      });

      $('.tabladata tbody').on('click', 'td.details-control', async function () {
        var tr = $(this).closest('tr');
        var row = tabla1.row(tr);
        console.log(row);
        if ( row.child.isShown() ) {
          row.child.hide();
          tr.removeClass('shown');
          return;
        }
        let tab = await detalle(row.data());
        row.child(tab).show();
        tr.addClass('shown');
      });

    });

    $(document).ready(function() {
      $('#reasonDiv').hide();
      $('#statusid').hide();
      $('#staid').hide();
      $('#call').hide();
      $('#file_upload').hide();
      $('#status').hide();
      $("#body").attr('onbeforeunload', 'HandleBackFunctionality()');
      $('#hora_init').mdtimepicker({
        theme: 'green',// 'red', 'purple', 'indigo', 'teal', 'green', 'dark'
        readOnly: false,
        hourPadding: false,
        clearBtn: true
      });
      $('#hora_init').val(moment.utc().add('hours',-4).format('HH:mm'));


    });

    $('.resetForm').on('click',async function () {
      $('#supervisor').val('');
      $('#id').val('');
      $('#employesid').val('').change();
      $('#motiveid').val('').change();
      // $('#motiveid2').val('').change();
      // $('#reason').val('');
      $('#violation_id').val('').change();
      $('#point').html('');
      //$('#days').val('');
      $('#fecha_init').val('');
      // $('#fecha_end').val('');
      $('#hora_init').val('');
      // $('#hora_end').val('');
      $('#status_id').val('').change();
      $('#comment').val('');
      // $('#required_note_oficial').val('');
      $('#horas_justificacion_real').html('');
      $('#no_call_no').val('');
      $('#depart').html('');
      $('#super').html('');
      $('#reasonDiv').hide();
    });

    $('.opend').on('click',async function () {
     tabla1.rows(':not(.parent)').nodes().to$().find('td.details-control').trigger('click');
    });

    $('.closed').on('click',async function () {
      tabla1.rows(':not(.parent)').nodes().to$().find('td.details-control').trigger('click');
    });

    $('#days').change(function(){
      setDate();
    });

    $('#days').keyup(function(){
      setDate();
    });

    $('#hora_init').keyup(calculardiferencia);

    //$('#hora_end').keyup(calculardiferencia);

    $('#absenceAprob').on('submit',async function () {
      try{
        let params = {
            points : $('#points').val(),
            status : $('#status_id2').find(':selected').val(),
            note : $('#note_aprov').val(),
        };

        let resp = await request(`absences/aprove/${$('#id2').val()}`,'post',params);
        if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
        searchTable1();
        $('#approveModal').modal('hide');
      }catch (error) {
        Swal.fire('Operation failed',error.msg,error.status);
      }
    });

    $('.close-modal').on('click',function() {
        $('#approveModal').modal('hide');
    });

    function resetAll(){
      $('#desde').val('');
      $('#hora').val('');
      $('#empleado').val('').change();
      $('#motiveid').val('').change();
      $('#violacion').val('').change();
      $('#estatus').val('').change();
      searchTable1();
    }

    async function editAbsence(sender){
      $("#form").trigger("click");
      $('#label').html("Violation Absences edit");
      let id = $(sender).data('id');
      $('#id').val(id);
      let absences = @json($absences);
      absences.forEach((element) =>{
        if(id == element.id){
          $('#employesid').val(element.employesid).trigger('change');
          $('#motiveid').val(element.motiveid).trigger('change');
          // $('#motiveid2').val(element.motiveid2).trigger('change');
          $('#status_id').val(element.status_id).trigger('change');
          // $('#reason').val(element.reason);
          $('#violation_id').val(element.violation_id).trigger('change');
          $('#fecha_init').val((element.fecha_init != '') ? moment(element.fecha_init).format('MM/DD/YYYY') : '');
          // $('#fecha_end').val((element.fecha_end != '') ? moment(element.fecha_end).format('MM/DD/YYYY') : '');
          // $('#hora_end').val(element.hora_end);
          $('#hora_init').val(element.hora_init);
          $('#comment').val(element.comment);
          if(element.no_call_no == 0){
             $("input:checkbox[value='0']").prop('checked',true);
          }else{
             $("input:checkbox[value='']").prop('checked',false); 
          }
          changeDate();
          calculardiferencia();
          changeViolation($('#violation_id'));
        }
      });
    }

    async function deleteAbsence(sender) {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(sender).data('id');
            let resp = await request(`absences/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            searchTable1();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    }

    function setDate() {
      if($('#days').val() == ""){
        $('#fecha_init').val($('#fecha_init').val());
        return;
      }
      let inicio = moment($('#fecha_init').val(),'MM/DD/YYYY').format('YYYY-MM-DD');
      let fechaI = moment(inicio);

      date = fechaI.add($('#days').val(), 'd').format('MM/DD/YYYY');
      //$('#fecha_end').val(date);
    }

    async function request(url,type, params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

    function changeMotive(sender){
      if($(sender).val() == 4){
          $('#reasonDiv').show();

          return;
      }
      $('#reasonDiv').show();
       let note = $(sender).find(':selected').data('note');
      if (note == 'Si') {
         $('#file_upload').show();
      }else{
        $('#file_upload').hide();
      }
    }

    function setRelation(sender) {
      
      
    }

    function nocall(sender) {
      //let sender=31;
     // console.log("llego");
      if ($("input:checkbox[value='0']").prop('checked',true)) {
        //alert("EN DESARROLLO");
           let vio = @json($violation_nocall);

          $('#violation_id option').remove();
          vio.forEach((element) =>{
          //   console.log(element.id);
            if(element.id == 12){
              $("#violation_id option[value='12']").attr("selected", true);
              $('#violation_id').append(`<option value="12">${element.description}</option>}`);
              $('#violation_id').attr('readonly', true);
            }
          });

      }else{
        alert("2");
      }
    }

    function pasarValor(sender){
     
       $('#comment').val($(sender).val()+' -');
      //$('#reasonDiv').hide();
    }

    function changeViolation(sender){
      let points = $(sender).find(':selected').data('point');
      $('#point').html(`Points: ${points}`);
    }

    function changeEmployee(sender) {
      $('#depart').text('');
      $('#super').text('');
      $('#supervisor').val('');

      let departamento = @json($departaments);
      let employes = @json($employes);
      let supervisor = '';
      departamento.forEach((element) =>{
        if($(sender).find(':selected').data('depart') == element.id){
          $('#depart').text(`(${element.description})`);
          supervisor = element.employesid;
          $('#supervisor').val(element.employesid);
        }
      });
      employes.forEach((element) =>{
        if(supervisor == element.id){
          $('#super').text(`(${element.firstname} ${element.lastname})`);
        }
      });
    }

    function changeDate() {
      let inicio = moment($('#fecha_init').val(),'MM/DD/YYYY').format('YYYY-MM-DD');
      let fin = moment($('#fecha_init').val(),'MM/DD/YYYY').format('YYYY-MM-DD');
      let fechaI = moment(inicio);
      let fechaF = moment(fin);
      let dias = (fechaF.diff(fechaI, 'days') == 0) ? 1 : fechaF.diff(fechaI, 'days');
      $('#days').val(dias);
      //$('#days').val(fechaF.diff(fechaI, 'days'));
    }

    function calculardiferencia(){
      let hora_inicio = $('#hora_init').val();
      let hora_final = $('#hora_init').val();

      // Expresión regular para comprobar formato
      let formatohora = /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/;

      // Si algún valor no tiene formato correcto sale
      if (!(hora_inicio.match(formatohora) && hora_final.match(formatohora))) return;

      // Calcula los minutos de cada hora
      let minutos_inicio = hora_inicio.split(':').reduce((p, c) => parseInt(p) * 60 + parseInt(c));
      let minutos_final = hora_final.split(':').reduce((p, c) => parseInt(p) * 60 + parseInt(c));

      // Si la hora final es anterior a la hora inicial sale
      if (minutos_final < minutos_inicio) return;

      // Diferencia de minutos
      let diferencia = minutos_final - minutos_inicio;

      // Cálculo de horas y minutos de la diferencia
      let horas = Math.floor(diferencia / 60);
      let minutos = diferencia % 60;

      $('#horas_justificacion_real').html(`Hours total: ${horas}:${(minutos < 10 ? '0' : '')}${minutos}`);
}

    function detalle(data) {
      return new Promise((resolve,reject)=>{
        let points = 0;
        let absenc = @json($violation_nocall);
        console.log(absenc);
        absenc.forEach((element) =>{
          if(data.violation_id == element.id){
            points = element.points;
          }
        });

        let detalle = `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
          // <tr width="10%">
          //   <td class="text-center">Days:</td>
          //   <td>${data.days}</td>
          // </tr>
          <tr width="10%">
            <td class="text-center">Deduction Point:</td>
            <td>${points}</td>
          </tr>
        </table>`;

        let horaInit = `<p><strong>Hour Start: </strong>${(data.hora_init) ? data.hora_init : ''}</p>`;
        let comment = `<p><strong>Comment: </strong>${(data.comment) ? data.comment : ''}</p>`;

        detalle += `<tr width="40%">
          <td class="text-center">action:</td>`;

        let button1 = `<a data-toggle="tooltip" data-animation="true" data-placement="top" title="Approve" data-id="${data.id}" data-point="${data.points}" onclick=\"aprobar(this);\" >
          <img src="{{ asset('images/aproval.png') }}" style="width: 32px !important;"></a>`;

        let button2 = `<a data-toggle="tooltip" data-animation="true" data-placement="top" title="Reject" data-id="${data.id}" onclick=\"rechazar(this);\">
          <img src="{{ asset('images/rejected.png') }}" style="width: 51px !important; margin-top: -5px !important;"></a>`;

        let button3 = `<a onclick=\"editAbsence(this);\" data-toggle="tooltip" data-animation="true" data-placement="top" title="Edit" data-id="${data.id}">
          <img src="{{ asset('images/edit.png') }}" style="width: 32px !important;"></a>`;

        let button4 = `<a onclick=\"deleteAbsence(this)\" data-toggle="tooltip" data-animation="true" data-placement="top" title="Delete" data-id="${data.id}">
          <img src="{{ asset('images/delete.png') }}" style="width: 30px !important; margin-left : 15px !important;"></a>`;

        detalle = `
          <div class="row">
            <div class="col-lg-4">
              <div class="block block-rounded block-bordered">
                <div class="block-content">
                  <p><strong>Deduction Point: </strong>${points}</p>
                  ${(data.hora_init) ? horaInit : ''}
                  ${(data.comment) ? comment : ''}
                </div>
              </div>
            </div>
        `;

        if($.inArray(data.status_id,[1,6,7])){
          detalle += `<div class="col-lg-1 text-center">
                        <p>${button1}</p>
                      </div>`;
        }

        if(data.status_id != 2){
          detalle += `<div class="col-lg-1 text-center">
                        <p>${button2}</p>
                      </div>`;
        }


        detalle += `<div class="col-lg-1 text-center">
                      <p>${button3}</p>
                    </div>`;
        detalle += `<div class="col-lg-1 text-center">
                      <p>${button4}</p>
                    </div>`;

        detalle += `</div>`;

        resolve(detalle);
      });
    }

    function searchTable1(){
      urlTabla1 +=`?desde=${$('#desde').val()}&empleado=${$('#empleado').find(':selected').val()}&violacion=${$('#violacion').find(':selected').val()}`;

      console.log(urlTabla1);
      tabla1.ajax.url( urlTabla1 ).load();
    }

    async function aprobar(sender) {
      $('#id2').val($(sender).data('id'));
      $('#points').val($(sender).data('point'));
      $('#status_id2').val('').change();
      $('#note_aprov').val('');
      $('#approveModal').modal('show');
    }

    async function rechazar(sender) {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, Reject it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(sender).data('id');
            let resp = await request(`absences/reject/${id}`,'post',{});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            searchTable1();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    }

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

    function validateFecha() {
      let fechaI = moment($('#fecha_init').val());
      let fechaF = moment();

      if(!fechaF.isAfter(fechaI)){
        Swal.fire('Warning','The date of the violation cannot be greater than the current date','warning');
        $('#fecha_init').val('');
      }
    }

  </script>
@endsection