@extends('layouts.backend')

@section('content')
<div class="content">

  <div class="block block-rounded block-bordered">
    <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" href="#btabs-animated-fade-form" id="form">TimeSheet</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#btabs-animated-fade-list" id="list">TimeSheet list</a>
      </li>
    </ul>
    <div class="block-content tab-content overflow-hidden">

      {{-- Tab 1--}}
      <div class="tab-pane fade show active" id="btabs-animated-fade-form" role="tabpanel">
        <form action="timesheet" method="post">
          {{ csrf_field() }}
          <!-- <input type="hidden" name="supervisor" id="supervisor" value=""> -->
          <input type="hidden" name="id" id="id" value="">
          <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
              <div class="block-options">
                <button type="submit" class="btn btn-sm btn-outline-primary">
                  <i class="fa fa-check"></i> Save
                </button>
                <button type="reset" class="btn btn-sm btn-outline-danger">
                  <i class="fa fa-repeat"></i> Reset
                </button>
              </div>
            </div>
            <div class="block-content">
              <div class="row push">
                <div class="col-lg-12">

                  <div class="form-group form-row">
                    <div class="col-4">
                      <label for="employesid">Employee <span class="text-danger">*</span></label>
                      <select class="js-select2 form-control" id="employesid" name="employesid" style="width: 100%;" data-placeholder="Choose one.." onchange="changeEmployee(this)" required>
                        <option></option>
                        @foreach($employes as $employe)
                          <option value="{{ $employe->id }}" data-depart="{{ $employe->departamentid }}">{{ $employe->code }}-{{ $employe->firstname }} {{ $employe->lastname }}</option>
                        @endforeach
                      </select>
                      <span id="depart"></span>
                      <span id="super"></span>
                    </div>
                  <div class="col-8">
                      <label for="">TimeSheet Date <span class="text-danger">*</span></label>
                      <div class="input-daterange input-group" data-date-format="mm/dd/yyyy" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                          <input type="text" class="form-control" id="timesheet_date" name="timesheet_date" placeholder="From" data-week-start="1" data-autoclose="true" data-today-highlight="true" required value="{{ date('m/d/Y') }}" disabled="true">
                          <div class="input-group-prepend input-group-append">
                              <span class="input-group-text font-w600">
                                  <i class="fa fa-fw fa-arrow-right"></i>
                              </span>
                          </div>
                   <!--        <input type="text" class="form-control" id="fecha_end" name="fecha_end" placeholder="To" data-week-start="1" data-autoclose="true" data-today-highlight="true" required value="{{ date('m/d/Y') }}" onchange="changeDate();"> -->
                      </div>
                    </div>
                    

                  <div class="form-group form-row">
                    <div class="col-4">
                      <label for="hour_start">Start</label>
                      <input type="text" class="js-masked-time form-control" id="hour_start" name="hour_start" placeholder="00:00">
                     <!--  <strong><span id="horas_justificacion_real"></span></strong> -->
                    </div>
                    <div class="col-4">
                      <label for="hour_stop">Stop</label>
                      <input type="text" class="js-masked-time form-control" id="hour_stop" name="hour_stop" placeholder="00:00">
                    </div>
                    
                  </div>

                    <div class="form-group form-row">
                      <div class="col-8">
                        <label for="comments">Comments</label>
                        <textarea class="js-maxlength form-control" id="comments" name="comments" rows="3" cols="200" maxlength="100" placeholder="It even works on textareas.." data-always-show="true"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      {{-- End Tab 1--}}

      {{-- Tab 2--}}
      <div class="tab-pane fade" id="btabs-animated-fade-list" role="tabpanel" style="overflow-x: auto;">
      <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination" style="width: 100%">
          <thead>
            <tr>
              <th>Employe</th>
              <th>TimeSheet Date</th>
              <th>Hour Start</th>
              <th>Hour Stop</th>
              <th>Comments</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            @foreach($timesheet as $timesheet)
              @if($timesheet->employee()->idsite != Auth::user()->site_id)
                @if(Auth::user()->site_id != 4)
                  @continue
                @endif
              @endif
              <tr id="trrol_{{ $timesheet->id }}" class="text-left">
                <td>{{ $timesheet->employee()->firstname }} {{ $timesheet->employee()->lastname }}</td>
                <td>{{ \Carbon\Carbon::parse($timesheet->timesheet_date)->format('m/d/Y') }}</td>
                <td>{{ $timesheet->hour_start }}</td>
                <td>{{ $timesheet->hour_stop }}</td>
                <td>{{ $timesheet->comments }}</td>
                <td class="d-none d-sm-table-cell">
                  <a class="link-fx font-w300 font-size-h1 edit" data-toggle="tooltip" data-animation="true" data-placement="top" title="Edit" data-id="{{ $timesheet->id }}">
                    <img src="{{ asset('images/edit.png') }}" width="15%">
                  </a>
                  <a class="link-fx font-w300 font-size-h1 delete" data-toggle="tooltip" data-animation="true" data-placement="top" title="Delete" data-id="{{ $timesheet->id }}">
                    <img src="{{ asset('images/delete.png') }}" width="15%" style="margin-left: 7px;">
                  </a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      {{-- End Tab 2--}}

    </div>
  </div>
</div>

  <script>
    $(document).ready(function() {
      $('#reasonDiv').hide();
      $("#body").attr('onbeforeunload', 'HandleBackFunctionality()');
    });

    


    $('.addnew').on('click',async function () {
      $('#label').html("Add New TimeSheet");
      $('#id').val('');
      $('#employesid').val('');
      $('#timesheet_date').val('');
      $('#hour_start').val('');
      $('#hour_stop').val('');
      $('#comments').val('');
       // $('#required_note_oficial').val('');

    });

    $('.edit').on('click',async function () {
      $("#form").trigger("click");
      $('#label').html("TimeSheet edit");
      let id = $(this).data('id');
      $('#id').val(id);
      let timesheets = @json($timesheetss);
      timesheets.forEach((element) =>{
        if(id == element.id){
          $('#employesid').val(element.employesid).trigger('change');
          $('#timesheet_date').val((element.timesheet_date != '') ? moment(element.timesheet_date).format('MM/DD/YYYY') : '');
          $('#hour_start').val(element.hour_start);
          $('#hour_stop').val(element.hour_stop);
          $('#comments').val(element.comments);
          //changeDate();
        }
      });
    });

    $('.delete').on('click',async function () {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(this).data('id');
            let resp = await request(`timesheet/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            $(`#trrol_${id}`).remove();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    });

    async function request(url,type, params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }


    function changeEmployee(sender) {
      $('#depart').text('');
      $('#super').text('');
      $('#supervisor').val('');

      let departamento = @json($departaments);
      let employes = @json($employes);
      let supervisor = '';
      departamento.forEach((element) =>{
        if($(sender).find(':selected').data('depart') == element.id){
          $('#depart').text(`(${element.description})`);
          supervisor = element.employesid;
          $('#supervisor').val(element.employesid);
        }
      });
      employes.forEach((element) =>{
        if(supervisor == element.id){
          $('#super').text(`(${element.firstname} ${element.lastname})`);
        }
      });
    }

   

    function HandleBackFunctionality(e){
        if(window.event)
    {
          if(window.event.clientX < 40 && window.event.clientY < 0)
         {

         }
         else
         {
             document.getElementById("body").addEventListener("onbeforeunload", function(event){
              event.preventDefault()
              });

         }
     }
     else
     {
          if(event.currentTarget.performance.navigation.type == 1)
         {

         }
         if(event.currentTarget.performance.navigation.type == 2)
        {

        }
     }
    }

   

  </script>
@endsection