@extends('layouts.backend')
<style>
  div.dataTables_wrapper div.dataTables_processing {
    position: absolute;
    top: 30%;
    left: 50%;
    width: 30%;
    height: 64px;
    margin-left: -20%;
    margin-top: -25px;
    padding-top: 20px;
    text-align: center;
    font-size: 1.2em;
    background:#00703c;
    color: mintcream;
    border-radius: 10px;
  }
  td.details-control {
    background: url('{{ asset("images/details_open.png") }}') no-repeat center center;
    cursor: pointer;
  }
  tr.shown td.details-control {
    background: url('{{ asset("images/details_close.png") }}') no-repeat center center;
  }
  .timeleft {
    background-color: white !important;
    color: #FFB119;
  }
  .approved {
    background-color: white !important;
    color: green;
  }
  .reject {
    background-color: white !important;
    color: red;
  }
  .pending {
    background-color: white !important;
    color: #3C90DF;
  }
</style>

@section('content')
  <div class="content">
  </div>
  
  <script>

    var tabla1;
    var urlTabla1 = "absencelist";

    $(function() {
      tabla1 = $('.tabladata').DataTable({
        processing: true,
        serverSide: true,
        ajax: urlTabla1,
        dom: 'Bfrtip',
        buttons: [
           'copy', 'excel', 'csv', 'pdf', 'print'
        ],
        columns: [
          { data: 'employee', name: 'employee' },
          { data: 'reasons', name: 'reasons' },
          { data: 'desde', name: 'desde' },
          { data: 'hasta', name: 'hasta' },
          { data: 'estatus', name: 'estatus' },
          {
            "className":      'details-control',
            "orderable":      false,
            "data":           null,
            "defaultContent": '',
            "searchable": false
          }
        ],
        "order": [[ 2, "desc" ]],
        "createdRow": function( row, data, dataIndex ) {
          if(data.status_id == "3") {
            $(row).addClass('timeleft');
          }
          if($.inArray(data.status_id,["1","6","7"])) {
            $(row).addClass('approved');
          }
          if(data.status_id == 2) {
            $(row).addClass('reject');
          }

          if(data.status_id == 5) {
            $(row).addClass('pending');
          }
        }
      });

      $('.tabladata tbody').on('click', 'td.details-control', async function () {
        var tr = $(this).closest('tr');
        var row = tabla1.row(tr);

        if ( row.child.isShown() ) {
          row.child.hide();
          tr.removeClass('shown');
          return;
        }
        let tab = await detalle(row.data());
        row.child(tab).show();
        tr.addClass('shown');
      });

    });

    $(document).ready(function() {
      $('#reasonDiv').hide();
      $('#file_upload').hide();
      $('#status').hide();
      $("#body").attr('onbeforeunload', 'HandleBackFunctionality()');
    });

    $('.resetForm').on('click',async function () {
      $('#supervisor').val('');
      $('#id').val('');
      $('#employesid').val('').change();
      $('#motiveid').val('').change();
      $('#motiveid2').val('').change();
      $('#reason').val('');
      $('#violation_id').val('').change();
      $('#point').html('');
      $('#days').val('');
      $('#fecha_init').val('');
      $('#fecha_end').val('');
      $('#hora_init').val('');
      $('#hora_end').val('');
      $('#status_id').val('').change();
      $('#comment').val('');
      $('#required_note_oficial').val('');
      $('#horas_justificacion_real').html('');
      $('#no_call_no').val('');
      $('#depart').html('');
      $('#super').html('');
      $('#reasonDiv').hide();
    });

    $('.opend').on('click',async function () {
     tabla1.rows(':not(.parent)').nodes().to$().find('td.details-control').trigger('click');
    });

    $('.closed').on('click',async function () {
      tabla1.rows(':not(.parent)').nodes().to$().find('td.details-control').trigger('click');
    });

    $('#days').change(function(){
      setDate();
    });

    $('#days').keyup(function(){
      setDate();
    });

    $('#hora_init').keyup(calculardiferencia);

    $('#hora_end').keyup(calculardiferencia);

    $('#absenceAprob').on('submit',async function () {
      try{
        let params = {
            points : $('#points').val(),
            status : $('#status_id2').find(':selected').val(),
            note : $('#note_aprov').val(),
        };

        let resp = await request(`absences/aprove/${$('#id2').val()}`,'post',params);
        if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
        searchTable1();
        $('#approveModal').modal('hide');
      }catch (error) {
        Swal.fire('Operation failed',error.msg,error.status);
      }
    });

    $('.close-modal').on('click',function() {
        $('#approveModal').modal('hide');
    });

    function resetAll(){
      $('#desde').val('');
      $('#hasta').val('');
      $('#empleado').val('').change();
      $('#razon').val('').change();
      $('#violacion').val('').change();
      $('#estatus').val('').change();
      searchTable1();
    }

    async function editAbsence(sender){
      $("#form").trigger("click");
      $('#label').html("Employe edit");
      let id = $(sender).data('id');
      $('#id').val(id);
      let absences = @json($absences);
      absences.forEach((element) =>{
        if(id == element.id){
          $('#employesid').val(element.employesid).trigger('change');
          $('#motiveid').val(element.motiveid).trigger('change');
          $('#motiveid2').val(element.motiveid2).trigger('change');
          $('#status_id').val(element.status_id).trigger('change');
          $('#reason').val(element.reason);
          $('#violation_id').val(element.violation_id).trigger('change');
          $('#fecha_init').val((element.fecha_init != '') ? moment(element.fecha_init).format('MM/DD/YYYY') : '');
          $('#fecha_end').val((element.fecha_end != '') ? moment(element.fecha_end).format('MM/DD/YYYY') : '');
          $('#hora_end').val(element.hora_end);
          $('#hora_init').val(element.hora_init);
          $('#comment').val(element.comment);
          if(element.no_call_no == 0){
             $("input:checkbox[value='0']").prop('checked',true);
          }else{
             $("input:checkbox[value='']").prop('checked',false); 
          }
          changeDate();
          calculardiferencia();
          changeViolation($('#violation_id'));
        }
      });
    }

    async function deleteAbsence(sender) {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(sender).data('id');
            let resp = await request(`absences/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            searchTable1();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    }

    function setDate() {
      if($('#days').val() == ""){
        $('#fecha_end').val($('#fecha_init').val());
      //  console.log($('#fecha_end').val($('#fecha_init').val()));
        return;
      }
      let inicio = moment($('#fecha_init').val(),'MM/DD/YYYY').format('YYYY-MM-DD');
      let fechaI = moment(inicio);
       //console.log(fechaI);

      date = fechaI.add($('#days').val(), 'd').format('MM/DD/YYYY');
       //console.log(date);
      $('#fecha_end').val(date);
    }

    async function request(url,type, params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

    function changeMotive(sender){
      if($(sender).val() == 4){
          $('#reasonDiv').show();

          return;
      }
      $('#reasonDiv').show();
       let note = $(sender).find(':selected').data('note');
      if (note == 'Si') {
         $('#file_upload').show();
      }else{
        $('#file_upload').hide();
      }
    }

    function nocall(sender) {
      //let sender=31;
     // console.log("llego");
      if ($("input:checkbox[value='0']").prop('checked',true)) {
        //alert("EN DESARROLLO");
          let moti = @json($motives_nocall);

          $('#motiveid option').remove();
          moti.forEach((element) =>{
           //  console.log(element.id);
            if(element.id == 31){
              $("#motiveid option[value='31']").attr("selected", true);
              $('#motiveid').append(`<option value="31">${element.description}</option>}`);
              $('#motiveid').attr('readonly', true);
            }
          });

           let vio = @json($violation_nocall);

          $('#violation_id option').remove();
          vio.forEach((element) =>{
          //   console.log(element.id);
            if(element.id == 12){
              $("#violation_id option[value='12']").attr("selected", true);
              $('#violation_id').append(`<option value="12">${element.description}</option>}`);
              $('#violation_id').attr('readonly', true);
            }
          });

           let moti2 = @json($motives2_nocall);

          $('#motiveid2 option').remove();
          moti2.forEach((element) =>{
             console.log(element.id);
            if(element.id == 32){
              $("#motiveid2 option[value='32']").attr("selected", true);
              $('#motiveid2').append(`<option value="32">${element.description}</option>}`);
              $('#motiveid2').attr('readonly', true);
             $('#reasonDiv').show();
            }

            $('#reason').val($('select[name="motiveid2"] option:selected').text());
            $('#reason').attr('readonly', true);
          });

      }else{
        alert("2");
      }
    }

    function pasarValor(sender){
     
       $('#comment').val($(sender).val()+' -');
      //$('#reasonDiv').hide();
    }

    function changeViolation(sender){
      let points = $(sender).find(':selected').data('point');
      $('#point').html(`Points: ${points}`);
    }

    function changeEmployee(sender) {
      $('#depart').text('');
      $('#super').text('');
      $('#supervisor').val('');

      let departamento = @json($departaments);
      let employes = @json($employes);
      let supervisor = '';
      departamento.forEach((element) =>{
        if($(sender).find(':selected').data('depart') == element.id){
          $('#depart').text(`(${element.description})`);
          supervisor = element.employesid;
          $('#supervisor').val(element.employesid);
        }
      });
      employes.forEach((element) =>{
        if(supervisor == element.id){
          $('#super').text(`(${element.firstname} ${element.lastname})`);
        }
      });
    }

    function changeDate() {
      let inicio = moment($('#fecha_init').val(),'MM/DD/YYYY').format('YYYY-MM-DD');
      let fin = moment($('#fecha_end').val(),'MM/DD/YYYY').format('YYYY-MM-DD');
      let fechaI = moment(inicio);
      let fechaF = moment(fin);
      let dias = (fechaI.diff(fechaF, 'days') == 0) ? 1 : fechaF.diff(fechaI, 'days');
      console.log(fin);
      if (fin >= inicio) {
        //console.log("1");
        $('#days').val(dias+1);
      }

      if (fin == inicio) {
        //console.log("1");
        $('#days').val(1);
      }
      //$('#days').val(fechaF.diff(fechaI, 'days'));
    }

    function HandleBackFunctionality(e){
        if(window.event)
    {
          if(window.event.clientX < 40 && window.event.clientY < 0)
         {

         }
         else
         {
             document.getElementById("body").addEventListener("onbeforeunload", function(event){
              event.preventDefault()
              });

         }
     }
     else
     {
          if(event.currentTarget.performance.navigation.type == 1)
         {

         }
         if(event.currentTarget.performance.navigation.type == 2)
        {

        }
     }
    }

    function calculardiferencia(){
      let hora_inicio = $('#hora_init').val();
      let hora_final = $('#hora_end').val();

      // Expresión regular para comprobar formato
      let formatohora = /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/;

      // Si algún valor no tiene formato correcto sale
      if (!(hora_inicio.match(formatohora) && hora_final.match(formatohora))) return;

      // Calcula los minutos de cada hora
      let minutos_inicio = hora_inicio.split(':').reduce((p, c) => parseInt(p) * 60 + parseInt(c));
      let minutos_final = hora_final.split(':').reduce((p, c) => parseInt(p) * 60 + parseInt(c));

      // Si la hora final es anterior a la hora inicial sale
      if (minutos_final < minutos_inicio) return;

      // Diferencia de minutos
      let diferencia = minutos_final - minutos_inicio;

      // Cálculo de horas y minutos de la diferencia
      let horas = Math.floor(diferencia / 60);
      let minutos = diferencia % 60;

      $('#horas_justificacion_real').html(`Hours total: ${horas}:${(minutos < 10 ? '0' : '')}${minutos}`);
}

    function detalle(data) {
      return new Promise((resolve,reject)=>{
        let points = 0;
        let violations = @json($violations);
        violations.forEach((element) =>{
          if(data.violation_id == element.id){
            points = element.points;
          }
        });

        let detalle = `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
          <tr width="10%">
            <td class="text-center">Days:</td>
            <td>${data.days}</td>
          </tr>
          <tr width="10%">
            <td class="text-center">Deduction Point:</td>
            <td>${points}</td>
          </tr>
        </table>`;

        let horaInit = `<p><strong>Hour Start: </strong>${(data.hora_init) ? data.hora_init : ''}</p>`;
        let horaEnd = `<p><strong>Hour End: </strong>${(data.hora_end) ? data.hora_end : ''}</p>`;
        let comment = `<p><strong>Comment: </strong>${(data.comment) ? data.comment : ''}</p>`;
        let reason = `<p><strong>Comment: </strong>${(data.reason) ? data.reason : ''}</p>`;

        detalle += `<tr width="40%">
          <td class="text-center">action:</td>`;

        let button1 = `<a data-toggle="tooltip" data-animation="true" data-placement="top" title="Approve" data-id="${data.id}" data-point="${data.points}" onclick=\"aprobar(this);\" >
          <img src="{{ asset('images/aproval.png') }}" style="width: 32px !important;"></a>`;

        let button2 = `<a data-toggle="tooltip" data-animation="true" data-placement="top" title="Reject" data-id="${data.id}" onclick=\"rechazar(this);\">
          <img src="{{ asset('images/rejected.png') }}" style="width: 51px !important; margin-top: -5px !important;"></a>`;

        let button3 = `<a onclick=\"editAbsence(this);\" data-toggle="tooltip" data-animation="true" data-placement="top" title="Edit" data-id="${data.id}">
          <img src="{{ asset('images/edit.png') }}" style="width: 32px !important;"></a>`;

        let button4 = `<a onclick=\"deleteAbsence(this)\" data-toggle="tooltip" data-animation="true" data-placement="top" title="Delete" data-id="${data.id}">
          <img src="{{ asset('images/delete.png') }}" style="width: 30px !important; margin-left : 15px !important;"></a>`;

        detalle = `
          <div class="row">
            <div class="col-lg-4">
              <div class="block block-rounded block-bordered">
                <div class="block-content">
                  <p><strong>Days: </strong>${(data.days) ? data.days : 1}</p>
                  <p><strong>Deduction Point: </strong>${points}</p>
                  ${(data.hora_init) ? horaInit : ''}
                  ${(data.hora_end) ? horaEnd : ''}
                  ${(data.comment) ? comment : ''}
                  ${(data.reason) ? reason : ''}
                </div>
              </div>
            </div>
        `;

        if($.inArray(data.status_id,[1,6,7])){
          detalle += `<div class="col-lg-1 text-center">
                        <p>${button1}</p>
                      </div>`;
        }

        if(data.status_id != 2){
          detalle += `<div class="col-lg-1 text-center">
                        <p>${button2}</p>
                      </div>`;
        }


        detalle += `<div class="col-lg-1 text-center">
                      <p>${button3}</p>
                    </div>`;
        detalle += `<div class="col-lg-1 text-center">
                      <p>${button4}</p>
                    </div>`;

        detalle += `</div>`;

        resolve(detalle);
      });
    }

    function searchTable1(){
      urlTabla1 +=`?desde=${$('#desde').val()}&hasta=${$('#hasta').val()}&empleado=${$('#empleado').find(':selected').val()}&razon=${$('#razon').find(':selected').val()}&violacion=${$('#violacion').find(':selected').val()}&status=${$('#estatus').find(':selected').val()}`;
      tabla1.ajax.url( urlTabla1 ).load();
    }

    async function aprobar(sender) {
      $('#id2').val($(sender).data('id'));
      $('#points').val($(sender).data('point'));
      $('#status_id2').val('').change();
      $('#note_aprov').val('');
      $('#approveModal').modal('show');
    }

    async function rechazar(sender) {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, Reject it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(sender).data('id');
            let resp = await request(`absences/reject/${id}`,'post',{});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            searchTable1();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    }

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

  </script>
@@endsection