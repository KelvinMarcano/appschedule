@extends('layouts.backend')
<style>
  div.dataTables_wrapper div.dataTables_processing {
    position: absolute;
    top: 30%;
    left: 50%;
    width: 30%;
    height: 64px;
    margin-left: -20%;
    margin-top: -25px;
    padding-top: 20px;
    text-align: center;
    font-size: 1.2em;
    background:#00703c;
    color: mintcream;
    border-radius: 10px;
  }
  td.details-control {
    background: url('{{ asset("images/details_open.png") }}') no-repeat center center;
    cursor: pointer;
  }
  tr.shown td.details-control {
    background: url('{{ asset("images/details_close.png") }}') no-repeat center center;
  }
  .timeleft {
    background-color: white !important;
    color: #FFB119;
  }
  .approved {
    background-color: white !important;
    color: green;
  }
  .reject {
    background-color: white !important;
    color: red;
  }
  .pending {
    background-color: white !important;
    color: #3C90DF;
  }

.mt-100 {
    margin-top: 100px
}

/*body {
    background: #00B4DB;
    background: -webkit-linear-gradient(to right, #0083B0, #00B4DB);
    background: linear-gradient(to right, #0083B0, #00B4DB);
    color: #514B64;
    min-height: 100vh
}*/
</style>

@section('content')
  <div class="content">
    <div class="block block-rounded block-bordered">
      <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" href="#btabs-animated-fade-form" id="form">Attendance</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#btabs-animated-fade-list" id="list">Attendance List</a>
        </li>
      </ul>
      <div class="block-content tab-content overflow-hidden">

        {{-- Tab 1--}}
        <div class="tab-pane fade show active" id="btabs-animated-fade-form" role="tabpanel">
          <form action="absences" method="post" enctype="multipart/form-data" id="formAbsence">
            {{ csrf_field() }}
            <input type="hidden" name="supervisor" id="supervisor" value="">
            <input type="hidden" name="id" id="id" value="">
            <div class="block block-rounded block-bordered">
              <div class="block-header block-header-default">
                <div class="block-options">
                  <button type="submit" class="btn btn-sm btn-outline-primary" onclick="llenaRason();">
                    <i class="fa fa-check"></i> Save
                  </button>
                  <button type="reset" class="btn btn-sm btn-outline-danger resetForm">
                    <i class="si si-reload"></i> Reset
                  </button>
                </div>
                <div class="block-options">
                  <span class="badge badge-success" id="createdby">Created by:</span>
                  <span class="badge badge-primary" id="platform">Platform:</span>
                </div>
              </div>
              <div class="block-content">
                <div class="form-group form-row">
                  <div class="col-sm-4">
                    <div class="custom-control custom-switch custom-control-info custom-control-lg mb-2">
                      <input type="checkbox" class="custom-control-input" id="multiple" name="multiple" onchange="showMultiple(this);">
                      <label class="custom-control-label" for="multiple">Is it for multiple employees?</label>
                    </div>
                  </div>
                </div>
                <div class="form-group form-row">
                  <div class="col-sm-4" id="multipleDiv">
                    <label for="employesidM">Employee <span class="text-danger">*</span></label>
                    <select class="form-control select2" id="employesidM" name="employesidM[]" style="width: 100%;" data-placeholder="Choose one.." multiple>
                      <option></option>
                      @foreach($employes as $employe)
                        <option value="{{ $employe->id }}" data-depart="{{ $employe->departamentid }}">{{ $employe->code }}-{{ $employe->firstname }} {{ $employe->lastname }}-{{ $employe->numbersocial }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-sm-4" id="divEmploye">
                    <label for="employesid">Employee <span class="text-danger">*</span></label>
                    <select class="select2 form-control" id="employesid" name="employesid" style="width: 100%;" data-placeholder="Choose one.." onchange="changeEmployee(this)" required>
                      <option></option>
                      @foreach($employes as $employe)
                        <option value="{{ $employe->id }}" data-depart="{{ $employe->departamentid }}">{{ $employe->code }}-{{ $employe->firstname }} {{ $employe->lastname }}-{{ $employe->numbersocial }}</option>
                      @endforeach
                    </select>
                    <span id="depart"></span>
                    <span id="super"></span>
                  </div>
                  <div class="col-sm-4">
                    <label for="motiveid">Type of Absence <span class="text-danger">*</span></label>
                    <select class="js-select2 form-control motiveid" id="motiveid" name="motiveid" style="width: 100%;" data-placeholder="Choose one.." onchange="changeMotive(this);setRelation(this);" required>
                      <option></option>
                      @foreach($motives_catgen as $motives_catgen)
                        <option value="{{ $motives_catgen->id }}" data-id="{{ $motives_catgen->id }}">{{ $motives_catgen->description }} - {{ $motives_catgen->code }}</option>
                      @endforeach
                    </select>
                    <span id="idmotive"></span>
                  </div>
                  <div class="col-sm-4">
                    <label for="motiveid2">Reason <span class="text-danger">*</span></label>
                    <select class="select2 form-control motiveid2" id="motiveid2" name="motiveid2" style="width: 100%;" data-placeholder="Choose one.." onchange="changeMotive(this);" required>
                      <option></option>
                      @foreach($motives as $motive)
                        <option value="{{ $motive->id }}" data-note="{{ $motive->required_note }}">{{ $motive->description }} - {{ $motive->code }}</option>
                      @endforeach
                    </select>
                    <span id="note"></span>
                    <div id="reasonDiv">
                      <label for="reason">Reason for Attendance</label>
                      <input type="text" class="form-control" id="reason" name="reason" placeholder="Add your comment (detail reason).." onchange="pasarValor(this);">
                    </div>
                  </div>
                </div>
                <div class="form-group form-row">
                  @if(in_array(Auth::user()->rol_id,[5,6,11]))
                    <input type="hidden" name="violation_id" value="16">
                  @endif
                  @if(!in_array(Auth::user()->rol_id,[5,6,11]))
                    <div class="col-sm-5">
                      <label for="violation_id">Personal Time<span class="text-danger">*</span></label>
                    <!--   <label for="violation_id">Personal time / Points / Violations<span class="text-danger">*</span></label> -->
                      <select class="js-select2 form-control violation_id" id="violation_id" name="violation_id" style="width: 100%;" data-placeholder="Choose one.." required onchange="changeViolation(this,1)">
                        <option></option>
                        @foreach($violations as $violation)
                          <option value="{{ $violation->id }}" data-point="{{ $violation->points }}">{{ $violation->description }}-{{ $violation->code }} {{ $violation->points }}</option>
                        @endforeach
                      </select>
                      <strong><span id="point"></span></strong>
                    </div>
                  @endif
                  <div class="col-sm-7">
                    <label for="">Attendance Date <span class="text-danger">*</span></label>
                    <div class="input-daterange input-group" data-date-format="mm/dd/yyyy" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                        <input type="text" class="form-control" id="fecha_init" name="fecha_init" placeholder="From" data-week-start="1" data-autoclose="true" data-today-highlight="true" required value="{{ date('m/d/Y') }}" onchange="validateFecha();">
                        <div class="input-group-prepend input-group-append">
                            <span class="input-group-text font-w600">
                                <i class="fa fa-fw fa-arrow-right"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control" id="fecha_end" name="fecha_end" placeholder="To" data-week-start="1" data-autoclose="true" data-today-highlight="true" required value="{{ date('m/d/Y') }}" onchange="validateFecha(); changeDate();">
                    </div>
                  </div>
                </div>
                <div class="form-group form-row">
                  <div class="col-sm-4">
                    <label for="status_id">Days <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" id="days" name="days" min="1" required value="1">
                  </div>

                  <div class="col-sm-4">
                    <label for="hora_init">Check in Time</label>
                    <input type="text" class="form-control" id="hora_init" name="hora_init" placeholder="00:00" onchange="calculardiferencia();">
                    <strong><span id="horas_justificacion_real"></span></strong>
                  </div>
                  <div class="col-sm-4">
                    <label for="hora_end">Departure Time</label>
                    <input type="text" class="form-control" id="hora_end" name="hora_end" placeholder="00:00" onchange="calculardiferencia();">
                  </div>
                  <div class="col-sm-4" id="status">
                    <label for="status_id">Status <span class="text-danger">*</span></label>
                    <select class="js-select2 form-control status_id" id="status_id" name="status_id" style="width: 100%;" data-placeholder="Choose one.." required>
                      <option></option>
                      @foreach($status as $stat)
                        <option value="{{ $stat->id }}" @if($stat->id == 5) selected @endif>{{ $stat->description }}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group form-row">
                  <div class="col-sm-8">
                    <label for="comment">Comments</label>
                    <textarea class="js-maxlength form-control" id="comment" name="comment" rows="3" maxlength="100" placeholder="Write the comments you consider necessary.." data-always-show="true"></textarea>
                  </div>
                  <br>
                  <div class="col-sm-4" style="text-align: -webkit-center;margin: auto; ">
                    <div class="custom-control custom-switch custom-control-info custom-control-lg mb-2" style="padding-top: 28px">
                      <input type="checkbox" class="custom-control-input" id="no_call_no" name="no_call_no" value="0" onchange="nocall(this);">
                      <label class="custom-control-label" for="no_call_no">No Call, No Show</label>
                    </div>
                  </div>
                  <br>
                  <div class="col-sm-8" id="file_upload">
                    <label for="file_upload">This Employee required note official</label>
                    <input type="file" name="file" class="form-control" >
                  </div>
                </div>
                <div class="form-group form-row">
                  <div class="col-sm-4" style="padding-top: 28px">
                    <button type="submit" class="btn btn-sm btn-outline-primary" style='width: 100%' onclick="llenaRason();">
                      <i class="fa fa-check"></i> Save
                    </button>

                    <br>
                  </div>
                  <div class="col-sm-4" style="padding-top: 28px">
                    <button type="reset" class="btn btn-sm btn-outline-danger resetForm" style='width: 100%'>
                      <i class="si si-reload"></i> Reset
                    </button>
                  </div>
                </div>

              </div>
            </div>
          </form>
        </div>
        {{-- End Tab 1--}}

        {{-- Tab 2--}}
        <div class="tab-pane fade" id="btabs-animated-fade-list" role="tabpanel" {{--style="overflow-x: auto;"--}}>
          <form method="get" action="#" id="search1">
            <div class="form-group form-row">
              <div class="col-sm-4">
                <label for="employesid">Employee</label>
                <select class="js-select2 form-control" id="empleado" style="width: 100%;" data-placeholder="Choose one.." onchange="changeEmployee(this)" required>
                  <option></option>
                  @foreach($employes as $employe)
                    <option value="{{ $employe->id }}" data-depart="{{ $employe->departamentid }}">{{ $employe->code }}-{{ $employe->firstname }} {{ $employe->lastname }}-{{ $employe->numbersocial }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-sm-4">
                <label for="motiveid">Type of Reason</label>
                  <select class="js-select2 form-control" id="razon" style="width: 100%;" data-placeholder="Choose one.." onchange="changeMotive(this);" required>
                    <option></option>
                    @foreach($motives as $motive)
                      <option value="{{ $motive->id }}">{{ $motive->code }}-{{ $motive->description }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="col-sm-4">
                <label for="violation_id">Personal Time</label>
                  <select class="js-select2 form-control" id="violacion" style="width: 100%;" data-placeholder="Choose one.." required onchange="changeViolation(this,1)">
                    <option></option>
                    @foreach($violations as $violation)
                      <option value="{{ $violation->id }}" data-point="{{ $violation->points }}">{{ $violation->code }}-{{ $violation->description }}</option>
                    @endforeach
                  </select>
              </div>
            </div>
            <div class="form-group form-row">
              <div class="col-sm-4">
                <label for="estatus">Status</label>
                <select class="js-select2 form-control" id="estatus" style="width: 100%;" data-placeholder="Choose one.." required>
                  <option></option>
                  @foreach($status as $stat)
                    <option value="{{ $stat->id }}">{{ $stat->description }}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-sm-8" style="padding-top: 28px">
                <div class="row">
                  <div class="col-6" {{--style="padding-top: 28px"--}}>
                    <button type="button" class="btn btn-hero-info" onclick="searchTable1();">
                      <i class="fa fa-fw fa-search mr-1"></i> Search
                    </button>
                    <br>
                  </div>

                  <div class="col-6" {{--style="padding-top: 28px"--}}>
                    <button type="button" class="btn btn-hero-secondary" onclick="resetAll();">
                      <i class="fa fa-fw fa-redo-alt mr-1"></i> Reset
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <br>
          <br>
          <div class="row">
            <div class="col-sm-4 ml-auto" style="padding-top: 28px">
              <a class="btn btn-rounded btn-outline-success opend" style='width: 100%'>
                <img src="{{ asset('images/details_open.png') }}"> Expand all
              </a>
            </div>
            <div class="col-sm-4 mr-auto" style="padding-top: 28px">
              <a class="btn btn-rounded btn-outline-danger closed" style='width: 100%'>
                <img src="{{ asset('images/details_close.png') }}"> Collapse all
              </a>
            </div>
          </div>
          <br>
          <br>

          <div class="row">
            <div class="col-sm-12">
              <table class="table table-bordered table-striped table-vcenter tabladata" style="width: 100%" >
                <thead>
                  <tr>
                    <th>Employee</th>
                    <th>Type of Reason</th>
                    <th>Date Start</th>
                    <th>Date End</th>
                    <th>Status</th>
                    <th>More</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>

        </div>
        {{-- End Tab 2--}}

      </div>
    </div>
  </div>

  <!-- From Left Block Modal -->
  <div class="modal fade" id="approveModal" role="dialog" aria-labelledby="modal-block-fromleft" aria-hidden="true">
    <div class="modal-dialog modal-dialog-fromleft" role="document">
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark" style="background-color: #00703c !important;">
            <h3 class="block-title">Attendance Process</h3>
            <div class="block-options">
              <button type="button" class="btn-block-option close-modal" aria-label="Close">
                <i class="fa fa-fw fa-times"></i>
              </button>
            </div>
          </div>
          <form action="absences_aprob" method="post" onsubmit="return false;" id="absenceAprob">
            <div class="block-content">
              <input type="hidden" id="id2" name="id">
              <div class="form-group">
                <label for="status_id">Personal Time<span class="text-danger">*</span></label>
                <select class="js-select2 form-control" id="violation_approve" name="violation_approve" style="width: 100%;" data-placeholder="Choose one.." required onchange="changeViolation(this,2);">
                  <option></option>
                    @foreach($violations as $violation)
                      <option value="{{ $violation->id }}" data-point="{{ $violation->points }}">{{ $violation->description }}-{{ $violation->code }} {{ $violation->points }}</option>
                    @endforeach
                </select>
              <div>
              <div class="form-group" id="div_points">
                <label for="points" id="lblpoints">Points <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="points" name="points" required>
              <div>
              <div class="form-group" id="div_personal_time">
                <label for="personal_time" id="lblpersonal_time">Personal Time <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="personal_time" name="personal_time" required>
              <div>
              <div class="form-group">
                <label for="status_id">Status <span class="text-danger">*</span></label>
                <select class="js-select2 form-control" id="status_id2" name="status_id" style="width: 100%;" data-placeholder="Choose one.." required>
                  <!-- <option></option> -->
                    @foreach($status as $stat)
                      @if($stat->category_id == 1 )
                        <option value="{{ $stat->id }}" @if($stat->id == 1) selected @endif>{{ $stat->description }}</option>
                      @endif
                    @endforeach
                </select>
              <div>
              <div class="form-group">
                <label for="note_aprov">Note</label>
                <input type="text" class="form-control" id="note_aprov" name="note_aprov" placeholder="Note">
              </div>
            </div>
            <div class="block-content block-content-full text-right ">
              <a type="button" class="btn btn-sm btn-hero-light close-modal">Close</a>
              <button type="submit" class="btn btn-sm btn-hero-primary">Submit</button>
            </div>


        </div>
        </form>
      </div>

    </div>
  </div>
  <!-- END From Left Block Modal -->

  <script>

    var tabla1;
    var urlTabla1 = "absencelist";

    $(function() {
      tabla1 = $('.tabladata').DataTable({
        processing: true,
        serverSide: true,
        ajax: urlTabla1,
        dom: 'Bfrtip',
        pageLength: 5,
        responsive: true,
        scrollX : true,
        buttons : [
          {
            "extend": 'copy',
            "text": `<i class="far fa-copy" style="color: white;"></i>`,
            "titleAttr": 'Copy',
            "action": exportTable
          },
          {
            "extend": 'csv',
            "text": `<i class="fa fa-file" style="color: white;"></i>`,
            "titleAttr": 'CSV',
            "action": exportTable
          },
          {
            "extend": 'excel',
            "text": `<i class="far fa-file-excel" style="color: white;"></i>`,
            "titleAttr": 'Excel',
            "action": exportTable
          },
          {
            "extend": 'pdf',
            "text": `<i class="far fa-file-pdf" style="color: white;"></i>`,
            "titleAttr": 'PDF',
            "action": exportTable
          },
          {
            "extend": 'print',
            "text": `<i class="fa fa-print" style="color: white;"></i>`,
            "titleAttr": 'Print',
            "action": exportTable
          }
        ],
        columns: [
          { data: 'employee', name: 'employee' },
          { data: 'reasons', name: 'reasons' },
          { data: 'desde', name: 'desde' },
          { data: 'hasta', name: 'hasta' },
          { data: 'estatus', name: 'estatus' },
          {
            "className":      'details-control',
            "orderable":      false,
            "data":           null,
            "defaultContent": '',
            "searchable": false
          }
        ],
        "order": [],
        "createdRow": function( row, data, dataIndex ) {
          if(data.status_id == "3") {
            $(row).addClass('timeleft');
          }
          if($.inArray(data.status_id,["1","6","7"])) {
            $(row).addClass('approved');
          }
          if(data.status_id == 2) {
            $(row).addClass('reject');
          }

          if(data.status_id == 5) {
            $(row).addClass('pending');
          }
        }
      });

      $('.tabladata tbody').on('click', 'td.details-control', async function () {
        var tr = $(this).closest('tr');
        var row = tabla1.row(tr);

        if ( row.child.isShown() ) {
          row.child.hide();
          tr.removeClass('shown');
          return;
        }
        let tab = await detalle(row.data());
        row.child(tab).show();
        tr.addClass('shown');
      });

    });

    $(document).ready(function() {
      $('#reasonDiv').hide();
      $('#file_upload').hide();
      $('#status').hide();
      $('#createdby').hide();
      $('#platform').hide(); 
      $('#multipleDiv').hide();
      $('.select2').select2();

      // var multipleCancelButton = new Choices('#employesid', {
      //     removeItemButton: true,
      //     maxItemCount:10,
      //     searchResultLimit:200,
      //     renderChoiceLimit:10
      // });

      $('#hora_init').mdtimepicker({
        theme: 'green',// 'red', 'purple', 'indigo', 'teal', 'green', 'dark'
        readOnly: false,
        hourPadding: false,
        clearBtn: true
      });
      $('#hora_end').mdtimepicker({
        theme: 'green',// 'red', 'purple', 'indigo', 'teal', 'green', 'dark'
        readOnly: false,
        hourPadding: false,
        clearBtn: true
      });
    });

    $('.resetForm').on('click',async function () {
      $('#supervisor').val('');
      $('#id').val('');
      $('#employesid').val('').change();
      $('#motiveid').val('').change();
      $('#motiveid2').val('').change();
      $('#reason').val('');
      $('#violation_id').val('').change();
      $('#point').html('');
      $('#days').val('');
      $('#fecha_init').val('');
      $('#fecha_end').val('');
      $('#hora_init').val('');
      $('#hora_end').val('');
      $('#status_id').val('').change();
      $('#comment').val('');
      $('#required_note_oficial').val('');
      $('#horas_justificacion_real').html('');
      $('#no_call_no').val('');
      $('#depart').html('');
      $('#super').html('');
      $('#reasonDiv').hide();

      /*const { value: formValues } = await Swal.fire({
        title: 'Save Attendance',
        icon: 'question',

        confirmButtonText: 'Save Pending',

        html:`<button type="button" class="btn btn-success" onclick="changeStatus(1)">Save Approved</button>
              <button type="button" class="btn btn-danger" onclick="changeStatus(2)">Save Rejected</button>`,
        focusConfirm: false,
        preConfirm: () => {
          return [
            document.getElementById('swal-input1').value,
            document.getElementById('swal-input2').value
          ]
        }
      });

      if (formValues) {
        changeStatus(5);
        Swal.fire(JSON.stringify(formValues));
      }*/
    });

    $('.opend').on('click',async function () {
     tabla1.rows(':not(.parent)').nodes().to$().find('td.details-control').trigger('click');
    });

    $('.closed').on('click',async function () {
      tabla1.rows(':not(.parent)').nodes().to$().find('td.details-control').trigger('click');
    });

    $('#days').change(function(){
      setDate();
    });

    $('#days').keyup(function(){
      setDate();
    });

    $('#absenceAprob').on('submit',async function () {
      try{
        let params = {
            points : $('#points').val(),
            status : $('#status_id2').find(':selected').val(),
            note : $('#note_aprov').val(),
        };

        let resp = await request(`absences/aprove/${$('#id2').val()}`,'post',params);
        if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
        searchTable1();
        $('#approveModal').modal('hide');
      }catch (error) {
        Swal.fire('Operation failed',error.msg,error.status);
      }
    });

    $('.close-modal').on('click',function() {
        $('#approveModal').modal('hide');
    });

    /*$("#formAbsence").submit(async function(event){
      event.preventDefault();
      const { value: formValues } = await Swal.fire({
        title: 'Save Attendance',
        icon: 'error',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        html:
          '<input id="swal-input1" class="swal2-input">' +
          '<input id="swal-input2" class="swal2-input">',
        focusConfirm: false,
        preConfirm: () => {
          return [
            document.getElementById('swal-input1').value,
            document.getElementById('swal-input2').value
          ]
        }
      })

      if (formValues) {
        Swal.fire(JSON.stringify(formValues))
      }
    });*/

    function resetAll(){
     // $('#desde').val('');
      //$('#hasta').val('');
      $('#empleado').val('').change();
      $('#razon').val('').change();
      $('#violacion').val('').change();
      $('#estatus').val('').change();
      searchTable1();
    }

    async function editAbsence(sender){
      $('#multiple').prop('checked',false);
      showMultiple($('#multiple'));

      $("#form").trigger("click");
      $('#label').html("Employe edit");
      let id = $(sender).data('id');
      $('#id').val(id);
      let absences = @json($absences);
      absences.forEach((element) =>{
        if(id == element.id){
          $('#employesid').val(element.employesid).trigger('change');
          $('#motiveid').val(element.motiveid).trigger('change');
          $('#motiveid2').val(element.motiveid2).trigger('change');
          $('#status_id').val(element.status_id).trigger('change');
          $('#reason').val(element.reason);
          $('#violation_id').val(element.violation_id).trigger('change');
          $('#fecha_init').val((element.fecha_init != '') ? moment(element.fecha_init).format('MM/DD/YYYY') : '');
          $('#fecha_end').val((element.fecha_end != '') ? moment(element.fecha_end).format('MM/DD/YYYY') : '');
          $('#hora_end').val(element.hora_end);
          $('#hora_init').val(element.hora_init);
          $('#comment').val(element.comment);

          if(element.usuario != "" && element.usuario != null){
            $('#createdby').text(`Created by: ${element.usuario}`);
            $('#platform').text(`Pltaform: ${element.platform}`);
            $('#createdby').show();
            $('#platform').show();
          }

          if(element.no_call_no == 0){
             $("input:checkbox[value='0']").prop('checked',true);
          }else{
             $("input:checkbox[value='']").prop('checked',false); 
          }

          changeDate();
          calculardiferencia();
          changeViolation($('#violation_id'),1);
        }
      });
    }

    async function deleteAbsence(sender) {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(sender).data('id');
            let resp = await request(`absences/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            searchTable1();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    }

    function setDate() {
      if($('#days').val() == ""){
        $('#fecha_end').val($('#fecha_init').val());
        $('#days').val(1);
        return;
      }
      let inicio = moment($('#fecha_init').val(),'MM/DD/YYYY').format('YYYY-MM-DD');
      let fechaI = moment(inicio);

      date = fechaI.add($('#days').val() - 1, 'd').format('MM/DD/YYYY');

      $('#fecha_end').val(date);
    }

    async function request(url,type, params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

    function changeMotive(sender){
      if($(sender).val() == 4){
          $('#reasonDiv').show();

          return;
      }
      $('#reasonDiv').show();
       let note = $(sender).find(':selected').data('note');
      if (note == 'Si') {
         $('#file_upload').show();
      }else{
        $('#file_upload').hide();
      }
    }

    function nocall(sender) {
      if ($("input:checkbox[value='0']").prop('checked',false)) return;

      let moti = @json($motives_nocall);

      $('#motiveid option').remove();
      $('#motiveid2 option').remove();
      moti.forEach((element) =>{
        switch(element.id){
            case 31:
              $('#motiveid').append(`<option value="31">${element.description}</option>}`);
              $('#motiveid').attr('readonly', true);
              $("#motiveid option[value='31']").attr("selected", true);
              break;
            case 32:
              $('#motiveid2').append(`<option value="32">${element.description}</option>}`);
              $('#motiveid2').attr('readonly', true);
              $("#motiveid2 option[value='32']").attr("selected", true);
              $('#reasonDiv').show();
              $('#reason').val($('select[name="motiveid2"] option:selected').text());
              $('#reason').attr('readonly', true);
              break;
            default:
              $('#motiveid').append(`<option value="${element.id}">${element.description}</option>}`);
              $('#motiveid').attr('readonly', false);

              $('#motiveid2').append(`<option value="${element.id}">${element.description}</option>}`);
              $('#motiveid2').attr('readonly', false);
              $('#reasonDiv').hide();
              $('#reason').val('');
              $('#reason').attr('readonly', false);
        }
      });

      let vio = @json($violations);

      $('#violation_id option').remove();
      vio.forEach((element) =>{
        switch(element.id){
          case 12:
            $('#violation_id').append(`<option value="12">${element.description}</option>}`);
            $('#violation_id').attr('readonly', true);
            $("#violation_id option[value='12']").attr("selected", true);
            break;
          default:
            $('#violation_id').append(`<option value="${element.id}">${element.description}</option>}`);
            $('#violation_id').attr('readonly', false);
        }
      });
    }

    function pasarValor(sender){
     
       $('#comment').val($(sender).val()+' -');
      //$('#reasonDiv').hide();
    }

    function changeViolation(sender, option){
      let points = $(sender).find(':selected').data('point');
      let value = $(sender).find(':selected').val();
      if(option == 1){
        $('#point').html(`Points: ${points}`);
        return;
      }

      if(value == 16){
        $('#personal_time').hide();
        $('#lblpersonal_time').hide();
        $('#personal_time').val(0);
        $('#points').hide();
        $('#lblpoints').hide();
        $('#points').val(0);
        return;
      }

      if(points <= 0){
        $('#points').show();
        $('#lblpoints').show();
        $('#points').val(points);

        $('#personal_time').hide();
        $('#lblpersonal_time').hide();
        $('#personal_time').val(0);
        return;
      }

      $('#points').hide();
      $('#lblpoints').hide();
      $('#points').val(0);

      $('#personal_time').show();
      $('#lblpersonal_time').show();
      $('#personal_time').val(0);
    }

    function changeEmployee(sender) {
      $('#depart').text('');
      $('#super').text('');
      $('#supervisor').val('');

      let departamento = @json($departaments);
      let employes = @json($employes);
      let supervisor = '';
      departamento.forEach((element) =>{
        if($(sender).find(':selected').data('depart') == element.id){
          $('#depart').text(`(${element.description})`);
          supervisor = element.employesid;
          $('#supervisor').val(element.employesid);
        }
      });
      employes.forEach((element) =>{
        if(supervisor == element.id){
          $('#super').text(`(${element.firstname} ${element.lastname})`);
        }
      });
    }

    function changeDate() {
      let inicio = moment($('#fecha_init').val(),'MM/DD/YYYY').format('YYYY-MM-DD');
      let fin = moment($('#fecha_end').val(),'MM/DD/YYYY').format('YYYY-MM-DD');
      let fechaI = moment(inicio);
      let fechaF = moment(fin);
      let dias = (fechaI.diff(fechaF, 'days') == 0) ? 1 : fechaF.diff(fechaI, 'days');
      if (fin >= inicio) {
        $('#days').val(dias+1);
      }

      if (fin == inicio) {
        $('#days').val(1);
      }
      //$('#days').val(fechaF.diff(fechaI, 'days'));
    }

    function calculardiferencia(){
      if($('#hora_init').val() == "" || $('#hora_end').val() == "") return;
      let hora_inicio = $('#hora_init').val().substring(0,$('#hora_init').val().length - 3);
      let hora_final = $('#hora_end').val().substring(0,$('#hora_end').val().length - 3);

      // Expresión regular para comprobar formato
      let formatohora = /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/;

      // Si algún valor no tiene formato correcto sale
      if (!(hora_inicio.match(formatohora) && hora_final.match(formatohora))) return;

      // Calcula los minutos de cada hora
      let minutos_inicio = hora_inicio.split(':').reduce((p, c) => parseInt(p) * 60 + parseInt(c));
      let minutos_final = hora_final.split(':').reduce((p, c) => parseInt(p) * 60 + parseInt(c));

      // Si la hora final es anterior a la hora inicial sale
      if (minutos_final < minutos_inicio) return;

      // Diferencia de minutos
      let diferencia = minutos_final - minutos_inicio;

      // Cálculo de horas y minutos de la diferencia
      let horas = Math.floor(diferencia / 60);
      let minutos = diferencia % 60;

      $('#horas_justificacion_real').html(`Hours total: ${horas}:${(minutos < 10 ? '0' : '')}${minutos}`);
}

    function detalle(data) {
      return new Promise((resolve,reject)=>{
        let points = 0;
        let violations = @json($violations);
        violations.forEach((element) =>{
          if(data.violation_id == element.id){
            points = element.points;
          }
        });

        let detalle = `<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
          <tr width="10%">
            <td class="text-center">Days:</td>
            <td>${data.days}</td>
          </tr>
          <tr width="10%">
            <td class="text-center">Deduction Point:</td>
            <td>${points}</td>
          </tr>
        </table>`;

        let horaInit = `<p><strong>Hour Start: </strong>${(data.hora_init) ? data.hora_init : ''}</p>`;
        let horaEnd = `<p><strong>Hour End: </strong>${(data.hora_end) ? data.hora_end : ''}</p>`;
        let comment = `<p><strong>Comment: </strong>${(data.comment) ? data.comment : ''}</p>`;
        let reason = `<p><strong>Reason: </strong>${(data.reason) ? data.reason : ''}</p>`;
        let created = `<p><strong>Created by: </strong>${(data.usuario) ? data.usuario : ''}</p>`;
        let platform = `<p><strong>Platform: </strong>${(data.platform) ? data.platform : ''}</p>`;

        detalle += `<tr width="40%">
          <td class="text-center">action:</td>`;

        let btnApprove = `<a data-toggle="tooltip" data-animation="true" data-placement="top" title="Approve" data-id="${data.id}" data-point="${data.points}" data-violation="${data.violation_id}" onclick=\"aprobar(this);\">
          <img src="{{ asset('images/aproval.png') }}" style="width: 32px !important;"></a>`;

        let btnReject = `<a data-toggle="tooltip" data-animation="true" data-placement="top" title="Reject" data-id="${data.id}" onclick=\"rechazar(this);\">
          <img src="{{ asset('images/rejected.png') }}" style="width: 51px !important; margin-top: -5px !important;"></a>`;

        let btnEdit = `<a onclick=\"editAbsence(this);\" data-toggle="tooltip" data-animation="true" data-placement="top" title="Edit" data-id="${data.id}">
          <img src="{{ asset('images/edit.png') }}" style="width: 32px !important;"></a>`;

        let btnFinished = `<a data-toggle="tooltip" data-animation="true" data-placement="top" title="Finished" data-id="${data.id}" onclick=\"finishedAbsence(this);\" >
          <img src="{{ asset('images/finished.png') }}" style="width: 32px !important;></a>`;

        let btnDelete = `<a onclick=\"deleteAbsence(this)\" data-toggle="tooltip" data-animation="true" data-placement="top" title="Delete" data-id="${data.id}">
          <img src="{{ asset('images/delete.png') }}" style="width: 30px !important; margin-left : 15px !important;"></a>`;

        detalle = `
          <div class="row">
            <div class="col-sm-7">
              <div class="block block-rounded block-bordered">
                <div class="block-content">
                  <p><strong>Days: </strong>${(data.days) ? data.days : 1}</p>
                  <p><strong>Deduction Point: </strong>${data.violation_name} (${points})</p>
                  ${(data.hora_init) ? horaInit : ''}
                  ${(data.hora_end) ? horaEnd : ''}
                  ${(data.comment) ? comment : ''}
                  ${(data.reason) ? reason : ''}
                  ${(data.usuario) ? created : ''}
                  ${(data.platform) ? platform : ''}
                </div>
              </div>
            </div>
        `;

        if(_.includes([1, 5], data.status_id)){
          detalle += `<div class="col-sm-1 text-center" id="bt0" name="bt0">
                        <p>${btnFinished}</p>
                      </div>`;
        }

        if(_.includes([1, 6, 7, 5], data.status_id)){
          detalle += `<div class="col-sm-1 text-center" id="bt1" name="bt1">
                        <p>${btnApprove}</p>
                      </div>`;
        }

        if(!_.includes([2, 10], data.status_id)){
          detalle += `<div class="col-sm-1 text-center" id="bt2" name="bt2">
                        <p>${btnReject}</p>
                      </div>`;
        }


        detalle += `<div class="col-sm-1 text-center" id="bt3" name="bt3">
                      <p>${btnEdit}</p>
                    </div>`;

        if(data.status_id != 10){
          detalle += `<div class="col-sm-1 text-center" id="bt4" name="bt4">
                      <p>${btnDelete}</p>
                    </div>`;
        }


        detalle += `</div>`;
        resolve(detalle);
      });
    }

    function searchTable1(){
      //desde=${$('#desde').val()}&hasta=${$('#hasta').val()}&
      urlTabla1 +=`?empleado=${$('#empleado').find(':selected').val()}&razon=${$('#razon').find(':selected').val()}&violacion=${$('#violacion').find(':selected').val()}&status=${$('#estatus').find(':selected').val()}`;
     //console.log(urlTabla1);
      tabla1.ajax.url( urlTabla1 ).load();
    }

    async function aprobar(sender) {
      let violation = @json($violations);
      $('#personal_time').hide();
      $('#lblpersonal_time').hide();
      $('#points').hide();
      $('#lblpoints').hide();

      violation.forEach((item)=>{
        if(item.id == $(sender).data('violation')){
          $('#violation_approve').val(item.id).change();
          if(item.points === ""){
            $('#personal_time').show();
            $('#lblpersonal_time').show();
            $('#personal_time').val(0);
          }
          if(item.points <= 0){
            $('#points').show();
            $('#lblpoints').show();
            $('#points').val(item.points);
          }

          if(item.id === 16){
            $('#personal_time').hide();
            $('#lblpersonal_time').hide();
            $('#personal_time').val(0);
            $('#points').hide();
            $('#lblpoints').hide();
            $('#points').val(0);
          }
        }
      });

      $('#id2').val($(sender).data('id'));
      $('#status_id2').val('').change();
      $('#note_aprov').val('');
      $('#approveModal').modal('show');
    }

    async function rechazar(sender) {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, Reject it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(sender).data('id');
            let resp = await request(`absences/reject/${id}`,'post',{});
            if(resp.status == 'success') Swal.fire(resp.title,resp.msg,resp.status);
            searchTable1();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    }

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

    function exportTable(e, dt, button, config) {
      var self = this;
      var oldStart = dt.settings()[0]._iDisplayStart;
      dt.one('preXhr', function (e, s, data) {
        data.start = 0;
        data.length = 2147483647;
        dt.one('preDraw', function (e, settings) {
          if (button[0].className.indexOf('buttons-copy') >= 0) {
            $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-excel') >= 0) {
            $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
              $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
              $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-csv') >= 0) {
            $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
              $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
              $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
            $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
              $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
              $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-print') >= 0) {
            $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
          }
          dt.one('preXhr', function (e, s, data) {
            settings._iDisplayStart = oldStart;
            data.start = oldStart;
          });
          setTimeout(dt.ajax.reload, 0);
          return false;
        });
      });
      dt.ajax.reload();
    }

    function setRelation(sender) {
      let relation = @json($relation);
      let moti = @json($motives_nocall);
      let violat = @json($violations);
      var motivo = '';
      var violacion = '';

      if(relation == "" || relation == null || relation == "null") return;

      relation.forEach((element) =>{
          if($(sender).find(':selected').val() == element.idmotive){
            motivo = element.idmotive2;
            violacion = element.violationid;
            return;
          }

          if($(sender).find(':selected').val() == element.idmotive){
            //motivo = element.idmotive2;
            violacion = element.violationid;
            return;
          }
      });

      $('#motiveid2 option').remove();
      moti.forEach((element) =>{
        if(element.id == motivo){
          $('#motiveid2').append(`<option value=${element.id}>${element.description}</option>}`);
          $(`#motiveid2 option[value=${element.id}]`).attr("selected", true);
          $('#motiveid2').attr('readonly', true);
        }else{
          //$('#motiveid2').append(`<option value=${element.id}>${element.description}</option>}`);
         // $('#motiveid2').attr('readonly', false);
        }

      });

      $('#violation_id option').remove();
      violat.forEach((element) =>{
        if(element.id == violacion){
          $('#violation_id').append(`<option value=${element.id}>${element.description}</option>}`);
          $(`#violation_id option[value=${element.id}]`).attr("selected", true);
          $('#violation_id').attr('readonly', true);
        }else{
         // $('#violation_id').append(`<option value=${element.id}>${element.description}</option>}`);
          //$('#violation_id').attr('readonly', false);
        }
      });
    }

    function changeStatus(status) {
        $('#status_id').val(status).change();
        $("#formAbsence").submit();
    }

    function validateFecha() {
        let fechaI = moment($('#fecha_init').val());
        let fechaF = moment($('#fecha_end').val());
    }

    async function finishedAbsence(sender) {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, finished it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(sender).data('id');
             let resp = await request(`absences/finished/${id}`,'post',{});

              if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
                searchTable1();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    }

    function showMultiple(sender){
      if ($(sender).is(':checked')){
          $('#multipleDiv').show();
          $('#divEmploye').hide();
          $('#employesid').val('').change();
          $('#employesid').prop("required",false);
          $('#employesidM').prop("required",true);
          return;
      }
      $('#employesidM').val('').change();
      $('#multipleDiv').hide();
      $('#divEmploye').show();
      $('#employesid').prop("required",true);
      $('#employesidM').prop("required",false);
      return;
    }

    function llenaRason(){
      if($('#reason').val() == '') $('#reason').val($('#motiveid2').find(':selected').text());
    }

  </script>
@endsection