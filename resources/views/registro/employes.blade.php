@extends('layouts.backend')
<style>
  div.dataTables_wrapper div.dataTables_processing {
    position: absolute;
    top: 30%;
    left: 50%;
    width: 30%;
    height: 64px;
    margin-left: -20%;
    margin-top: -25px;
    padding-top: 20px;
    text-align: center;
    font-size: 1.2em;
    background:#00703c;
    color: mintcream;
    border-radius: 10px;
  }
  td.details-control {
    background: url('{{ asset("images/details_open.png") }}') no-repeat center center;
    cursor: pointer;
  }
  tr.shown td.details-control {
    background: url('{{ asset("images/details_close.png") }}') no-repeat center center;
  }
  .timeleft {
    background-color: white !important;
    color: #FFB119;
  }
  .approved {
    background-color: white !important;
    color: green;
  }
  .reject {
    background-color: white !important;
    color: red;
  }
  .pending {
    background-color: white !important;
    color: #3C90DF;
  }
</style>

@section('content')
 <div class="content">

    <div class="block block-rounded block-bordered">
      <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" href="#btabs-animated-fade-form" id="form">New Employee</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#btabs-animated-fade-list" id="list">Employee List</a>
        </li>
      </ul>
      <div class="block-content tab-content overflow-hidden">

       {{-- Tab 1--}}
      <div class="tab-pane fade show active" id="btabs-animated-fade-form" role="tabpanel">
          <form method="post" action="employes" id="employes">
            {{ csrf_field() }}
            <input type="hidden" id="id" name="id" value="">
                <!-- Wizard Progress Bar -->
            <div class="block block-rounded block-bordered">
              <div class="block-header block-header-default">
                <div class="block-options">
                  <button type="submit" class="btn btn-sm btn-outline-primary">
                    <i class="fa fa-check"></i> Save
                  </button>
                  <button type="reset" class="btn btn-sm btn-outline-danger resetForm">
                    <i class="fa fa-repeat"></i> Reset
                  </button>
                </div>
              </div>
              <div class="block-content">
                <div class="row push">
                  <div class="col-lg-12">
                    <div class="form-group form-row">
                        <div class="col-4">
                           <div class="form-group">
                              <label for="role" class="col-form-label">Employee Code<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" id="code" name="code" placeholder="Insert Code" required>
                            </div>
                            <div class="form-group">
                              <label for="role" class="col-form-label">First Name<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" id="firstname" name="firstname" placeholder="Insert First Name" required>
                            </div>
                            <div class="form-group">
                              <label for="role" class="col-form-label">Last Name<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Insert Last Name" required>
                            </div>
                            <div class="form-group">
                              <label for="role" class="col-form-label">Sex</label>
                                <select id="sex" name="sex" class="custom-select">
                                  @foreach($sexo as $sexos)
                                    <option value="{{$sexos->id}}">{{$sexos->description}}</option>
                                  @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                              <label for="role" class="col-form-label">Birth Date<span class="text-danger">*</span></label>
                              <input type="date" class="form-control" id="birthdate" name="birthdate" placeholder="Insert Birthdate" required>
                            </div>

                            <div class="form-group">
                              <label for="role" class="col-form-label">Address<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" id="address" name="address" placeholder="Insert Address" required>
                            </div>
                             <div class="form-group">
                              <label for="role" class="col-form-label">Email<span class="text-danger">*</span></label>
                              <input type="email" class="form-control" id="email" name="email" placeholder="Insert Email" required>
                            </div>
                        </div>
        
                        <div class="col-4">
                           <div class="form-group">
                              <label for="role" class="col-form-label">Social Security Number<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" id="numbersocial" name="numbersocial" placeholder="Insert Social Security Number" required>
                            </div>
                            <div class="form-group">
                              <label for="role" class="col-form-label">Zip Code</label>
                              <input type="text" class="form-control" id="zipcode" name="zipcode" placeholder="Insert Zip Code" required>
                            </div>
                            <div class="form-group">
                              <label for="role" class="col-form-label">Phone Number<span class="text-danger">*</span></label>
                              <input type="text" class="form-control" id="numberphone" name="numberphone" placeholder="Insert Phone Number" required>
                            </div>

                             <div class="form-group">
                              <label for="role" class="col-form-label">Home Number</label>
                              <input type="text" class="form-control" id="numberhome" name="numberhome" placeholder="Insert Home Number">
                            </div>
                            <div class="form-group">
                              <label for="role" class="col-form-label">Social Networks</label>
                              <input type="text" class="form-control" id="sociallink" name="sociallink" splaceholder="Insert Social Networks">
                            </div>
                            <div class="form-group">
                              <label for="role" class="col-form-label">Area<span class="text-danger">*</span></label>
                                <select id="area" name="area" class="custom-select" required>
                                  @foreach($area as $areas)
                                    <option value="{{$areas->id}}">{{$areas->description}}</option>
                                  @endforeach
                                </select>
                            </div>
                             <div class="form-group">
                              <label for="role" class="col-form-label">Departments<span class="text-danger">*</span></label>
                                <select id="departamento" name="departamento" class="custom-select" required>
                                  @foreach($departamento as $departamentos)
                                    <option value="{{$departamentos->id}}">{{$departamentos->description}}</option>
                                  @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-4">
                          <div class="form-group">
                            <label for="role" class="col-form-label">Site<span class="text-danger">*</span></label>
                              <select id="idsite" name="idsite" class="custom-select" required>
                                @foreach($idsite as $idsites)
                                  <option value="{{$idsites->id}}">{{$idsites->name}}</option>
                                @endforeach
                              </select>
                          </div>
                          <div class="form-group">
                            <label for="role" class="col-form-label">Positions<span class="text-danger">*</span></label>
                              <select id="position" name="position" class="custom-select" required>
                                @foreach($cargo as $cargos)
                                  <option value="{{$cargos->id}}">{{$cargos->description}}</option>
                                @endforeach
                              </select>
                          </div>
                          <div class="form-group">
                            <label for="role" class="col-form-label">Schedules</label>
                              <select id="horario" name="horario" class="custom-select">
                                @foreach($horario as $horarios)
                                  <option value="{{$horarios->id}}">{{$horarios->description}}</option>
                                @endforeach
                              </select>
                          </div>
                          <div class="form-group">
                            <label for="role" class="col-form-label">Professions</label>
                              <select id="profesion" name="profesion" class="custom-select">
                                @foreach($profesion as $profesions)
                                  <option value="{{$profesions->id}}">{{$profesions->description}}</option>
                                @endforeach
                              </select>
                          </div>
                          <div class="form-group">
                            <label for="role" class="col-form-label">Roles</label>
                              <select id="roles" name="roles" class="custom-select" required>
                                @foreach($roles as $role)
                                  <option value="{{$role->id}}">{{$role->name}}</option>
                                @endforeach
                              </select>
                          </div>
                       
                          <div class="form-group">
                            <label for="role" class="col-form-label">Status</label>
                              <select id="status" name="status" class="custom-select" onchange="fecha_culminacion();">
                                <option value="AC">Active</option>
                                <option value="IN">Inactive</option>
                                <option value="SP">Suspended</option>
                              </select>
                          </div>
                          <div class="form-group" id="fechaculminacion">
                              <label for="finished_date" class="col-form-label">Completion Date</label>
                              <input type="date" class="form-control" id="finished_date" name="finished_date" placeholder="Insert Completion Date">
                           </div>
                        </div>
                    </div>
              </div>
            </div>
          </form>
        </div>       
      </div>
    </div>



    {{-- Tab 2--}}
        <div class="tab-pane fade" id="btabs-animated-fade-list" role="tabpanel" style="overflow-x: auto;">
          <div class="col-md-auto">
            <form method="get" action="#" id="search1">
              <div class="form-group form-row">
                <div class="col-4">
                    <div class="form-group">
                      <label for="role" class="col-form-label">Employee Code<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" id="code1" name="code1" placeholder="Insert Code">
                    </div>
                    <div class="form-group">
                      <label for="role" class="col-form-label">First Name<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" id="firstname1" name="firstname1" placeholder="Insert First Name">
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group">
                      <label for="role" class="col-form-label">Last Name<span class="text-danger">*</span></label>
                      <input type="text" class="form-control" id="lastname1" name="lastname1" placeholder="Insert Last Name">
                    </div>
                    <div class="form-group">
                      <label for="role" class="col-form-label">Departments</label>
                        <select id="departamento1" name="departamento1" class="custom-select">
                          <option></option>
                          @foreach($departamento as $departamentos)
                            <option value="{{$departamentos->id}}">{{$departamentos->description}}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
               <!--  <div class="form-group">
                  <label for="role" class="col-form-label">Status</label>
                    <select id="status" name="status" class="custom-select">
                      <option value="AC">Active</option>
                      <option value="IN">Inactive</option>
                      <option value="SP">Suspended</option>
                    </select>
                </div> -->
                <div class="col-2" style="padding-top: 28px">
                  <button type="button" class="btn btn-hero-info" onclick="searchTable1();">
                    <i class="fa fa-fw fa-search mr-1"></i> Search
                  </button>
                </div>
                <div class="col-2" style="padding-top: 28px">
                  <button type="button" class="btn btn-hero-secondary" onclick="resetAll();">
                    <i class="fa fa-fw fa-redo-alt mr-1"></i> Reset
                  </button>
                </div>
              </div>
            </form>
          </div>
          <br>
          <br>
        <!--   <div class="row">
            <div class="col-md-4 offset-md-8">
              <a class="btn btn-rounded btn-outline-success opend">
                <img src="{{ asset('images/details_open.png') }}"> Expand all
              </a>
              <a class="btn btn-rounded btn-outline-danger closed">
                <img src="{{ asset('images/details_close.png') }}"> Collapse all
              </a>
            </div>
          </div> -->


      <table class="table table-bordered table-striped table-vcenter tabladata" style="width: 100%" >
            <thead>
                <tr>
                  <th class="text-center">Id Employee</th>
                  <th class="text-center">First Name</th>
                  <th class="text-center">Last Name</th>
                  <th class="text-center">Birth Date</th>
                  <th class="text-center">Address</th>
                  <th class="text-center">Areas</th>
                  <th class="text-center">Departaments</th>
                  <th class="text-center">Site</th>
                  <th class="text-center"  style="width: 20%;">Actions</th>
                </tr>
            </thead>
          
          </table>
      </div>
  </div>
</div>

<script>

  var tabla1;
  var urlTabla1 = "employeelist";

    $(function(){
      tabla1 = $('.tabladata').DataTable({
        processing: true,
        serverSide: true,
        ajax: urlTabla1,
        dom: 'Bfrtip',
        buttons: [
          {
            "extend": 'copy',
            "text": `<i class="far fa-copy" style="color: white;"></i>`,
            "titleAttr": 'Copy',
            "action": exportTable
          },
          {
            "extend": 'csv',
            "text": `<i class="fa fa-file" style="color: white;"></i>`,
            "titleAttr": 'CSV',
            "action": exportTable
          },
          {
            "extend": 'excel',
            "text": `<i class="far fa-file-excel" style="color: white;"></i>`,
            "titleAttr": 'Excel',
            "action": exportTable
          },
          {
            "extend": 'pdf',
            "text": `<i class="far fa-file-pdf" style="color: white;"></i>`,
            "titleAttr": 'PDF',
            "action": exportTable
          },
          {
            "extend": 'print',
            "text": `<i class="fa fa-print" style="color: white;"></i>`,
            "titleAttr": 'Print',
            "action": exportTable
          }
        ],
        columns: [
          { data: 'code', name: 'code' },
          { data: 'firstname', name: 'firstname' },
          { data: 'lastname', name: 'lastname' },
          { data: 'birthdate', name: 'birthdate' },
          { data: 'address', name: 'address' },
          { data: 'area', name: 'area' },
          { data: 'departament', name: 'departament' },
          { data: 'site', name: 'site' },
          { data: 'action', name: 'action' },
        ],
        });
    });

    //   $('.tabladata tbody').on('click', 'td.details-control', async function () {
    //     var tr = $(this).closest('tr');
    //     var row = tabla1.row(tr);

    //     if ( row.child.isShown() ) {
    //       row.child.hide();
    //       tr.removeClass('shown');
    //       return;
    //     }
    //     let tab = await detalle(row.data());
    //     row.child(tab).show();
    //     tr.addClass('shown');
    //   });
    // });

     $(document).ready(function(){
      $('#reasonDiv').hide();
      $('#fechaculminacion').hide();
      $("#body").attr('onbeforeunload');
        
    });

    $('.addnew').on('click',async function (){
      $('#label').html("Add new Employee");
      $('#id').val('');
      $('#code').val('');
      $('#firstname').val('');
      $('#lastname').val('');
      $('#sex').val('');
      $('#birthdate').val('');
      $('#address').val('');
      $('#email').val('');
      $('#numbersocial').val('');
      $('#zipcode').val('');
      $('#numberphone').val('');
      $('#numberhome').val('');
      $('#numberfax').val('');
      $('#sociallink').val('');
      $('#area').val('');
      $('#departamento').val('');
      $('#idsite').val('');
      $('#horario').val('');
      $('#cargo').val('');
      $('#profesion').val('');
      $('#roles').val('');
      $('#status').val('');
      $('#finished_date').val('');
    });

    async function editEmploye(sender){
      $("#form").trigger("click");
      let id = $(sender).data('id');
      $('#id').val(id);
     // console.log(id);
      let employe = @json($employes2);
      employe.forEach((element) =>{
        if(id == element.id){
          $('#code').val(element.code);
          $('#firstname').val(element.firstname);
          $('#lastname').val(element.lastname);
          $('#sex').val(element.sex);
          $('#birthdate').val(element.birthdate);
          $('#address').val(element.address);
          $('#email').val(element.email);
          $('#numbersocial').val(element.numbersocial);
          $('#zipcode').val(element.zipcode);
          $('#numberphone').val(element.numberphone);
          $('#numberhome').val(element.numberhome);
          $('#sociallink').val(element.sociallink);
          $('#area').val(element.areaid);
          $('#departamento').val(element.departamentid);
          $('#position').val(element.positionid);
          $('#idsite').val(element.idsite);
          $('#horario').val(element.scheduleid);
          $('#cargo').val(element.positionid);
          $('#profesion').val(element.professionid);
          $('#roles').val(element.rolesid);
          $('#status').val(element.status);
          $('#finished_date').val(element.finished_date);
         
        }
      });      
    }

    async function deleteEmploye(sender){
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(this).data('id');
            let resp = await request(`employes/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            $(`#trrol_${id}`).remove();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    };

    function HandleBackFunctionality(e){
        if(window.event){
          if(window.event.clientX < 40 && window.event.clientY < 0){
          }else{
            document.getElementById("body").addEventListener("onbeforeunload", function(event){
             event.preventDefault()
            });
          }
       }else{
        if(event.currentTarget.performance.navigation.type == 1){
         }
         if(event.currentTarget.performance.navigation.type == 2){
        }}
    }

    async function request(url,type,params = null){
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

    function fecha_culminacion(sender){
      // body...
      console.log(sender);
       if($(sender).find(':selected').val() == 'SP'){
           $('#fechaculminacion').hide(); 
       }else{
          $('#fechaculminacion').show();
       }
    }

    function exportTable(e, dt, button, config){
      var self = this;
      var oldStart = dt.settings()[0]._iDisplayStart;
      dt.one('preXhr', function (e, s, data) {
        data.start = 0;
        data.length = 2147483647;
        dt.one('preDraw', function (e, settings) {
          if (button[0].className.indexOf('buttons-copy') >= 0) {
            $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-excel') >= 0) {
            $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
              $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
              $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-csv') >= 0) {
            $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
              $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
              $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
            $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
              $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
              $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-print') >= 0) {
            $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
          }
          dt.one('preXhr', function (e, s, data) {
            settings._iDisplayStart = oldStart;
            data.start = oldStart;
          });
          setTimeout(dt.ajax.reload, 0);
          return false;
        });
      });
      dt.ajax.reload();
    }

    function searchTable1(){
      urlTabla1 +=`?code=${$('#code1').val()}&firstname=${$('#firstname1').val()}&lastname=${$('#lastname1').val()}&departamento=${$('#departamento1').find(':selected').val()}`;
      //console.log(urlTabla1);
      tabla1.ajax.url( urlTabla1 ).load();
    }

    function resetAll(){
      $('#code1').val('');
      $('#firstname1').val('');
      $('#lastname1').val('').change();
      $('#departamento1').val('').change();
      
      searchTable1();
    }
   
  </script>
@endsection