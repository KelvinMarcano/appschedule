@extends('layouts.backend')

@section('content')
  <!-- Page Content -->
  <div class="block block-rounded block-bordered">

    <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" href="#btabs-animated-fade-list1" id="list">DashBoard</a>
      </li>
      @if(Auth::user()->rol_id != 5)
        <li class="nav-item">
          <a class="nav-link" href="#btabs-animated-fade-list2" id="list2">Reason Analysts</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#btabs-animated-fade-list3" id="list3">Attendance</a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="#btabs-animated-fade-list4" id="list4">Status Analysts</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#btabs-animated-fade-list5" id="list5">Deduction Point Analysts</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#btabs-animated-fade-list6" id="list6">Vacation</a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="#btabs-animated-fade-list7" id="list7">Vacation Time Line</a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="#btabs-animated-fade-list8" id="list8">Warning Note</a>
        </li>
      @endif
    </ul>

    <div class="block-content tab-content overflow-hidden">
      <div class="tab-pane active approveModal" id="btabs-animated-fade-list1" role="tabpanel">
        <div class="content">
          <div class="row">
            <div class="col-sm-2">
              <button type="button" class="btn btn-hero-primary" data-toggle="modal" data-target="#modal-default-vcenter">
                <i class="fa fa-fw fa-bars"></i>
              </button>
              <!-- Vertically Centered Default Modal -->
              <div class="modal" id="modal-default-vcenter" tabindex="-1" role="dialog" aria-labelledby="modal-default-vcenter" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <form method="get" action="#" id="search1">
                      <div class="modal-body pb-1">
                        <div class="form-group form-row">
                          <div class="col-sm-6">
                            <label for="">Attendance Date</label>
                            <div class="input-daterange input-group" data-date-format="mm/dd/yyyy" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                              <input type="text" class="form-control" id="desde" placeholder="From" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                              <div class="input-group-prepend input-group-append">
                                <span class="input-group-text font-w600">
                                  <i class="fa fa-fw fa-arrow-right"></i>
                                </span>
                              </div>
                              <input type="text" class="form-control" id="hasta" placeholder="To" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <label for="employesid">Employee</label>
                            <select class="js-select2 form-control" id="empleado" style="width: 100%;" data-placeholder="Choose one.." required>
                              <option></option>
                              @foreach($employes as $employe)
                                <option value="{{ $employe->id }}" data-depart="{{ $employe->departamentid }}">{{ $employe->code }}-{{ $employe->firstname }} {{ $employe->lastname }}-{{ $employe->numbersocial }}</option>
                              @endforeach
                            </select>
                          </div>
                          <div class="col-sm-6">
                            <label for="motiveid">Reason</label>
                              <select class="js-select2 form-control" id="razon" style="width: 100%;" data-placeholder="Choose one.." required>
                                <option></option>
                                @foreach($motives as $motive)
                                  <option value="{{ $motive->id }}">{{ $motive->code }}-{{ $motive->description }}</option>
                                @endforeach
                              </select>
                          </div>
                          <div class="col-sm-6">
                            <label for="violation_id">Deduction Point</label>
                              <select class="js-select2 form-control" id="violacion" style="width: 100%;" data-placeholder="Choose one.." required>
                                <option></option>
                                @foreach($violations as $violation)
                                  <option value="{{ $violation->id }}" data-point="{{ $violation->points }}">{{ $violation->code }}-{{ $violation->description }}</option>
                                @endforeach
                              </select>
                          </div>
                          <div class="col-sm-6">
                            <label for="estatus">Status</label>
                            <select class="js-select2 form-control" id="estatus" style="width: 100%;" data-placeholder="Choose one.." required>
                              <option></option>
                              @foreach($status as $stat)
                                <option value="{{ $stat->id }}">{{ $stat->description }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-hero-info" onclick="search();" style='width: 100%' data-dismiss="modal">
                          <i class="fa fa-fw fa-search mr-1"></i> Search
                        </button>
                        <button type="button" class="btn btn-hero-secondary" onclick="resetAll();" style='width: 100%'>
                              <i class="fa fa-fw fa-redo-alt mr-1"></i> reset
                            </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              {{--<!-- END Vertically Centered Default Modal -->
              <div class="dropdown-menu" aria-labelledby="dropdown-dropdown-primary" x-placement="right-start">
                <form method="get" action="#" id="search1">
                  <div class="form-group form-row">
                    <div class="col-sm-12">
                      <label for="">Attendance Date</label>
                      <div class="input-daterange input-group" data-date-format="mm/dd/yyyy" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                        <input type="text" class="form-control" id="desde" placeholder="From" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                        <div class="input-group-prepend input-group-append">
                          <span class="input-group-text font-w600">
                            <i class="fa fa-fw fa-arrow-right"></i>
                          </span>
                        </div>
                        <input type="text" class="form-control" id="hasta" placeholder="To" data-week-start="1" data-autoclose="true" data-today-highlight="true">
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <label for="employesid">Employee</label>
                      <select class="js-select2 form-control" id="empleado" style="width: 100%;" data-placeholder="Choose one.." required>
                        <option></option>
                        @foreach($employes as $employe)
                          <option value="{{ $employe->id }}" data-depart="{{ $employe->departamentid }}">{{ $employe->code }}-{{ $employe->firstname }} {{ $employe->lastname }}-{{ $employe->numbersocial }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-sm-12">
                      <label for="motiveid">Reason</label>
                        <select class="js-select2 form-control" id="razon" style="width: 100%;" data-placeholder="Choose one.." required>
                          <option></option>
                          @foreach($motives as $motive)
                            <option value="{{ $motive->id }}">{{ $motive->code }}-{{ $motive->description }}</option>
                          @endforeach
                        </select>
                    </div>
                  </div>

                  <div class="form-group form-row">
                    <div class="col-sm-12">
                      <label for="violation_id">Deduction Point</label>
                        <select class="js-select2 form-control" id="violacion" style="width: 100%;" data-placeholder="Choose one.." required>
                          <option></option>
                          @foreach($violations as $violation)
                            <option value="{{ $violation->id }}" data-point="{{ $violation->points }}">{{ $violation->code }}-{{ $violation->description }}</option>
                          @endforeach
                        </select>
                    </div>
                    <div class="col-sm-12">
                      <label for="estatus">Status</label>
                      <select class="js-select2 form-control" id="estatus" style="width: 100%;" data-placeholder="Choose one.." required>
                        <option></option>
                        @foreach($status as $stat)
                          <option value="{{ $stat->id }}">{{ $stat->description }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="col-sm-12" style="padding-top: 28px">
                      <div class="row">
                        <div class="col-12">
                          <button type="button" class="btn btn-hero-info" onclick="search();" style='width: 100%'>
                        <i class="fa fa-fw fa-search mr-1"></i> Search
                      </button>
                          <br>
                        </div>

                        <div class="col-12">
                          <button type="button" class="btn btn-hero-secondary" onclick="resetAll();" style='width: 100%'>
                        <i class="fa fa-fw fa-redo-alt mr-1"></i> reset
                      </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>--}}
            </div>
            <div class="col-sm-10">
              <div class="single-item text-center">
                @foreach($dates as $date)
                  <div class="py-4" data-fecha="{{ $date->fecha_init }}">
                    <div class="font-size-sm page-item {{ ($fechaInit == $date->fecha_init ? 'active' : '' ) }}" onclick="searchChild(this)" data-fecha="{{ $date->fecha_init }}">
                      <a class="page-link" href="javascript:void(0)">{{ \Carbon\Carbon::parse($date->fecha_init)->format('m/d/Y') }}</a>
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>


          <br>

          <div id="childDash">
            @include('dashboard-child')
          </div>
        </div>
      </div>
      @if(Auth::user()->rol_id != 5)
        <div class="tab-pane fade" id="btabs-animated-fade-list2" role="tabpanel">
          <iframe width="1080" height="600" src="https://datastudio.google.com/embed/reporting/1vsE09kPKrpo65zpQif3fwsFxSca93Gl1/page/GQHWB" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="tab-pane fade" id="btabs-animated-fade-list3" role="tabpanel">
          <iframe width="1080" height="600" src="https://datastudio.google.com/embed/reporting/16rKH3a1s8zo5m0kO1tDlbwgMCRMbwJBH/page/GQHWB" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="tab-pane fade" id="btabs-animated-fade-list4" role="tabpanel">
          <iframe width="1080" height="600" src="https://datastudio.google.com/embed/reporting/15E1auOB0D-B-CN8bIZRLk2Jp-VEYrSvs/page/GQHWB" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="tab-pane fade" id="btabs-animated-fade-list5" role="tabpanel">
          <iframe width="1080" height="600" src="https://datastudio.google.com/embed/reporting/1WsWeOcOsFxAjh9ktNTjVpa-p97PZf2cu/page/GQHWB" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="tab-pane fade" id="btabs-animated-fade-list6" role="tabpanel">
          <iframe width="1080" height="600" src="https://datastudio.google.com/embed/reporting/1vGlvuXM4nbez6P1IGYMxgij3aydUI5_L/page/GQHWB" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="tab-pane fade" id="btabs-animated-fade-list7" role="tabpanel">
          <iframe width="1080" height="600" src="https://datastudio.google.com/embed/reporting/10hMOfFWAA6FQ4e-SPS-ZrjqjsQXbwCM4/page/iaVWB" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <div class="tab-pane fade" id="btabs-animated-fade-list8" role="tabpanel">
         <iframe width="1080" height="600" src="https://datastudio.google.com/embed/reporting/60bc5b2b-5985-4fa1-8cdc-4db049044772/page/5suZB" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      @endif
    </div>
  </div>

  <!-- From Left Block Modal -->
  <div class="modal fade" id="approveModal"  role="dialog" aria-labelledby="modal-block-fromleft" aria-hidden="true">
    <div class="modal-dialog modal-dialog-fromleft" role="document">
      <div class="modal-content">
        <div class="block block-themed block-transparent mb-0">
          <div class="block-header bg-primary-dark" style="background-color: #00703c !important;">
            <h3 class="block-title">Approve Attendance</h3>
            <div class="block-options">
              <button type="button" class="btn-block-option close-modal" aria-label="Close">
                <i class="fa fa-fw fa-times"></i>
              </button>
            </div>
          </div>
          <form action="absences_aprob" method="post" onsubmit="return false;" id="absenceAprob">
            <div class="block-content">
              <input type="hidden" id="id" name="id">
              <input type="hidden" id="points" name="points">
              <div class="form-group">
                <label for="status_id">Status <span class="text-danger">*</span></label>
                <select class="js-select2 form-control" id="status_id" name="status_id" style="width: 100%;" data-placeholder="Choose one.." required>
                  <option></option>
                    @foreach($status as $stat)
                      @if($stat->category_id == 1 )
                        <option value="{{ $stat->id }}">{{ $stat->description }}</option>
                      @endif
                    @endforeach
                </select>
              <div>
              <div class="form-group">
                <label for="note_aprov">Note</label>
                <input type="text" class="form-control" id="note_aprov" name="note_aprov" placeholder="Note">
              </div>
            </div>
            <div class="block-content block-content-full text-right ">
              <a type="button" class="btn btn-sm btn-hero-light close-modal">Close</a>
              <button type="submit" class="btn btn-sm btn-hero-primary">Approve</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!-- END From Left Block Modal -->

  <script>

    $(window).on('hashchange', function() {
      if (window.location.hash) {
        var page = window.location.hash.replace('#', '');
        if (page == Number.NaN || page <= 0) {
            return false;
        } else {
            get(page);
        }
      }
    });

    $(document).ready(function() {
      let fecha = '{{ $fechaInit }}';

      $('.single-item').slick({
        infinite: true,
        slidesToShow: 7,
        slidesToScroll: 7,
        speed: 300,
        //initialSlide :64,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
        ]
      });
      $(".slick-slide").each(function(){
        if($(this).data('fecha') == fecha){
          $('.single-item').slick('goTo',$(this).data('slick-index'));
        }
      });


      $(document).on('click', '.pagination a', function (e) {
        let page = $(this).attr('href').split('page=')[1];
        let url = `dashboard?desde=${$('#desde').val()}&hasta=${$('#hasta').val()}&empleado=${$('#empleado').find(':selected').val()}&razon=${$('#razon').find(':selected').val()}&violacion=${$('#violacion').find(':selected').val()}&status=${$('#estatus').find(':selected').val()}&fecha=&page=${page}`;
        get(url);
        e.preventDefault();
      });
    });

    $('.close-modal').on('click',function() {
        $('#approveModal').modal('hide');
    });

    $('#absenceAprob').on('submit',async function () {
      try{
        let params = {
            points : $('#points').val(),
            status : $('#status_id').find(':selected').val(),
            note : $('#note_aprov').val(),
        };

        let resp = await request(`absences/aprove/${$('#id').val()}`,'post',params);
        if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
        search();
        $('#approveModal').modal('hide');
      }catch (error) {
        Swal.fire('Operation failed',error.msg,error.status);
      }
    });

    async function search(){
      try{
        let url = `dashboard?desde=${$('#desde').val()}&hasta=${$('#hasta').val()}&empleado=${$('#empleado').find(':selected').val()}&razon=${$('#razon').find(':selected').val()}&violacion=${$('#violacion').find(':selected').val()}&status=${$('#estatus').find(':selected').val()}&&fecha=`;
        let data = await get(url,'get');
      }catch (error) {
        Swal.fire('Operation failed',error.msg,error.status);
      }
    }

    function resetAll(){
      $('#desde').val('');
      $('#hasta').val('');
      $('#empleado').val('').change();
      $('#razon').val('').change();
      $('#violacion').val('').change();
      $('#estatus').val('').change();
      search();
    }

    async function get(url) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: 'GET',
          url: url,
          data: {},
          success: function(response){
            $('#childDash').html(response);
            location.hash = `dashboard?desde=${$('#desde').val()}&hasta=${$('#hasta').val()}&empleado=${$('#empleado').find(':selected').val()}&razon=${$('#razon').find(':selected').val()}&violacion=${$('#violacion').find(':selected').val()}&status=${$('#estatus').find(':selected').val()}`;
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

    async function aprobar(sender) {
      $('#id').val($(sender).data('id'));
      $('#points').val($(sender).data('point'));
      $('#status_id').val('').change();
      $('#note_aprov').val('');
      $('#approveModal').modal('show');
    }

    async function rechazar(sender) {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, Reject it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(sender).data('id');
            let resp = await request(`absences/reject/${id}`,'post',{});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            search()
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    }

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

    async function searchChild(sender){
      $(".page-item").each(function(){
        $(this).removeClass("active");
      });
      $(sender).addClass("active");
      let fecha = $(sender).data('fecha');
      try{
        let url = `dashboard?desde=&hasta=&empleado=&razon=&violacion=&status=&fecha=${fecha}`;
        let data = await get(url,'get');
      }catch (error) {
        Swal.fire('Operation failed',error.msg,error.status);
      }
    }
  </script>

@endsection
