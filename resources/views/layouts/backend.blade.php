<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>TiBann Intergrow</title>
        <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
        <meta http-equiv="Pragma" content="no-cache" />
        <meta http-equiv="Expires" content="0" />

        <meta name="description" content="Intergrow">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Icons -->
        <link rel="shortcut icon" href="{{ asset('media/favicons/favicon.png') }}">
        <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

        @yield('css_before')
        <!-- Plugins -->
        <link rel="stylesheet" id="css-main" href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,600,700">
        <link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('js/plugins/DataTables-1.10.21/datatables.min.css') }}">

        <link rel="stylesheet" href="{{ asset('js/plugins/select2/css/select2.css') }}">
        <link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
        <link rel="stylesheet" href="{{ asset('js/plugins/time-picker/mdtimepicker.min.css') }}">
        <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick.css') }}">
        <link rel="stylesheet" href="{{ asset('js/plugins/slick-carousel/slick-theme.css') }}">

        <link rel="stylesheet" id="css-theme" href="{{ asset('css/dashmix.css') }}">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        @yield('css_after')

        <!-- Scripts -->
        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>

    </head>
    <body>

        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-fixed page-header-dark main-content-narrow">
            <!-- Side Overlay-->
            <aside id="side-overlay">
                <!-- Side Header -->
                <div class="bg-image" style="background-image: url('{{ asset('media/various/bg_side_overlay_header.jpg') }}');">
                    <div class="bg-primary-op">
                        <div class="content-header">
                            <!-- User Avatar -->
                            <a class="img-link mr-1" href="javascript:void(0)">
                                <img class="img-avatar img-avatar48" src="{{ asset('media/avatars/avatar10.jpg') }}" alt="">
                            </a>
                            <!-- END User Avatar -->

                            <!-- User Info -->
                            <div class="ml-2">
                                <a class="text-white font-w600" href="javascript:void(0)">George Taylor</a>
                                <div class="text-white-75 font-size-sm font-italic">Full Stack Developer</div>
                            </div>
                            <!-- END User Info -->

                            <!-- Close Side Overlay -->
                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                            <a class="ml-auto text-white" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_close">
                                <i class="fa fa-times-circle"></i>
                            </a>
                            <!-- END Close Side Overlay -->
                        </div>
                    </div>
                </div>
                <!-- END Side Header -->

                <!-- Side Content -->
                <div class="content-side">
                    <p>
                        Content..
                    </p>
                </div>
                <!-- END Side Content -->
            </aside>
            <!-- END Side Overlay -->

            <!-- Sidebar -->
            <nav id="sidebar" aria-label="Main Navigation">
                <!-- Side Header -->
                <div class="bg-header-dark">
                    <div class="content-header bg-white-10">
                        <!-- Logo -->
                        <a class="link-fx font-w600 font-size-lg text-white" href="/">
                            <span class="smini-visible">
                                <span class="text-white-75">Ti</span><span class="text-white">Bann</span>
                            </span>
                            <span class="smini-hidden">
                                <span class="text-white-75">Ti</span><span class="text-white">Bann</span>
                            </span>
                        </a>
                        <!-- END Logo -->

                        <!-- Options -->
                        <div>
                            <!-- Toggle Sidebar Style -->
                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                            <!-- Class Toggle, functionality initialized in Helpers.coreToggleClass() -->
                            <a class="js-class-toggle text-white-75" data-target="#sidebar-style-toggler" data-class="fa-toggle-off fa-toggle-on" data-toggle="layout" data-action="sidebar_style_toggle" href="javascript:void(0)">
                                <i class="fa fa-toggle-off" id="sidebar-style-toggler"></i>
                            </a>
                            <!-- END Toggle Sidebar Style -->

                            <!-- Close Sidebar, Visible only on mobile screens -->
                            <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                            <a class="d-lg-none text-white ml-2" data-toggle="layout" data-action="sidebar_close" href="javascript:void(0)">
                                <i class="fa fa-times-circle"></i>
                            </a>
                            <!-- END Close Sidebar -->
                        </div>
                        <!-- END Options -->
                    </div>
                </div>
                <!-- END Side Header -->

                <!-- Side Navigation -->
                <div class="content-side content-side-full">
                    <ul class="nav-main">
                        <li class="nav-main-item">
                            <a class="nav-main-link{{ request()->is('dashboard') ? ' active' : '' }}" href="dashboard">
                                <i class="nav-main-link-icon si si-cursor"></i>
                                <span class="nav-main-link-name">Dashboard</span>
                            </a>
                        </li>

                        <li class="nav-main-heading">System</li>
                            <li class="nav-main-item{{ (request()->is('employes') || request()->is('absences')) ? ' open' : '' }}">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                                    <i class="nav-main-link-icon si si-notebook"></i>
                                    <span class="nav-main-link-name">Register</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    @if(!in_array(Auth::user()->rol_id,[5,6,11]))
                                      <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('employes') ? ' active' : '' }}" href="employes">
                                          <i class="nav-main-link-icon si si-users"></i>
                                          <span class="nav-main-link-name">Employee</span>
                                        </a>
                                      </li>
                                    @endif
                                     <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('absences') ? ' active' : '' }}" href="absences">
                                            <i class="nav-main-link-icon si si-clock"></i>
                                            <span class="nav-main-link-name">Absence Request</span>
                                        </a>
                                    </li>
                                     <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('violation_absences') ? ' active' : '' }}" href="violation_absences">
                                            <i class="nav-main-link-icon si si-clock"></i>
                                            <span class="nav-main-link-name">Violations</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @if(!in_array(Auth::user()->rol_id,[5,6,11]))
                            <li class="nav-main-item{{ (request()->is('control-point')) ? ' open' : '' }}">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                                    <i class="nav-main-link-icon si si-calendar"></i>
                                    <span class="nav-main-link-name">Monitors</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('control-point') ? ' active' : '' }}" href="control-point">
                                            <i class="nav-main-link-icon si si-users"></i>
                                            <span class="nav-main-link-name">Deduction Points</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-main-item{{ (request()->is('departament') ||
                                request()->is('position') ||
                                request()->is('schedule') ||
                                request()->is('profession') ||
                                request()->is('motive') ||
                                request()->is('violation') ||
                                request()->is('import-excel') ||
                                request()->is('roles') ||
                                request()->is('roles-filter') ||
                                request()->is('users') ||
                                request()->is('workingday')||
                                request()->is('tasks') ||
                                request()->is('categories') ||
                                request()->is('activities') ||
                                request()->is('area') ||
                                request()->is('relationtype') ||
                                request()->is('unitofmeasure') ||
                                request()->is('city') ||
                                request()->is('state') ||
                                request()->is('zipcode')||
                                request()->is('site')
                                ) ? ' open' : '' }}">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                    <i class="nav-main-link-icon si si-settings"></i>
                                    <span class="nav-main-link-name">Configurations</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('area') ? ' active' : '' }}" href="area">
                                            <i class="nav-main-link-icon si si-screen-desktop"></i>
                                            <span class="nav-main-link-name">Areas</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('departament') ? ' active' : '' }}" href="departament">
                                            <i class="nav-main-link-icon si si-screen-desktop"></i>
                                            <span class="nav-main-link-name">Departments</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('position') ? ' active' : '' }}" href="position">
                                            <i class="nav-main-link-icon si si-size-fullscreen"></i>
                                            <span class="nav-main-link-name">Positions</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('schedule') ? ' active' : '' }}" href="schedule">
                                            <i class="nav-main-link-icon si si-target"></i>
                                            <span class="nav-main-link-name">Schedules</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('profession') ? ' active' : '' }}" href="profession">
                                            <i class="nav-main-link-icon si si-trophy"></i>
                                            <span class="nav-main-link-name">Professions</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('motive') ? ' active' : '' }}" href="motive">
                                            <i class="nav-main-link-icon si si-share-alt"></i>
                                            <span class="nav-main-link-name">Reasons</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('control') ? ' active' : '' }}" href="control">
                                            <i class="nav-main-link-icon si si-shield"></i>
                                            <span class="nav-main-link-name">Deduction Points Types</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('violation') ? ' active' : '' }}" href="violation">
                                            <i class="nav-main-link-icon si si-shield"></i>
                                            <span class="nav-main-link-name">Deduction Points</span>
                                        </a>
                                    </li>
                                     <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('relationtype') ? ' active' : '' }}" href="relationtype">
                                            <i class="nav-main-link-icon si si-share-alt"></i>
                                            <span class="nav-main-link-name">Relations Types Reasons</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('status') ? ' active' : '' }}" href="status">
                                            <i class="nav-main-link-icon si si-share-alt"></i>
                                            <span class="nav-main-link-name">Status</span>
                                        </a>
                                    </li>
                                     <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('tasks') ? ' active' : '' }}" href="tasks">
                                            <i class="nav-main-link-icon si si-share-alt"></i>
                                            <span class="nav-main-link-name">Tasks</span>
                                        </a>
                                    </li>
                                     <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('activities') ? ' active' : '' }}" href="activities">
                                            <i class="nav-main-link-icon si si-share-alt"></i>
                                            <span class="nav-main-link-name">Activities</span>
                                        </a>
                                    </li>
                                     <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('city') ? ' active' : '' }}" href="city">
                                            <i class="nav-main-link-icon si si-share-alt"></i>
                                            <span class="nav-main-link-name">Cities</span>
                                        </a>
                                    </li>
                                     <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('site') ? ' active' : '' }}" href="site">
                                            <i class="nav-main-link-icon si si-share-alt"></i>
                                            <span class="nav-main-link-name">Site</span>
                                        </a>
                                    </li>
                                     <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('state') ? ' active' : '' }}" href="state">
                                            <i class="nav-main-link-icon si si-share-alt"></i>
                                            <span class="nav-main-link-name">States</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('zipcode') ? ' active' : '' }}" href="zipcode">
                                            <i class="nav-main-link-icon si si-share-alt"></i>
                                            <span class="nav-main-link-name">Zip Codes</span>
                                        </a>
                                    </li>
                                     <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('categories') ? ' active' : '' }}" href="categories">
                                            <i class="nav-main-link-icon si si-share-alt"></i>
                                            <span class="nav-main-link-name">Categories</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('import-excel') ? ' active' : '' }}" href="import-excel">
                                            <i class="nav-main-link-icon si si-share"></i>
                                            <span class="nav-main-link-name">Import/Export Employee</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a href="roles" class="nav-main-link{{ request()->is('roles') ? ' active' : '' }}">
                                            <i class="nav-main-link-icon si si-wrench"></i>
                                            <span class="nav-main-link-name">Roles</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a href="unitofmeasure" class="nav-main-link{{ request()->is('unitofmeasure') ? ' active' : '' }}">
                                            <i class="nav-main-link-icon si si-calendar"></i>
                                            <span class="nav-main-link-name">Unit of Measurement</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a href="workingday" class="nav-main-link{{ request()->is('workingday') ? ' active' : '' }}">
                                            <i class="nav-main-link-icon si si-calendar"></i>
                                            <span class="nav-main-link-name">WorkDay</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('users') ? ' active' : '' }}" href="users">
                                            <i class="nav-main-link-icon si si-user"></i>
                                            <span class="nav-main-link-name">Users</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-main-item{{ (request()->is('timesheet')) ? ' open' : '' }}">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                    <i class="nav-main-link-icon si si-doc"></i>
                                    <span class="nav-main-link-name">Labor Planning</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('timesheet') ? ' active' : '' }}" href="timesheet">
                                            <span class="nav-main-link-name">TimeSheet</span>
                                        </a>
                                    </li>
                                     <li class="nav-main-item">
                                </ul>
                            </li>

                            <li class="nav-main-item{{ (request()->is('')) ? ' open' : '' }}">
                                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="true" href="#">
                                    <i class="nav-main-link-icon si si-doc"></i>
                                    <span class="nav-main-link-name">Reports</span>
                                </a>
                                <ul class="nav-main-submenu">
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('') ? ' active' : '' }}" href="https://datastudio.google.com/reporting/1vsE09kPKrpo65zpQif3fwsFxSca93Gl1/page/GQHWB">
                                            <span class="nav-main-link-name">Reason Analysts</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('') ? ' active' : '' }}" href="https://datastudio.google.com/embed/reporting/1vGlvuXM4nbez6P1IGYMxgij3aydUI5_L/page/GQHWB">
                                            <span class="nav-main-link-name">Vacation</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('') ? ' active' : '' }}" href="https://datastudio.google.com/embed/reporting/16rKH3a1s8zo5m0kO1tDlbwgMCRMbwJBH/page/GQHWB">
                                            <span class="nav-main-link-name">Attendance List</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('') ? ' active' : '' }}" href="https://datastudio.google.com/reporting/15E1auOB0D-B-CN8bIZRLk2Jp-VEYrSvs/page/GQHWB">
                                            <span class="nav-main-link-name">Status Analysts</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('') ? ' active' : '' }}" href="https://datastudio.google.com/reporting/1WsWeOcOsFxAjh9ktNTjVpa-p97PZf2cu/page/GQHWB">
                                            <span class="nav-main-link-name">Deduction Point Analysts</span>
                                        </a>
                                    </li>
                                    <li class="nav-main-item">
                                        <a class="nav-main-link{{ request()->is('') ? ' active' : '' }}" href="https://datastudio.google.com/embed/reporting/60bc5b2b-5985-4fa1-8cdc-4db049044772/page/5suZB">
                                            <span class="nav-main-link-name">Warning Note</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
                <!-- END Side Navigation -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                <!-- Header Content -->
                <div class="content-header">
                    <!-- Left Section -->
                    <div>
                        <!-- Toggle Sidebar -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout()-->
                        <button type="button" class="btn btn-dual mr-1" data-toggle="layout" data-action="sidebar_toggle">
                            <i class="fa fa-fw fa-bars"></i>
                        </button>
                        <!-- END Toggle Sidebar -->

                        <!-- Open Search Section -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        {{--<button type="button" class="btn btn-dual" data-toggle="layout" data-action="header_search_on">
                            <i class="fa fa-fw fa-search"></i> <span class="ml-1 d-none d-sm-inline-block">Search</span>
                        </button>--}}
                        <!-- END Open Search Section -->
                    </div>
                    <!-- END Left Section -->

                    <!-- Right Section -->
                    <div>
                        <!-- User Dropdown -->
                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn btn-dual" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-fw fa-user d-sm-none"></i>
                                <span class="d-none d-sm-inline-block">{{ Auth::user()->name }} {{ (!empty(Auth::user()->role()) ? '('.Auth::user()->role()->name.')' : '') }}</span>
                                <i class="fa fa-fw fa-angle-down ml-1 d-none d-sm-inline-block"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="page-header-user-dropdown">
                                <div class="bg-primary-darker rounded-top font-w600 text-white text-center p-3">
                                   User Settings
                                </div>
                                <div class="p-2">
                                    <a class="dropdown-item" href="javascript:void(0)">
                                        <i class="far fa-fw fa-user mr-1"></i> Profile
                                    </a>
                                    <div role="separator" class="dropdown-divider"></div>
                                    <!-- Toggle Side Overlay -->
                                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                    <a class="dropdown-item" href="javascript:void(0)" data-toggle="layout" data-action="side_overlay_toggle">
                                        <i class="far fa-fw fa-building mr-1"></i> Configuration
                                    </a>
                                    <!-- END Side Overlay -->

                                    <div role="separator" class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="logout">
                                        <i class="far fa-fw fa-arrow-alt-circle-left mr-1"></i> Exit
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- END User Dropdown -->

                        <!-- Notifications Dropdown -->
                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn btn-dual" id="page-header-notifications-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-fw fa-bell"></i>
                                <span class="badge badge-secondary badge-pill"></span>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0" aria-labelledby="page-header-notifications-dropdown">
                                <div class="bg-primary-darker rounded-top font-w600 text-white text-center p-3">
                                   Notification
                                </div>
                                <ul class="nav-items my-2">
    <!--                                     <li>
                                            <a class="text-dark media py-2" href="javascript:void(0)">
                                                <div class="mx-3">
                                                    <i class="fa fa-fw fa-check-circle text-success"></i>
                                                </div>
                                                <div class="media-body font-size-sm pr-2">
                                                    <div class="font-w600">App was updated to v5.6!</div>
                                                    <div class="text-muted font-italic">3 min ago</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="text-dark media py-2" href="javascript:void(0)">
                                                <div class="mx-3">
                                                    <i class="fa fa-fw fa-user-plus text-info"></i>
                                                </div>
                                                <div class="media-body font-size-sm pr-2">
                                                    <div class="font-w600">New Subscriber was added! You now have 2580!</div>
                                                    <div class="text-muted font-italic">10 min ago</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="text-dark media py-2" href="javascript:void(0)">
                                                <div class="mx-3">
                                                    <i class="fa fa-fw fa-times-circle text-danger"></i>
                                                </div>
                                                <div class="media-body font-size-sm pr-2">
                                                    <div class="font-w600">Server backup failed to complete!</div>
                                                    <div class="text-muted font-italic">30 min ago</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="text-dark media py-2" href="javascript:void(0)">
                                                <div class="mx-3">
                                                    <i class="fa fa-fw fa-exclamation-circle text-warning"></i>
                                                </div>
                                                <div class="media-body font-size-sm pr-2">
                                                    <div class="font-w600">You are running out of space. Please consider upgrading your plan.</div>
                                                    <div class="text-muted font-italic">1 hour ago</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="text-dark media py-2" href="javascript:void(0)">
                                                <div class="mx-3">
                                                    <i class="fa fa-fw fa-plus-circle text-primary"></i>
                                                </div>
                                                <div class="media-body font-size-sm pr-2">
                                                    <div class="font-w600">New Sale! + $30</div>
                                                    <div class="text-muted font-italic">2 hours ago</div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="p-2 border-top">
                                        <a class="btn btn-light btn-block text-center" href="javascript:void(0)">
                                            <i class="fa fa-fw fa-eye mr-1"></i> View All
                                        </a>
                                    </div> -->
                            </div>
                        </div>
                        <!-- END Notifications Dropdown -->

                        <!-- Toggle Side Overlay -->
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        {{--<button type="button" class="btn btn-dual" data-toggle="layout" data-action="side_overlay_toggle">
                            <i class="far fa-fw fa-list-alt"></i>
                        </button>--}}
                        <!-- END Toggle Side Overlay -->
                    </div>
                    <!-- END Right Section -->
                </div>
                <!-- END Header Content -->

                {{--<!-- Header Search -->
                <div id="page-header-search" class="overlay-header bg-primary">
                    <div class="content-header">
                        <form class="w-100" action="/dashboard" method="POST">
                            @csrf
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                                    <button type="button" class="btn btn-primary" data-toggle="layout" data-action="header_search_off">
                                        <i class="fa fa-fw fa-times-circle"></i>
                                    </button>
                                </div>
                                <input type="text" class="form-control border-0" placeholder="Search or hit ESC.." id="page-header-search-input" name="page-header-search-input">
                            </div>
                        </form>
                   </div>
                </div>
                <!-- END Header Search -->--}}

                <!-- Header Loader -->
                <!-- Please check out the Loaders page under Components category to see examples of showing/hiding it -->
                <div id="page-header-loader" class="overlay-header bg-primary-darker">
                    <div class="content-header">
                        <div class="w-100 text-center">
                            <i class="fa fa-fw fa-2x fa-sun fa-spin text-white"></i>
                        </div>
                    </div>
                </div>
                <!-- END Header Loader -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
                @yield('content')
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="bg-body-light">
                <div class="content py-0">
                    <div class="row font-size-sm">
                        {{--<div class="col-sm-3 order-sm-2">
                          <a class="font-w600" href="https://play.google.com/store/apps/details?id=syh.computacion.intergrowsupervisor" target="_blank">
                            <img src="{{ asset('images/google.png') }}" style="width: 120px !important; margin-top: 10px;">
                          </a>
                        </div>
                        <div class="col-sm-3 order-sm-2">
                          <a class="font-w600" href="https://www.intergrow.site/appAndroid/AppIntergrowSupervisor.apk" target="_blank">
                            <img src="{{ asset('images/descarga-directa.png') }}" style="width: 150px !important;">
                          </a>
                        </div>--}}
                        <div class="col-sm-3 order-sm-2 mb-1 mb-sm-0 text-center text-sm-right">
                            Versión <a class="font-w600" href="#" target="_blank">1.0</a>
                        </div>
                        <div class="col-sm-3 order-sm-1 text-center text-sm-left">
                            <a class="font-w600" href="#" >Tibann</a> &copy; <span data-toggle="year-copy">2020</span>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <script src="{{ asset('js/dashmix.app.min.js') }}"></script>
        <script src="{{ asset('js/dashmix.core.min.js') }}"></script>
        <script src="{{ asset('js/plugins/DataTables-1.10.21/datatables.min.js') }}"></script>
        <script src="{{ asset('js/pages/be_tables_datatables.min.js') }}"></script>
        <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('js/plugins/select2/js/select2.full.js') }}"></script>
        <script type="text/javascript" src='https://code.highcharts.com/highcharts.js'></script>

        <!-- Page JS Plugins -->
        <script src="{{ asset('js/plugins/jquery-bootstrap-wizard/bs4/jquery.bootstrap.wizard.js') }}"></script>
        <script src="{{ asset('js/plugins/jquery-validation/jquery.validate.js') }}"></script>
        <script src="{{ asset('js/plugins/jquery-validation/additional-methods.js') }}"></script>
        <script src="{{ asset('js/plugins/time-picker/mdtimepicker.min.js') }}"></script>
        <!-- Page JS Code -->
        <script src="{{ asset('js/pages/be_forms_wizard.min.js') }}"></script>
        <script src="{{ asset('js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
        <script src="{{ asset('js/plugins/jquery.maskedinput/jquery.maskedinput.min.js') }}"></script>
        <script src="{{ asset('js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment-with-locales.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
        <script src="{{ asset('js/plugins/lodash.js') }}"></script>
        <script src="{{ asset('js/plugins/slick-carousel/slick.min.js') }}"></script>

        <script>jQuery(function(){ Dashmix.helpers(['datepicker','select2','sparkline','notify','table-tools-checkable','table-tools-sections','tooltips','masked-inputs','maxlength','slick']); });</script>

        @yield('js_after')

        <script>
          $(document).ready(function()
          {
            @if(session()->has('status'))
              @php
                $status = session('status');
              @endphp
              Swal.fire('{{ $status['title'] }}', '{{ $status['msg'] }}', '{{ $status['status'] }}')
            @endif
          });
        </script>
    </body>
</html>
