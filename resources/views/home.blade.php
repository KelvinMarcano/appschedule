@extends('layouts.backend')

@section('content')
<!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">Intergrow</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item">Layout</li>
                        <li class="breadcrumb-item">Content</li> -->
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <div class="block block-rounded block-bordered text-center">
            <div class="block-content" id="grafica">
                <script>
                     $(function($){
                         $('#grafica').highcharts({
                             title:{text:'Graphics Attendances'},
                             xAxis:{categories:['2002','2004','2015']},
                             yAxis:{title:'Porcentaje %',plotLines:[{value:0,width:1,color:'#808080'}]},
                             tooltip:{valueSuffix:'%'},
                             legend:{layout:'vertical',align:'right',verticalAlign:'middle',borderWidth:0},
                             series:[{type: 'column',name: 'Domingo',data: [25,23, 21]}, 
                                     {name: 'Lunes',data: [20,18, 19]}, 
                                     {type: 'column',name: 'Martes',data: [15, 17,11]}, 
                                     {type: 'spline',name: 'Miercoles',data: [0, 4, 4]},
                                     {name: 'Jueves',data: [0,1, 1.5]}
                           ],
                             plotOptions:{line:{dataLabels:{enabled:true}}}
                         });
                     });
                    </script>
                <!-- <p>Content always takes up all the available width of the main container.</p> -->
            </div>
        </div>
        <br>
        <div class="row">
            
        <div class="wrapper">
            <a class="button" href="employes">Add Employe</a>
        </div>
        <div class="wrapper">
            <a class="button" href="absences">Add Attendances</a>
        </div>
        <div class="wrapper">
            <a class="button" href="import-excel">Import/Export Employe</a>
        </div>

        <!-- Filter: https://css-tricks.com/gooey-effect/ -->
        <svg style="visibility: hidden; position: absolute;" width="0" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1">
            <defs>
                <filter id="goo"><feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />    
                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo" />
                    <feComposite in="SourceGraphic" in2="goo" operator="atop"/>
                </filter>
            </defs>
        </svg>

        </div>
    </div>
    <!-- END Page Content -->
@endsection
<style type="text/css">
:root {
    --bg: #1a1e24;
    --color: #eee;
    --font: Montserrat, Roboto, Helvetica, Arial, sans-serif;
}

.wrapper {
    padding: 1.5rem 0;
    filter: url('#goo');
}

.button {
    display: inline-block;
    text-align: center;
    background: var(--color);
    color: var(--bg);
    font-weight: bold;
    padding: 1.18em 1.32em 1.03em;
    line-height: 1;
    border-radius: 1em;
    position: relative;
    min-width: 8.23em;
    text-decoration: none;
    font-family: var(--font);
    font-size: 1.25rem;
}

.button:before,
.button:after {
    width: 4.4em;
    height: 2.95em;
    position: absolute;
    content: "";
    display: inline-block;
    background: var(--color);
    border-radius: 50%;
    transition: transform 1s ease;
    transform: scale(0);
    z-index: -1;
}

.button:before {
    top: -25%;
    left: 20%;
}

.button:after {
    bottom: -25%;
    right: 20%;
}

.button:hover:before,
.button:hover:after {
    transform: none;
}


/* Demo styles */

body {
    width: 100%;
    height: 100%;
    min-height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    background: var(--bg)
}
</style>
