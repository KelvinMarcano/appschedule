@extends('layouts.backend')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card" style="width: 68rem;">
                <br><br>
                    <div style="font-size: 32px; text-align: center;">{{ __('Employes') }}</div>
                <br><br>
                <div class="card-body">
                    <div class="form-group row">
                        <label for="code" class="col-md-4 col-form-label text-md-right">{{ __('ID Employe') }}</label>

                        <div class="col-md-6">
                            <input id="code" type="text" class="form-control @error('code') is-invalid @enderror" name="code" value="{{ old('code') }}" required autocomplete="code" autofocus>

                            @error('code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <form method="POST" action="employes">
                        {{ csrf_field() }}
                        <!-- @csrf -->
                        <div class="form-group row">
                            <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('FirstName') }}</label>

                            <div class="col-md-6">
                                <input id="firstname" type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" value="{{ old('firstname') }}" required autocomplete="firstname" autofocus>

                                @error('firstname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('LastName') }}</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>

                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sex" class="col-md-4 col-form-label text-md-right">{{ __('Sex') }}</label>

                             <div class="col-md-6">
                                    <select id="sex" name="sex" class="custom-select">     
                                      @foreach($sexo as $sexo)
                                        <option value="{{$sexo->id}}">{{$sexo->item}}</option>
                                      @endforeach
                                    </select>
                                @error('sexo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birthdate" class="col-md-4 col-form-label text-md-right">{{ __('Birthdate') }}</label>

                            <div class="col-md-6">
                                <input id="birthdate" type="date" class="form-control @error('birthdate') is-invalid @enderror" name="birthdate" value="{{ old('birthdate') }}" required autocomplete="birthdate" autofocus data-date="" data-date-format="DD MMMM YYYY">

                                @error('birthdate')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus>

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="numbersocial" class="col-md-4 col-form-label text-md-right">{{ __('Number Social') }}</label>

                            <div class="col-md-6">
                                <input id="numbersocial" type="text" class="form-control @error('numbersocial') is-invalid @enderror" name="numbersocial" value="{{ old('numbersocial') }}" required autocomplete="numbersocial" autofocus>

                                @error('numbersocial')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="zipcode" class="col-md-4 col-form-label text-md-right">{{ __('Zip Code') }}</label>

                            <div class="col-md-6">
                                <input id="zipcode" type="text" class="form-control @error('zipcode') is-invalid @enderror" name="zipcode" value="{{ old('zipcode') }}" required autocomplete="zipcode" autofocus>

                                @error('zipcode')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="numberphone" class="col-md-4 col-form-label text-md-right">{{ __('Number Phone') }}</label>
                            <div class="col-md-6">
                                <input id="numberphone" type="text" class="form-control @error('numberphone') is-invalid @enderror" name="numberphone" value="{{ old('numberphone') }}" required autocomplete="numberphone" autofocus>
                                @error('numberphone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="numberhome" class="col-md-4 col-form-label text-md-right">{{ __('Number Home') }}</label>
                            <div class="col-md-6">
                                <input id="numberhome" type="text" class="form-control @error('numberhome') is-invalid @enderror" name="numberhome" value="{{ old('numberhome') }}" required autocomplete="numberhome" autofocus>
                                @error('numberhome')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="numberfax" class="col-md-4 col-form-label text-md-right">{{ __('Number Fax') }}</label>
                            <div class="col-md-6">
                                <input id="numberfax" type="text" class="form-control @error('numberfax') is-invalid @enderror" name="numberfax" value="{{ old('numberfax') }}" autocomplete="numberfax" autofocus>
                                @error('numberfax')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sociallink" class="col-md-4 col-form-label text-md-right">{{ __('Social Link') }}</label>

                            <div class="col-md-6">
                                <input id="sociallink" type="text" class="form-control @error('sociallink') is-invalid @enderror" name="sociallink" value="{{ old('sociallink') }}" autocomplete="sociallink" autofocus>

                                @error('sociallink')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="area" class="col-md-4 col-form-label text-md-right">{{ __('Area') }}</label>

                            <div class="col-md-6">
                                    <select id="area" name="area" class="custom-select">     
                                      @foreach($area as $areas)
                                        <option value="{{$areas->id}}">{{$areas->item}}</option>
                                      @endforeach
                                    </select>
                                @error('area')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="departamento" class="col-md-4 col-form-label text-md-right">{{ __('Departament') }}</label>

                            <div class="col-md-6">
                                    <select id="departamento" name="departamento" class="custom-select">     
                                      @foreach($departamento as $departamentos)
                                        <option value="{{$departamentos->id}}">{{$departamentos->item}}</option>
                                      @endforeach
                                    </select>
                                @error('departamento')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cargo" class="col-md-4 col-form-label text-md-right">{{ __('Position') }}</label>

                            <div class="col-md-6">
                                    <select id="cargo" name="cargo" class="custom-select">          
                                        @foreach($cargo as $cargo)
                                        <option value="{{$cargo->id}}">{{$cargo->item}}</option>
                                      @endforeach
                                    </select>

                                @error('cargo')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="horario" class="col-md-4 col-form-label text-md-right">{{ __('Schedule') }}</label>

                            <div class="col-md-6">
                                    <select id="horario" name="horario" class="custom-select">          
                                        @foreach($horario as $horario)
                                            <option value="{{$horario->id}}">{{$horario->item}}</option>
                                        @endforeach   
                                    </select>

                                @error('horario')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="profesion" class="col-md-4 col-form-label text-md-right">{{ __('Profession') }}</label>

                            <div class="col-md-6">
                                    <select id="profesion" name="profesion" class="custom-select">    
                                     @foreach($profesion as $profesion)      
                                        <option value="{{$profesion->id}}">{{$profesion->item}}</option>    
                                         @endforeach   
                                    </select>

                                @error('profesion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="profesion" class="col-md-4 col-form-label text-md-right">{{ __('Roles') }}</label>

                            <div class="col-md-6">
                                    <select id="roles" name="roles" class="custom-select">    
                                     @foreach($roles as $roles)      
                                        <option value="{{$roles->id}}">{{$roles->item}}</option>    
                                         @endforeach   
                                    </select>

                                @error('roles')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="col-md-4 col-form-label text-md-right">{{ __('Status') }}</label>

                            <div class="col-md-6">
                                    <select id="status" name="status" class="custom-select">          
                                        <option value="AC">Active</option> 
                                        <option value="IN">Inactive</option>    
                                    </select>

                                @error('status')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Register') }}
                                </button>
                                   <!-- <a href="{{ action('HomeController@index') }}" class="btn btn-primary">Exit</a> -->
                            </div>
                        </div>

                         <br>
                        <br>
                        <br>
                        <br>
                       
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

 <div>
    <table id="tablaexp" class="table table-striped table-bordered nowrap" style="width:100%">
          <thead>
            <tr>
              <th>#</th>
               <th>ID EMPLOYES</th>
              <th>FIRSTNAME</th>
              <th>LASTNAME</th>
              <th>BIRTHDAY</th>
              <th>ADDRESS</th>
              <th>NUMBER PHONE</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
          @foreach($employes as $employes) 
          <tr>
            <td>{{$employes->id}}</td>
            <td>{{$employes->code}}</td>
            <td>{{$employes->firstname}}</td>
            <td>{{$employes->lastname}}</td>
            <td>{{$employes->birthdate}}</td>
            <td>{{$employes->address}}</td>
            <td>{{$employes->numberphone}}</td>                                  
             <td class="text-center">
                <div class="btn-group">
                     <a class="fa fa-pencil btn btn-sm btn-success customerEditModal" data-toggle="modal" data-target="#modalAfiliacion" title="Edit" id="{{$employes->id}}" data-code="{{$employes->code}}" 
                        data-firstname="{{$employes->firstname}}" data-lastname="{{$employes->lastname}}" data-sex="{{$employes->sex}}" data-birthdate="{{$employes->birthdate}}" data-address="{{$employes->address}}"    data-numbersocial="{{$employes->numbersocial}}" data-zipcode="{{$employes->zipcode}}"
                        data-numberphone="{{$employes->numberphone}}" data-numberhome="{{$employes->numberhome}}"
                        data-numberfax="{{$employes->numberfax}}" data-sociallink="{{$employes->sociallink}}"
                        data-departamento="{{$employes->departamentid}}" data-horario="{{$employes->scheduleid}}"
                        data-cargo="{{$employes->positionid}}" data-profesion="{{$employes->professionid}}" data-roles="{{$employes->rolesid}}" data-status="{{$employes->status}}"></a>
                        <a class="fa fa-trash btn btn-sm btn-success DeleteModal" data-toggle="modal" data-target="#DeleteModal" title="Delete" id="{{$employes->id}}"></a>
               
                     </button>
                 </div>
             </td>
          </tr>
          @endforeach 
          </tbody>

    </table>
</div>

@endsection

 <!-- {{-- modalModificarDepartamento--}} -->
        <div class="modal fade" id="modalAfiliacion"  role="dialog" aria-labelledby="modal-block-popout" aria-hidden="true">
            <div class="modal-dialog modal-dialog-popout" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">Modificar Registro</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-fw fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <form class="form-horizontal" role="form" method="POST" action="{{ url('updateEmployes') }}">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                  <div class="row push">
                                      <div class="col-lg-12 col-xl-12">
                                        
                                         <div class="form-group">
                                              <label for="code1" class="col-12 col-form-label">ID Employe</label>
                                                <input id="code1" type="text" class="form-control @error('code1') is-invalid @enderror" name="code1" value="{{ old('code1') }}" required autocomplete="code1" autofocus>

                                                @error('code1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                          </div>

                                         <div class="form-group">
                                              <label for="firstname1" class="col-12 col-form-label">FirstName</label>
                                                <input id="firstname1" type="text" class="form-control @error('firstname1') is-invalid @enderror" name="firstname1" value="{{ old('firstname1') }}" required autocomplete="firstname1" autofocus>

                                                @error('firstname1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                          </div>

                                          <div class="form-group">
                                              <label for="lastname1" class="col-12 col-form-label">LastName</label>
                                                <input id="lastname1" type="text" class="form-control @error('lastname1') is-invalid @enderror" name="lastname1" value="{{ old('lastname1') }}" required autocomplete="lastname1" autofocus>

                                                @error('lastname1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                          </div>
                                         
                                           <div class="form-group">
                                              <label for="sex1" class="col-12 col-form-label">Sex</label>
                                               <select id="sex1" name="sex1" class="custom-select">     
                                                  @foreach($sexo1 as $sexo1)
                                                    <option value="{{$sexo1->id}}">{{$sexo1->item}}</option>
                                                  @endforeach
                                                </select>

                                            @error('sex1')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                          </div>

                                       <div class="form-group">
                                              <label for="birthdate1" class="col-12 col-form-label">Birthdate</label>
                                                 <input id="birthdate1" type="date" class="form-control @error('birthdate1') is-invalid @enderror" name="birthdate1" value="{{ old('birthdate1') }}" required autocomplete="birthdate1" autofocus >

                                                @error('birthdate1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                          </div>

                                           <div class="form-group">
                                              <label for="address1" class="col-12 col-form-label">Adress</label>
                                               <input id="address1" type="text" class="form-control @error('address1') is-invalid @enderror" name="address1" value="{{ old('address1') }}" required autocomplete="address1" autofocus>

                                            @error('address1')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                          </div>

                                           <div class="form-group">
                                              <label for="numbersocial1" class="col-12 col-form-label">Number Social</label>
                                               <input id="numbersocial1" type="text" class="form-control @error('numbersocial1') is-invalid @enderror" name="numbersocial1" value="{{ old('numbersocial1') }}" required autocomplete="numbersocial1" autofocus>

                                                @error('numbersocial1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                          </div>

                                           <div class="form-group">
                                              <label for="zipcode1" class="col-12 col-form-label">ZipCode</label>
                                                 <input id="zipcode1" type="text" class="form-control @error('zipcode1') is-invalid @enderror" name="zipcode1" value="{{ old('zipcode1') }}" required autocomplete="zipcode1" autofocus>

                                                @error('zipcode1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                          </div>

                                           <div class="form-group">
                                              <label for="numberphone1" class="col-12 col-form-label">NumberPhone</label>
                                               <input id="numberphone1" type="text" class="form-control @error('numberphone1') is-invalid @enderror" name="numberphone1" value="{{ old('numberphone1') }}" required autocomplete="numberphone1" autofocus>

                                                @error('numberphone1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                          </div>

                                           <div class="form-group">
                                              <label for="numberhome1" class="col-12 col-form-label">Number Home</label>
                                                <input id="numberhome1" type="text" class="form-control @error('numberhome1') is-invalid @enderror" name="numberhome1" value="{{ old('numberhome1') }}" required autocomplete="numberhome1" autofocus>

                                            @error('numberhome1')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                          </div>

                                           <div class="form-group">
                                              <label for="numberfax1" class="col-12 col-form-label">Number Fax</label>
                                               <input id="numberfax1" type="text" class="form-control @error('numberfax1') is-invalid @enderror" name="numberfax1" value="{{ old('numberfax1') }}" autocomplete="numberfax1" autofocus>

                                                @error('numberfax1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror

                                          </div> 

                                           <div class="form-group">
                                              <label for="sociallink1" class="col-12 col-form-label">Social Link</label>
                                                <input id="sociallink1" type="text" class="form-control @error('sociallink1') is-invalid @enderror" name="sociallink1" value="{{ old('sociallink1') }}" autocomplete="sociallink1" autofocus>

                                                @error('sociallink1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                          </div>

                                           <div class="form-group">
                                              <label for="departamento1" class="col-12 col-form-label">Departament</label>
                                                <select id="departamento1" name="departamento1" class="custom-select">     
                                                      @foreach($departamento1 as $departamento1)
                                                        <option value="{{$departamento1->id}}">{{$departamento1->item}}</option>
                                                      @endforeach
                                                    </select>
                                                @error('departamento1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                          </div>

                                          <div class="form-group">
                                              <label for="cargo1" class="col-12 col-form-label">Position</label>
                                             <select id="cargo1" name="cargo1" class="custom-select">          
                                                        @foreach($cargo1 as $cargo1)
                                                        <option value="{{$cargo1->id}}">{{$cargo1->item}}</option>
                                                      @endforeach
                                                    </select>

                                                @error('cargo1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                          </div>

                                         <div class="form-group">
                                              <label for="horario1" class="col-12 col-form-label">Schedule</label>
                                               <select id="horario1" name="horario1" class="custom-select">          
                                                        @foreach($horario1 as $horario1)
                                                            <option value="{{$horario1->id}}" selected>{{$horario1->item}}</option>
                                                        @endforeach   
                                                    </select>

                                                @error('horario1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                          </div>
                                  
                                            
                                        <div class="form-group">
                                              <label for="profesion1" class="col-12 col-form-label">Profession</label>
                                                 <select id="profesion1" name="profesion1" class="custom-select">    
                                                     @foreach($profesion1 as $profesion1)      
                                                        <option value="{{$profesion1->id}}">{{$profesion1->item}}</option>    
                                                         @endforeach   
                                                    </select>

                                                @error('profesion1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                        </div>

                                        <div class="form-group row">
                                            <label for="roles1" class="col-md-4 col-form-label text-md-right">{{ __('Roles') }}</label>

                                            <div class="col-md-6">
                                                    <select id="roles1" name="roles" class="custom-select">    
                                                     @foreach($roles1 as $roles1)      
                                                        <option value="{{$roles1->id}}">{{$roles1->item}}</option>    
                                                         @endforeach   
                                                    </select>

                                                @error('roles1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>

                                            <div class="form-group">
                                              <label for="status1" class="col-12 col-form-label">Status</label>
                                             <select id="status1" name="status1" class="custom-select">          
                                                <option value="AC">Active</option> 
                                                <option value="IN">Inactive</option>    
                                            </select>

                                                @error('status1')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                          </div>

                                          <div class="form-group">
                                              <input id="id1" name="id1" type="hidden" class="form-control" required="required" value="">
                                          </div>
                                      </div>
                                  </div>
                                </div>
                                 <div class="modal-footer">
                                  <div class="form-group">
                                    <div class="block-content block-content-full text-right bg-light">
                                      <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cerrar</button>
                                        <button type="submit" class="btn btn-primary">
                                          Submit
                                        </button>
                                    </div>
                                  </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- end-of-soft-edit-->

    <!-- deleteConfirmation of customer -->
     <div id="DeleteModal" class="modal fade text-danger" role="dialog">
     <div class="modal-dialog ">
       <!-- Modal content-->
        <div class="block-content">
           <form id="deleteForm" method="POST" action="{{ url('destroyEmployes') }}" role="form" class="form-horizontal">
               <div class="modal-content">
                 <div class="modal-header bg-danger">
                    <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                        <i class="fa fa-fw fa-times"></i>
                    </button>
                </div>
                   <div class="modal-body">
                       {{ csrf_field() }}
                      <p class="text-center">Are You Sure Want To Delete ?</p>
                       <div class="form-group">
                            <input id="id2" name="id2" type="hidden" class="form-control" required="required" value="">
                        </div>
                   </div>

                   <div class="modal-footer">
                      <div class="form-group">
                        <div class="block-content block-content-full text-right bg-light">
                          <button type="button" class="btn btn-sm btn-light" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-danger">Yes, Delete</button>
                        </div>
                      </div>
                    </div>
               </div>
           </form>
        </div>
     </div>
    </div>
        <!-- end-of-soft-delete-conformation -->

   <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
  <script>
    $(document).ready(function(){ 
      $(document).on("click", ".customerEditModal", function () {
        var empid = $(this).attr('id');
        var empcode = $(this).data('code');
        var empfname = $(this).data('firstname');
        var emplname= $(this).data('lastname');
        var empsex = $(this).data('sex');
        var empbirth= $(this).data('birthdate');
        var empadres= $(this).data('address');
        var empnumbers = $(this).data('numbersocial');
        var empzipc= $(this).data('zipcode');
        var empnumberp= $(this).data('numberphone');
        var empnumberh = $(this).data('numberhome');
        var empnumberf= $(this).data('numberfax');
        var empsociall= $(this).data('sociallink');
        var empdepart = $(this).data('departamento');
        var empsched= $(this).data('horario');
        var empposit= $(this).data('cargo');
        var empprofe= $(this).data('profesion');
        var empstat= $(this).data('status');
     // var f = new Date(empbirth);

        //var today = new Date(empbirth);
        var d= new Date(empbirth);

        var dia = d.getDate(); 
        var mes =  ("0" + (d.getMonth() + 1));
        var anio = d.getFullYear(); 

        var fechatotal = mes + "/"+ dia +"/" + anio;

        //$("#fechaalta").html('fechatotal');
       //alert("ID: "+empid+" CODE: "+empdepart+" DESCRIPTION: "+fechatotal);
        $(".modal-body #id1").val(empid);
        $(".modal-body #code1").val(empcode);
        $(".modal-body #code1").prop('disabled', true);
        $(".modal-body #firstname1").val(empfname);
        $(".modal-body #lastname1").val(emplname);
        $(".modal-body #sex1").val(empsex);
        $(".modal-body #birthdate1").val(fechatotal);
        $(".modal-body #address1").val(empadres);
        $(".modal-body #numbersocial1").val(empnumbers);
        $(".modal-body #zipcode1").val(empzipc);
        $(".modal-body #numberphone1").val(empnumberp);
        $(".modal-body #numberhome1").val(empnumberh);
        $(".modal-body #numberfax1").val(empnumberf);
        $(".modal-body #sociallink1").val(empsociall);
        $(".modal-body #departamento1").val(empdepart);
        $(".modal-body #horario1").val(empsched);
        $(".modal-body #cargo1").val(empposit);
        $(".modal-body #profesion1").val(empprofe);
        $(".modal-body #status1").val(empstat);
      });

       $(document).on("click", ".DeleteModal", function () {
        var depId2 = $(this).attr('id');
       
       //alert("ID: "+depId2);
        $(".modal-body #id2").val(depId2);
      });

      
  });
  </script> 


  
