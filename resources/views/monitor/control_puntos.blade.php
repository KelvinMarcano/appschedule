@extends('layouts.backend')

@section('content')
  <!-- Hero -->
  <div class="bg-body-light">
    <div class="content content-full">
      <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
        <h1 class="flex-sm-fill font-size-h4 font-w400 mt-sm-0 mb-0 mb-sm-0 text-center">Monitor Deduction Point</h1>
      </div>
    </div>
  </div>
  <!-- END Hero -->
  <div class="content">
    <table class="table table-bordered table-striped table-vcenter tabladata table-responsive" style="width: 100%" >
      <thead>
        <tr>
          <th class="text-center">Employes</th>
          <th class="text-center">Points</th>
          <th class="text-center">Points Adiccionals</th>
          <th class="text-center">Consumed</th>
          <th class="text-center">Vacation days</th>
          <th class="text-center">Vacation days consumed</th>
          <th class="text-center">Personal Time</th>
          <th class="text-center">Personal Time Consumed</th>
          <th class="text-center">Actions</th>
        </tr>
    </thead>
    </table>
  </div>

  <script>

    var tabla1;
    var urlTabla1 = "get-control-points";

    $(function() {
      tabla1 = $('.tabladata').DataTable({
        processing: true,
        serverSide: true,
        ajax: urlTabla1,
        dom: 'Bfrtip',
        pageLength: 5,
        buttons: [
          {
            "extend": 'copy',
            "text": `<i class="far fa-copy" style="color: white;"></i>`,
            "titleAttr": 'Copy',
            "action": exportTable
          },
          {
            "extend": 'csv',
            "text": `<i class="fa fa-file" style="color: white;"></i>`,
            "titleAttr": 'CSV',
            "action": exportTable
          },
          {
            "extend": 'excel',
            "text": `<i class="far fa-file-excel" style="color: white;"></i>`,
            "titleAttr": 'Excel',
            "action": exportTable
          },
          {
            "extend": 'pdf',
            "text": `<i class="far fa-file-pdf" style="color: white;"></i>`,
            "titleAttr": 'PDF',
            "action": exportTable
          },
          {
            "extend": 'print',
            "text": `<i class="fa fa-print" style="color: white;"></i>`,
            "titleAttr": 'Print',
            "action": exportTable
          }
        ],
        columns: [
          { data: 'employee', name: 'employee' },
          { data: 'points', name: 'points' },
          { data: 'pointsAdi', name: 'pointsAdi' },
          { data: 'consumed', name: 'consumed' },
          { data: 'vacationDay', name: 'vacationDay' },
          { data: 'vacationDayC', name: 'vacationDayC' },
          { data: 'personalTime', name: 'personalTime' },
          { data: 'personalTimeC', name: 'personalTimeC' },
          { data: 'action', name: 'action' }
        ]
      });
    });

    function exportTable(e, dt, button, config) {
      var self = this;
      var oldStart = dt.settings()[0]._iDisplayStart;
      dt.one('preXhr', function (e, s, data) {
        data.start = 0;
        data.length = 2147483647;
        dt.one('preDraw', function (e, settings) {
          if (button[0].className.indexOf('buttons-copy') >= 0) {
            $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-excel') >= 0) {
            $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
              $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
              $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-csv') >= 0) {
            $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
              $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
              $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
            $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
              $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
              $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-print') >= 0) {
            $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
          }
          dt.one('preXhr', function (e, s, data) {
            settings._iDisplayStart = oldStart;
            data.start = oldStart;
          });
          setTimeout(dt.ajax.reload, 0);
          return false;
        });
      });
      dt.ajax.reload();
    }

  </script>
@endsection
