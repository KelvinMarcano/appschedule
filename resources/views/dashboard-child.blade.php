<div id="rowchild">


  <div class="row row-deck">
    @php
      $clase = '';
      $puntos = null; 
    @endphp
    @foreach($absences as $absence)
      @if(in_array($absence->status_id,[1,6,7]))
        @php $clase = 'bg-success' @endphp
      @endif
      @if($absence->status_id == 2)
        @php $clase = 'bg-danger' @endphp
      @endif
      @if($absence->status_id == 3)
        @php $clase = 'bg-warning' @endphp
      @endif
      @if($absence->status_id == 5)
        @php $clase = 'bg-info' @endphp
      @endif
      @if($absence->status_id == 9)
        @php $clase = 'bg-primary' @endphp
      @endif
      @php
        $puntos = $absence->control();
      @endphp
      <div class="col-sm-3" {{--style="font-size: xxx-large;"--}}>
        <a class="block block-rounded block-link-pop" href="javascript:void(0)" data-toggle="tooltip" data-html="true" data-placement="top" title="
          <div class='col-md-12'>
            <p class='text-left' style='font-size: 12px; margin-bottom: 0.25rem !important;'><strong>{{ $absence->employee()->firstname }} {{ $absence->employee()->lastname }}</strong></p>
          </div>
          <div class='col-md-12'>
            <p class='text-left' style='font-size: 12px; margin-bottom: 0.25rem !important;'><strong>4 Hrs PT/ {{ (!empty($puntos)) ? $puntos->saldo : 0}} Points/{{ (!empty($puntos)) ? $puntos->consumido : 0}} VD</strong></p>
          </div>
          @if(!empty($absence->hora_init))
            <div class='col-md-12'>
              <p class='text-left' style='font-size: 12px;margin-bottom: 0.25rem !important; text-overflow: ellipsis;white-space: nowrap;overflow: hidden;'><strong>Check in time:</strong> {{ $absence->hora_init }}</p>
            </div>
          @endif
          @if(!empty($absence->hora_end))
            <div class='col-md-12'>
              <p class='text-left' style='font-size: 12px;margin-bottom: 0.25rem !important; text-overflow: ellipsis;white-space: nowrap;overflow: hidden;'><strong>Departure time:</strong> {{ $absence->hora_end }}</p>
            </div>
          @endif
          @if(!empty($absence->platform))
            <div class='col-md-12'>
              <p class='text-left' style='font-size: 12px;margin-bottom: 0.25rem !important;'><strong>Platform:</strong> {{ $absence->platform }}</p>
            </div>
          @endif
          @if(!empty($absence->usuario))
            <div class='col-md-12'>
              <p class='text-left' style='font-size: 12px;margin-bottom: 0.25rem !important; text-overflow: ellipsis;white-space: nowrap;overflow: hidden;'><strong>Created by:</strong> {{ $absence->usuario }}</p>
            </div>
          @endif
        ">
          <div class="block-header {{ $clase }}">
            <h4 class="block-title" style="font-size: 16px;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"><strong>{{ $absence->employee()->firstname }} {{ $absence->employee()->lastname }}</strong></h4>
            <div class="block-options">
              {{--<button type="button" class="btn btn-sm btn-primary mr-1 mb-3">
                <i class="fa fa-exclamation-circle"></i>
              </button>--}}
              {{--@if(!in_array($absence->status_id,[1,6,7]))
                <img data-toggle="tooltip" data-animation="true" data-placement="top" title="Approve" src="{{ asset('images/aproval.png') }}" style="width: 32px !important;" onclick="aprobar(this);" data-id="{{ $absence->id }}" data-point="{{ $absence->points }}"  >
              @endif
              @if($absence->status_id != 2)
                <img data-toggle="tooltip" data-animation="true" data-placement="top" title="Reject" src="{{ asset('images/rejected.png') }}" style="width: 51px !important; margin-top: -2px !important;" onclick="rechazar(this)" data-id="{{ $absence->id }}">
              @endif--}}
            </div>
          </div>
          <div class="block-content" {{--style="background-color: whitesmoke;"--}} style="padding: 1.10rem 0.1rem 1px !important;">
            <div class="row">
              @if(!in_array($absence->status_id,[1,6,7,5 ]))
                <div class="col-md-6">
                  <img data-toggle="tooltip" data-animation="true" data-placement="top" title="Approve" src="{{ asset('images/aproval.png') }}" style="width: 35px !important;" onclick="aprobar(this);" data-id="{{ $absence->id }}" data-point="{{ $absence->points }}"  >
                </div>
              @endif
              @if($absence->status_id != 2)
                <div class="col-md-6">
                  <img data-toggle="tooltip" data-animation="true" data-placement="top" title="Reject" src="{{ asset('images/rejected.png') }}" style="width: 35px !important;" onclick="rechazar(this)" data-id="{{ $absence->id }}">
                </div>
              @endif
            </div>
            <hr>
            {{--<div class="col-md-12">
              <p class="text-left" style="font-size: 12px; margin-bottom: 0.25rem !important;"><strong>4 Hrs PT/ {{ (!empty($puntos)) ? $puntos->saldo : 0}} Points/{{ (!empty($puntos)) ? $puntos->consumido : 0}} VD</strong></p>
            </div>--}}
            <div class="col-md-12">
              <p class="text-left" style="font-size: 12px;margin-bottom: 0.25rem !important;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"><strong>Reason:</strong> {{ (!empty($absence->motiveid)) ? $absence->motive()->description : ''}}</p>
            </div>
            <div class="col-md-12">
              <p class="text-left" style="font-size: 12px;margin-bottom: 0.25rem !important;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"><strong>Violation:</strong> {{ (!empty($absence->violation_id)) ? $absence->violation()->description : ''}}</p>
            </div>

            <div class="col-md-12">
              <p class="text-left" style="font-size: 12px;margin-bottom: 0.25rem !important;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"><strong>Status:</strong> {{ (!empty($absence->status_id)) ? $absence->status()->description : ''}}</p>
            </div>
            <div class="col-md-12">
              <p class="text-left" style="font-size: 12px;margin-bottom: 0.25rem !important;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"><strong>Days:</strong> {{ $absence->days }}</p>
            </div>

            <div class="col-md-12">
              <p class="text-left" style="font-size: 12px;margin-bottom: 0.25rem !important;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"><strong>Date Start:</strong> {{ (!empty($absence->fecha_init)) ? \Carbon\Carbon::parse($absence->fecha_init)->format('m/d/Y') : '' }}</p>
            </div>
            <div class="col-md-12">
              <p class="text-left" style="font-size: 12px;margin-bottom: 0.25rem !important;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"><strong>Date End:</strong>  {{ !empty($absence->fecha_end) ?  \Carbon\Carbon::parse($absence->fecha_end)->format('m/d/Y') : '' }}</p>
            </div>

            {{--@if(!empty($absence->usuario))
              <div class="col-md-12">
                <p class="text-left" style="font-size: 12px;margin-bottom: 0.25rem !important; text-overflow: ellipsis;white-space: nowrap;overflow: hidden;"><strong>Created by:</strong> {{ $absence->usuario }}</p>
              </div>
            @endif--}}

            {{--@if(!empty($absence->platform))
              <div class="col-md-12">
                <p class="text-left" style="font-size: 12px;margin-bottom: 0.25rem !important;"><strong>Platform</strong> {{ $absence->platform }}</p>
              </div>
            @endif--}}

          </div>
        </a>
      </div>
    @endforeach
  </div>
  <br>
  <br>
  <div class="row">
    <div class="col-sm-12 justify-content-center in row">
     {{-- {{ $absences->links() }}--}}
    </div>
  </div>
</div>