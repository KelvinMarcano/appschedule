@extends('layouts.backend')

<style>
  div.dataTables_wrapper div.dataTables_processing {
    position: absolute;
    top: 30%;
    left: 50%;
    width: 30%;
    height: 64px;
    margin-left: -20%;
    padding-top: 20px;
    text-align: center;
    font-size: 1.2em;
    background:#00703c;
    color: mintcream;
    border-radius: 10px;
  }
  /*td.details-control {
    background: url('{{ asset("images/details_open.png") }}') no-repeat center center;
    cursor: pointer;
  }
  tr.shown td.details-control {
    background: url('{{ asset("images/details_close.png") }}') no-repeat center center;
  }
  .timeleft {
    background-color: white !important;
    color: #FFB119;
  }
  .approved {
    background-color: white !important;
    color: green;
  }
  .reject {
    background-color: white !important;
    color: red;
  }
  .pending {
    background-color: white !important;
    color: #3C90DF;
  }*/
</style>

@section('content')
  <div class="content">

    <div class="block block-rounded block-bordered">
      <div class="block-header block-header-default">
          <h3 class="block-title">Zip Code</h3>
      </div>
      <div class="block-content block-content-full">
        <div>
          <button type="button" class="btn btn-primary btn-sm text-white addnew" data-toggle="modal" data-target="#Modal" data-whatever="@getbootstrap" data-backdrop="static" data-keyboard="false">
            <i class="si si-plus text-white"></i>
            Add new
          </button>
        </div>
        <br>
        <table class="table table-bordered table-striped table-vcenter tabladata" style="width: 100%">
          <thead>
              <tr>
                  <th class="text-center">Id</th>
                  <th class="text-center">Code</th>
                  <th class="text-center">Description</th>
                 <!--  <th class="text-center">State</th> -->
                  <th class="text-center" style="width: 20%;">Actions</th>
              </tr>
          </thead>
        
        </table>
      </div>
    </div>

  </div>

  {{--Modal para Agregar o Editar el rol--}}
  <div class="modal fade" id="Modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="label">Add New Zip Code</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="zipcode" method="post">
          {{ csrf_field() }}
          <div class="modal-body">
            <input type="hidden" id="id" name="id" value="">
            <div class="form-group">
              <label for="role" class="col-form-label">Code</label>
              <input type="text" class="form-control" id="code" name="code" required placeholder="Insert ZipCode">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Description</label>
              <input type="text" class="form-control" id="description" name="description" required placeholder="Insert Description">
            </div>
         
          <div class="modal-footer">
            <a type="button" class="btn btn-secondary text-light" data-dismiss="modal">Close</a>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
    var tabla1;
    $(function() {
      tabla1 = $('.tabladata').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'zipcodelist',
        dom: 'Bfrtip',
        pageLength: 5,
        buttons: [
          {
            "extend": 'copy',
            "text": `<i class="far fa-copy" style="color: white;"></i>`,
            "titleAttr": 'Copy',
            "action": exportTable
          },
          {
            "extend": 'csv',
            "text": `<i class="fa fa-file" style="color: white;"></i>`,
            "titleAttr": 'CSV',
            "action": exportTable
          },
          {
            "extend": 'excel',
            "text": `<i class="far fa-file-excel" style="color: white;"></i>`,
            "titleAttr": 'Excel',
            "action": exportTable
          },
          {
            "extend": 'pdf',
            "text": `<i class="far fa-file-pdf" style="color: white;"></i>`,
            "titleAttr": 'PDF',
            "action": exportTable
          },
          {
            "extend": 'print',
            "text": `<i class="fa fa-print" style="color: white;"></i>`,
            "titleAttr": 'Print',
            "action": exportTable
          }
        ],
        columns: [
          { data: 'id', name: 'id' },
          { data: 'code', name: 'code' },
          { data: 'description', name: 'description' },
          { data: 'action', name: 'action' },
        ],
      });
    });

    $(document).ready(function() {
      $('#reasonDiv').hide();
      $("#body").attr('onbeforeunload', 'HandleBackFunctionality()');
        
    });

    $('.addnew').on('click',async function () {
      $('#label').html("Add New Zipcode");
      $('#id').val('');
      $('#code').val('');
      $('#description').val('');
    });

    function edit(sender){
      $('#label').html("City edit");
      let id = $(sender).data('id');
      $('#id').val(id);
      let codigopostal = @json($zipcode);
      codigopostal.forEach((element) =>{
        if(id == element.id){
          $('#code').val(element.code);
          $('#description').val(element.description);
        }
      });
    }

    function deleted(sender){
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(sender).data('id');
            let resp = await request(`zipcode/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            tabla1.ajax.reload();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    }

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

      function HandleBackFunctionality(e){
        if(window.event)
    {
          if(window.event.clientX < 40 && window.event.clientY < 0)
         {

         }
         else
         {
             document.getElementById("body").addEventListener("onbeforeunload", function(event){
              event.preventDefault()
              });

         }
     }
     else
     {
          if(event.currentTarget.performance.navigation.type == 1)
         {

         }
         if(event.currentTarget.performance.navigation.type == 2)
        {

        }
     }
    }

      function exportTable(e, dt, button, config) {
      var self = this;
      var oldStart = dt.settings()[0]._iDisplayStart;
      dt.one('preXhr', function (e, s, data) {
        data.start = 0;
        data.length = 2147483647;
        dt.one('preDraw', function (e, settings) {
          if (button[0].className.indexOf('buttons-copy') >= 0) {
            $.fn.dataTable.ext.buttons.copyHtml5.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-excel') >= 0) {
            $.fn.dataTable.ext.buttons.excelHtml5.available(dt, config) ?
              $.fn.dataTable.ext.buttons.excelHtml5.action.call(self, e, dt, button, config) :
              $.fn.dataTable.ext.buttons.excelFlash.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-csv') >= 0) {
            $.fn.dataTable.ext.buttons.csvHtml5.available(dt, config) ?
              $.fn.dataTable.ext.buttons.csvHtml5.action.call(self, e, dt, button, config) :
              $.fn.dataTable.ext.buttons.csvFlash.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-pdf') >= 0) {
            $.fn.dataTable.ext.buttons.pdfHtml5.available(dt, config) ?
              $.fn.dataTable.ext.buttons.pdfHtml5.action.call(self, e, dt, button, config) :
              $.fn.dataTable.ext.buttons.pdfFlash.action.call(self, e, dt, button, config);
          } else if (button[0].className.indexOf('buttons-print') >= 0) {
            $.fn.dataTable.ext.buttons.print.action(e, dt, button, config);
          }
          dt.one('preXhr', function (e, s, data) {
            settings._iDisplayStart = oldStart;
            data.start = oldStart;
          });
          setTimeout(dt.ajax.reload, 0);
          return false;
        });
      });
      dt.ajax.reload();
    }


  </script>
@endsection