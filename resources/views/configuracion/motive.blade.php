@extends('layouts.backend')

@section('content')
  <div class="content">

    <div class="block block-rounded block-bordered">
      <div class="block-header block-header-default">
          <h3 class="block-title">Reasons</h3>
      </div>
      <div class="block-content block-content-full">
        <div>
          <button type="button" class="btn btn-primary btn-sm text-white addnew" data-toggle="modal" data-target="#Modal" data-whatever="@getbootstrap" data-backdrop="static" data-keyboard="false">
            <i class="si si-plus text-white"></i>
            Add new
          </button>
        </div>
        <br>
      <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination" style="width: 100%">
          <thead>
              <tr>
                  <th class="text-center">Code</th>
                  <th class="text-center">Description</th>
                   <th class="text-center">Violation</th>
                   <th class="text-center">Categoria</th>
                      <th class="text-center">Note Official</th>
                  <th class="text-center" style="width: 20%;">Actions</th>
              </tr>
          </thead>
          <tbody>
            @foreach($motives as $motive)
              <tr class="text-center" id="trrol_{{ $motive->id }}">
                <td class="text-center">{{ $motive->code }}</td>
                <td class="font-w600">{{ $motive->description }}</td>
                <td class="font-w600">{{ (!empty($motive->violation()->id)) ? $motive->violation()->description : ''}}</td>
                <td class="font-w600">{{ (!empty($motive->categories()->id)) ? $motive->categories()->description : ''}}</td>
                <td class="font-w600">{{ $motive->required_note }}</td>
                <td class="d-none d-sm-table-cell">
                  <button type="button" class="btn btn-primary btn-sm text-white edit" data-toggle="modal" data-target="#Modal" data-whatever="@getbootstrap" data-id="{{ $motive->id }}">
                      <i class="si si-pencil text-white"></i>
                      Edit
                  </button>
                  <button type="button" class="btn btn-danger btn-sm text-white delete" data-id="{{ $motive->id }}">
                    <i class="si si-close text-white"></i>
                    Delete
                  </button>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

  </div>

  {{--Modal para Agregar o Editar el rol--}}
  <div class="modal fade" id="Modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="label">Add new Reasons</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="motive" method="post">
          {{ csrf_field() }}
          <div class="modal-body">
            <input type="hidden" id="id" name="id" value="">
            <div class="form-group">
              <label for="role" class="col-form-label">Code</label>
              <input type="text" class="form-control" id="code" name="code" required placeholder="Insert Code">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Description</label>
              <input type="text" class="form-control" id="description" name="description" required placeholder="Insert Description">
            </div>
            <div class="form-group">
                <label for="role" class="col-form-label">Violation</label>
                <select id="violation_id" name="violation_id" class="custom-select">   
                    @foreach($violations as $violation)
                    <option value="{{$violation->id}}">{{$violation->description}}</option>
                @endforeach
              </select>
            </div>
             <div class="form-group">
                <label for="role" class="col-form-label">Categories</label>
                <select id="categories_id" name="categories_id" class="custom-select">   
                    @foreach($categories as $categoria)
                    <option value="{{$categoria->id}}">{{$categoria->description}}</option>
                @endforeach
              </select>
            </div>
             <div class="form-group">
                <label for="role" class="col-form-label">Required Note Official</label>
                <select id="required_note" name="required_note" class="custom-select">    
                    <option value="0">Choose Option....</option>
                    <option value="Si">Yes</option>
                    <option value="No">No</option>

              </select>
            </div>
          <div class="modal-footer">
            <a type="button" class="btn btn-secondary text-light" data-dismiss="modal">Close</a>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
   $(document).ready(function() {
      $('#reasonDiv').hide();
      $("#body").attr('onbeforeunload', 'HandleBackFunctionality()');
        
    });

    $('.addnew').on('click',async function () {
      $('#label').html("Add new Reasons");
      $('#id').val('');
      $('#code').val('');
      $('#description').val('');

    });

    $('.edit').on('click',async function () {
      $('#label').html("Reasons edit");
      let id = $(this).data('id');
      $('#id').val(id);
      let motive = @json($motives);
      motive.forEach((element) =>{
        if(id == element.id){
          $('#code').val(element.code);
          $('#description').val(element.description);
          $('#violation_id').val(element.violation_id);
          $('#categories_id').val(element.categories_id);
          $('#required_note').val(element.required_note);
        }
      });
    });

    $('.delete').on('click',async function () {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(this).data('id');
            let resp = await request(`motive/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            $(`#trrol_${id}`).remove();
          }catch (error) {

            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    });

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

     function HandleBackFunctionality(e){
        if(window.event)
    {
          if(window.event.clientX < 40 && window.event.clientY < 0)
         {

         }
         else
         {
             document.getElementById("body").addEventListener("onbeforeunload", function(event){
              event.preventDefault()
              });

         }
     }
     else
     {
          if(event.currentTarget.performance.navigation.type == 1)
         {

         }
         if(event.currentTarget.performance.navigation.type == 2)
        {

        }
     }
    }
  </script>
@endsection