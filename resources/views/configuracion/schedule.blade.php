@extends('layouts.backend')

@section('content')
  <div class="content">

    <div class="block block-rounded block-bordered">
      <div class="block-header block-header-default">
          <h3 class="block-title">Schedules</h3>
      </div>
      <div class="block-content block-content-full">
        <div>
          <button type="button" class="btn btn-primary btn-sm text-white addnew" data-toggle="modal" data-target="#Modal" data-whatever="@getbootstrap" data-backdrop="static" data-keyboard="false">
            <i class="si si-plus text-white"></i>
            Add new
          </button>
        </div>
        <br>
      <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination" style="width: 100%">
          <thead>
              <tr>
                  <th class="text-center">Code</th>
                  <th class="text-center">Description</th>
                  <th class="text-center">Hours Start</th>
                  <th class="text-center">Hours End</th>
                  <th class="text-center">Break Time Start</th>
                  <th class="text-center">Break Time End</th>
                   <th class="text-center">Break Time Start 2</th>
                  <th class="text-center">Break Time End 2</th>
                  <th class="text-center" style="width: 20%;">Actions</th>
              </tr>
          </thead>
          <tbody>
            @foreach($schedules as $schedule)
              <tr class="text-center" id="trrol_{{ $schedule->id }}">
                <td class="text-center">{{ $schedule->code }}</td>
                <td class="font-w600">{{ $schedule->description }}</td>
                <td class="font-w600">{{ $schedule->hour_start }}</td>
                <td class="font-w600">{{ $schedule->hour_end }}</td>
                <td class="font-w600">{{ $schedule->break_time_start }}</td>
                <td class="font-w600">{{ $schedule->break_time_end }}</td>
                <td class="font-w600">{{ $schedule->break_time_start_2 }}</td>
                <td class="font-w600">{{ $schedule->break_time_end_2 }}</td>
                <td class="d-none d-sm-table-cell">
                  <button type="button" class="btn btn-primary btn-sm text-white edit" data-toggle="modal" data-target="#Modal" data-whatever="@getbootstrap" data-id="{{ $schedule->id }}">
                      <i class="si si-pencil text-white"></i>
                      Edit
                  </button>
                  <button type="button" class="btn btn-danger btn-sm text-white delete" data-id="{{ $schedule->id }}">
                    <i class="si si-close text-white"></i>
                    Delete
                  </button>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

  </div>

  {{--Modal para Agregar o Editar el rol--}}
  <div class="modal fade" id="Modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="label">Add new Schedules</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="schedule" method="post">
          {{ csrf_field() }}
          <div class="modal-body">
            <input type="hidden" id="id" name="id" value="">
            <div class="form-group">
              <label for="role" class="col-form-label">Code</label>
              <input type="text" class="form-control" id="code" name="code" required placeholder="Insert Code">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Description</label>
              <input type="text" class="form-control" id="description" name="description" required placeholder="Insert Description">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Hours Start</label>
              <input type="text" class="form-control" id="hour_start" name="hour_start" placeholder="00:00">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Hours End</label>
              <input type="text" class="form-control" id="hour_end" name="hour_end" placeholder="00:00">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Break Time Start</label>
              <input type="text" class="form-control" id="break_time_start" name="break_time_start" placeholder="00:00">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Break Time End</label>
              <input type="text" class="form-control" id="break_time_end" name="break_time_end" placeholder="00:00">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Break Time Start 2</label>
              <input type="text" class="form-control" id="break_time_start_2" name="break_time_start_2" placeholder="00:00">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Break Time End 2</label>
              <input type="text" class="form-control" id="break_time_end_2" name="break_time_end_2" placeholder="00:00">
            </div>
             <div class="form-group">
                <label for="role" class="col-form-label">Working Day</label>
                <select id="working_day_id" name="working_day_id" class="custom-select">   
                    @foreach($working_day as $working_day)
                       <option value="{{$working_day->id}}">{{$working_day->code}} -   {{$working_day->description}}</option>
                    @endforeach
              </select>
            </div>
          <div class="modal-footer">
            <a type="button" class="btn btn-secondary text-light" data-dismiss="modal">Close</a>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>

    $(document).ready(function() {
      $('#reasonDiv').hide();
      $("#body").attr('onbeforeunload', 'HandleBackFunctionality()');

       $('#hour_start').mdtimepicker({
        theme: 'green',// 'red', 'purple', 'indigo', 'teal', 'green', 'dark'
        readOnly: false,
        hourPadding: false,
        clearBtn: true
      });
      $('#hour_end').mdtimepicker({
        theme: 'green',// 'red', 'purple', 'indigo', 'teal', 'green', 'dark'
        readOnly: false,
        hourPadding: false,
        clearBtn: true
      });

       $('#break_time_start').mdtimepicker({
        theme: 'green',// 'red', 'purple', 'indigo', 'teal', 'green', 'dark'
        readOnly: false,
        hourPadding: false,
        clearBtn: true
      });
      $('#break_time_end').mdtimepicker({
        theme: 'green',// 'red', 'purple', 'indigo', 'teal', 'green', 'dark'
        readOnly: false,
        hourPadding: false,
        clearBtn: true
      });

       $('#break_time_start_2').mdtimepicker({
        theme: 'green',// 'red', 'purple', 'indigo', 'teal', 'green', 'dark'
        readOnly: false,
        hourPadding: false,
        clearBtn: true
      });
      $('#break_time_end_2').mdtimepicker({
        theme: 'green',// 'red', 'purple', 'indigo', 'teal', 'green', 'dark'
        readOnly: false,
        hourPadding: false,
        clearBtn: true
      });
        
    });

    $('.addnew').on('click',async function () {
      $('#label').html("Add New Schedules");
      $('#id').val('');
      $('#code').val('');
      $('#description').val('');
      $('#hour_start').val('');
      $('#hour_end').val('');
      $('#break_time_start').val('');
      $('#break_time_end').val('');
      $('#break_time_start_2').val('');
      $('#break_time_end_2').val('');
      $('#working_day_id').val('');
    });

    $('.edit').on('click',async function () {
      $('#label').html("Schedules Edit");
      let id = $(this).data('id');
      $('#id').val(id);
      let schedule = @json($schedules);
      schedule.forEach((element) =>{
        if(id == element.id){
          $('#code').val(element.code);
          $('#description').val(element.description);
          $('#hour_start').val(element.hour_start);
          $('#hour_end').val(element.hour_end);
          $('#break_time_start').val(element.break_time_start);
          $('#break_time_end').val(element.break_time_end);
          $('#break_time_start_2').val(element.break_time_start_2);
          $('#break_time_end_2').val(element.break_time_end_2);
          $('#working_day_id').val(element.working_day_id);
        }
      });
    });

    $('.delete').on('click',async function () {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(this).data('id');
            let resp = await request(`schedule/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            $(`#trrol_${id}`).remove();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    });

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

       function HandleBackFunctionality(e){
        if(window.event)
    {
          if(window.event.clientX < 40 && window.event.clientY < 0)
         {

         }
         else
         {
             document.getElementById("body").addEventListener("onbeforeunload", function(event){
              event.preventDefault()
              });

         }
     }
     else
     {
          if(event.currentTarget.performance.navigation.type == 1)
         {

         }
         if(event.currentTarget.performance.navigation.type == 2)
        {

        }
     }
    }
  </script>
@endsection