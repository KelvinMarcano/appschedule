@extends('layouts.backend')

@section('content')
  <div class="content">

    <div class="block block-rounded block-bordered">
      <div class="block-header block-header-default">
          <h3 class="block-title">Deduction Points</h3>
      </div>
      <div class="block-content block-content-full">
        <div>
          <button type="button" class="btn btn-primary btn-sm text-white addnew" data-toggle="modal" data-target="#Modal" data-whatever="@getbootstrap" data-backdrop="static" data-keyboard="false">
            <i class="si si-plus text-white"></i>
            Add new
          </button>
        </div>
        <br>
      <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination" style="width: 100%">
          <thead>
              <tr>
                  <th class="text-center">Code</th>
                  <th class="text-center">Description</th>
                   <th class="d-none d-sm-table-cell text-center" style="width: 15%;">Unit of Measurement</th>
                    <th class="d-none d-sm-table-cell text-center" style="width: 15%;">Quantity</th>
                  <th class="d-none d-sm-table-cell text-center" style="width: 15%;">Points</th>
                  <th class="d-none d-sm-table-cell text-center" style="width: 10%;">Type Violation</th>
                  <th class="text-center" style="width: 20%;">Actions</th>
              </tr>
          </thead>
          <tbody>
            @foreach($violations as $violation)
              <tr class="text-center" id="trrol_{{ $violation->id }}">
                <td class="text-center">{{ (!empty($violation->id)) ? $violation->code : ''}}</td>
                <td class="font-w600">{{ (!empty($violation->id)) ? $violation->description : ''}}</td>
                <td class="font-w600">{{ (!empty($violation->unitofmeasure()->id)) ? $violation->unitofmeasure()->description : ''}}</td>
                <td class="font-w600">{{ $violation->quantity }}</td>
                <td class="font-w600">{{ $violation->points }}</td>
                <td class="font-w600">{{ $violation->violationType()->name }}</td>

                <td class="d-none d-sm-table-cell">
                  <button type="button" class="btn btn-primary btn-sm text-white edit" data-toggle="modal" data-target="#Modal" data-whatever="@getbootstrap" data-id="{{ $violation->id }}">
                      <i class="si si-pencil text-white"></i>
                      Edit
                  </button>
                  <button type="button" class="btn btn-danger btn-sm text-white delete" data-id="{{ $violation->id }}">
                    <i class="si si-close text-white"></i>
                    Delete
                  </button>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

  </div>

  {{--Modal para Agregar o Editar el rol--}}
  <div class="modal fade" id="Modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="label">Add new Control Points</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="violation" method="post">
          {{ csrf_field() }}
          <div class="modal-body">
            <input type="hidden" id="id" name="id" value="">
            <div class="form-group">
              <label for="role" class="col-form-label">Code</label>
              <input type="text" class="form-control" id="code" name="code" required placeholder="Insert Code">
            </div>

            <div class="form-group">
              <label for="role" class="col-form-label">Description</label>
              <input type="text" class="form-control" id="description" name="description" required placeholder="Insert Description">
            </div>
            <div class="form-group">
              <label for="type_violation_id">Violation Types</label>
                <select class="custom-select" id="type_violation_id" name="type_violation_id" required style="width: 100%;" data-placeholder="Choose one..">
                   <option value="0">Choose Option....</option>
                  @foreach($violationType as $type)
                    <option value="{{ $type->id }}">{{ $type->code }} - {{ $type->name }}</option>
                  @endforeach
                </select>
            </div>
             <div class="form-group">
              <label for="unitofmeasure_id">Unit of Measurement</label>
                <select class="custom-select" id="unitofmeasure_id" name="unitofmeasure_id" required style="width: 100%;" data-placeholder="Choose one.." onchange="habilitar(this);">
                   <option value="0">Choose Option....</option>
                  @foreach($unitofmeasure as $unitofmeasures)
                    <option value="{{ $unitofmeasures->id }}" data-code="{{ $unitofmeasures->code }}">{{ $unitofmeasures->code }} - {{ $unitofmeasures->description }}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group" id="pt">
              <label for="role" class="col-form-label">Points</label>
              <input type="number" class="form-control" id="points" name="points" placeholder="Insert Points">
            </div>
            <div class="form-group" id="ct">
              <label for="role" class="col-form-label">Quantity</label>
              <input type="text" class="form-control" id="quantity" name="quantity" placeholder="Insert Quantity">
            </div>
          <div class="modal-footer">
            <a type="button" class="btn btn-secondary text-light" data-dismiss="modal">Close</a>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
    $(document).ready(function() {
      $('#reasonDiv').hide();
       $('#pt').hide();
        $('#ct').hide();
      $("#body").attr('onbeforeunload', 'HandleBackFunctionality()');

        
    });


    $('.addnew').on('click',async function () {
      $('#label').html("Add new Deduction Points");
      $('#id').val('');
      $('#code').val('');
      $('#description').val('');
      $('#points').val('');
      $('#unitofmeasure_id').val('');
      $('#quantity').val('');
      $('#type_violation_id').val('');
    });

    $('.edit').on('click',async function () {
       $('#pt').hide();
        $('#ct').hide();
      $('#label').html("Deduction Points edit");
      let id = $(this).data('id');
      $('#id').val(id);
      let violation = @json($violations);
     // console.log(violations);
      violation.forEach((element) =>{
          if(id == element.id){
             //console.log(element.unitofmeasure_id);
              $('#code').val(element.code);
              $('#description').val(element.description);
              $('#points').val(element.points);
              $('#unitofmeasure_id').val(element.unitofmeasure_id);
              $('#quantity').val(element.quantity);
             $('#type_violation_id').val(element.type_violation_id);

             if($("#unitofmeasure_id option:selected").data("code")=='PO'){
                 $('#pt').show();
             }

              if($("#unitofmeasure_id option:selected").data("code")=='PT'){
                 $('#ct').show();
             }

              if($("#unitofmeasure_id option:selected").data("code")=='VC'){
                 $('#ct').show();
             }
          }
      });
    });


    $('.delete').on('click',async function () {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(this).data('id');
            let resp = await request(`violation/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            $(`#trrol_${id}`).remove();
              location.reload();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    });

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

    function habilitar(sender) {
     
      var micode = $("#unitofmeasure_id option:selected").data("code");

      if(micode=='PO'){
         $('#pt').show();
      }else{
         $('#pt').hide();
      }

      if(micode=='PT' || micode=='VC'){
        console.log("show"+micode);
        $('#ct').show();
      }else{
        console.log("hide"+micode);
        $('#ct').hide();
      }

      // if(micode=='VD'){
      //   $('#ct').show();
      // }else{
      //   $('#ct').hide();
      // }
    }

    function HandleBackFunctionality(e){
      if(window.event){
        if(window.event.clientX < 40 && window.event.clientY < 0){
        }
       else
       {
         document.getElementById("body").addEventListener("onbeforeunload", function(event){
          event.preventDefault()
          });
       }
      }
      else{
        if(event.currentTarget.performance.navigation.type == 1){
        }
        if(event.currentTarget.performance.navigation.type == 2){
        }
      }
    }
  </script>
@endsection