@extends('layouts.backend')

@section('content')
  <div class="content">

    <div class="block block-rounded block-bordered">
      <div class="block-header block-header-default">
          <h3 class="block-title">Relations Types Reasons Violations</h3>
      </div>
      <div class="block-content block-content-full">
        <div>
          <button type="button" class="btn btn-primary btn-sm text-white addnew" data-toggle="modal" data-target="#Modal" data-whatever="@getbootstrap" data-backdrop="static" data-keyboard="false">
            <i class="si si-plus text-white"></i>
            Add new
          </button>
        </div>
        <br>
      <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination" style="width: 100%">
          <thead>
              <tr>
                  <th class="text-center">Type of Absences</th>
                  <th class="text-center">Deduction Points Default</th>
                  <th class="text-center">Reason</th>
                  <th class="text-center">Type</th>
                  <th class="text-center" style="width: 20%;">Actions</th>
              </tr>
          </thead>
          <tbody>
            @foreach($relation as $relations)
              <tr class="text-center" id="trrol_{{ $relations->id }}">
                <td class="text-center">{{ (!empty($relations->motive()->id)) ? $relations->motive()->description : ''}}</td>
                <td class="font-w600">{{ (!empty($relations->violation()->id)) ? $relations->violation()->description : ''}}</td>
                <td class="text-center">{{ (!empty($relations->motive2()->id)) ? $relations->motive2()->description : ''}}</td>
                 <td class="text-center">{{ $relations->tipo }}</td>
                <td class="d-none d-sm-table-cell">
                  <button type="button" class="btn btn-primary btn-sm text-white edit" data-toggle="modal" data-target="#Modal" data-whatever="@getbootstrap" data-id="{{ $relations->id }}">
                      <i class="si si-pencil text-white"></i>
                      Edit
                  </button>
                  <button type="button" class="btn btn-danger btn-sm text-white delete" data-id="{{ $relations->id }}">
                    <i class="si si-close text-white"></i>
                    Delete
                  </button>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

  </div>

  {{--Modal para Agregar o Editar el rol--}}
  <div class="modal fade" id="Modal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="label">Add New Relation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="relationtype" method="post">
          {{ csrf_field() }}
          <div class="modal-body">
            <input type="hidden" id="id" name="id" value="">
            <div class="form-group">
                <label for="role" class="col-form-label">Type of Absences</label>
                <select id="motiveid" name="motiveid" class="custom-select">   
                    @foreach($motives_catgen as $cat)
                    <option value="{{$cat->id}}">{{$cat->description}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
                <label for="role" class="col-form-label">Deduction Points Default</label>
                <select id="violationid" name="violationid" class="custom-select">   
                    @foreach($violations as $violation)
                    <option value="{{$violation->id}}">{{$violation->description}}</option>
                @endforeach
              </select>
            </div>
             <div class="form-group">
                <label for="role" class="col-form-label">Reason</label>
                <select id="motiveid2" name="motiveid2" class="custom-select">   
                    @foreach($motives as $esp)
                    <option value="{{$esp->id}}">{{$esp->description}}</option>
                @endforeach
              </select>
            </div>
             <div class="form-group">
              <label for="exampleFormControlSelect1">Active</label>
                <select class="form-control" id="tipo" name="tipo" required placeholder="seleccione">
                  <option value="M">Manual</option>
                  <option value="A">Automatic</option>
                </select>
            </div>
          <div class="modal-footer">
            <a type="button" class="btn btn-secondary text-light" data-dismiss="modal">Close</a>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
    $(document).ready(function() {
      $("#body").attr('onbeforeunload');
        
    });

    $('.addnew').on('click',async function () {
      $('#label').html("Add New Relation");
      $('#id').val('');
      $('#motiveid').val('');
       $('#motiveid2').val('');
      $('#violationid').val('');
      $('#tipo').val('');
    });

    $('.edit').on('click',async function () {
      $('#label').html("Relation edit");
      let id = $(this).data('id');
      $('#id').val(id);
      let rel = @json($relation);
      rel.forEach((element) =>{
        if(id == element.id){
          $('#motiveid').val(element.motiveid);
          $('#violationid').val(element.violationid);
          $('#motiveid2').val(element.motiveid2);
           $('#tipo').val(element.tipo);
        }
      });
    });

    $('.delete').on('click',async function () {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(this).data('id');
            let resp = await request(`relationtype/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            $(`#trrol_${id}`).remove();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    });

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

      function HandleBackFunctionality(e){
        if(window.event)
    {
          if(window.event.clientX < 40 && window.event.clientY < 0)
         {

         }
         else
         {
             document.getElementById("body").addEventListener("onbeforeunload", function(event){
              event.preventDefault()
              });

         }
     }
     else
     {
          if(event.currentTarget.performance.navigation.type == 1)
         {

         }
         if(event.currentTarget.performance.navigation.type == 2)
        {

        }
     }
    }

  </script>
@endsection