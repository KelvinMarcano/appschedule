@extends('layouts.backend')

@section('content')
  <div class="content">

    <div class="block block-rounded block-bordered">
      <div class="block-header block-header-default">
          <h3 class="block-title">Departments</h3>
      </div>
      <div class="block-content block-content-full">
        <div>
          <button type="button" class="btn btn-primary btn-sm text-white addnew" data-toggle="modal" data-target="#Modal" data-whatever="@getbootstrap" data-backdrop="static" data-keyboard="false">
            <i class="si si-plus text-white"></i>
            Add new
          </button>
        </div>
        <br>
      <table class="table table-bordered table-striped table-vcenter js-dataTable-full-pagination" style="width: 100%">
          <thead>
              <tr>
                  <th class="text-center">Code</th>
                  <th class="text-center">Description</th>
                  <th class="text-center">Supervisor</th>
                  <th class="text-center">Site</th>
                  <th class="text-center">Area</th>
                  <th class="text-center" style="width: 20%;">Actions</th>
              </tr>
          </thead>
          <tbody>
            @foreach($departaments as $departament)
              <tr class="text-center" id="trrol_{{ $departament->id }}">
                <td class="text-center">{{ $departament->code }}</td>
                <td class="font-w600">{{ $departament->description }}</td>
                <td class="font-w600">{{ (!empty($departament->employee()->id)) ? ($departament->employee()->firstname) : ''}} {{ (!empty($departament->employee()->id)) ? ($departament->employee()->lastname) : ''}}</td>
                 <td class="font-w600">{{ (!empty($departament->site()->id)) ? $departament->site()->name : ''}}</td>
                  <td class="font-w600">{{ (!empty($departament->area()->id)) ? $departament->area()->description : ''}}</td>
                <td class="d-none d-sm-table-cell">
                  <button type="button" class="btn btn-primary btn-sm text-white edit" data-toggle="modal" data-target="#Modal" data-whatever="@getbootstrap" data-id="{{ $departament->id }}">
                      <i class="si si-pencil text-white"></i>
                      Edit
                  </button>
                  <button type="button" class="btn btn-danger btn-sm text-white delete" data-id="{{ $departament->id }}">
                    <i class="si si-close text-white"></i>
                    Delete
                  </button>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

  </div>

  {{--Modal para Agregar o Editar el rol--}}
  <div class="modal fade" id="Modal"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="label">Add new Departments</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="departament" method="post">
          {{ csrf_field() }}
          <div class="modal-body">
            <input type="hidden" id="id" name="id" value="">
            <div class="form-group">
              <label for="role" class="col-form-label">Code</label>
              <input type="text" class="form-control" id="code" name="code" required placeholder="Insert Code">
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Description</label>
              <input type="text" class="form-control" id="description" name="description" required placeholder="Insert Description">
            </div>
            <div class="form-group">
                <label for="role" class="col-form-label">Supervisor</label>
                <select id="employesid" name="employesid" class="custom-select">   
                    @foreach($supervisor as $supervisor)
                    <option value="{{$supervisor->id}}">{{$supervisor->code}} -   {{$supervisor->firstname}} {{$supervisor->lastname}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Site</label>
                <select id="idsite" name="idsite" class="custom-select" required>
                  @foreach($idsite as $idsites)
                    <option value="{{$idsites->id}}">{{$idsites->name}}</option>
                  @endforeach
                </select>
            </div>
            <div class="form-group">
              <label for="role" class="col-form-label">Area</label>
                <select id="areaid" name="areaid" class="custom-select" required>
                  @foreach($areaid as $areas)
                    <option value="{{$areas->id}}">{{$areas->description}}</option>
                  @endforeach
                </select>
            </div>
          <div class="modal-footer">
            <a type="button" class="btn btn-secondary text-light" data-dismiss="modal">Close</a>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
    $(document).ready(function() {
      $('#reasonDiv').hide();
      $("#body").attr('onbeforeunload', 'HandleBackFunctionality()');
        
    });

    $('.addnew').on('click',async function () {
      $('#label').html("Add new Departments");
      $('#id').val('');
      $('#code').val('');
      $('#description').val('');
      $('#employesid').val('');
      $('#idsite').val('');
      $('#areaid').val('');
    });

    $('.edit').on('click',async function () {
      $('#label').html("Departments edit");
      let id = $(this).data('id');
      $('#id').val(id);
      let departament = @json($departaments);
      departament.forEach((element) =>{
        if(id == element.id){
          $('#code').val(element.code);
          $('#description').val(element.description);
          $('#employesid').val(element.employesid);
          $('#idsite').val(element.idsite);
          $('#areaid').val(element.areaid);
        }
      });
    });

    $('.delete').on('click',async function () {
      Swal.fire({
        title: '¿Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#C62D2D',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
      }).then(async (result) => {
        if (result.value) {
          try{
            let id = $(this).data('id');
            let resp = await request(`departament/${id}`,'delete',{table : ''});
            if(resp.status = 'success') Swal.fire(resp.title,resp.msg,resp.status);
            $(`#trrol_${id}`).remove();
          }catch (error) {
            Swal.fire(error.title,error.msg,error.status);
          }
        }
      });
    });

    async function request(url,type,params = null) {
      return new Promise((resolve,reject)=> {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        $.ajax({
          type: type,
          url: url,
          data: params,
          success: function(response){
            resolve(response);
          },
          error: function(xhr) {
            var obj = JSON.parse(xhr.responseText);
            reject(obj);
          }
        });
      });
    }

     function HandleBackFunctionality(e){
        if(window.event)
    {
          if(window.event.clientX < 40 && window.event.clientY < 0)
         {

         }
         else
         {
             document.getElementById("body").addEventListener("onbeforeunload", function(event){
              event.preventDefault()
              });

         }
     }
     else
     {
          if(event.currentTarget.performance.navigation.type == 1)
         {

         }
         if(event.currentTarget.performance.navigation.type == 2)
        {

        }
     }
    }
  </script>
@endsection