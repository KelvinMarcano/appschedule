<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Intergrow</title>
  <link href="http://fonts.googleapis.com/css?family=Roboto:300,100,400" rel="stylesheet" type="text/css" />
  <style type="text/css">
     .ReadMsgBody { width: 100%; background-color: #ffffff;}
     .ExternalClass {width: 100%; background-color: #ffffff;}
     .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%;}
     html{width: 100%; }
     body {-webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
     body {margin:0; padding:0;}
     table {border-spacing:0;}
     img{display:block !important;}
     table td {border-collapse:collapse;}
     .yshortcuts a {border-bottom: none !important;}
     html, body { background-color: #ffffff; margin: 0; padding: 0; }
     img { height: auto; line-height: 100%; outline: none; text-decoration: none; display: block;}
     br, strong br, b br, em br, i br { line-height:100%; }
     h1, h2, h3, h4, h5, h6 { line-height: 100% !important; -webkit-font-smoothing: antialiased; }
     h1 a, h2 a, h3 a, h4 a, h5 a, h6 a { color: blue !important; }
     h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active { color: red !important; }
     /* Preferably not the same color as the 300 header link color.  There is limited support for psuedo classes in email clients, this was added just for good measure. */
     h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited { color: purple !important; }
     /* Preferably not the same color as the 300 header link color. There is limited support for psuedo classes in email clients, this was added just for good measure. */
     table td, table tr { border-collapse: collapse; }
     .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span {
     color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;
     } /* Body text color for the New Yahoo.  This example sets the font of Yahoos Shortcuts to black. */
     /* This most probably wont work in all email clients. Dont include
     <sales_second_contact _tmplitem="406" >
     blocks in email. */
     sales_second_contact {
     white-space: 300;
     word-break: break-all;
     }
     span a {
     text-decoration: none !important;
     }
     a{
     text-decoration: none !important;
     }
     .default-edit-image{
     height:20px;
     }
     .nav-ul{
     margin-left:-23px !important;
     margin-top:0px !important;
     margin-bottom:0px !important;
     }
     img{
     height:auto !important;
     }
     td[class="image-270px"] img{
     width:270px !important;
     height:auto !important;
     max-width:270px !important;
     }
     td[class="image-170px"] img{
     width:170px !important;
     height:auto !important;
     max-width:170px !important;
     }
     td[class="image-185px"] img{
     width:185px !important;
     height:auto !important;
     max-width:185px !important;
     }
     td[class="image-124px"] img{
     width:124px !important;
     height:auto !important;
     max-width:124px !important;
     }
     @media only screen and (max-width: 640px){
     body{
     width:auto!important;
     }
     table[class="container"]{
     width: 100%!important;
     padding-left: 20px!important;
     padding-right: 20px!important;
     }
     td[class="image-270px"] img{
     width:100% !important;
     height:auto !important;
     max-width:100% !important;
     }
     td[class="image-170px"] img{
     width:100% !important;
     height:auto !important;
     max-width:100% !important;
     }
     td[class="image-185px"] img{
     width:185px !important;
     height:auto !important;
     max-width:185px !important;
     }
     td[class="image-124px"] img{
     width:100% !important;
     height:auto !important;
     max-width:100% !important;
     }
     td[class="image-100-percent"] img{
     width:100% !important;
     height:auto !important;
     max-width:100% !important;
     }
     td[class="small-image-100-percent"] img{
     width:100% !important;
     height:auto !important;
     }
     table[class="full-width"]{
     width:100% !important;
     }
     table[class="full-width-text"]{
     width:100% !important;
     background-color:#ffffff;
     padding-left:20px !important;
     padding-right:20px !important;
     }
     table[class="full-width-text2"]{
     width:100% !important;
     background-color:#ffffff;
     padding-left:20px !important;
     padding-right:20px !important;
     }
     table[class="col-2-3img"]{
     width:50% !important;
     margin-right: 20px !important;
     }
     table[class="col-2-3img-last"]{
     width:50% !important;
     }
     table[class="col-2-footer"]{
     width:55% !important;
     margin-right:20px !important;
     }
     table[class="col-2-footer-last"]{
     width:40% !important;
     }
     table[class="col-2"]{
     width:47% !important;
     margin-right:20px !important;
     }
     table[class="col-2-last"]{
     width:47% !important;
     }
     table[class="col-3"]{
     width:29% !important;
     margin-right:20px !important;
     }
     table[class="col-3-last"]{
     width:29% !important;
     }
     table[class="row-2"]{
     width:50% !important;
     }
     td[class="text-center"]{
     text-align: center !important;
     }
     /* start clear and remove*/
     table[class="remove"]{
     display:none !important;
     }
     td[class="remove"]{
     display:none !important;
     }
     /* end clear and remove*/
     table[class="fix-box"]{
     padding-left:20px !important;
     padding-right:20px !important;
     }
     td[class="fix-box"]{
     padding-left:20px !important;
     padding-right:20px !important;
     }
     td[class="font-resize"]{
     font-size: 18px !important;
     line-height: 22px !important;
     }
     table[class="space-scale"]{
     width:100% !important;
     float:none !important;
     }
     table[class="clear-align-640"]{
     float:none !important;
     }
     }
     @media only screen and (max-width: 479px){
     body{
     font-size:10px !important;
     }
     table[class="container"]{
     width: 100%!important;
     padding-left: 10px!important;
     padding-right:10px!important;
     }
     table[class="container2"]{
     width: 100%!important;
     float:none !important;
     }
     td[class="full-width"] img{
     width:100% !important;
     height:auto !important;
     max-width:100% !important;
     min-width:124px !important;
     }
     td[class="image-270px"] img{
     width:100% !important;
     height:auto !important;
     max-width:100% !important;
     min-width:124px !important;
     }
     td[class="image-170px"] img{
     width:100% !important;
     height:auto !important;
     max-width:100% !important;
     min-width:124px !important;
     }
     td[class="image-185px"] img{
     width:185px !important;
     height:auto !important;
     max-width:185px !important;
     min-width:124px !important;
     }
     td[class="image-124px"] img{
     width:100% !important;
     height:auto !important;
     max-width:100% !important;
     min-width:124px !important;
     }
     td[class="image-100-percent"] img{
     width:100% !important;
     height:auto !important;
     max-width:100% !important;
     min-width:124px !important;
     }
     td[class="small-image-100-percent"] img{
     width:100% !important;
     height:auto !important;
     max-width:100% !important;
     min-width:124px !important;
     }
     table[class="full-width"]{
     width:100% !important;
     }
     table[class="full-width-text"]{
     width:100% !important;
     background-color:#ffffff;
     padding-left:20px !important;
     padding-right:20px !important;
     }
     table[class="full-width-text2"]{
     width:100% !important;
     background-color:#ffffff;
     padding-left:20px !important;
     padding-right:20px !important;
     }
     table[class="col-2-footer"]{
     width:100% !important;
     margin-right:0px !important;
     }
     table[class="col-2-footer-last"]{
     width:100% !important;
     }
     table[class="col-2"]{
     width:100% !important;
     margin-right:0px !important;
     }
     table[class="col-2-last"]{
     width:100% !important;
     }
     table[class="col-3"]{
     width:100% !important;
     margin-right:0px !important;
     }
     table[class="col-3-last"]{
     width:100% !important;
     }
     table[class="row-2"]{
     width:100% !important;
     }
     table[id="col-underline"]{
     float: none !important;
     width: 100% !important;
     border-bottom: 1px solid #eee;
     }
     td[id="col-underline"]{
     float: none !important;
     width: 100% !important;
     border-bottom: 1px solid #eee;
     }
     td[class="col-underline"]{
     float: none !important;
     width: 100% !important;
     border-bottom: 1px solid #eee;
     }
     /*start text center*/
     td[class="text-center"]{
     text-align: center !important;
     }
     div[class="text-center"]{
     text-align: center !important;
     }
     table[id="clear-padding"]{
     padding:0 !important;
     }
     td[id="clear-padding"]{
     padding:0 !important;
     }
     td[class="clear-padding"]{
     padding:0 !important;
     }
     table[class="remove-479"]{
     display:none !important;
     }
     td[class="remove-479"]{
     display:none !important;
     }
     table[class="clear-align"]{
     float:none !important;
     }
     /* end  clear and remove */
     table[class="width-small"]{
     width:100% !important;
     }
     table[class="fix-box"]{
     padding-left:0px !important;
     padding-right:0px !important;
     }
     td[class="fix-box"]{
     padding-left:0px !important;
     padding-right:0px !important;
     }
     td[class="font-resize"]{
     font-size: 14px !important;
     }
     td[class="increase-Height"]{
     height:10px !important;
     }
     td[class="increase-Height-20"]{
     height:20px !important;
     }
     }
     @media only screen and (max-width: 320px){
     table[class="width-small"]{
     width:125px !important;
     }
     img[class="image-100-percent"]{
     width:100% !important;
     height:auto !important;
     max-width:100% !important;
     min-width:124px !important;
     }
     }
     .button{
     text-decoration:none;
     text-align:center;
     padding:11px 32px;
     border:solid 1px #004F72;
     -webkit-border-radius:4px;
     -moz-border-radius:4px;
     border-radius: 4px;
     font:18px Arial, Helvetica, sans-serif;
     font-weight:bold;
     color:#101212;
     background:#e8d528;
     -webkit-box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff;
     -moz-box-shadow: 0px 0px 2px #bababa,  inset 0px 0px 1px #ffffff;
     box-shadow:0px 0px 2px #bababa, inset 0px 0px 1px #ffffff;}
  </style>
 </head>
 <body  style="font-size:12px;">
  <table id="mainStructure" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#7FB79D;">
  <!--START VIEW ONLINE AND ICON SOCAIL -->
    <tr>
      <td align="center" valign="top" style="background-color:#7FB79D; ">
        <br>
        <!-- start container 600 -->
        <table width="600" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color:#FFF; ">
          <tr>
            <td height="40" valign="top">
              <table width="560" align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" style="background-color: #FFF; ">
                <!-- start space -->
                <tr>
                  <td valign="top" height="10">
                  </td>
                </tr>
                <!-- end space -->
                <tr>
                  <td valign="top">
                    <!-- start container -->
                    <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td valign="top">
                          <!-- start view online -->
                          <table align="left" border="0" cellspacing="0" cellpadding="0" class="container2">
                            <tr>
                              <td>
                                 <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr></tr>
                                 </table>
                              </td>
                            </tr>
                            <!-- start space -->
                            <tr>
                              <td valign="top" class="increase-Height"></td>
                            </tr>
                            <!-- end space -->
                          </table>
                          <!-- end view online -->
                          <!--start icon socail -->
                          <table border="0" align="center" cellpadding="0" cellspacing="0" class="container2">
                            <tr>
                              <td valign="top" align="center">
                                <table border="0" align="center" cellpadding="0" cellspacing="0" class="container">
                                  <tr>
                                    <td valign="top" align="center">
                                      <table width="670" border="0" align="center" cellpadding="0" cellspacing="0" class="container">
                                        <tr>
                                          <!-- Seccion para el logo -->
                                          <td s="" align="left" valign="middle" id="clear-padding">
                                            <a href="#" style="text-decoration: none;">
                                              <img src="{{ asset('images/email_logo.jpg') }}"  style="max-width:200px;" border="0" hspace="0" vspace="0">
                                            </a>
                                          </td>
                                          <!-- ********************** -->
                                          <td s="" align="right" valign="middle" id="clear-padding"></td>
                                        </tr>
                                      </table>
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                          <!--end icon socail -->
                        </td>
                      </tr>
                    </table>
                    <!-- end container  -->
                  </td>
                </tr>
                <!-- start space -->
                <tr>
                  <td valign="top" height="5"></td>
                </tr>
                <!-- end space -->
                <!-- start space -->
                <tr>
                  <td valign="top" class="increase-Height"></td>
                </tr>
                <!-- end space -->
              </table>
              <!-- end container 600-->
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <!--END VIEW ONLINE AND ICON SOCAIL-->
    <!--START TOP NAVIGATION LAYOUT-->
    <tr>
    <td valign="top">
      <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
        <!-- START CONTAINER NAVIGATION -->
        <tbody>
          <tr>
            <td align="center" valign="top">
              <!-- start top navigation container -->
              <table width="670" align="center" border="0" cellspacing="0" cellpadding="0" class="container" style="background-color: #fff;border-bottom: 2px solid #E51937;">
                <tbody>
                  <tr>
                    <td valign="top"></td>
                  </tr>
                </tbody>
              </table>
              <!-- end top navigation container -->
            </td>
          </tr>
          <!-- END CONTAINER NAVIGATION -->
        </tbody>
      </table>
    </td>
  </tr>
  <!--END TOP NAVIGATION LAYOUT-->
  <!-- START LAYOUT-1/2 -->
  <tr>
    <td align="center" valign="top"   class="fix-box">
      <!-- start  container width 600px -->
      <table width="670"  align="center" border="0" cellspacing="0" cellpadding="0" class="container"  style="background-color: #ffffff; ">
        <!-- ******* Seccion detalle Ausencia ***** -->
        <div>
          <p style='text-align:center'>
            <span style='font-size:12.0pt;line-height:107%;'>
              <strong>{{ ($origenValidation == "absence") ? 'From: (No Reply, Intergrow Attendance Control)' : 'From: (No Reply, Intergrow Violation Control)' }}</strong>
            </span>
          </p>
          <br>
          <p style='text-align:justify; margin: 20px;'>
            <span style='font-size:12.0pt;line-height:100%;'>
               <strong>Platform: </strong>{{ $absence->platform }}
            </span>
          </p>
          <p style='text-align:center'>
            <span style='font-size:12.0pt;line-height:107%'>
              <strong>{{ ($origenValidation=="absence") ? 'Attendance Notice' : 'Violation Notice' }}</strong>
            </span>
          </p>
          <br>
          <p style='text-align:justify; margin: 20px;'>
            <span style='font-size:12.0pt;line-height:100%;'>
              <strong>Employee: </strong>{{ $employee->code }}-{{ $employee->firstname }} {{ $employee->lastname }}
            </span>
          </p>
          <p style='text-align:justify; margin: 20px;'>
            @if(!empty($supervisor))
              <span style='font-size:12.0pt;line-height:100%;'>
                <strong>Supervisor: </strong>{{ $supervisor->firstname }} {{ $supervisor->lastname }}
              </span>
            @endif
          </p>
          <p style='text-align:justify; margin: 20px;'>
            <span style='font-size:12.0pt;line-height:100%;'>
              <strong>Site: </strong>{{ (!empty($employee->idsite)) ? $employee->site()->name : '' }}
            </span>
          </p>
          <p style='text-align:justify; margin: 20px;'>
            <span style='font-size:12.0pt;line-height:100%;'>
              <strong>Area: </strong>{{ (!empty($employee->departamentid)) ? $employee->departament()->description : '' }}
            </span>
          </p>
          <p style='text-align:justify; margin: 20px;'>
            <span style='font-size:12.0pt;line-height:100%;'>
              <strong>Reason: </strong>{{ (!empty($absence->motiveid)) ? $absence->motive()->description : '' }}
            </span>
          </p>
          <p style='text-align:justify; margin: 20px;'>
            <span style='font-size:12.0pt;line-height:100%;'>
              <strong>Reason for Attendance: </strong>{{ $absence->reason }}
            </span>
          </p>
          <p style='text-align:justify; margin: 20px;'>
            <span style='font-size:12.0pt;line-height:100%;'>
            <strong>Personal time / Points / Violations: </strong>{{ (!empty($absence->violation_id)) ? $absence->violation()->description : '' }}
            </span>
          </p>
          <p style='text-align:justify; margin: 20px;'>
            <span style='font-size:12.0pt;line-height:100%;'>
            <strong>From: </strong>{{ \Carbon\Carbon::parse($absence->fecha_init)->format('m/d/Y') }} <strong>Hour: </strong>{{ $absence->hora_init }}
            </span>
          </p>
          <p style='text-align:justify; margin: 20px;'>
            <span style='font-size:12.0pt;line-height:100%;'>
            <strong>To: </strong>{{ \Carbon\Carbon::parse($absence->fecha_end)->format('m/d/Y') }} <strong>Hour: </strong>{{ $absence->hora_end }}
            </span>
          </p>
          <p style='text-align:justify; margin: 20px;'>
            <span style='font-size:12.0pt;line-height:100%;'>
            <strong>Comment: </strong>{{ $absence->comment }}
            </span>
          </p>
          <br>
          <p style='text-align:center'>This is an automatic notification issued by the
            <span style='font-size:12.0pt;line-height:100%;'>TiBaan</span>
            Application by Intergrow Greenhouses Inc.
          </p>
        </div>
        <!-- ********************************************************** -->
                <tr>
                   <td valign="top">
                      <!-- start container width 560px -->
                      <table width="540"  align="center" border="0" cellspacing="0" cellpadding="0" class="full-width" bgcolor="#ffffff" style="background-color:#ffffff;">
                         <!-- start text content -->
                         <tr>
                            <td valign="top">
                               <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
                                  <!-- start text content -->
                                  <tr>
                                     <td valign="top">
                                        <table border="0" cellspacing="0" cellpadding="0" align="center" >
                                           <!--start space height -->
                                           <tr>
                                              <td height="15" ></td>
                                           </tr>
                                           <!--end space height -->
                                           <tr>
                                              <td  style="font-size: 13px; line-height: 22px; font-family:Roboto,Open Sans,Arial,Tahoma, Helvetica, sans-serif; color:#a3a2a2; font-weight:300; text-align:left; ">
                                                 <p>
                                                 </p>
                                                 <p></p>
                                                 <table>
                                                    <tr>
                                                       <td>
                                                       </td>
                                                       <td>
                                                       </td>
                                                    </tr>
                                                 </table>
                                                 <p style="font-size:12px;line-height:145%"></p>
                                              </td>
                                           </tr>
                                           <!--start space height -->
                                           <tr>
                                              <td height="15" ></td>
                                           </tr>
                                           <!--end space height -->
                                        </table>
                                     </td>
                                  </tr>
                                  <!-- end text content -->
                               </table>
                            </td>
                         </tr>
                         <!-- end text content -->
                         <!--start space height -->
                         <tr>
                            <td height="20" ></td>
                         </tr>
                         <!--end space height -->
                      </table>
                      <!-- end  container width 560px -->
                   </td>
                </tr>
             </table>
             <!-- end  container width 600px -->
             <br>
          </td>
       </tr>
       <!-- END LAYOUT-1/2 -->
    </table>
    <table id="mainStructure" width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color:#7FB79D; padding-bottom:20px;">
       <!--START TOP NAVIGATION LAYOUT-->
       <tr>
          <td valign="top">
             <table width="100%" align="center" border="0" cellspacing="0" cellpadding="0">
             </table>
          </td>
       </tr>
       <!--END TOP NAVIGATION LAYOUT-->
       <!-- START LAYOUT-1/2 -->
       <tr>
          <td align="center" valign="top"   class="fix-box">
             <!-- start  container width 600px -->
             <!-- end  container width 600px -->
          </td>
       </tr>
       <!-- END LAYOUT-1/2 -->
    </table>
 </body>
</html>