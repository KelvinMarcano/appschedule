<!doctype html>
<html lang="es">
    <head>
        <meta http-equiv=Content-Type content="text/html; charset=utf-8">
        <meta name=ProgId content=Word.Document>
        <meta name=Generator content="Microsoft Word 15">
        <meta name=Originator content="Microsoft Word 15">
        <style>
        <!--
         /* Font Definitions */
         @font-face
            {font-family:"Cambria Math";
            panose-1:2 4 5 3 5 4 6 3 2 4;
            mso-font-charset:0;
            mso-generic-font-family:roman;
            mso-font-pitch:variable;
            mso-font-signature:3 0 0 0 1 0;}
        @font-face
            {font-family:Calibri;
            panose-1:2 15 5 2 2 2 4 3 2 4;
            mso-font-charset:0;
            mso-generic-font-family:swiss;
            mso-font-pitch:variable;
            mso-font-signature:-469750017 -1073732485 9 0 511 0;}
         /* Style Definitions */
         p.MsoNormal, li.MsoNormal, div.MsoNormal
            {mso-style-unhide:no;
            mso-style-qformat:yes;
            mso-style-parent:"";
            margin-top:0cm;
            margin-right:0cm;
            margin-bottom:8.0pt;
            margin-left:0cm;
            line-height:107%;
            mso-pagination:widow-orphan;
            font-size:11.0pt;
            font-family:"Calibri",sans-serif;
            mso-ascii-font-family:Calibri;
            mso-ascii-theme-font:minor-latin;
            mso-fareast-font-family:Calibri;
            mso-fareast-theme-font:minor-latin;
            mso-hansi-font-family:Calibri;
            mso-hansi-theme-font:minor-latin;
            mso-bidi-font-family:Arial;
            mso-bidi-theme-font:minor-bidi;
            mso-ansi-language:ES-VE;
            mso-fareast-language:EN-US;}
        span.SpellE
            {mso-style-name:"";
            mso-spl-e:yes;}
        span.GramE
            {mso-style-name:"";
            mso-gram-e:yes;}
        .MsoChpDefault
            {mso-style-type:export-only;
            mso-default-props:yes;
            font-size:11.0pt;
            mso-ansi-font-size:11.0pt;
            mso-bidi-font-size:11.0pt;
            font-family:"Calibri",sans-serif;
            mso-ascii-font-family:Calibri;
            mso-ascii-theme-font:minor-latin;
            mso-fareast-font-family:Calibri;
            mso-fareast-theme-font:minor-latin;
            mso-hansi-font-family:Calibri;
            mso-hansi-theme-font:minor-latin;
            mso-bidi-font-family:Arial;
            mso-bidi-theme-font:minor-bidi;
            mso-ansi-language:ES-VE;
            mso-fareast-language:EN-US;}
        .MsoPapDefault
            {mso-style-type:export-only;
            margin-bottom:8.0pt;
            line-height:107%;}
        @page WordSection1
            {size:612.0pt 792.0pt;
            margin:72.0pt 72.0pt 72.0pt 72.0pt;
            mso-header-margin:36.0pt;
            mso-footer-margin:36.0pt;
            mso-paper-source:0;}
        div.WordSection1
            {page:WordSection1;}
        </style>
    </head>

    <body style='tab-interval:35.4pt'>
        <div>
            <p style='text-align:center'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>From: (No Reply, Intergrow Attendance Control)</strong>
                </span>
            </p>

            <br>

            <p style='text-align:center'>
                <span style='font-size:12.0pt;line-height:107%'>
                    <strong>Attendance Notice</strong>
                </span>
            </p>

            <br>

            <p style='text-align:justify'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>Employee: </strong>{{ $employee->code }}-{{ $employee->firstname }} {{ $employee->lastname }}
                </span>
            </p>
            <p style='text-align:justify'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>Supervisor: </strong>{{ $supervisor->firstname }} {{ $supervisor->lastname }}
                </span>
            </p>

            <p style='text-align:justify'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>Site: </strong>{{ (!empty($employee->idsite)) ? $employee->site()->name : '' }}
                </span>
            </p>

            <p style='text-align:justify'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>Area: </strong>{{ (!empty($employee->departamentid)) ? $employee->departament()->description : '' }}
                </span>
            </p>

            <p style='text-align:justify'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>Reason: </strong>{{ $absence->motive()->description }}
                </span>
            </p>
            <p style='text-align:justify'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>Reason for Attendance: </strong>{{ $absence->reason }}
                </span>
            </p>

            <p style='text-align:justify'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>Personal time / Points / Violations: </strong>{{ $absence->violation()->description }}
                </span>
            </p>

            <p style='text-align:justify'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>From: </strong>{{ \Carbon\Carbon::parse($absence->fecha_init)->format('m/d/Y') }} <strong>Hour: </strong>{{ $absence->hora_init }}
                </span>
            </p>

            <p style='text-align:justify'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>To: </strong>{{ \Carbon\Carbon::parse($absence->fecha_end)->format('m/d/Y') }} <strong>Hour: </strong>{{ $absence->hora_end }}
                </span>
            </p>

            <p style='text-align:justify'>
                <span style='font-size:12.0pt;line-height:107%;'>
                    <strong>Comment: </strong>{{ $absence->comment }}
                </span>
            </p>

            <br>

            <p style='text-align:center'>This is an automatic notification issued by the
                <span style='font-size:12.0pt;line-height:107%;'>TiBaan</span>
                Application by Intergrow Greenhouses Inc.
            </p>

        </div>

    </body>

</html>