<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta http-equiv="Content-Security-Policy" content="default-src * 'self' 'unsafe-inline' 'unsafe-eval' data: gap:">
    <link rel="icon" href="{{ asset('kiosko/images/favicon.png') }}">
    <title>Kiosko - App TiBaan</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('kiosko/css/framework7.bundle.min.css') }}">
    <link rel="stylesheet" href="{{ asset('kiosko/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('kiosko/css/style.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="{{ asset('js/plugins/time-picker/mdtimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/signature-pad-master/assets/jquery.signaturepad.css') }}">
  </head>
  <body>

    <div id="app">
      <div class="view view-main view-init" data-url="/">
        <div class="page page-home page-with-subnavbar">
          <div class="toolbar tabbar tabbar-labels toolbar-bottom">
            <div class="toolbar-inner">
              <a href="#tab-1" class="tab-link tab-link-active" id="tab1">
                <i class="fa fa-home"></i>
                <p>{{ trans('lang.home') }}</p>
              </a>
              <a href="#tab-2" class="tab-link" id="tab2"></a>
              <a href="#tab-2" class="tab-link">
                <i class="fas fa-list"></i>
                <p>{{ trans('lang.form') }}</p>
              </a>
            </div>
          </div>

          <!-- tabs -->
          <div class="tabs-animated-wrap">
            <div class="tabs">
              <!-- tabs 1 -->
              <div id="tab-1" class="tab page-content tab-active">
                <!-- home -->
                <div class="container">

                  <!-- logo -->
                    <div class="segments-page">
                      <div class="container">
                        <div class="gallery">
                          <div class="row">
                            <div class="col">
                              <div class="content-image">
                                <a href="#" class="link popup-open">
                                  <img src="{{ asset('kiosko/images/logo.png') }}" alt="" style="width: 40% !important; box-shadow: none !important;">
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  <!-- end logo -->
                  <div class="separator"></div>
                  <!-- features -->
                  <div class="features-home">


                    <div class="row icon-color">
                      <div class="col">
                        <a href="lang/en" onclick="language('en')" data-reload-current="true">
                          <div class="content">
                            <img src="{{ asset('kiosko/images/bandera/ingles.png') }}" style="width: 20% !important;">
                            <h6>{{ trans('lang.english') }}</h6>
                          </div>
                        </a>
                      </div>
                      <div class="col">
                        <a href="lang/es" onclick="language('es')">
                          <div class="content">
                            <img src="{{ asset('kiosko/images/bandera/spain.png') }}" style="width: 20% !important;">
                            <h6>{{ trans('lang.spain') }}</h6>
                          </div>
                        </a>
                      </div>
                      <div class="col">
                        <a href="lang/en" onclick="language('ch')">
                          <div class="content">
                            <img src="{{ asset('kiosko/images/bandera/china.png') }}" style="width: 20% !important;">
                            <h6>{{ trans('lang.chinese') }}</h6>
                          </div>
                        </a>
                      </div>
                      <div class="col">
                        <a href="lang/en" onclick="language('ht')">
                          <div class="content">
                            <img src="{{ asset('kiosko/images/bandera/haiti.png') }}" style="width: 20% !important;">
                            <h6>{{ trans('lang.haiti') }}</h6>
                          </div>
                        </a>
                      </div>
                    </div>
                    <div class="separator"></div>
                    <div class="separator"></div>
                    <div class="separator"></div>
                    <div class="row icon-color2">
                      <div class="col">
                        <div class="content" onclick="openSearch(1);">
                          <img src="{{ asset('kiosko/images/ssn.png') }}" style="width: 20% !important;">
                          <h6>{{ trans('lang.lastSSN') }}</h6>
                        </div>
                      </div>
                      <div class="col">
                        <div class="content" onclick="openSearch(2);">
                          <img src="{{ asset('kiosko/images/phone.png') }}" style="width: 20% !important;" >
                          <h6>{{ trans('lang.cellPhone') }}</h6>
                        </div>
                      </div>
                      <div class="col">
                        <div class="content" onclick="openSearch(3);">
                          <img src="{{ asset('kiosko/images/usuario.png') }}" style="width: 20% !important;" class="open-preloader">
                          <h6>{{ trans('lang.employeeID') }}</h6>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end features -->

                  <div class="separator"></div>

                  <!-- end features -->
                  <div class="separator-big"></div>
                </div>
                <!-- end home -->
              </div>
              <!-- end tabs 1 -->

              <!-- tabs 2 -->
              <div id="tab-2" class="tab page-content">
                <!-- elements -->
                <div class="container">
                  <!-- logo -->
                  <div class="segments-page">
                    <div class="container">
                      <div class="gallery">
                        <div class="row">
                          <div class="col">
                            <div class="content-image">
                              <a href="#" class="link popup-open">
                                <img src="{{ asset('kiosko/images/logo.png') }}" alt="" style="width: 40% !important; box-shadow: none !important;">
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                    <!-- end logo -->
                  <div class="separator"></div>
                  <h1 class="text-center text-color-white">{{ trans('lang.titulo1') }}</h1>
                  <div class="separator"></div>

                  <div class="row">
                    <div class="col">
                      <div class="card card-expandable card-prevent-open" style="height: 100px !important">
                        <div class="card-content" style="padding: 0px !important;">
                          <div style="height: 100px; background-color: #EBF6DC;">
                            <div class="card-header text-color-red display-block" >
                              Personal Time:
                              <br>
                              <small style="opacity: 0.7">Available: 20</small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col">
                      <div class="card card-expandable card-prevent-open" style="height: 100px !important">
                        <div class="card-content" style="padding: 0px !important;">
                          <div class="" style="height: 100px; background-color: #F5D7B5;">
                            <div class="card-header text-color-red display-block">
                              Points Balance:
                              <br>
                              <small style="opacity: 0.7">5/5</small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col">
                      <div class="card card-expandable card-prevent-open" style="height: 100px !important">
                        <div class="card-content" style="padding: 0px !important;">
                          <div class="" style="height: 100px; background-color: #F8E367;">
                            <div class="card-header text-color-red display-block">
                              Day`s Vacations
                              <br>
                              <small style="opacity: 0.7">Available: 10</small>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <!--register -->
                  <div class="login-register">
                    <div class="container">
                      <form class="text-center">
                        <div class="row">
                          <h2 class="text-color-white" id="inputCode">ID:</h2>
                          <input type="hidden" value="" class="text-color-white" id="id">
                          <input type="hidden" value="" class="text-color-white" id="supervisor">
                        </div>
                        <div class="row">
                          <h2 class="text-color-white" id="h2name">{{ trans('lang.textName') }}:</h2>
                        </div>
                        <div class="row">
                          <h2 class="text-color-white" id="h2depart">{{ trans('lang.textDepart') }}:</h2>
                        </div>
                        <div class="row">
                          <h2 class="text-color-white" id="h2manager">{{ trans('lang.textManage') }}:</h2>
                        </div>

                        <h1 class="text-center text-color-white">{{ trans('lang.typeAbsence') }}</h1>
                        <div class="separator-small"></div>
                        <div class="separator-small"></div>
                        <div class="separator-small"></div>
                        <div class="separator-small"></div>
                        <div class="separator-small"></div>
                        <div class="row">
                          <div class="col center">
                            <li>
                              <a class="item-link smart-select smart-select-init text-color-white" data-open-in="sheet">
                                <select name="mac-windows" id="motive" style="font-size: 22px;" onchange="changeMotive(this);">
                                  <option value="">{{ trans('lang.selectOne') }}</option>
                                  @foreach($motives as $motive)
                                    <option value="{{ $motive->id }}" style="font-size: 22px;">{{ $motive->code }}-{{ $motive->description }}</option>
                                  @endforeach
                                </select>
                                <div class="item-content">
                                  <div class="item-inner">
                                    <div class="item-title text-color-white" style="font-size: 22px;">{{ trans('lang.reason') }}</div>
                                  </div>
                                </div>
                              </a>
                            </li>

                          </div>
                          <div class="col">
                            <div class="col center">
                              <li>
                                <a class="item-link smart-select smart-select-init text-color-white" data-open-in="sheet">
                                  <select name="mac-windows" id="motive2" style="font-size: 22px;">
                                    <option value="">{{ trans('lang.selectOne') }}</option>
                                    @foreach($category as $catego)
                                      <option value="{{ $catego->cat_descri_en }}" style="font-size: 22px;">{{ $catego->cat_descri_en }}</option>
                                    @endforeach
                                  </select>
                                  <div class="item-content">
                                    <div class="item-inner">
                                      <div class="item-title text-color-white" style="font-size: 22px;">{{ trans('lang.reasonFor') }}</div>
                                    </div>
                                  </div>
                                </a>
                              </li>
                            </div>
                          </div>
                        </div>
                        <div class="separator"></div>
                        <div class="row">
                          <label for="commnet" class="text-color-white" style="font-size: 22px;">{{ trans('lang.textComment') }}</label>
                          <textarea id="commnet" class="text-color-white" style="font-size: 22px;" placeholder="{{ trans('lang.textComment') }}"></textarea>
                        </div>
                        <div class="separator"></div>

                        <div class="row">
                          <div class="col">
                            <div class="list">
                              <ul>
                                <li>
                                  <label class="item-radio item-radio-icon-start item-content">
                                    <input type="radio" name="radio-type" value="20" />
                                    <i class="icon icon-radio"></i>
                                    <div class="item-inner">
                                      <div class="item-title">{{ trans('lang.lateArrival') }}</div>
                                    </div>
                                  </label>
                                </li>
                                <li>
                                  <label class="item-radio item-radio-icon-start item-content">
                                    <input type="radio" name="radio-type" value="2"/>
                                    <i class="icon icon-radio"></i>
                                    <div class="item-inner">
                                      <div class="item-title">{{ trans('lang.leavingEarly') }}</div>
                                    </div>
                                  </label>
                                </li>
                                <li>
                                  <label class="item-radio item-radio-icon-start item-content">
                                    <input type="radio" name="radio-type" value="16"/>
                                    <i class="icon icon-radio"></i>
                                    <div class="item-inner">
                                      <div class="item-title">{{ trans('lang.fullDayAbsen') }}</div>
                                    </div>
                                  </label>
                                </li>
                              </ul>
                            </div>

                          </div>
                          <div class="col">
                            <div class="item-inner text-color-white" style="font-size: 22px;">
                              <div class="item-title item-label">{{ trans('lang.time') }}</div>
                              <div class="item-input-wrap">
                                <input type="text"  placeholder="{{ trans('lang.selectOne') }}" id="hora_init">
                              </div>
                            </div>
                            <div class="item-inner text-color-white" style="font-size: 22px;">
                              <div class="item-title item-label">{{ trans('lang.days') }}</div>
                              <div class="item-input-wrap">
                                <input type="number" placeholder="{{ trans('lang.selectOne') }}" id="days">
                              </div>
                            </div>
                          </div>
                          <div class="col">
                            <div class="item-inner text-color-white" style="font-size: 22px;">
                              <div class="item-title item-label">{{ trans('lang.from') }}</div>
                              <div class="item-input-wrap">
                                <input type="date" placeholder="{{ trans('lang.selectOne') }}" id="from" onchange="validateFecha();" value="{{ date('m/d/Y') }}">
                              </div>
                            </div>
                            <div class="item-inner text-color-white" style="font-size: 22px;">
                              <div class="item-title item-label">{{ trans('lang.to') }}</div>
                              <div class="item-input-wrap">
                                <input type="date" placeholder="{{ trans('lang.selectOne') }}" id="to" onchange="validateFecha(); changeDate();" value="{{ date('m/d/Y') }}">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="separator"></div>

                        <div class="row">
                          <div class="col">
                            <div class="sigPad" id="smoothed" style="width:404px;">
                              <h2>{{ trans('lang.signature') }}</h2>
                              <ul class="sigNav">
                                <li class="drawIt"><a href="#clear" >Clear</a></li>
                              </ul>
                              <div class="sig sigWrapper" style="height:auto;">
                                <div class="typed"></div>
                                  <canvas class="pad" width="400" height="250"></canvas>
                                  <input type="hidden" name="output" class="output" id="signature">
                                </div>
                              </div>

                          </div>
                          <div class="col">
                            <div class="item-inner text-color-white" style="font-size: 22px;">
                              <div class="item-title item-label">{{ trans('lang.date') }}</div>
                              <div class="item-input-wrap">
                                <input type="date" placeholder="{{ trans('lang.selectOne') }}" id="date">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="separator"></div>
                        <div class="separator"></div>
                      </form>
                      <div class="row">
                        <div class="col">
                          <button class="buttons buttons-center buttons-full" id="resetSign" style="background-color: #00703C;" onclick="save()">{{ trans('lang.save') }}</button>
                        </div>
                        <div class="col">
                          <button class="buttons buttons-center buttons-full" id="resetSign" style="background-color: #F8E367;" onclick="clear();">{{ trans('lang.clear') }}</button>
                        </div>
                        <div class="col">
                          <button class="buttons buttons-center buttons-full" id="resetSign" style="background-color: #C5032B;" onclick="$('#tab1').click()">{{ trans('lang.cancel') }}</button>
                        </div>
                      </div>


                    </div>
                    <div class="separator"></div>
                  </div>
                  <!-- end register -->


                </div>
                <!-- end elements -->
                <div class="separator-big"></div>
              </div>
              <!-- end tabs 2 -->
            </div>
          </div>
          <!-- end tabs -->
        </div>
      </div>
    </div>
    <!-- script -->

    <script src="{{ asset('kiosko/js/framework7.bundle.min.js') }}"></script>
    <script src="{{ asset('kiosko/js/routes.js') }}"></script>
    <script src="{{ asset('kiosko/js/app.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/time-picker/mdtimepicker.min.js') }}"></script>
    <script src="{{ asset('js/plugins/signature-pad-master/jquery.signaturepad.js') }}"></script>
    <script src="{{ asset('js/plugins/signature-pad-master/assets/json2.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment-with-locales.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>

    <script>

      var $$ = Dom7;
      $('#tab2').hide();

      jQuery(document).ready(function() {
        changeMotive($('#motive'));
          jQuery('#hora_init').mdtimepicker({
          theme: 'green',// 'red', 'purple', 'indigo', 'teal', 'green', 'dark'
          readOnly: false,
          hourPadding: false,
          clearBtn: true
        });
        jQuery('#smoothed').signaturePad({
          drawOnly:true,
          drawBezierCurves:true,
          lineTop:200,
          clear : '.drawIt'
        });
      });

      jQuery('#days').change(function(){
        setDate();
      });

      jQuery('#days').keyup(function(){
        setDate();
      });

      async function language(sender) {
        await request(`lang/${sender}`,'get',{});
        location.reload();
      }

      async function openSearch(option){
        app.dialog.prompt('','{{ trans("lang.titleSearch") }}', async function (name) {
          //app.dialog.preloader('{{ trans("lang.searching") }}');
          let params = {
              option : option,
              value  : name,
          };
          try{
            let data = await request(`employes-filter`,'post',params);
            if(data.status != 'success'){
              app.dialog.alert(data.msg, data.title);
                return;
            }

            if(data.data == null){
              console.log("entra");
              app.dialog.alert('employee dont exit', data.title);
            }else{
              let empleado = data.data;
              let text1 = '{{ trans('lang.textName') }}:';
              let text2 = '{{ trans('lang.textDepart') }}:';
              let text3 = '{{ trans('lang.textManage') }}:';

              $('#inputCode').text(`ID: ${empleado.code}`);
              $('#id').val(empleado.id);
              $('#h2name').text(`${text1} ${empleado.firstname} ${empleado.lastname}`);

              if(empleado.departament != null){
                $('#h2depart').text(`${text2} ${empleado.departament.code} - ${empleado.departament.description}`);
              }

              if(empleado.supervisor != null){
                $('#h2manager').text(`${text3} ${empleado.supervisor.firstname} ${empleado.supervisor.lastname}`);
                $('#supervisor').val(empleado.supervisor.id);
              }

              app.dialog.close();
              $('#tab2').click();
            }

          }catch (error) {
            app.dialog.alert(error.msg, error.title);
          }
        });
      }

      async function request(url,type,params = null) {
        return new Promise((resolve,reject)=> {
          jQuery.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
          jQuery.ajax({
            type: type,
            url: url,
            data: params,
            success: function(response){
              resolve(response);
            },
            error: function(xhr) {
              var obj = JSON.parse(xhr.responseText);
              reject(obj);
            }
          });
        });
      }

      function changeMotive(sender) {
        let category = @json($category);
        let lang ='{{ str_replace('_', '-', app()->getLocale()) }}';

        $('#motive2 option').remove();
        $('#motive2').append(`<option value="">Select one...</option>}`);
        category.forEach((element)=>{
          if(element.motive_id == $(sender).val()){
            if(lang == 'en'){
              $('#motive2').append(`<option value="${element.cat_descri_en}">${element.cat_descri_en}</option>}`);
            }
            if(lang == 'es'){
              $('#motive2').append(`<option value="${element.cat_descri_es}">${element.cat_descri_es}</option>}`);
            }
            if(lang == 'ch'){
              $('#motive2').append(`<option value="${element.cat_descri_ch}">${element.cat_descri_ch}</option>}`);
            }
            if(lang == 'ht'){
              $('#motive2').append(`<option value="${element.cat_descri_ht}">${element.cat_descri_ht}</option>}`);
            }

          }
        });
      }

      async function save() {
        let params = {
          employesid    : $('#id').val(),
          motiveid      : 17,
          supervisor    : $('#supervisor').val(),
          motiveid2     : $('#motive').val(),
          reason        : $('#motive2').val(),
          comment       : $('#commnet').val(),
          hora_init     : $('#hora_init').val(),
          days          : $('#days').val(),
          fecha_init    : $('#from').val(),
          fecha_end     : $('#to').val(),
          created_at    : $('#date').val(),
          signature     : $('#signature').val(),
          violation_id  : $("input[name='radio-type']:checked").val(),
          user_add      : "",
          platform      : 'Kiosko'
        };
        app.dialog.preloader('');
        try{
          let data = await request(`register_absences`,'post',params);
          app.dialog.close();
          Swal.fire({
            title: data.title,
            text: data.msg,
            icon: data.status,
            showCancelButton: false,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ok!'
          }).then((result) => {
            if (result.value) {
              location.reload();
            }
          });

        }catch (error) {
          app.dialog.close();
          Swal.fire(error.title,error.msg,error.status);
        }
      }

      function clear() {
        $('#motive').val("");
        $('#motive2').val("");
        $('#commnet').val("");
        $('#hora_init').val("");
        $('#days').val("");
        $('#from').val("");
        $('#to').val("");
        $('#date').val("");
        $('.drawIt').click();
      }

      function setDate() {
        if($('#days').val() == ""){
          $('#fecha_end').val($('#fecha_init').val());
          $('#days').val(1);
          return;
        }
        let inicio = moment($('#fecha_init').val(),'MM/DD/YYYY').format('YYYY-MM-DD');
        let fechaI = moment(inicio);

        date = fechaI.add($('#days').val() - 1, 'd').format('MM/DD/YYYY');

        $('#fecha_end').val(date);
      }

      function changeDate() {
        let inicio = moment($('#fecha_init').val(),'MM/DD/YYYY').format('YYYY-MM-DD');
        let fin = moment($('#fecha_end').val(),'MM/DD/YYYY').format('YYYY-MM-DD');
        let fechaI = moment(inicio);
        let fechaF = moment(fin);
        let dias = (fechaI.diff(fechaF, 'days') == 0) ? 1 : fechaF.diff(fechaI, 'days');
        console.log(fin);
        if (fin >= inicio) {
          $('#days').val(dias+1);
        }

        if (fin == inicio) {
          $('#days').val(1);
        }
        //$('#days').val(fechaF.diff(fechaI, 'days'));
      }

      function validateFecha() {
          console.log($('#fecha_init').val());
          console.log($('#fecha_end').val());

          let fechaI = moment($('#fecha_init').val());
          let fechaF = moment($('#fecha_end').val());

          console.log(fechaF.isAfter(fechaI));
      }
    </script>
    <!-- end script -->
  </body>
</html>