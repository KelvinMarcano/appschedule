<?php

//header("Access-Control-Allow-Origin: *");
//header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

include_once 'datos/ConexionBD.php';
require_once 'datos/login_mysql.php';

class licencia
{
    const tipo = "application/json";
    public $_datosPeticion = array();
    private $_codEstado = 200;

    const OK = 200;
    const MENSAJE_OK = "correcto";
    const NOT_FOUND = 400;
    const MENSAJE_ERROR = "error";

    const UNAUTHORIZED = 401;
    const MENSAJE_UNAUTHORIZED = "error Unauthorized";

    const MENSAJE_NOT_ACTIVE_CLAVE = "El vendedor no esta activo o la clave es erronea";
    const MENSAJE_NOT_FOUND = "No se encontraron registros";
    const METHOD_NOT_ALLOWED = 405;
    const MENSAJE_METHOD_NOT_ALLOWED = "Url mal formada o el metodo no es el correcto";
    const UNPROCESSABLE_ENTITY = 422;
    const MENSAJE_UNPROCESSABLE_ENTITY = "No indico el codigo del grupo";
    const CREATED = 201;
    const MENSAJE_CREATED = "Registro creado correctamen";

    public static function post($peticion)
    {
        if ($peticion[0] == 'validaLicencia') {
            return self::getLicencia();
        } elseif ($peticion[0] == 'validaLicenciaVfp'){
            return self::getLicenciaVfp();
        } elseif ($peticion[0] == 'validaLicenciaVfpPlano'){
            return self::getLicenciaVfpPlano();
        } else {
            header("HTTP/1.1 405 Method Not Allowed");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_ERROR;
            $respuesta['code'] = self::METHOD_NOT_ALLOWED;
            $respuesta['mensaje'] = self::MENSAJE_METHOD_NOT_ALLOWED;

            echo json_encode($respuesta);
            exit();
        }
    }

    private function getLicencia()
    {
        $body = file_get_contents('php://input');
        $licenciaApp = json_decode($body);
        $licencia = $licenciaApp->licencia;
        $aplicacionId = $licenciaApp->aplicacion_id;

        if ($aplicacionId=="") {
            header("HTTP/1.1 404 Not Found");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_ERROR;
            $respuesta['codeError'] = self::NOT_FOUND;
            $respuesta['mensaje'] = "No indico el parametro del id de la aplicacion que esta realizando la peticion";

            echo json_encode($respuesta);
            exit();
        }

        $comando = "SELECT limite, " .
                   "       aplicacion_id, " .
                   "       security_licencias.user_id, " .
                   "       licencia, " .
                   "       expires AS expira, " .
                   "       security_apps.descripcion, " .
                   "       IF(ISNULL(authtoken_token.key), '', authtoken_token.key) AS token, " .
                   "       perfil_user.username, " .
                   "       perfil_user.nombre_empresa, " .
                   "       perfil_user.first_name, " .
                   "       perfil_user.last_name, " .
                   "       perfil_user.nrorif, " .
                   "       perfil_user.direccion, " .
                   "       perfil_user.email, " .
                   "       perfil_user.pagina_web, " .
                   "       perfil_user.telefonos, " .
                   "       perfil_user.logoemp " .
                   "FROM security_licencias " .
                   "INNER JOIN security_apps ON security_apps.id=aplicacion_id " .
                   "INNER JOIN perfil_user ON perfil_user.id=user_id " .
                   "LEFT JOIN authtoken_token ON authtoken_token.user_id=security_licencias.user_id " .
                   "WHERE licencia=? AND aplicacion_id=?; ";

        $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
        $sentencia->bindParam(1, $licencia);
        $sentencia->bindParam(2, $aplicacionId);

        ComprobarLicencia::obtenerInstancia()->validaNombreLicencia($licencia, $aplicacionId);

        // Getting headers sent by the client.
        $headers = apache_request_headers();

        //$url = 'http://www.example.com';
        //$headers = get_headers($url);

        if ($headers["Content-Type"] == self::tipo) {
            if ($sentencia->execute()){
                //$autos = array($sentencia->fetchColumn());
                $getLicen = self::compruebaNombreLicencia($licencia);
                //$nombLicen = $getLicen["licencia"];

		if ($getLicen>0){
		    $getTokenLicencia = $sentencia->fetch(PDO::FETCH_ASSOC);
		    $tokenLicencia = $getTokenLicencia["token"];

		    if (empty($tokenLicencia)){
 			header("HTTP/1.1 404 Not Found");
                        self::setCabecera();

                        $respuesta['estado'] = self::MENSAJE_ERROR;
                        $respuesta['codeError'] = self::NOT_FOUND;
                        $respuesta['mensaje'] = "No posee codigo de autorizacion para el uso de la app movil, verifique!";

                        echo json_encode($respuesta);
                        exit();
		    }else{
                        header("HTTP/1.1 200 OK");
	                self::setCabecera();

                        $respuesta['estado'] = self::MENSAJE_OK;
                        $respuesta['code'] = self::OK;
			//$respuesta['licencia'] = $sentencia->fetch(PDO::FETCH_ASSOC);
			$respuesta['licencia'] = $getTokenLicencia;

                        echo json_encode($respuesta);
		        exit();
		    }
                }else{
                    header("HTTP/1.1 404 Not Found");
                    self::setCabecera();

                    $respuesta['estado'] = self::MENSAJE_ERROR;
                    $respuesta['codeError'] = self::NOT_FOUND;
                    $respuesta['mensaje'] = "El nombre de la licencia es erroneo o no existe";

                    echo json_encode($respuesta);
                    exit();
                }
            }else{
                header("HTTP/1.1 400 Bad Request");
                self::setCabecera();

                $respuesta['estado'] = self::MENSAJE_ERROR;
                $respuesta['codeError'] = self::NOT_FOUND;
                $respuesta['mensaje'] = self:: MENSAJE_NOT_FOUND;

                //echo json_encode($sentencia->fetch(PDO::FETCH_ASSOC));
                echo json_encode($respuesta);
                exit();
            }
        }else{
            header("HTTP/1.1 404 Not Found");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_ERROR;
            $respuesta['codeError'] = self::NOT_FOUND;
            $respuesta['mensaje'] = "No envio el Header [Content-Type:application/json]";

            //echo json_encode($sentencia->fetch(PDO::FETCH_ASSOC));
            echo json_encode($respuesta);
            exit();
        }
    }

    private static function compruebaNombreLicencia($licencia){
        $comando = "SELECT count(*) AS licencia FROM security_licencias WHERE licencia=?; ";

        $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
        $sentencia->bindParam(1, $licencia);

        if ($sentencia->execute()) {
            return $sentencia->fetchColumn();
        }
    }

    private function getLicenciaVfp()
    {
        $body = file_get_contents('php://input');
        $licenciaApp = json_decode($body);
        $licencia = $licenciaApp->licencia;

        $comando = "SELECT limite, " .
            "       aplicacion_id, " .
            "       security_licencias.user_id, " .
            "       licencia, " .
            "       expires AS expira, " .
            "       security_apps.descripcion, " .
            "       authtoken_token.key AS token " .
            "FROM security_licencias " .
            "INNER JOIN security_apps ON security_apps.id=aplicacion_id " .
            "INNER JOIN authtoken_token ON authtoken_token.user_id=security_licencias.user_id " .
            "WHERE licencia=? AND aplicacion_id=4; ";

        $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
        $sentencia->bindParam(1, $licencia);

        // Getting headers sent by the client.
        $headers = apache_request_headers();

        //$url = 'http://www.example.com';
        //$headers = get_headers($url);

        self::authenticate();

        if ($headers["Content-Type"] == self::tipo) {
            if ($sentencia->execute()){
                header("HTTP/1.1 200 OK");
                self::setCabecera();

                $respuesta['estado'] = self::MENSAJE_OK;
                $respuesta['code'] = self::OK;
                $respuesta['licencia'] = $sentencia->fetch(PDO::FETCH_ASSOC);

                //echo json_encode($sentencia->fetch(PDO::FETCH_ASSOC));
                echo json_encode($respuesta);
                exit();
            }else{
                header("HTTP/1.1 400 Bad Request");
                self::setCabecera();

                $respuesta['estado'] = self::MENSAJE_ERROR;
                $respuesta['codeError'] = self::NOT_FOUND;
                $respuesta['mensaje'] = self:: MENSAJE_NOT_FOUND;

                //echo json_encode($sentencia->fetch(PDO::FETCH_ASSOC));
                echo json_encode($respuesta);
                exit();
            }
        }else{
            header("HTTP/1.1 404 Not Found");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_ERROR;
            $respuesta['codeError'] = self::NOT_FOUND;
            $respuesta['mensaje'] = "No envio el Header [Content-Type:application/json]";

            //echo json_encode($sentencia->fetch(PDO::FETCH_ASSOC));
            echo json_encode($respuesta);
            exit();
        }
    }

    private function getLicenciaVfpPlano()
    {
        $body = file_get_contents('php://input');
        $licenciaApp = json_decode($body);
        $licencia = $licenciaApp->licencia;

        $comando = "SELECT limite, " .
            "       aplicacion_id, " .
            "       security_licencias.user_id, " .
            "       licencia, " .
            "       expires AS expira, " .
            "       security_apps.descripcion, " .
            "       authtoken_token.key AS token " .
            "FROM security_licencias " .
            "INNER JOIN security_apps ON security_apps.id=aplicacion_id " .
            "INNER JOIN authtoken_token ON authtoken_token.user_id=security_licencias.user_id " .
            "WHERE licencia=? AND aplicacion_id=4; ";

        $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
        $sentencia->bindParam(1, $licencia);

        // Getting headers sent by the client.
        $headers = apache_request_headers();

        //$url = 'http://www.example.com';
        //$headers = get_headers($url);

        if ($headers["Content-Type"] == self::tipo) {
            if ($sentencia->execute()){
                header("HTTP/1.1 200 OK");
                self::setCabecera();

                $respuesta = $sentencia->fetch(PDO::FETCH_ASSOC);
                echo json_encode($respuesta);
                exit();
            }else{
                header("HTTP/1.1 400 Bad Request");
                self::setCabecera();

                $respuesta['estado'] = self::MENSAJE_ERROR;
                $respuesta['codeError'] = self::NOT_FOUND;
                $respuesta['mensaje'] = self:: MENSAJE_NOT_FOUND;

                //echo json_encode($sentencia->fetch(PDO::FETCH_ASSOC));
                echo json_encode($respuesta);
                exit();
            }
        }else{
            header("HTTP/1.1 404 Not Found");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_ERROR;
            $respuesta['codeError'] = self::NOT_FOUND;
            $respuesta['mensaje'] = "No envio el Header [Content-Type:application/json]";

            //echo json_encode($sentencia->fetch(PDO::FETCH_ASSOC));
            echo json_encode($respuesta);
            exit();
        }
    }

    private function setCabecera() {
        header("Content-Type:" . self::tipo . ";charset=utf-8");

        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
        header("Access-Control-Allow-Headers: X-Requested-With");
        header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');
    }

    private function authenticate(){
        $headers = apache_request_headers();
        $response = array();

        if (isset($headers['Authorization'])){
            $token = $headers['Authorization'];

            if (!($token == API_KEY)){
                header("HTTP/1.1 401 Unauthorized");
                self::setCabecera();

                $response['estado'] = self::MENSAJE_UNAUTHORIZED;
                $response['codeError'] = self::UNAUTHORIZED;
                $response["mensaje"] = "Acceso denegado. Token invalido";

                echo json_encode($response);
                exit();
            }else{

            }
        }else{
            header("HTTP/1.1 400 Bad Request");
            self::setCabecera();

            $response['estado'] = self::MENSAJE_ERROR;
            $response['codeError'] = self::NOT_FOUND;
            $response["mensaje"] = "Falta el token de autorización";

            echo json_encode($response);
            exit();
        }

        /*
         $autos = array(
        array('make'=>'Toyota', 'model'=>'Corolla', 'year'=>'2006),
        array('make'=>'Nissan', 'model'=>'Sentra', 'year'=>'2010)
        );
         */
    }
}
