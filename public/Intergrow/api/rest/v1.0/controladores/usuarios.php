<?php

//require '/../datos/ConexionBD.php';
//include_once 'C:/xampp/htdocs/tuencargo/api/rest/v1.0/datos/ConexionBD.php';
include_once 'datos/ConexionBD.php';
//include("ConexionBD.php");

class usuarios
{
    const contentTypeValue = "application/json";

    // Datos de la tabla "usuario"
    const OK = 200;
    const MENSAJE_OK = "correcto";
    const CREATED = 201;
    const MENSAJE_CREATED = "Created";
    const MENSAJE_CREATED_2 = "Registro creado correctamen";
    const NOT_BAD_REQUEST = 400;
    const MENSAJE_BAD_REQUEST = "No se encontraron registros";
    const MENSAJE_ERROR = "error";
    const UNAUTHORIZED = 401;
    const MENSAJE_UNAUTHORIZED = "error Unauthorized";
    const NOT_FOUND = 404;
    const MENSAJE_NOT_FOUND = "Not Found";
    const CONFLICT = 409;
    const MENSAJE_CONFLICT = "Conflict";
    const MENSAJE_NOT_FOUND_BUSQUEDA = "No se encontro el articulo buscado";
    const METHOD_NOT_ALLOWED = 405;
    const MENSAJE_METHOD_NOT_ALLOWED = "Url mal formada o el metodo no es el correcto";
    const UNPROCESSABLE_ENTITY = 422;
    const MENSAJE_UNPROCESSABLE_ENTITY_GROUP = "No indico el codigo del grupo";
    const MENSAJE_UNPROCESSABLE_ENTITY_NAME = "No indico la descripcion del articulo";
    const MENSAJE_INSERT = true;
    const MENSAJE_NO_INSERT = false;

    const ESTADO_CREACION_EXITOSA = "correcto";
    const ESTADO_CREACION_FALLIDA = 2;
    const ESTADO_ERROR_BD = 3;
    const ESTADO_AUSENCIA_CLAVE_API = 4;
    const ESTADO_CLAVE_NO_AUTORIZADA = 5;
    const ESTADO_URL_INCORRECTA = 6;
    const ESTADO_FALLA_DESCONOCIDA = 7;
    const ESTADO_PARAMETROS_INCORRECTOS = 8;
    const ESTADO_ROL_USUARIO = 9;
    const ESTADO_DATO_VACIO = "Error";

    const METHOD="AES-256-CBC";
    const SECRET_KEY='SISTEMASBWEB7FOC01T8VNQDZ6SDEQ24';
    const SECRET_IV='EZ98BBS7XJ0>GQ4JS63GDSISTEMASBWEB';

    public static function post($peticion)
    {
        if ($peticion[0] == 'registro') {
            return self::registrar();
        } else if ($peticion[0] == 'login') {
            return self::loguear();
        } else if ($peticion[0] == 'getUsuario') {
            return self::obtenerUsuario();
        } else if ($peticion[0] == 'validaEmpleado') {
            return self::obtenerEmpleado();
        } else if ($peticion[0] == 'subirFotoAvatar') {
            return self::subirFoto();
        } else {
            throw new ExcepcionApi(self::ESTADO_URL_INCORRECTA, "Url mal formada", 400);
        }
    }

    /**
     * Crea un nuevo usuario en la base de datos
     */
    private function registrar()
    {
        $cuerpo = file_get_contents('php://input');
        $usuario = json_decode($cuerpo);

        $resultado = self::crear($usuario);

        switch ($resultado) {
            case self::ESTADO_CREACION_EXITOSA:
                header("HTTP/1.1 201 Created");
                self::setCabecera();

                $respuesta['estado'] = self::MENSAJE_CREATED;
                $respuesta['code'] = self::CREATED;
                $respuesta['mensaje'] = "Usuario Registrado con exito!";

                echo json_encode($respuesta);
                exit();

                break;
            case self::ESTADO_CREACION_FALLIDA:
                throw new ExcepcionApi(self::ESTADO_CREACION_FALLIDA, "Ha ocurrido un error");
                break;
            case self::ESTADO_ROL_USUARIO:
                throw new ExcepcionApi(self::ESTADO_ROL_USUARIO, "Tiene que inndicar el rol del usuario");
                break;
            default:
                throw new ExcepcionApi(self::ESTADO_FALLA_DESCONOCIDA, "Falla desconocida", 400);
        }
    }

    /**
     * Crea un nuevo usuario en la tabla "usuario"
     * @param mixed $datosUsuario columnas del registro
     * @return int codigo para determinar si la inserci�n fue exitosa
     */
    private function crear($datosUsuario)
    {
        $nombre = $datosUsuario->nombre;
        $dni = $datosUsuario->dni;
        $correo = $datosUsuario->correo;
        $contrasena = $datosUsuario->contrasena;
        $contrasenaEncriptada = self::encryption($contrasena);

        $fecha = new DateTime();
        $createdFecha = $fecha->format('Y-m-d H:i:s.u');
        $updateFecha = $fecha->format('Y-m-d H:i:s.u');

        if (empty($correo)) {
            throw new ExcepcionApi(self::ESTADO_DATO_VACIO, "no indico el correo", 400);
        }elseif (empty($nombre)) {
            throw new ExcepcionApi(self::ESTADO_DATO_VACIO, "no indico el nombre del usuario", 400);
        }elseif (empty($contrasena)) {
            throw new ExcepcionApi(self::ESTADO_DATO_VACIO, "la contraseña esta en blanco", 400);
        }elseif (empty($dni)) {
            throw new ExcepcionApi(self::ESTADO_DATO_VACIO, "no indico el numero de identificación", 400);
        }

        $exitUsuario = self::existeUsuario($correo);
        if ($exitUsuario>0){
            throw new ExcepcionApi(self::ESTADO_DATO_VACIO, "usuario ya existe", 400);
        }

        if (!empty($correo) and !empty($contrasena) and !empty($nombre)) {
            try {
                $pdo = ConexionBD::obtenerInstancia()->obtenerBD();

                // Sentencia INSERT
                $comando = "INSERT INTO users (name, email, password, created_at, updated_at) " .
                           "VALUES (?, ?, ?, ?, ?);";

                //INSERT INTO `jagvmrhvyc`.`employes` (`code`, `firstname`, `lastname`, `dni`, `sex`, `birthdate`, `address`, `numbersocial`, `zipcode`, `numberphone`, `numberhome`, `numberfax`, `sociallink`, `departamentid`, `scheduleid`, `positionid`, `profession`, `created_at`, `updated_at`) VALUES ('001', 'Ricardo', 'Acurero', '15195133', '2', '1982-06-10 20:25:30', 'Monte Claro', '1519511336', '4001', '+584121609350', 'home', 'fax', 'socialink', '2', '1', '1', '1', '2020-04-25 20:26:32', '2020-04-25 20:26:34');

                $sentencia = $pdo->prepare($comando);

                $sentencia->bindParam(1, $nombre);
                $sentencia->bindParam(2, $correo);
                $sentencia->bindParam(3, $contrasenaEncriptada);
                $sentencia->bindParam(4, $createdFecha);
                $sentencia->bindParam(5, $updateFecha);

                $resultado = $sentencia->execute();

                $comando = "SELECT id FROM users WHERE email = ?;";

                $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
                $sentencia->bindParam(1, $correo);

                if ($sentencia->execute()) {
                    //$sentencia->fetch(PDO::FETCH_ASSOC);
                    $id = $sentencia->fetchColumn();

                    if ($id>0){
                        $comando = "UPDATE employes SET users_id=? WHERE dni=?;";

                        $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
                        $sentencia->bindParam(1, $id);
                        $sentencia->bindParam(2, $dni);

                        $sentencia->execute();

                        if ($resultado) {
                            return self::ESTADO_CREACION_EXITOSA;
                        } else {
                            return self::ESTADO_CREACION_FALLIDA;
                        }
                    }
                }
            } catch (PDOException $e) {
                throw new ExcepcionApi(self::ESTADO_ERROR_BD, $e->getMessage());
            }
        }else{
            throw new ExcepcionApi(self::ESTADO_FALLA_DESCONOCIDA, "Falta un parametro requerido ", 400);
        }
    }

    public function subirFoto() {
        sleep(1);
        date_default_timezone_set('UTC');
        $imagen_64 = $this->post('imagen');
        $ext = $this->post('ext');
        $user_id = $this->post('user_id');

        $url = realpath(FCPATH.'./public_html/sistema/img/avatar/' . $user_id);
        $date = new DateTime();
        $nombre = $user_id . '_' . $date->getTimestamp();
        $targetFile = $url.'/'.$nombre.'.'.$ext;
        $target_thumbnail = $url.'/thumbnail/';
        $target_lq = $url.'/lowq/'.$nombre.'.'.$ext;
        $imagen_64 = base64_decode($imagen_64);

        if ( $ext == 'png' or $ext == 'jpg' or $ext == 'jpeg' or $ext == 'gif') {
            $success = file_put_contents($url.'/'.$nombre.'.'.$ext, $imagen_64);
        } else {
            $success = FALSE;
        }

            if($success) {
                $this->imagenes->createThumbnail($nombre.'.'.$ext, 235, 235, $url, $target_thumbnail);
                $this->imagenes->compress($targetFile, $target_lq, 50, 9);
                $date = date('Y-m-d H:i:s');
                $data = array(
                    'fecha_creacion' => $date,
                    'is_visible' => '1',
                    'hq_foto' => $nombre.'.'.$ext
                );
                
                $query = $this->rest_model->insert_foto($data);
                $codigo = $this->db->insert_id();
                if ($query) {
                    $this->response([
                        'status' => TRUE,
                        'imagen' => $nombre.'.'.$ext,
                        'message' => 'Imagen subida con exito',
                        'url' => $url,
                        'codigo' => $codigo
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Error con la base de datos'
                    ], REST_Controller::HTTP_NOT_FOUND);
                }
            }else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Error al subir la imagen'
                ], REST_Controller::HTTP_NOT_FOUND);
            }
    }

    /**
     * Protege la contrase�a con un algoritmo de encriptado
     * @param $contrasenaPlana
     * @return bool|null|string
     */
    private function encriptarContrasena($contrasenaPlana)
    {
        if ($contrasenaPlana)
            return password_hash($contrasenaPlana, PASSWORD_DEFAULT);
        else return null;
    }

    private function generarClaveApi()
    {
        return md5(microtime() . rand());
    }

    private function loguear()
    {
        $respuesta = array();

        $body = file_get_contents('php://input');
        $usuario = json_decode($body);

        $correo = $usuario->correo;
        $contrasena = $usuario->contrasena;

        $login = self::autenticar($correo, $contrasena);

        if ($login==true) {
            http_response_code(200);
            header("HTTP/1.1 200 OK");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_OK;
            $respuesta['code'] = self::OK;
            $respuesta['mensaje'] = "Login correcto";

            echo json_encode($respuesta);
            exit();
        } else {
            header("HTTP/1.1 404 Not Found");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_NOT_FOUND;
            $respuesta['code'] = self::NOT_FOUND;
            $respuesta['mensaje'] = "Login incorrecto";

            echo json_encode($respuesta);
            exit();
        }
    }

    private function autenticar($correo, $contrasena)
    {
        $comando = "SELECT password FROM users WHERE email = ? AND password = ?";

        try {
            $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
            $sentencia->bindParam(1, $correo);
            $contrasenaMD5 = self::encryption($contrasena);
            $sentencia->bindParam(2, $contrasenaMD5);
            $sentencia->execute();

            if ($sentencia) {
                $resultado = $sentencia->fetch();
                $contrasenaDesencriptada = self::encryption($contrasena);

                if ($resultado['password']==$contrasenaDesencriptada){
                    return true;
                }else{
                    return false;
                }
            } else {
                return false;
            }
        } catch (PDOException $e) {
            throw new ExcepcionApi(self::ESTADO_ERROR_BD, $e->getMessage());
        }
    }

    private function validarContrasena($contrasenaPlana, $contrasenaHash)
    {
        return password_verify($contrasenaPlana, $contrasenaHash);
    }

    private function obtenerUsuario()
    {
        $respuesta = array();

        $body = file_get_contents('php://input');
        $usuario = json_decode($body);

        $correo = $usuario->correo;

        $op = 'usuario';
        $usuarioBD = self::obtenerDatos($correo, $op);

        if ($usuarioBD != NULL) {
            http_response_code(200);
            $respuesta["id"] = $usuarioBD["id"];
            $respuesta["email"] = $usuarioBD["email"];
            $respuesta["name"] = $usuarioBD["name"];
            $respuesta["codeEmploye"] = $usuarioBD["code"];
            $respuesta["firstname"] = $usuarioBD["firstname"];
            $respuesta["lastname"] = $usuarioBD["lastname"];
            $respuesta["dni"] = $usuarioBD["dni"];
            $respuesta["sex"] = $usuarioBD["sex"];
            $respuesta["birthdate"] = $usuarioBD["birthdate"];
            $respuesta["address"] = $usuarioBD["address"];
            $respuesta["numbersocial"] = $usuarioBD["numbersocial"];
            $respuesta["zipcode"] = $usuarioBD["zipcode"];
            $respuesta["numberphone"] = $usuarioBD["numberphone"];
            $respuesta["numberhome"] = $usuarioBD["numberhome"];
            $respuesta["numberfax"] = $usuarioBD["numberfax"];
            $respuesta["sociallink"] = $usuarioBD["sociallink"];
            $respuesta["departamentid"] = $usuarioBD["departamentid"];
            $respuesta["scheduleid"] = $usuarioBD["scheduleid"];
            $respuesta["positionid"] = $usuarioBD["positionid"];
            $respuesta["profession"] = $usuarioBD["profession"];

            header("HTTP/1.1 200 OK");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_OK;
            $respuesta['code'] = self::OK;
            $respuesta['usuario'] = $respuesta;

            echo json_encode($respuesta);
            exit();
        } else {
            throw new ExcepcionApi(self::ESTADO_FALLA_DESCONOCIDA, "Ha ocurrido un error");
        }
    }

    private function obtenerEmpleado()
    {
        $body = file_get_contents('php://input');
        $empleado = json_decode($body);

        $idEmpleado = $empleado->idEmpleado;
        $op = 'empleado';
        $usuarioBD = self::obtenerDatos($idEmpleado, $op);

        if ($usuarioBD != NULL) {
            http_response_code(200);
            
            $respuesta["id"] = $usuarioBD["id"];
            $respuesta["rolesid"] = $usuarioBD["rolesid"];
            $respuesta["codeEmploye"] = $usuarioBD["code"];
            $respuesta["firstname"] = $usuarioBD["firstname"];
            $respuesta["lastname"] = $usuarioBD["lastname"];
            $respuesta["dni"] = $usuarioBD["dni"];
            $respuesta["sex"] = $usuarioBD["sex"];
            $respuesta["birthdate"] = $usuarioBD["birthdate"];
            $respuesta["address"] = $usuarioBD["address"];
            $respuesta["numbersocial"] = $usuarioBD["numbersocial"];
            $respuesta["zipcode"] = $usuarioBD["zipcode"];
            $respuesta["numberphone"] = $usuarioBD["numberphone"];
            $respuesta["numberhome"] = $usuarioBD["numberhome"];
            $respuesta["numberfax"] = $usuarioBD["numberfax"];
            $respuesta["sociallink"] = $usuarioBD["sociallink"];
            $respuesta["departamentid"] = $usuarioBD["departamentid"];
            $respuesta["scheduleid"] = $usuarioBD["scheduleid"];
            $respuesta["positionid"] = $usuarioBD["positionid"];
            $respuesta["profession"] = $usuarioBD["profession"];
            $respuesta["site"] = $usuarioBD["site"];

            if(is_null($usuarioBD["supervisor"])){
                $respuesta["supervisor"] = "";
            }else{
                $respuesta["supervisor"] = $usuarioBD["supervisor"];
            }

            header("HTTP/1.1 200 OK");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_OK;
            $respuesta['code'] = self::OK;
            $respuesta['empleado'] = $respuesta;

            echo json_encode($respuesta);
            exit();
        } else {
            header("HTTP/1.1 400 Bad Request");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_ERROR;
            $respuesta['codeError'] = self::NOT_FOUND;
            $respuesta['mensaje'] = "Empleado no existe o esta inactivo";

            echo json_encode($respuesta);
            exit();
        }
    }

    private function obtenerDatos($parametro1, $op)
    {
        if ($op == 'usuario') {
            $comando = "SELECT a.id, " .
                "       a.name, " .
                "       a.email, " .
                "       a.password, " .
                "       IF(ISNULL(b.code), '', b.code) AS code, " .
                "       IF(ISNULL(b.firstname), '', b.firstname) AS firstname, " .
                "       IF(ISNULL(b.lastname), '', b.lastname) AS lastname, " .
                //"       IF(ISNULL(b.dni), '', b.dni) AS dni, " .
                "       '' AS dni, " .
                "       IF(ISNULL(b.sex), '', (SELECT description FROM sex WHERE id=b.sex)) AS sex, " .
                "       IF(ISNULL(b.birthdate), '', b.birthdate) AS birthdate, " .
                "       IF(ISNULL(b.address), '', b.address) AS address, " .
                "       IF(ISNULL(b.numbersocial), '', b.numbersocial) AS numbersocial, " .
                "       IF(ISNULL(b.zipcode), '', b.zipcode) AS zipcode, " .
                "       IF(ISNULL(b.numberphone), '', b.numberphone) AS numberphone, " .
                "       IF(ISNULL(b.numberhome), '', b.numberhome) AS numberhome, " .
                "       IF(ISNULL(b.numberfax), '', b.numberfax) AS numberfax, " .
                "       IF(ISNULL(b.sociallink), '', b.sociallink) AS sociallink, " .
                "       IF(ISNULL(b.departamentid), '', (SELECT description FROM departament WHERE id=b.departamentid)) AS departamentid, " .
                "       IF(ISNULL(b.scheduleid), '', (SELECT description FROM schedule WHERE id=b.scheduleid)) AS scheduleid, " .
                "       IF(ISNULL(b.positionid), '', (SELECT description FROM position WHERE id=b.positionid)) AS positionid, " .
                "       IF(ISNULL(b.professionid), '', (SELECT description FROM profession WHERE id=b.professionid)) AS profession " .
                "FROM users AS a " .
                "LEFT JOIN employes AS b ON b.users_id=a.id " .
                "WHERE a.email = ?";

            $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
            $sentencia->bindParam(1, $parametro1);

            if ($sentencia->execute()) {
                return $sentencia->fetch(PDO::FETCH_ASSOC);
            } else {
                return null;
            }
        }

        if ($op == 'empleado') {
            $comando = "SELECT b.id, \n" .
                       "       IF(ISNULL(b.rolesid), '', b.rolesid) AS rolesid, " .
                       "       IF(ISNULL(b.code), '', b.code) AS code, \n" .
                       "       IF(ISNULL(b.firstname), '', b.firstname) AS firstname, \n" .
                       "       IF(ISNULL(b.lastname), '', b.lastname) AS lastname, \n" .
                       //"       IF(ISNULL(b.dni), '', b.dni) AS dni, " .
                       "       '' AS dni, \n" .
                       "       IF(ISNULL(b.sex), '', (SELECT description FROM sex WHERE id=b.sex)) AS sex, \n" .
                       "       IF(ISNULL(b.birthdate), '', b.birthdate) AS birthdate, \n" .
                       "       IF(ISNULL(b.address), '', b.address) AS address, \n" .
                       "       IF(ISNULL(b.numbersocial), '', b.numbersocial) AS numbersocial, \n" .
                       "       IF(ISNULL(b.zipcode), '', b.zipcode) AS zipcode, \n" .
                       "       IF(ISNULL(b.numberphone), '', b.numberphone) AS numberphone, \n" .
                       "       IF(ISNULL(b.numberhome), '', b.numberhome) AS numberhome, \n" .
                       "       IF(ISNULL(b.numberfax), '', b.numberfax) AS numberfax, \n" .
                       "       IF(ISNULL(b.sociallink), '', b.sociallink) AS sociallink, \n" .
                       "       IF(ISNULL(b.departamentid), '', (SELECT description FROM departament WHERE id=b.departamentid)) AS departamentid, \n" .
                       "       IF(ISNULL(b.scheduleid), '', (SELECT description FROM schedule WHERE id=b.scheduleid)) AS scheduleid, \n" .
                       "       IF(ISNULL(b.positionid), '', (SELECT description FROM position WHERE id=b.positionid)) AS positionid, \n" .
                       "       IF(ISNULL(b.professionid), '', (SELECT description FROM profession WHERE id=b.professionid)) AS profession, \n" .
                       "       IF(ISNULL(b.idsite), '', (SELECT name FROM site WHERE id=b.idsite)) AS site, \n" .
                       "       IF(ISNULL(b.departamentid), '', (SELECT CONCAT(d.firstname,' ', d.lastname) AS supervisor FROM departament AS a \n" .
                       "                                        INNER JOIN employes AS d ON a.employesid=d.id \n" .
                       "                                        WHERE a.id=b.departamentid)) AS supervisor,  \n" .
                       "       b.active \n" .
                       "FROM employes AS b \n" .
                       "WHERE (" . (strlen($parametro1)>=4 ? "b.code = ? OR numbersocial LIKE '%". $parametro1 ."'" : "b.code = ? "). ") AND active = 1";
                       //"WHERE (b.code = ? OR numbersocial LIKE '%". $parametro1 ."') AND active = 1";

            if (strlen($parametro1)>=4){

            }
            $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
            $sentencia->bindParam(1, $parametro1);

            if ($sentencia->execute()) {
                return $sentencia->fetch(PDO::FETCH_ASSOC);
            } else {
                return null;
            }
        }
    }

    /**
     * Otorga los permisos a un usuario para que acceda a los recursos
     * @return null o el id del usuario autorizado
     * @throws Exception
     */
    public static function autorizar()
    {
        $cabeceras = apache_request_headers();

        if (isset($cabeceras["Authorization"])) {

            $claveApi = $cabeceras["Authorization"];

            if (usuarios::validarClaveApi($claveApi)) {
                return usuarios::obtenerIdUsuario($claveApi);
            } else {
                throw new ExcepcionApi(
                    self::ESTADO_CLAVE_NO_AUTORIZADA, "Clave de API no autorizada", 401);
            }

        } else {
            throw new ExcepcionApi(
                self::ESTADO_AUSENCIA_CLAVE_API,
                utf8_encode("Se requiere Clave del API para autenticaci�n"));
        }
    }

    /**
     * Obtiene el valor de la columna "idUsuario" basado en la clave de api
     * @param $correo
     * @return 0 si este no fue encontrado
     */
    //private function obtenerIdUsuario($claveApi)
    private function existeUsuario($correo)
    {
        $comando = "SELECT COUNT(*) AS numReg FROM users WHERE email =?";

        $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
        $sentencia->bindParam(1, $correo);

        if ($sentencia->execute()) {
            $resultado = $sentencia->fetch();

            return $resultado['numReg'];
        } else
            return 0;
    }

    private function encryption($string)
    {
        //$output=false;
        //$key=hash('sha256', self::SECRET_KEY);
        //$iv=substr(hash('sha256', self::SECRET_IV), 0, 16);
        //$output=openssl_encrypt($string, self::METHOD, $key, 0, $iv);
        //$output=base64_encode($output);

        $output=md5($string);
        //$output=password_hash($string, PASSWORD_BCRYPT);

        return $output;
    }

    protected function decryption($string)
    {
        $key=hash('sha256', self::SECRET_KEY);
        $iv=substr(hash('sha256', self::SECRET_IV), 0, 16);
        $output=openssl_decrypt(base64_decode($string), self::METHOD, $key, 0, $iv);

        return $output;
    }

    private function setCabecera() {
        header("Content-Type:" . self::contentTypeValue . ";charset=utf-8");

        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
        header("Access-Control-Allow-Headers: X-Requested-With");
        header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');
    }
}