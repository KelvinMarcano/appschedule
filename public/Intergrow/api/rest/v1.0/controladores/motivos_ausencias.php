<?php

//require '/../datos/ConexionBD.php';
//include_once 'C:/xampp/htdocs/tuencargo/api/rest/v1.0/datos/ConexionBD.php';
include_once 'datos/ConexionBD.php';
//include("ConexionBD.php");

class motivos_ausencias
{
    const contentTypeValue = "application/json";

    // Datos de la tabla "usuario"
    const OK = 200;
    const MENSAJE_OK = "correcto";
    const CREATED = 201;
    const MENSAJE_CREATED = "Created";
    const MENSAJE_CREATED_2 = "Registro creado correctamen";
    const NOT_BAD_REQUEST = 400;
    const MENSAJE_BAD_REQUEST = "No se encontraron registros";
    const MENSAJE_ERROR = "error";
    const UNAUTHORIZED = 401;
    const MENSAJE_UNAUTHORIZED = "error Unauthorized";
    const NOT_FOUND = 404;
    const MENSAJE_NOT_FOUND = "Not Found";
    const CONFLICT = 409;
    const MENSAJE_CONFLICT = "Conflict";
    const MENSAJE_NOT_FOUND_BUSQUEDA = "No se encontro el articulo buscado";
    const METHOD_NOT_ALLOWED = 405;
    const MENSAJE_METHOD_NOT_ALLOWED = "Url mal formada o el metodo no es el correcto";
    const UNPROCESSABLE_ENTITY = 422;
    const MENSAJE_UNPROCESSABLE_ENTITY_GROUP = "No indico el codigo del grupo";
    const MENSAJE_UNPROCESSABLE_ENTITY_NAME = "No indico la descripcion del articulo";
    const MENSAJE_INSERT = true;
    const MENSAJE_NO_INSERT = false;

    const ESTADO_CREACION_EXITOSA = "correcto";
    const ESTADO_CREACION_FALLIDA = 2;
    const ESTADO_ERROR_BD = 3;
    const ESTADO_AUSENCIA_CLAVE_API = 4;
    const ESTADO_CLAVE_NO_AUTORIZADA = 5;
    const ESTADO_URL_INCORRECTA = 6;
    const ESTADO_FALLA_DESCONOCIDA = 7;
    const ESTADO_PARAMETROS_INCORRECTOS = 8;
    const ESTADO_ROL_USUARIO = 9;
    const ESTADO_DATO_VACIO = "Error";

    public static function get($peticion)
    {
        if ($peticion[0] == 'cargar_motivos_ausencias') {
            return self::motivos();
        } else {
            //throw new ExcepcionApi(self::ESTADO_URL_INCORRECTA, "Url mal formada", 400);
            header("HTTP/1.1 405 Method Not Allowed");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_ERROR;
            $respuesta['code'] = self::METHOD_NOT_ALLOWED;
            $respuesta['mensaje'] = self::MENSAJE_METHOD_NOT_ALLOWED;

            echo json_encode($respuesta);
            exit();
        }
    }

    private function motivos()
    {
        //$cuerpo = file_get_contents('php://input');

        $comando = "SELECT id, CODE, description FROM motive ORDER BY id ASC;";

        $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);

        if ($sentencia->execute()) {
            $motivosausencias = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            header("HTTP/1.1 200 Ok");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_OK;
            $respuesta['code'] = self::OK;
            $respuesta['datoMotivosAusencia'] = $motivosausencias;

            echo json_encode($respuesta);
            exit();
        } else {
            return null;
        }
    }

    private function setCabecera() {
        header("Content-Type:" . self::contentTypeValue . ";charset=utf-8");

        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
        header("Access-Control-Allow-Headers: X-Requested-With");
        header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');
    }
}