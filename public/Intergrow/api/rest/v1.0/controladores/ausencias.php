<?php

//require '/../datos/ConexionBD.php';
//include_once 'C:/xampp/htdocs/tuencargo/api/rest/v1.0/datos/ConexionBD.php';
include_once 'datos/ConexionBD.php';
//include("ConexionBD.php");

class ausencias
{
    const contentTypeValue = "application/json";

    // Datos de la tabla "usuario"
    const OK = 200;
    const MENSAJE_OK = "correcto";
    const CREATED = 201;
    const MENSAJE_CREATED = "Created";
    const MENSAJE_CREATED_2 = "Registro creado correctamen";
    const NOT_BAD_REQUEST = 400;
    const MENSAJE_BAD_REQUEST = "No se encontraron registros";
    const MENSAJE_ERROR = "error";
    const UNAUTHORIZED = 401;
    const MENSAJE_UNAUTHORIZED = "error Unauthorized";
    const NOT_FOUND = 404;
    const MENSAJE_NOT_FOUND = "Not Found";
    const CONFLICT = 409;
    const MENSAJE_CONFLICT = "Conflict";
    const MENSAJE_NOT_FOUND_BUSQUEDA = "No se encontro el articulo buscado";
    const METHOD_NOT_ALLOWED = 405;
    const MENSAJE_METHOD_NOT_ALLOWED = "Url mal formada o el metodo no es el correcto";
    const UNPROCESSABLE_ENTITY = 422;
    const MENSAJE_UNPROCESSABLE_ENTITY_GROUP = "No indico el codigo del grupo";
    const MENSAJE_UNPROCESSABLE_ENTITY_NAME = "No indico la descripcion del articulo";
    const MENSAJE_INSERT = true;
    const MENSAJE_NO_INSERT = false;

    const ESTADO_CREACION_EXITOSA = "correcto";
    const ESTADO_CREACION_FALLIDA = 2;
    const ESTADO_ERROR_BD = 3;
    const ESTADO_AUSENCIA_CLAVE_API = 4;
    const ESTADO_CLAVE_NO_AUTORIZADA = 5;
    const ESTADO_URL_INCORRECTA = 6;
    const ESTADO_FALLA_DESCONOCIDA = 7;
    const ESTADO_PARAMETROS_INCORRECTOS = 8;
    const ESTADO_ROL_USUARIO = 9;
    const ESTADO_DATO_VACIO = "Error";

    public static function post($peticion)
    {
        if ($peticion[0] == 'registroAusencia') {
            return self::registrar();
        } elseif ($peticion[0] == 'cargarAusenciasDiaActual') {
            return self::cargarAusenciaDia();
        } elseif ($peticion[0] == 'cargarAusencias') {
            return self::cargarAusencia();
        } else {
            throw new ExcepcionApi(self::ESTADO_URL_INCORRECTA, "Url mal formada", 400);
        }
    }

    /**
     * Crea un nuevo usuario en la base de datos
     */
    private function registrar()
    {
        $cuerpo = file_get_contents('php://input');
        $ausencia = json_decode($cuerpo);

        $ausenciaBD = self::crear($ausencia);

        if ($ausenciaBD != NULL) {
            header("HTTP/1.1 201 Created");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_CREATED;
            $respuesta['code'] = self::CREATED;
            $respuesta['mensaje'] = "Ausencia Registrada con exito!";
            $respuesta['datoAusencia'] = $ausenciaBD;

            echo json_encode($respuesta);
            exit();
        }else{
            header("HTTP/1.1 404 Not Found");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_NOT_FOUND;
            $respuesta['code'] = self::NOT_FOUND;
            $respuesta['mensaje'] = "Usuario Registrado con exito!";

            echo json_encode($respuesta);
            exit();
        }
    }

    /**
     * Crea un nuevo usuario en la tabla "usuario"
     * @param mixed $datosAusencia columnas del registro
     * @return int codigo para determinar si la inserci�n fue exitosa
     */
    private function crear($datosAusencia)
    {
        $idEmpleado = $datosAusencia->employesid;
        $idMotivo = $datosAusencia->motiveid;
        $fechaInicio = $datosAusencia->fecha_init;
        $fechaFin = $datosAusencia->fecha_end;
        $horaInicio = $datosAusencia->hora_init;
        $horaFin = $datosAusencia->hora_end;
        $supervisor = $datosAusencia->supervisor;
        $justificada = $datosAusencia->justificada;

        $fecha = new DateTime();
        $createdFecha = $fecha->format('Y-m-d H:i:s.u');
        $updateFecha = $fecha->format('Y-m-d H:i:s.u');

        if (empty($idEmpleado)) {
            throw new ExcepcionApi(self::ESTADO_DATO_VACIO, "No paso el id del Empleado", 400);
        }elseif (empty($fechaInicio)) {
            throw new ExcepcionApi(self::ESTADO_DATO_VACIO, "No indico la fecha de inicio", 400);
        }elseif (empty($fechaFin)) {
            throw new ExcepcionApi(self::ESTADO_DATO_VACIO, "No indico la fecha de finalizacipn", 400);
        }

        if (!empty($idEmpleado) and !empty($fechaInicio) and !empty($fechaFin)) {
            try {
                $pdo = ConexionBD::obtenerInstancia()->obtenerBD();

                // Sentencia INSERT
                $comando = "INSERT INTO absences (employesid, motiveid, fecha_init, fecha_end, supervisor, justificada, hora_init, hora_end, created_at, updated_at) " .
                           "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

                $sentencia = $pdo->prepare($comando);

                $sentencia->bindParam(1, $idEmpleado);
                $sentencia->bindParam(2, $idMotivo);
                $sentencia->bindParam(3, $fechaInicio);
                $sentencia->bindParam(4, $fechaFin);
                $sentencia->bindParam(5, $supervisor);
                $sentencia->bindParam(6, $justificada);
                $sentencia->bindParam(7, $horaInicio);
                $sentencia->bindParam(8, $horaFin);
                $sentencia->bindParam(9, $createdFecha);
                $sentencia->bindParam(10, $updateFecha);

                if ($sentencia->execute()) {
                    $comando = "SELECT absences.id, motiveid, motive.description, fecha_init, fecha_end, absences.created_at FROM absences " .
                               "INNER JOIN motive ON motive.id=absences.motiveid " .
                               "WHERE absences.employesid=? ORDER BY absences.created_at DESC;";

                    $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
                    $sentencia->bindParam(1, $idEmpleado);

                    if ($sentencia->execute()) {
                        return $sentencia->fetch(PDO::FETCH_ASSOC);
                    } else {
                        return null;
                    }
                }
            } catch (PDOException $e) {
                throw new ExcepcionApi(self::ESTADO_ERROR_BD, $e->getMessage());
            }
        }else{
            throw new ExcepcionApi(self::ESTADO_FALLA_DESCONOCIDA, "Falta un parametro requerido ", 400);
        }
    }

    private function cargarAusenciaDia()
    {
        $cuerpo = file_get_contents('php://input');
        $ausenciaDia = json_decode($cuerpo);

        $idEmpleado = $ausenciaDia->employesid;

        $fecha = new DateTime();
        $createdFecha = $fecha->format('Y-m-d');

        $comando = "SELECT absences.id, motiveid, motive.description, fecha_init, fecha_end, absences.created_at FROM absences " .
                   "INNER JOIN motive ON motive.id=absences.motiveid " .
                   "WHERE absences.employesid=? AND DATE_FORMAT(absences.created_at, \"%Y-%m-%d\") = ? ORDER BY absences.created_at DESC;";

        $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
        $sentencia->bindParam(1, $idEmpleado);
        $sentencia->bindParam(2, $createdFecha);

        if ($sentencia->execute()) {
            $ausenciasDia = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            header("HTTP/1.1 200 Ok");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_OK;
            $respuesta['code'] = self::OK;
            $respuesta['datoAusenciaDia'] = $ausenciasDia;

            echo json_encode($respuesta);
            exit();
        } else {
            return null;
        }
    }

    private function cargarAusencia()
    {
        $cuerpo = file_get_contents('php://input');
        $ausenciaDia = json_decode($cuerpo);

        $nameSupervisor = $ausenciaDia->nameSupervisor;
        $limitDesde = $ausenciaDia->limitDesde;
        $limitHasta = $ausenciaDia->limitHasta;

        $fecha = new DateTime();
        $createdFecha = $fecha->format('Y-m-d');

        $comando = "SELECT a.id, " .
                   "       employesid, \n" .
                   "       CONCAT(b.firstname, ' ', b.lastname) AS nameEmploye, \n" .
                   "       motiveid, \n" .
                   "	   c.description AS descriptionMotive, \n" .
                   "	   violation_id, \n" .
                   "	   d.description AS descriptionViolation, \n" .
                   "	   fecha_init, \n" .
                   "	   fecha_end, \n" .
                   "	   IF(ISNULL(hora_init), '', hora_init) AS hora_init, \n" .
                   "	   IF(ISNULL(hora_end), '', hora_end) AS hora_end, \n" .
                   "	   status_id, \n" .
                   "	   e.description AS descriptionStatus, \n" .
                   "	   IF(ISNULL(comment), '', comment) AS comment, \n" .
                   "	   IF(ISNULL(supervisor), '', supervisor) AS supervisor \n" .
                   "FROM absences AS a \n" .
                   "INNER JOIN employes AS b ON b.id = a.employesid \n" .
                   "INNER JOIN motive AS c ON c.id = a.motiveid \n" .
                   "INNER JOIN violation AS d ON d.id = a.violation_id \n" .
                   "INNER JOIN status AS e ON e.id = a.status_id \n".
                   "ORDER BY a.created_at DESC LIMIT " . $limitDesde . ", " . $limitHasta . ";";

        $sentencia = ConexionBD::obtenerInstancia()->obtenerBD()->prepare($comando);
        //$sentencia->bindParam(1, $idEmpleado);
        //$sentencia->bindParam(2, $createdFecha);

        if ($sentencia->execute()) {
            $ausenciasDia = $sentencia->fetchAll(PDO::FETCH_ASSOC);

            header("HTTP/1.1 200 Ok");
            self::setCabecera();

            $respuesta['estado'] = self::MENSAJE_OK;
            $respuesta['code'] = self::OK;
            $respuesta['datoAusencia'] = $ausenciasDia;

            echo json_encode($respuesta);
            exit();
        } else {
            return null;
        }
    }

    private function setCabecera() {
        header("Content-Type:" . self::contentTypeValue . ";charset=utf-8");

        header("Access-Control-Allow-Origin: *");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
        header("Access-Control-Allow-Headers: X-Requested-With");
        header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');
    }
}